#!/bin/bash

#sudo mkdir data
#sudo mkdir log

#sudo rm -rf ./data/*
#sudo rm -rf ./log/*

sudo chown -R 999:999 ./build/databases.sql
#sudo chown -R 999:999 ./log
#sudo chown -R 999:999 ./data

sudo chmod -R 755 ./build/databases.sql
#sudo chmod -R 777 ./log
#sudo chmod -R 777 ./data

sudo docker-compose up --build -d
