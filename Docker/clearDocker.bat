@ECHO OFF

cls

ECHO.
ECHO Parando os servicos
ECHO.

docker stop seris_app
docker stop seris_mysql

ECHO.
ECHO Limpando o Docker 
ECHO.

docker system prune -a

cls

ECHO.
ECHO Servicos em execucao
ECHO.

docker ps -a

ECHO.