@ECHO OFF

cls

ECHO.
ECHO Criando MySQL
ECHO.

cd mysql
docker-compose up --build -d

cd ..

ECHO.
ECHO Criando Aplicacao
ECHO.

cd seris
docker-compose up --build -d

ECHO.
ECHO Servicos em Execucao
ECHO.

cd ..

docker ps -a

ECHO.