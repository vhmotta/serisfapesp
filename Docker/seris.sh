#!/bin/bash

clear

echo
echo "Criando MySQL"
echo

cd mysql
./container-up.sh

cd ..

echo
echo "Criando aplicação"
echo

cd seris
./container-up.sh

echo
echo "Serviços em execução"
echo

docker ps -a

echo
