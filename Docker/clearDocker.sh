#!/bin/bash

clear 

echo
echo "Parando os servicos"
echo

docker stop seris_app
docker stop seris_mysql

echo
echo "Limpando o Docker"
echo

docker system prune -a

clear

echo
echo "Serviços em execução"
echo

docker ps -a

echo