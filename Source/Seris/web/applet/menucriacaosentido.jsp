<%@page import="seris.functions.BasicFunctions" %>
<%@page import="java.util.Calendar" %>
<style type="text/css">
    <!--
    a, a:hover, a:active, a:focus {
        outline:0;
        direction:ltr;
    }

    .wrapper {
        position:relative; height:25px;
    }

    .mainmenu {
        position:absolute;
        z-index:100;
        font-family:Verdana, Geneva, sans-serif;
        font-weight:normal;
        font-size:90%;
        line-height:25px;
        width:1230px;
    }

    ul.menu {
        padding:0;
        margin:0;
        list-style:none;
        width:125px;
        overflow:hidden;
        float:left;
        margin-right:1px;
        background:#DDBCB7;
        z-index:50;
        border-right: solid 1px #ffffff;
    }

    ul.menu a {
        background:#DDBCB7;
        text-decoration:none;
        color:#000;
        padding-left:5px;
        z-index:50;
    }

    ul.menu li.list {
        float:left;
        width:250px;
        margin:-32767px -125px 0px 0px;
        z-index:50;
    }

    ul.menu li.list a.category {
        position:relative;
        z-index:50;
        display:block;
        float:left;
        width:120px;
        margin-top:32767px;
        background:transparent;
    }

    ul.menu li.list a.category:hover,
    ul.menu li.list a.category:focus,
    ul.menu li.list a.category:active {
        margin-right:1px;
        background-repeat:no-repeat;
        background-position:left top;
        z-index:50;
    }

    ul.submenu {
        width: 500;
        float:left;
        padding:25px 0px 0px 0px;
        margin:0;
        list-style:none;
        background-position:left top;
        margin:-25px 0px 0px 0px;
        z-index:50;
    }

    ul.submenu li a {
        float:left;
        width:120px;
        background:#DDBCB7;
        clear:left;
        color:#000;
        z-index:50;
    }

    ul.submenu a:hover,
    ul.submenu a:focus,
    ul.submenu a:active {
        background:#CD5C5C;
        margin-right:1px;
        color:#000;
        z-index:50;
    }

    table.menu {
        height:25px;
        background:#DDBCB7;
    }

    td.menu {
        font-family:Verdana, Geneva, sans-serif;
        font-weight:normal;
        font-size:90%;
        width: 125px;
        color: #000;
        border-right: solid 1px #ffffff;
        cursor: hand;
        cursor: pointer;
    }

    table.submenu {
        width: 100%;
        border: solid 1px #CD5C5C;
        height:25px;
        background:#ffffff;
    }

    td.titulo {
        font-family:Verdana, Geneva, sans-serif;
        font-weight:normal;
        font-size:90%;
        font-weight: bold;
        width: 80px;
        color: #000;
        padding-left: 5px;
    }

    td.livremenu {
        font-family:Verdana, Geneva, sans-serif;
        font-weight:normal;
        font-size:90%;
        color: #000;
        padding-left: 5px;
    }

    td.submenu {
        font-family:Verdana, Geneva, sans-serif;
        font-weight:normal;
        font-size:90%;
        width: 125px;
        color: #000;
        padding-left: 5px;
        cursor: hand;
        cursor: pointer;
    }

    a {
        text-decoration:none;
        width: 100px;
        height: 25px;
    }

    td.submenu:hover,
    td.submenu:focus,
    td.submenu:active {
        background: #CD5C5C;
    }

    -->
</style>

<script>
    function esconderTodosSubmenu(){
        var objArquivo = document.getElementById('arquivo');
        var objInserir = document.getElementById('inserir');
        var objLigacao = document.getElementById('ligacao');
        var objOntologia = document.getElementById('ontologia');
        var objPalavras = document.getElementById('palavras');

        objArquivo.style.visibility = 'hidden';
        objArquivo.style.display = 'none';
        objInserir.style.visibility = 'hidden';
        objInserir.style.display = 'none';
        objLigacao.style.visibility = 'hidden';
        objLigacao.style.display = 'none';
        objOntologia.style.visibility = 'hidden';
        objOntologia.style.display = 'none';
        objPalavras.style.visibility = 'hidden';
        objPalavras.style.display = 'none';
    }

    function exibirSubmenu(menu){
        esconderTodosSubmenu();
        var obj = document.getElementById(menu);
        obj.style.visibility = 'visible';
        obj.style.display = '';
    }

</script>

<table class="menu" width="100%" cellpadding="0" cellspacing="0">
    <tr>
        <td class="menu" onclick="exibirSubmenu('arquivo');"> &nbsp; <bean:message key="label.arquivo"/> </td>
<td class="menu" onclick="exibirSubmenu('inserir');"> &nbsp; <bean:message key="label.inserir"/> </td>
<td class="menu" onclick="exibirSubmenu('ligacao');"> &nbsp; <bean:message key="label.relacao"/> </td>
<td class="menu" onclick="exibirSubmenu('ontologia');"> &nbsp; <bean:message key="label.recursosWikonx"/> </td>
<td class="menu" onclick="exibirSubmenu('palavras');"> &nbsp; <bean:message key="label.atribuirSentido"/> </td>
<td> &nbsp; </td>
</tr>
</table>
<table id="arquivo" class="submenu" cellpadding="0" cellspacing="0" style="position: absolute; left: 0px; visibility: visible; display: '';">
    <tr>
        <td class="titulo"> <bean:message key="label.arquivo"/>: </td>
<td class="submenu" onclick="salvarGrafico('TRUE');"> <bean:message key="label.salvar"/> </td>
<td class="submenu" onclick="salvarComoGrafico();"> <bean:message key="label.salvarComo"/>: </td>
<td class="livremenu" width="10"> <input type="text" name="nomeSalvar" id="nomeSalvar"> </td>
<td class="submenu" onclick="finalizarGrafico();"> <bean:message key="label.finalizar"/> </td>
<td class="submenu" onclick="excluirGrafico();"> <bean:message key="label.excluirItem"/> </td>
<td class="submenu" onclick="excluirGraficoFull();"> <bean:message key="label.limparSerisMap"/> </td>
<td> &nbsp; </td>
</tr>
</table>

<table id="inserir" class="submenu" cellpadding="0" cellspacing="0" style="position: absolute; left: 0px; visibility: hidden; display: 'none';">
    <tr>
        <td class="titulo"> <bean:message key="label.inserir"/>: </td>
<td class="submenu" onclick="exibirJanela(0);"> <bean:message key="label.comentario"/> </td>
<td class="submenu" onclick="exibirJanela(1);"> <bean:message key="label.hipotese"/> </td>
<td class="submenu" onclick="exibirJanela(2);"> <bean:message key="label.acao"/> </td>
<td class="submenu" onclick="exibirJanela(3);"> <bean:message key="label.outros"/> </td>
<td> &nbsp; </td>
</tr>
</table>

<table id="ligacao" class="submenu" cellpadding="0" cellspacing="0" style="position: absolute; left: 0px; visibility: hidden; display: 'none';">
    <tr>
        <td style="width: 50px;" class="titulo"> <bean:message key="label.relacao"/>: </td>
<td class="livremenu"> <input type="checkbox" name="hipotetica" id="hipotetica"> <bean:message key="label.hipotetica"/> </td>
<td class="submenu" onclick="adicionarLigacaoConfirmacao();"> <bean:message key="label.confirmacao"/> </td>
<td class="submenu" onclick="adicionarLigacaoCausalidade();"> <bean:message key="label.causalidade"/> </td>
<td class="submenu" onclick="adicionarLigacaoSimilaridade();"> <bean:message key="label.similaridade"/> </td>
<td class="submenu" onclick="adicionarLigacaoContradicao();"> <bean:message key="label.contradicao"/> </td>
<td class="submenu" onclick="adicionarLigacaoIncoerencia();"> <bean:message key="label.incoerencia"/> </td>
<td class="submenu" onclick="adicionarLigacaoOutra();"> <bean:message key="label.outra"/> </td>
<td class="livremenu"> <input type="text" name="outraLegenda" id="outraLegenda" width="120"> </td>
<td> &nbsp; </td>
</tr>
</table>

<table id="ontologia" class="submenu" cellpadding="0" cellspacing="0" style="position: absolute; left: 0px; visibility: hidden; display: 'none';">
    <tr>
        <!--td class="titulo"> Sem�ntica: </td>
        <td class="submenu" onclick="exibirSinonimos();"> Conex�o Sem�ntica </td>
        <td class="livremenu"> N�vel Sem�ntica: <input type="text" id="nivelSinonimo" name="nivelSinonimo" value="1" style="width: 20px;"> </td>
        <td> &nbsp; </td-->

        <td class="titulo" style="width: 120px;"> <bean:message key="label.recursosWikonx"/>: </td>
<td class="submenu" onclick="exibirOntologia();" style="width: 150px;"> <bean:message key="label.conexaoSentido"/> </td>
<td class="livremenu" width="250">
    <select name="tipoSemantica" id="tipoSemantica" style="width: 200px;">
        <option value="0"><bean:message key="label.apenasDescricao"/></option>
        <option value="1"><bean:message key="label.apenasComentario"/></option>
        <option value="2"><bean:message key="label.ambos"/></option>
    </select>
</td>
<td class="livremenu" width="200">
<input type="checkbox" name="marcarPalavras" id="marcarPalavras" value="S"> <bean:message key="label.ativarMarcacaoSentido"/>
</td>
<td class="livremenu">
<bean:message key="label.limitarPeriodoConexao"/>
<%
    Calendar cal = Calendar.getInstance();
    BasicFunctions.sumDay(cal, -90);
%>
<input type="text" size="10" name="dataInicial" id="dataInicial" onkeyup="this.value=format(this.value,'##/##/####');" value="<%=BasicFunctions.formatCalendarDMY(cal, "/")%>">
<input type="text" size="10" name="dataInicial" id="dataFinal" onkeyup="this.value=format(this.value,'##/##/####');" value="<%=BasicFunctions.formatCalendarDMY(Calendar.getInstance(), "/")%>">
</td>
<!--td class="livremenu">
    <select name="nivelConexao" id="nivelConexao" style="width: 150px;">
        <option value="0"><bean:message key="label.conexaoForte"/></option>
        <option value="1"><bean:message key="label.conexaoMedia"/></option>
        <option value="2"><bean:message key="label.conexaoFraca"/></option>
    </select>
</td-->
<td> &nbsp; </td>
</tr>
</table>

<table id="palavras" class="submenu" cellpadding="0" cellspacing="0" style="position: absolute; left: 0px; visibility: hidden; display: 'none';">
    <tr>
        <td class="titulo" style="width: 120px"> <bean:message key="label.marcarPalavras"/>: </td>
<td class="livremenu" style="width: 350px;">
    <input type="text" id="palavraGrifar" name="palavraGrifar" value="" style="width: 150px;">
    <input type="button" name="adicionarPalavra" value="<bean:message key="label.atribuir"/>" onclick="addPalavraGrifar();">
           <input type="button" name="removerPalavra" value="<bean:message key="label.remover"/>" onclick="removerPalavraGrifar();">
</td>
<td class="livremenu">

    <table style="display: inline;" cellpadding="0" cellspacing="0">
        <th style="background-color: #000000; width: 30px; height: 20px;">
            <input type="radio" name="corPalavra" id="corPalavra" value="preto" checked>
        </th>
        <th style="background-color: #ff33cc; width: 30px;">
            <input type="radio" name="corPalavra" id="corPalavra" value="rosa">
        </th>
        <th style="background-color: #7f7f7f; width: 30px;">
            <input type="radio" name="corPalavra" id="corPalavra" value="cinza">
        </th>
        <th style="background-color: #ff0000; width: 30px;">
            <input type="radio" name="corPalavra" id="corPalavra" value="vermelho">
        </th>
        <th style="background-color: #33cc33; width: 30px;">
            <input type="radio" name="corPalavra" id="corPalavra" value="verde">
        </th>
        <th style="background-color: #0070c0; width: 30px;">
            <input type="radio" name="corPalavra" id="corPalavra" value="azul">
        </th>
    </table>

</td>
<td> &nbsp; </td>
</tr>
</table>

<br><br>

<script>
    var keyupSalvar = function(e){
        if (e.keyCode == 13) {
            salvarComoGrafico();
            return;
        }
        return;
    }
    document.getElementById('nomeSalvar').onkeyup = keyupSalvar;
    
    // Mascara para campo do tipo data
    function format(value,data){
        value = value.replace(/\D/g,"");
        var result="";
        if(data.length < value.length)
            return value;
        for(i=0,j=0;(i<data.length)&&(j<value.length);i++)
        {
            var ch = data.charAt(i);
            if(ch == '#')
            {
                result += value.charAt(j++);
                continue;
            }
            result += ch;
        }
        return result;
    }
</script>