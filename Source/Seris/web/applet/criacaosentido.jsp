<%--
    Document   : cadastrarCriacaoSentido
    Created on : nov/2010
    Author     : Vitor Hugo da Motta <vmotta@gmail.com>
--%>

<%@page import="seris2.database.DAO.CriacaoSentidoGraficosDAO"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <!-- Importa??o de Taglib -->
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>

    <!-- EDITAR - Importa??o de classes que ser?o utilizadas -->
    <%@page import="seris2.database.CriacaoSentido" %>
    <%@page import="seris2.database.CriacaoSentidoGraficos" %>
    <%@page import="seris2.database.DAO.CriacaoSentidoGraficosDAO" %>
    <%@page import="seris2.database.ListasSinaisFracos" %>
    <%@page import="seris.database.Kit" %>
    <%@page import="seris.database.Dominio" %>

    <%
        String complementar = request.getParameter("task");
        String complementarCSS = "";
        if (complementar != null && complementar.equals("salvargrafico")) {
            complementar = "applet/";
        } else {
            complementarCSS = "../";
            complementar = "";
        }
    %>

    <!-- Cabe?alho do site -->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso8859-1" />
        <%
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Pragma", "no-cache");
            response.setDateHeader("Expires", 0);
        %>
        <title>Seris</title>
        <link REL="SHORTCUT ICON" HREF="favicon2.ico">
        <link rel="stylesheet" type="text/css" href="<%=complementarCSS%>seris.css">
        <script type="text/javascript" language="javascript" src="<%=complementarCSS%>javascript/ajax.js"></script>
        <script type="text/javascript" language="javascript" src="<%=complementarCSS%>javascript/shortcut.js"></script>

        <%
            java.util.ResourceBundle resource = java.util.ResourceBundle.getBundle("seris.SerisIdioma");
        %>
        <script type="text/javascript" charset="utf-8">

            // EDITAR - Adicionar campos obrigat?rios
            function validaFormSalvar(){
                d = document.atualizarCriacaoSentido;
                if (d.id.value == "" || d.id.value == "0"){
                    alert("<bean:message key="message.naoIdentificadaSCS"/>");
                    return false;
                }
                if (d.grafico.value == ""){
                    alert("<bean:message key="message.graficoSemInformacao"/>");
                    d.data.focus();
                    return false;
                }
                return true;
            }

            function validaFormFinalizar(){
                d = document.finalizarCriacaoSentido;
                if (d.id.value == "" || d.id.value == "0"){
                    alert("<bean:message key="message.naoIdentificadaSCS"/>");
                    return false;
                }
                return true;
            }


        </script>

        <style>
            td.legenda{
                font-family:Verdana, Geneva, sans-serif;
                font-weight:normal;
                font-size:90%;
                text-align: center;
                width: 120px;
            }
        </style>

    </head>
    <body id="mybody" onload="javascript:init();">

        <!-- Inclui Cabe?alho e Menu -->
        <%
            if (complementar.equals("")) {
        %>
        <%@ include file="menucriacaosentido.jsp"%>
        <%                            } else {
        %>
        <%@ include file="menucriacaosentido.jsp"%>
        <%                    }
        %>

        <!-- DIV com o conte?do de todo site -->
        <div>

            <!-- Obt?m uma inst?ncia do domain da cria??o de sentido -->
            <jsp:useBean id="listaSinalFraco" scope="session" class="seris2.database.ListaSinalFraco"></jsp:useBean>
            <jsp:useBean id="criacaoSentido" scope="session" class="seris2.database.CriacaoSentido"></jsp:useBean>

            <%
                String xmlgrafico = "";

                int vKitId = 0;
                int vDominioId = 0;

                String vIdCriacaoSentido = request.getParameter("id");
                if (vIdCriacaoSentido == null) {
                    vIdCriacaoSentido = "0";
                }
                String vGraficoId = request.getParameter("graficoId");
                if (vGraficoId == null) {
                    vGraficoId = "0";
                }
                String vId = request.getParameter("listaId");
                String vTask = request.getParameter("task");
                String vData = seris.utils.Utils.getDataSistema();
                if (vTask == null) {
                    vTask = "";
                }

                // EDITAR - Consulta ou seta os campos para branco
                int id = 0;
                if (vId != null && !vId.equals("0") && (!vTask.equals("delete"))) {
                    id = Integer.parseInt(vId);
                    listaSinalFraco = seris2.database.DAO.ListaSinalFracoDAO.consultarListaSinalFraco(Integer.parseInt(vId));
                    criacaoSentido = seris2.database.DAO.CriacaoSentidoDAO.consultarCriacaoSentido(Integer.parseInt(vIdCriacaoSentido));

                    vKitId = listaSinalFraco.getKitId();

                    Dominio dominio = seris.database.DAO.DominioDAO.consultarDominio(listaSinalFraco.getKit().getDominioId());

                    vDominioId = dominio.getId();

                    if (vGraficoId != null && !vGraficoId.equals("") && !vGraficoId.equals("0")) {
                        CriacaoSentidoGraficos csg = CriacaoSentidoGraficosDAO.consultarCriacaoSentidoGraficos(vGraficoId);
                        xmlgrafico = csg.getGrafico();
                    } else {
                        xmlgrafico = criacaoSentido.getGrafico();
                    }
                    if (xmlgrafico == null) {
                        xmlgrafico = "";
                    } else {
                        xmlgrafico = xmlgrafico.replace("\r", "");
                        xmlgrafico = xmlgrafico.replace("\n", "");
                        xmlgrafico = xmlgrafico.replace("'", "<ASPAS>");
                        xmlgrafico = xmlgrafico.replace("\"", "<ASPAS2>");
                    }

            %>
            <div id="nomecont" align="center" style="width: 100%;"><bean:message key="label.sessaoCriacaoSentido" arg0="<%=criacaoSentido.getNome()%>"/> </div>

            <div style="position: absolute; top: 101px; left: 5px;">
                <b><bean:message key="label.dominio"/>:</b> <%=dominio.getNome()%> <BR>
                <b><bean:message key="label.kit"/>:</b> <%=criacaoSentido.getListaSinalFraco().getKit().getDescricao()%>
            </div>

            <table cellpadding="0" cellspacing="0" style="position: absolute; top: 134px; left: 10px;">
                <tr>
                    <td class="legenda" rowspan="2"> <input type="checkbox" name="hipotetica2" id="hipotetica2" onclick="document.getElementById('hipotetica').checked = this.checked;"> <bean:message key="label.hipotetica"/> </td>
                    <td class="legenda"> <span style="cursor: pointer; cursor: hand;" onclick="adicionarLigacaoConfirmacao();"> <bean:message key="label.confirmacao"/> </span> </td>
                    <td class="legenda"> <span style="cursor: pointer; cursor: hand;" onclick="adicionarLigacaoCausalidade();"> <bean:message key="label.causalidade"/> </span> </td>
                    <td class="legenda"> <span style="cursor: pointer; cursor: hand;" onclick="adicionarLigacaoSimilaridade();"> <bean:message key="label.similaridade"/> </span> </td>
                    <td class="legenda"> <span style="cursor: pointer; cursor: hand;" onclick="adicionarLigacaoContradicao();"> <bean:message key="label.contradicao"/> </span> </td>
                    <td class="legenda"> <span style="cursor: pointer; cursor: hand;" onclick="adicionarLigacaoIncoerencia();"> <bean:message key="label.incoerencia"/> </span> </td>
                    <td class="legenda"> <span style="cursor: pointer; cursor: hand;" onclick="adicionarLigacaoOutra();"> <bean:message key="label.outra"/> </span> </td>
                    <td class="legenda" rowspan="2"> <input type="text" name="outraLegenda2" id="outraLegenda2" onkeyup="document.getElementById('outraLegenda').value = this.value;"> </td>
                </tr>
                <tr>
                    <td align="center"> <img src="<%=complementarCSS%>imagens/legenda/confirmacao.png"> </td>
                    <td align="center"> <img src="<%=complementarCSS%>imagens/legenda/causalidade.png"> </td>
                    <td align="center"> <img src="<%=complementarCSS%>imagens/legenda/similaridade.png"> </td>
                    <td align="center"> <img src="<%=complementarCSS%>imagens/legenda/contradicao.png"> </td>
                    <td align="center"> <img src="<%=complementarCSS%>imagens/legenda/incoerencia.png"> </td>
                    <td align="center"> <img src="<%=complementarCSS%>imagens/legenda/outra.png"> </td>
                </tr>
            </table>

            <br><br>
            <table border="0" width="100%">
                <tr>
                    <td valign="top">
                        <DIV name="divApplet" id="divApplet" style="border: outset 2px #ad3831;width: 900px; height: 550px; overflow: auto" onScroll="repaintGraph();">
                            <applet id="draw" name="draw" style="z-index: -100; border: solid 0px; visibility: visible; display: '';" code="applet/CriacaoSentidoApplet.class" archive="<%=complementar%>CriacaoSentidoApplet93.jar" width=2500 height=2500>
                                <param name="grafico" value="<%=xmlgrafico%>">
                            </applet>
                        </DIV>
                    </td>
                    <td width="5"></td>
                    <td valign="top" style="width: 208px;">

                        <DIV style="border: outset 2px #640907 ; background-color: #ad3831; height: 22px ;width: 195px; text-align: center; color: #ffffff;">
                            <font style="font-size: 3px;"><br></font>
                                <bean:message key="label.listaSinaisFracos"/>
                        </DIV><br>

                        <%
                            java.util.List listasSinaisFracosList = seris2.database.DAO.ListasSinaisFracosDAO.consultarListasSinaisFracosByListaId(Integer.parseInt(vId));

                            ListasSinaisFracos vListasSinaisFracos;
                            for (int i = 0; i < listasSinaisFracosList.size(); i++) {
                                vListasSinaisFracos = (ListasSinaisFracos) listasSinaisFracosList.get(i);
                                if (vListasSinaisFracos.getSinalFraco() != null) {
                                    String desc = vListasSinaisFracos.getSinalFraco().getDescricao().replace("\n", "<BR>").replace("\r", "<BR>").replace("'", "<ASPAS>").replace("\"", "<ASPAS2>");
                        %>
                        <table id="tableSF<%=vListasSinaisFracos.getSinalFraco().getId()%>" border="0" onClick="adicionarSinalFraco(this,'<%=vListasSinaisFracos.getNumero()%> - <%=desc%>', '<%=vListasSinaisFracos.getSinalFraco().getId()%>');" cellpadding="0" cellspacing="0" style="width: 200px; cursor: hand; cursor: pointer; visibility: visible; display: '';">
                            <tr style="height: 16px">
                                <td style="background-image: url('<%=complementarCSS%>imagens/box/topoesquerda.png'); width: 16px;"> &nbsp; </td>
                                <td style="background-image: url('<%=complementarCSS%>imagens/box/topocentro.png'); "></td>
                                <td style="background-image: url('<%=complementarCSS%>imagens/box/topodireita.png'); width: 16px;"></td>
                            </tr>
                            <tr style="height: 16px">
                                <td style="background-image: url('<%=complementarCSS%>imagens/box/centroesquerda.png'); width: 16px;"></td>
                                <td bgcolor="#ffffff">
                                    <span style="cursor: hand; cursor: pointer;" onmouseover="exibirinfo('<%=vListasSinaisFracos.getSinalFraco().getId()%>');"> <%=vListasSinaisFracos.getNumero()%> </span>
                                    <%=vListasSinaisFracos.getSinalFraco().getDescricao().replace("<", "&lt;").replace(">", "&gt;")%>
                                </td>
                                <td style="background-image: url('<%=complementarCSS%>imagens/box/centrodireita.png'); width: 16px;"></td>
                            </tr>
                            <tr style="height: 16px">
                                <td style="background-image: url('<%=complementarCSS%>imagens/box/baseesquerda.png'); width: 16px;"></td>
                                <td style="background-image: url('<%=complementarCSS%>imagens/box/basecentro.png'); "></td>
                                <td style="background-image: url('<%=complementarCSS%>imagens/box/basedireita.png'); width: 16px;"></td>
                            </tr>
                        </table>
                        <%
                                }
                            }
                        %>

                    </td>
                </tr>
            </table>

            <form name="atualizarCriacaoSentido" target="targetOculto" action="<%=complementarCSS%>cadastrarCriacaoSentido.do" method="post" onSubmit="return validaFormSalvar()">
                <input type="hidden" name="id" value="<%=vIdCriacaoSentido%>">
                <input type="hidden" name="listaId" value="<%=vId%>">
                <input type="hidden" name="graficoId" value="<%=vGraficoId%>">
                <input type="hidden" name="task" value="salvargrafico">
                <input type="hidden" name="showmsg" value="TRUE">
                <input type="hidden" name="salvarcomo" value="FALSE">
                <input type="hidden" name="nomegrafico" value="">
                <input type="hidden" name="grafico" value="">
            </form>
            <form name="finalizarCriacaoSentido" target="targetOculto" action="<%=complementarCSS%>cadastrarCriacaoSentido.do" method="post" onSubmit="return validaFormFinalizar()">
                <input type="hidden" name="id" value="<%=vIdCriacaoSentido%>">
                <input type="hidden" name="listaId" value="<%=vId%>">
                <input type="hidden" name="task" value="finalizargrafico">
                <input type="hidden" name="showmsg" value="TRUE">
            </form>

            <%
                }
            %>

        </div>

        <IFRAME id="mainIframeJanela" name="mainIframeJanela" style="position: absolute; top: 10px; left: 10px; z-index: 100; visibility: hidden; display: 'none'; width: 305px; height: 215px; background-color: #ffffff;" src="" scrolling=no marginwidth=0 marginheight=0 frameborder=0 vspace=0 hspace=0></IFRAME>
        <div id="mainDivJanela" name="mainDivJanela" style="border: solid 2px #bb3a34 ; position: absolute; top: 10px; left: 10px; z-index: 100; visibility: hidden; display: none; width: 300px; height: 210px; overflow: hidden;">
            <div id="divTitulo" name="divTitulo" style="background-color: #bb3a34; height: 16px; border-top: solid 3px #bb3a34; "><center><b><bean:message key="label.fechar"/></b></center></div>
            <div id="divConteudoJanela" name="divConteudoJanela" width="100%" style="background-color: #ffffff; height: 323px;">

                <br>
                <center>
                    <table><td>
                            <bean:message key="label.descricao"/>:<br>
                            <textarea id="descricao" name="descricao" cols="30" rows="5" OnKeyDown="if (this.value.length > 160) this.value=this.value.substring(0,160);"></textarea><br>
                        </td></table>

                    <br><br>

                    <input type="button" value="<bean:message key="label.cancelar"/>" style="width: 100px;" onclick="esconderJanela();">
                    <input type="button" value="<bean:message key="label.ok"/>" style="width: 100px;" onclick="inserirBox();">
                </center>

            </div>
        </div>

        <IFRAME id="mainIframeInfoSinalFraco" name="mainIframeJanela" style="position: absolute; top: 10px; left: 10px; z-index: 100; visibility: hidden; display: 'none'; width: 515px; height: 350px; background-color: #ffffff;" src="" scrolling=no marginwidth=0 marginheight=0 frameborder=0 vspace=0 hspace=0></IFRAME>
        <div id="mainDivInfoSinalFraco" name="mainDivInfoSinalFraco" style="position: absolute; top: 10px; left: 10px; z-index: 100; visibility: hidden; display: none; width: 510px; height: 345px; overflow: hidden; border: solid 2px #bb3a34;">
            <div style="background-color: #eeeeee; cursor: hand; cursor: pointer;" OnClick="esconderInfo();"><center><b><bean:message key="label.fechar"/></b></center></div>
            <div id="divInfoSinalFraco" name="divInfoSinalFraco" style="background-color: #bb3a34;">
                <font color="#ffffff"><center><b><bean:message key="message.carregandoAguarde"/></b></center></font>
            </div>
        </div>

        <IFRAME id="mainIframeSinonimo" name="mainIframeSinonimo" style="position: absolute; top: 10px; left: 10px; z-index: 90; visibility: hidden; display: 'none'; width: 300px; height: 500px; background-color: #ffffff;" src="" scrolling=no marginwidth=0 marginheight=0 frameborder=0 vspace=0 hspace=0></IFRAME>
        <div id="mainDivSinonimos" name="mainDivSinonimos" style="position: absolute; top: 10px; left: 10px; z-index: 90; visibility: hidden; display: none; width: 300px; height: 495px; overflow: hidden; border: solid 2px #bb3a34;">
            <div style="background-color: #eeeeee; cursor: hand; cursor: pointer;" OnClick="esconderSinonimos();"><center><b><bean:message key="label.fechar"/></b></center></div>
            <div id="divInfoSinonimos" name="divInfoSinonimos" style="background-color: #bb3a34;">
                <font color="#ffffff"><center><b><bean:message key="message.carregandoAguarde"/></b></center></font>
            </div>
        </div>

        <IFRAME id="mainIframeOntologia" name="mainIframeOntologia" style="position: absolute; top: 10px; left: 10px; z-index: 90; visibility: hidden; display: 'none'; width: 400px; height: 500px; background-color: #ffffff;" src="" scrolling=no marginwidth=0 marginheight=0 frameborder=0 vspace=0 hspace=0></IFRAME>
        <div id="mainDivOntologia" name="mainDivOntologia" style="position: absolute; top: 10px; left: 10px; z-index: 90; visibility: hidden; display: none; width: 400px; height: 495px; overflow: hidden; border: solid 2px #bb3a34;">
            <div style="background-color: #eeeeee; cursor: hand; cursor: pointer;" OnClick="esconderOntologia();"><center><b><bean:message key="label.fechar"/></b></center></div>
            <div id="divInfoOntologia" name="divInfoOntologia" style="background-color: #bb3a34;">
                <font color="#ffffff"><center><b><bean:message key="message.aguardeConexaoSentido"/></b></center></font>
            </div>
            <br><br><br>
            <div align="center">
                <div style="background: url('../img/janelaontologia.png') no-repeat; width: 338px; height: 251px;" align="center">
                    <br><br><br><br><br><br><br>
                    <font color="#660000"><b><bean:message key="label.conectandoRedesWikonx"/></b></font>
                </div>
            </div>
        </div>

        <script>
            var inserirAcao = "";
            var isIE = (navigator.appName.indexOf('Microsoft') !=-1);
            var mouseX=0, mouseY=0;

            if (!isIE) document.captureEvents(Event.MOUSEMOVE | Event.MOUSEUP);
            document.onmousemove = handlerMM;

            function handlerMM(e){
                mouseX = (!isIE) ? e.pageX : event.clientX ;
                mouseY = (!isIE) ? e.pageY : event.clientY ;
            }

            function exibirinfo(id){

                esconderInfo();

                var url = '<%=complementar%>infoSinalFraco.jsp?scroll=1&id=' + id;
                ajaxLink("divInfoSinalFraco",url);

                var objiframe = document.getElementById("mainIframeInfoSinalFraco");
                var objdiv = document.getElementById("mainDivInfoSinalFraco");

                //var posX = mouseX - 250;
                //var posY = mouseY - 250;
                //if (posX >= (screen.availWidth-10)) posX = screen.availWidth - 600;
                //posX = screen.availWidth - 550;
                //if (posY <= 0) posY = 10;

                var posX = (screen.availWidth / 2) - 250;
                var posY = (screen.availHeight / 2) - 175;

                objiframe.style.top = posY + 'px';
                objiframe.style.left = posX + 'px';
                objdiv.style.top = posY + 'px';
                objdiv.style.left = posX + 'px';

                if (navigator.userAgent.indexOf('Mac') >= 0) {
                    var jsdraw = document.draw;
                    jsdraw.style.visibility = 'hidden';
                    jsdraw.style.display = 'none';
                }
                
                objiframe.style.visibility = 'visible';
                objiframe.style.display = '';
                objdiv.style.visibility = 'visible';
                objdiv.style.display = '';
            }

            function esconderInfo(){
                document.getElementById('mainIframeInfoSinalFraco').style.visibility = 'hidden';
                document.getElementById('mainIframeInfoSinalFraco').style.display = 'none';
                document.getElementById('mainDivInfoSinalFraco').style.visibility = 'hidden';
                document.getElementById('mainDivInfoSinalFraco').style.display = 'none';
                var content = '<font color="#ffffff"><center><b><bean:message key="message.carregandoAguarde"/></b></center></font>';
                document.getElementById('divInfoSinalFraco').innerHTML=content;

                if (navigator.userAgent.indexOf('Mac') >= 0) {
                    var jsdraw = document.draw;
                    jsdraw.style.visibility = 'visible';
                    jsdraw.style.display = '';
                }
            }

            function exibirSinonimos(){

                var jsdraw = document.draw;
                var sfText = jsdraw.getBoxText();

                if (sfText == "") {
                    alert('Selecione um sinal Fraco');
                } else {

                    esconderSinonimos();

                    var objNivel = document.getElementById('nivelSinonimo');
                    var nivelSinonimo = objNivel.value;

                    var url = '<%=complementar%>infoSinalFracoSinonimos.jsp?texto=' + sfText + '&idKit=<%=vKitId%>&complementar=<%=complementarCSS%>&nivelSinonimo='+nivelSinonimo;
                    ajaxLink("divInfoSinonimos",url);

                    var objiframe = document.getElementById("mainIframeSinonimo");
                    var objdiv = document.getElementById("mainDivSinonimos");

                    var posX = (screen.availWidth - 320);
                    var posY = (screen.availHeight / 2) - 250;

                    objiframe.style.top = posY + 'px';
                    objiframe.style.left = posX + 'px';
                    objdiv.style.top = posY + 'px';
                    objdiv.style.left = posX + 'px';

                    if (navigator.userAgent.indexOf('Mac') >= 0) {
                        var jsdraw = document.draw;
                        jsdraw.style.visibility = 'hidden';
                        jsdraw.style.display = 'none';
                    }

                    objiframe.style.visibility = 'visible';
                    objiframe.style.display = '';
                    objdiv.style.visibility = 'visible';
                    objdiv.style.display = '';
                }
            }
            
            function esconderSinonimos(){
                document.getElementById('mainIframeSinonimo').style.visibility = 'hidden';
                document.getElementById('mainIframeSinonimo').style.display = 'none';
                document.getElementById('mainDivSinonimos').style.visibility = 'hidden';
                document.getElementById('mainDivSinonimos').style.display = 'none';
                var content = '<font color="#ffffff"><center><b><bean:message key="message.carregandoAguarde"/></b></center></font>';
                document.getElementById('divInfoSinonimos').innerHTML=content;

                if (navigator.userAgent.indexOf('Mac') >= 0) {
                    var jsdraw = document.draw;
                    jsdraw.style.visibility = 'visible';
                    jsdraw.style.display = '';
                }
            }

            function exibirOntologia(){

                var jsdraw = document.draw;
                var sfText = jsdraw.getBoxText();
                var sfId = jsdraw.getSinalFracoId();
                var graficoIds = jsdraw.getSinalFracoIds();

                var kitText = '<%=criacaoSentido.getListaSinalFraco().getKit().getDescricao()%>';
                
                if (sfText == "") {
                    alert('Selecione um sinal Fraco');
                } else {

                    while (sfText.indexOf("%") != -1) {
                        sfText = sfText.replace("%", "\b");
                    }
                    while (sfText.indexOf("\b") != -1) {
                        sfText = sfText.replace("\b", "%25");
                    }

                    var objnivel = document.getElementById('nivelConexao');
                    var tipoSemantica = document.getElementById('tipoSemantica');
                    var rede = document.getElementById('selrede');
                    var marcarPalavras = document.getElementById('marcarPalavras');
                    var dataInicial = document.getElementById('dataInicial');
                    var dataFinal = document.getElementById('dataFinal');
                    
                    var redevalue=0;
                    if (rede) redevalue = rede.value;
                    
                    var marcarPalavrasValue = 'N';
                    if (marcarPalavras && marcarPalavras.checked) marcarPalavrasValue = 'S';

                    esconderOntologia();

                    var url = '<%=complementar%>infoSinalFracoOntologia.jsp?dataInicial='+ dataInicial.value +'&dataFinal='+ dataFinal.value +'&marcarPalavras='+ marcarPalavrasValue +'&rede='+redevalue+'&tipoSemantica='+tipoSemantica.value+'&nivelConexao='+'0'+'&graficoIds='+ graficoIds +'&listaId=<%=vId%>&id='+ sfId +'&texto=' + sfText + '&kit=' + kitText + '&idDominio=<%=vDominioId%>&complementar=<%=complementarCSS%>';
                    ajaxLink("divInfoOntologia",url);

                    var objiframe = document.getElementById("mainIframeOntologia");
                    var objdiv = document.getElementById("mainDivOntologia");

                    var posX = (screen.availWidth - 420);
                    var posY = (screen.availHeight / 2) - 250;

                    objiframe.style.top = posY + 'px';
                    objiframe.style.left = posX + 'px';
                    objdiv.style.top = posY + 'px';
                    objdiv.style.left = posX + 'px';

                    if (navigator.userAgent.indexOf('Mac') >= 0) {
                        var jsdraw = document.draw;
                        jsdraw.style.visibility = 'hidden';
                        jsdraw.style.display = 'none';
                    }

                    objiframe.style.visibility = 'visible';
                    objiframe.style.display = '';
                    objdiv.style.visibility = 'visible';
                    objdiv.style.display = '';
                }
            }

            function esconderOntologia(){
                document.getElementById('mainIframeOntologia').style.visibility = 'hidden';
                document.getElementById('mainIframeOntologia').style.display = 'none';
                document.getElementById('mainDivOntologia').style.visibility = 'hidden';
                document.getElementById('mainDivOntologia').style.display = 'none';
                var content = '<font color="#ffffff"><center><b><bean:message key="message.aguardeConexaoSentido"/></b></center></font>';
                document.getElementById('divInfoOntologia').innerHTML=content;

                if (navigator.userAgent.indexOf('Mac') >= 0) {
                    var jsdraw = document.draw;
                    jsdraw.style.visibility = 'visible';
                    jsdraw.style.display = '';
                }
            }

            function exibirJanela(janela){

                inserirAcao = janela;
                esconderJanela();

                var descricao = '';
                if (inserirAcao == 0) {
                    descricao = '<bean:message key="label.comentario"/>';
                } else if (inserirAcao == 1) {
                    descricao = '<bean:message key="label.hipotese"/>';
                } else if (inserirAcao == 2) {
                    descricao = '<bean:message key="label.acao"/>';
                } else if (inserirAcao == 3) {
                    descricao = '<bean:message key="label.outros"/>';
                }

                var objiframe = document.getElementById("mainIframeJanela");
                var objdiv = document.getElementById("mainDivJanela");
                var objdivTitulo = document.getElementById("divTitulo");

                var content = '<font color=#ffffff><center><b>'+ descricao +'</b></center></font>';
                objdivTitulo.innerHTML=content;

                var posX = (screen.availWidth / 2) - 150;
                var posY = (screen.availHeight / 2) - 105;

                objiframe.style.top = posY + 'px';
                objiframe.style.left = posX + 'px';
                objdiv.style.top = posY + 'px';
                objdiv.style.left = posX + 'px';

                if (navigator.userAgent.indexOf('Mac') >= 0) {
                    var jsdraw = document.draw;
                    jsdraw.style.visibility = 'hidden';
                    jsdraw.style.display = 'none';
                }
                
                objiframe.style.visibility = 'visible';
                objiframe.style.display = '';
                objdiv.style.visibility = 'visible';
                objdiv.style.display = '';
            }

            function esconderJanela(){
                document.getElementById('mainIframeJanela').style.visibility = 'hidden';
                document.getElementById('mainIframeJanela').style.display = 'none';
                document.getElementById('mainDivJanela').style.visibility = 'hidden';
                document.getElementById('mainDivJanela').style.display = 'none';

                if (navigator.userAgent.indexOf('Mac') >= 0) {
                    var jsdraw = document.draw;
                    jsdraw.style.visibility = 'visible';
                    jsdraw.style.display = '';
                }
            }

            function inserirBox(){
                if (inserirAcao == 0) {
                    adicionarComentario();
                } else if (inserirAcao == 1) {
                    adicionarHipotese();
                } else if (inserirAcao == 2) {
                    adicionarAcao();
                } else if (inserirAcao == 3) {
                    adicionarOutros();
                }

                var descobj = document.getElementById('descricao');
                descobj.value = '';
                esconderJanela();

            }

        </script>

        <!-- td class="submenu" onclick="exibirJanela('Coment?rio'); adicionarComentario();"> Coment?rio </td>
         <td class="submenu" onclick="exibirJanela('Hip?tese'); adicionarHipotese();"> Hip?tese </td>
         <td class="submenu" onclick="exibirJanela('A??o'); adicionarAcao();"> A??o </td>
         <td class="submenu" onclick="exibirJanela('Outros'); adicionarOutros();"> Outros </td -->


        <script>

            function startDraw() {

                //var tela = document.getElementById('mybody');

                var winW = 630, winH = 460;

                if (parseInt(navigator.appVersion)>3) {
                    if (navigator.appName=="Netscape") {
                        winW = window.innerWidth;
                        winH = window.innerHeight;
                    }
                    if (navigator.appName.indexOf("Microsoft")!=-1) {
                        winW = document.body.offsetWidth;
                        winH = document.body.offsetHeight-80;
                    }
                }

                var objDivApplet = document.getElementById("divApplet");
                objDivApplet.style.width = (winW-250) + 'px';
                objDivApplet.style.height = (winH-180) + 'px';

                //var jsdraw = document.draw;
                //jsdraw.style.width = (winW-250) + 'px';
                //jsdraw.style.height = (winH-140) + 'px';
            }
            window.setTimeout('startDraw();', 1000);

            //alert(document.getElementById('mybody').clientHeight);

            window.setInterval("salvarGrafico('FALSE')", 300000);

            function repaintGraph(){
                var jsdrawrepaint = document.draw;
                jsdrawrepaint.repaintGraph();
            }

            window.setTimeout('repaintGraph();', 1000);

            function confirma_sair(e){
                salvarGrafico('FALSE');
            }
            function init(){
                window.onbeforeunload = confirma_sair;
            }

        </script>

        <IFRAME id="targetOculto" name="targetOculto" style="width: 0px; height: 0px;" src="" scrolling=no marginwidth=0 marginheight=0 frameborder=0 vspace=0 hspace=0></IFRAME>

    </body>
</html>

<script>

    function getScrollTop(){
        var topRolagem = document.getElementById('divApplet').scrollTop;
        //if (topRolagem == 0) {
        //    topRolagem = document.documentElement.scrollTop;
        //}
        return topRolagem;
    }
    function getScrollLeft() {
        var leftRolagem = document.getElementById('divApplet').scrollLeft;
        //if (leftRolagem == 0) {
        //    leftRolagem = document.documentElement.scrollLeft;
        //}
        return leftRolagem;
    }

    //    function adicionarSinalFracoOntologiaSemComentario(table, descricao, id){
    //        alert(1);
    //        adicionarSinalFracoOntologia(table, descricao, id,0);
    //        alert(3);
    //    }

    function adicionarSinalFracoOntologia(table, descricao, id, comComentario){
        var jsdraw = document.draw;
        while (descricao.indexOf("<BR>") >= 0) {
            descricao = descricao.replace("<BR>", "\n");
        }
        while (descricao.indexOf("<ASPAS>") >= 0) {
            descricao = descricao.replace("<ASPAS>", "'");
        }
        while (descricao.indexOf("<ASPAS2>") >= 0) {
            descricao = descricao.replace("<ASPAS2>", "\"");
        }
        jsdraw.adicionarSinalFracoOntologia(id, descricao, comComentario);

        if (table != null) {
            table.style.visibility = 'hidden';
            table.style.display = 'none';
        }
    }
    function adicionarSinalFraco(table, descricao, id){
        var jsdraw = document.draw;
        while (descricao.indexOf("<BR>") >= 0) {
            descricao = descricao.replace("<BR>", "\n");
        }
        while (descricao.indexOf("<ASPAS>") >= 0) {
            descricao = descricao.replace("<ASPAS>", "'");
        }
        while (descricao.indexOf("<ASPAS2>") >= 0) {
            descricao = descricao.replace("<ASPAS2>", "\"");
        }

        var topScroll = getScrollTop();
        var leftScroll = getScrollLeft();

        jsdraw.setInitX(leftScroll + 10);
        jsdraw.setInitY(topScroll + 10);
        jsdraw.adicionarSinalFraco(id, descricao);

        table.style.visibility = 'hidden';
        table.style.display = 'none';
    }
    
    function adicionarSinalFracoComentario(table, descricaoSF, descricaoComentario, id){
        var jsdraw = document.draw;
        while (descricaoComentario.indexOf("<BR>") >= 0) {
            descricaoComentario = descricaoComentario.replace("<BR>", "\n");
        }
        while (descricaoComentario.indexOf("<ASPAS>") >= 0) {
            descricaoComentario = descricaoComentario.replace("<ASPAS>", "'");
        }
        while (descricaoComentario.indexOf("<ASPAS2>") >= 0) {
            descricaoComentario = descricaoComentario.replace("<ASPAS2>", "\"");
        }

        var topScroll = getScrollTop();
        var leftScroll = getScrollLeft();

        jsdraw.setInitX(leftScroll + 10);
        jsdraw.setInitY(topScroll + 10);
        jsdraw.adicionarSinalFracoComentario(id, descricaoComentario);
        
        adicionarSinalFracoOntologia(table, descricaoSF, id, 1);
    }
    
    function exibirSinalFraco(id){
        var table = document.getElementById('tableSF' + id);
        table.style.visibility = 'visible';
        table.style.display = '';
    }
    function exibirSinalFracoOntologia(id){
        var table = document.getElementById('tableSFOntologia' + id);
        if (table != null) {
            table.style.visibility = 'visible';
            table.style.display = '';
            table.style.color = '#000000';
        }
    }
    function esconderSinalFraco(id){
        table = document.getElementById('tableSF' + id);
        table.style.visibility = 'hidden';
        table.style.display = 'none';
    }
    function adicionarComentario(){
        var descobj = document.getElementById('descricao');
        var jsdraw = document.draw;
        jsdraw.adicionarComentario(descobj.value);
    }
    function adicionarHipotese(){
        var descobj = document.getElementById('descricao');
        var jsdraw = document.draw;
        jsdraw.adicionarHipotese(descobj.value);
    }
    function adicionarAcao(){
        var descobj = document.getElementById('descricao');
        var jsdraw = document.draw;
        jsdraw.adicionarAcao(descobj.value);
    }
    function adicionarOutros(){
        var descobj = document.getElementById('descricao');
        var jsdraw = document.draw;
        jsdraw.adicionarOutros(descobj.value);
    }

    function adicionarLigacaoConfirmacao(){
        var hipobj = document.getElementById('hipotetica');
        var jsdraw = document.draw;
        var hip = false;
        if (hipobj.checked) hip = true;
        jsdraw.adicionarLigacaoConfirmacao(hip);
    }
    function adicionarLigacaoCausalidade(){
        var hipobj = document.getElementById('hipotetica');
        var jsdraw = document.draw;
        var hip = false;
        if (hipobj.checked) hip = true;
        jsdraw.adicionarLigacaoCausalidade(hip);
    }
    function adicionarLigacaoSimilaridade(){
        var jsdraw = document.draw;
        jsdraw.adicionarLigacaoSimilaridade();
    }
    function adicionarLigacaoContradicao(){
        var hipobj = document.getElementById('hipotetica');
        var jsdraw = document.draw;
        var hip = false;
        if (hipobj.checked) hip = true;
        jsdraw.adicionarLigacaoContradicao(hip);
    }
    function adicionarLigacaoIncoerencia(){
        var jsdraw = document.draw;
        jsdraw.adicionarLigacaoIncoerencia();
    }
    function adicionarLigacaoOutra(){
        var descobj = document.getElementById('descricao');
        var legobj = document.getElementById('outraLegenda');
        var jsdraw = document.draw;
        jsdraw.adicionarLigacaoOutra(descobj.value, legobj.value);
    }

    function excluirGrafico(){
        var jsdraw = document.draw;
        jsdraw.excluirGrafico();
    }
    
    function excluirGraficoFull(){
        if (confirm('<bean:message key="message.limparSerisMapPerguntar"/>')) {
            var jsdraw = document.draw;
            jsdraw.excluirGraficoFull();
        }
    }
    
    function salvarGrafico(msg){
        var jsdraw = document.draw;
        var xml = jsdraw.getXML();

        var grafico = document.atualizarCriacaoSentido.grafico;
        var showmsg = document.atualizarCriacaoSentido.showmsg;
        var salvarcomo = document.atualizarCriacaoSentido.salvarcomo;
        grafico.value = xml;
        showmsg.value = msg;
        salvarcomo.value = 'FALSE';
        document.atualizarCriacaoSentido.submit();
    }
    function salvarComoGrafico(){
        var jsdraw = document.draw;
        var xml = jsdraw.getXML();

        var ngrafico = document.getElementById('nomeSalvar');

        var grafico = document.atualizarCriacaoSentido.grafico;
        var showmsg = document.atualizarCriacaoSentido.showmsg;
        var salvarcomo = document.atualizarCriacaoSentido.salvarcomo;
        var nomegrafico = document.atualizarCriacaoSentido.nomegrafico;
        grafico.value = xml;
        showmsg.value = 'TRUE';
        salvarcomo.value = 'TRUE';
        nomegrafico.value = ngrafico.value;
        document.atualizarCriacaoSentido.submit();
    }
    function finalizarGrafico(){
        if (confirm('<bean:message key="message.finalizarSessaoPerguntar"/>')) {
            document.finalizarCriacaoSentido.submit();
        }
    }
    function addPalavraGrifar(){
        var palavraobj = document.getElementById('palavraGrifar');
        var corPalavraobj = document.getElementsByName('corPalavra');
        var jsdraw = document.draw;
        var cor = 'preto';

        var radioLength = corPalavraobj.length;
        for(var i = 0; i < radioLength; i++) {
            if(corPalavraobj[i].checked) {
                cor = corPalavraobj[i].value;
            }
        }

        jsdraw.addPalavraGrifar(palavraobj.value, cor);
    }
    function removerPalavraGrifar(){
        var palavraobj = document.getElementById('palavraGrifar');
        var jsdraw = document.draw;
        jsdraw.removerPalavraGrifar(palavraobj.value);
    }

    function addPalavraGrifarOntologia(palavra){
        var jsdraw = document.draw;
        jsdraw.addPalavraGrifarSelected(palavra);
    }
    function limparPalavraGrifarSelected(){
        var jsdraw = document.draw;
        jsdraw.limparPalavraGrifarSelected();
    }

    shortcut.add("delete",function(){
        excluirGrafico();
    });

    /*            function abrirGrafico(){
                var jsdraw = document.draw;

                while (xml.indexOf("\"") > -1) {
                    xml = xml.replace("\"", "<ASPAS>");
                }
                while (xml.indexOf("<ASPAS>") > -1) {
                    xml = xml.replace("<ASPAS>", "\\\"");
                }

                alert(xml);
                jsdraw.abrirGrafico();
                alert(3);
            }
     */
</script>