<%--
    Document   : cadastrarListaSinalFraco
    Created on : nov/2010
    Author     : Vitor Hugo da Motta <vmotta@gmail.com>
--%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <!-- Importa??o de Taglib -->
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>

    <!-- EDITAR - Importa??o de classes que ser?o utilizadas -->
    <%@page import="seris.database.Sinalfraco" %>
    <%@page import="seris.database.KiqSinalfraco" %>
    <%@page import="seris.database.Kiq" %>
    <%@page import="seris.database.Kit" %>
    <%@page import="seris.database.Dominio" %>

    <!-- Cabe?alho do site -->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <%
                    response.setHeader("Cache-Control", "no-cache");
                    response.setHeader("Pragma", "no-cache");
                    response.setDateHeader("Expires", 0);
        %>
        <title>Seris</title>
        <link REL="SHORTCUT ICON" HREF="favicon2.ico">
        <style type="text/css" title="currentStyle">
            @import "media/css/demo_page.css";
            @import "media/css/demo_table.css";
        </style>
        <link rel="stylesheet" type="text/css" href="seris.css">

    </head>
    <body bgcolor="#ffffff">

        <!-- DIV com o conte?do de todo site -->
        <div id="conteudo">

            <%
                        String vScroll = request.getParameter("scroll");
                        if (vScroll == null) {
                            vScroll = "0";
                        }

                        String vId = request.getParameter("id");

                        Object[] infoSinalFraco = {new Sinalfraco(), "", "", "", "", "", "", ""};

                        Sinalfraco sinalFraco = new Sinalfraco();

                        // EDITAR - Consulta ou seta os campos para branco
                        int id = 0;
                        if (vId != null && !vId.equals("0")) {
                            id = Integer.parseInt(vId);
                            infoSinalFraco = seris.database.DAO.SinalfracoDAO.consultarInfoSinalFracoById(id);
                            sinalFraco = (Sinalfraco) infoSinalFraco[0];
                        }
            %>

            <div style="background-color: #ffffff; width: 545px;">
                <div style="background-color: #bb3a34; height: 18px; border-top: 4px solid #bb3a34;"> <font color="#ffffff"><b><center> <bean:message key="label.informacoesSinalFraco"/> </center></b></font></div>
                <div style="overflow: auto; height: 308px; width: 510px;">

                    <table width="495" cellpadding="4" cellspacing="0" style="background-color: #ffffff;">
                        <tr>
                            <td>
                                <table width="100%" cellpadding="4" cellspacing="0">
                                    <tr>
                                        <td align="right" valign="top"><b> <bean:message key="label.id"/>: </b></td>
                                        <td> <%=sinalFraco.getId()%> </td>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="top"><b> <bean:message key="label.descricao"/>: </b></td>
                                        <td> <%=sinalFraco.getDescricao()%> </td>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="top"><b> <bean:message key="label.data"/>: </b></td>
                                        <td> <%=seris.utils.Utils.ConverteDateParaString(sinalFraco.getData())%> </td>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="top"><b> <bean:message key="label.usuario"/>: </b></td>
                                        <td> <%=infoSinalFraco[1]%> </td>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="top"><b> <bean:message key="label.dominio"/>: </b></td>
                                        <td> <%=infoSinalFraco[2]%> </td>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="top"><b> <bean:message key="label.kit"/>: </b></td>
                                        <td> <%=infoSinalFraco[7]%> </td>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="top"><b> <bean:message key="label.ator"/>: </b></td>
                                        <td> <%=infoSinalFraco[3]%> </td>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="top"><b> <bean:message key="label.produto"/>: </b></td>
                                        <td> <%=infoSinalFraco[4]%> </td>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="top"><b> <bean:message key="label.mercado"/>: </b></td>
                                        <td> <%=infoSinalFraco[5]%> </td>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="top"><b> <bean:message key="label.fonte"/>: </b></td>
                                        <td> <%=infoSinalFraco[6]%> </td>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="top"><b> <bean:message key="label.comentarios"/>: </b></td>
                                        <td> <%=((sinalFraco.getComentarios() == null) ? "" : sinalFraco.getComentarios())%> </td>
                                    </tr>
                                    <tr>
                                        <td align="right" valign="top"><b> <bean:message key="label.informacoes"/>: </b></td>
                                        <td> <%=((sinalFraco.getInformacoes() == null) ? "" : sinalFraco.getInformacoes())%> </td>
                                    </tr>
                                </table>

                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>

    </div>

</body>
</html>