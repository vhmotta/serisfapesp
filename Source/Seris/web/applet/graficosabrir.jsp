<%--
    Document   : cadastrarListaSinalFraco
    Created on : nov/2010
    Author     : Vitor Hugo da Motta <vmotta@gmail.com>
--%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <!-- Importa??o de Taglib -->
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>

    <!-- EDITAR - Importa??o de classes que ser?o utilizadas -->
    <%@page import="java.util.List" %>
    <%@page import="seris2.database.DAO.CriacaoSentidoGraficosDAO" %>
    <%@page import="seris2.database.CriacaoSentidoGraficos" %>

    <!-- Cabe?alho do site -->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <%
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Pragma", "no-cache");
            response.setDateHeader("Expires", 0);
        %>
        <title>Seris</title>
        <link REL="SHORTCUT ICON" HREF="favicon2.ico">
        <style type="text/css" title="currentStyle">
            @import "media/css/demo_page.css";
            @import "media/css/demo_table.css";
        </style>
        <link rel="stylesheet" type="text/css" href="seris.css">
        <%
            java.util.ResourceBundle resource = java.util.ResourceBundle.getBundle("seris.SerisIdioma");
        %>
    </head>
    <body bgcolor="#ffffff">

        <div style="background-color: #ffffff;">

            <div style="background-color: #bb3a34; color: #ffffff; text-align: center; height: 25px;">
                <font style="font-size: 4px;"><br></font>
                <b><bean:message key="label.selecioneGraficoAbrir"/></b>
            </div>
            <br>
            <%
                String idSCS = request.getParameter("idSCS");
                String idLista = request.getParameter("idLista");
                String nome = request.getParameter("nome");
                List list = CriacaoSentidoGraficosDAO.consultarListaCriacaoSentidoBySCSid(idSCS);
                CriacaoSentidoGraficos csg;

                out.print("  &nbsp;&nbsp;&nbsp;<a target='_blank' href='applet/criacaosentido.jsp?id=" + idSCS + "&listaId=" + idLista + "'>" + nome + "</a>");
                out.print("  - <a target='baixarPDF' href='applet/baixarpng.jsp?id=" + idSCS + "'> " + resource.getString("label.baixar") + " </a><br><br>");

                for (int i = 0; i < list.size(); i++) {
                    csg = (CriacaoSentidoGraficos) list.get(i);
                    out.print("  &nbsp;&nbsp;&nbsp;<a target='_blank' href='applet/criacaosentido.jsp?id=" + idSCS + "&graficoId=" + csg.getId() + "&listaId=" + idLista + "'>" + csg.getNome() + "</a><br><br>");
                }
            %>

        </div>

    </body>
</html>