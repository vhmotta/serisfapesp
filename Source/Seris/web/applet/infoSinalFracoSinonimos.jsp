<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>

<div style="background-color: #ffffff;">
    <div style="background-color: #bb3a34; height: 18px; border-top: 4px solid #bb3a34;"> <font color="#ffffff"><b><center> Sinais Fracos </center></b></font></div>
    <div style="overflow: auto; height: 458px;">
        <center>
            <br>
            <%
                        String complementar = request.getParameter("complementar");
                        if (complementar == null) {
                            complementar = "";
                        }

                        String texto = request.getParameter("texto");
                        if (texto == null) {
                            texto = "";
                        }

                        int idKit = 0;
                        String strIdKit = request.getParameter("idKit");
                        if (strIdKit != null && !strIdKit.equals("")) {
                            idKit = Integer.parseInt(strIdKit);
                        }

                        int vNivel = 1;
                        String strNivel = request.getParameter("nivelSinonimo");
                        if (strNivel != null && !strNivel.equals("")) {
                            vNivel = Integer.parseInt(strNivel);
                        }

                        java.util.List sinaisFracosList = seris.database.DAO.SinalfracoDAO.consultarListasSinaisFracosBySinonimos(texto, idKit, vNivel);

                        seris.database.Sinalfraco vsinaisFraco;
                        for (int i = 0; i < sinaisFracosList.size(); i++) {
                            vsinaisFraco = (seris.database.Sinalfraco) sinaisFracosList.get(i);
            %>
            <table id="tableSFOntologia<%=vsinaisFraco.getId()%>" border="0" onClick="adicionarSinalFracoOntologia(this, '<%=vsinaisFraco.getDescricao().replace("\n", "<BR>").replace("\r", "<BR>").replace("'", "<ASPAS>")%>', '<%=vsinaisFraco.getId()%>');" cellpadding="0" cellspacing="0" style="width: 200px; cursor: hand; cursor: pointer; visibility: visible; display: '';">
                <tr style="height: 16px">
                    <td style="background-image: url('<%=complementar%>imagens/box/topoesquerda.png'); width: 16px;"> &nbsp; </td>
                    <td style="background-image: url('<%=complementar%>imagens/box/topocentro.png'); "></td>
                    <td style="background-image: url('<%=complementar%>imagens/box/topodireita.png'); width: 16px;"></td>
                </tr>
                <tr style="height: 16px">
                    <td style="background-image: url('<%=complementar%>imagens/box/centroesquerda.png'); width: 16px;"></td>
                    <td bgcolor="#ffffff">
                        <span style="cursor: hand; cursor: pointer;" onmouseover="exibirinfo('<%=vsinaisFraco.getId()%>');"> <%=(i + 1)%> </span>
                        <%=vsinaisFraco.getDescricao()%>
                    </td>
                    <td style="background-image: url('<%=complementar%>imagens/box/centrodireita.png'); width: 16px;"></td>
                </tr>
                <tr style="height: 16px">
                    <td style="background-image: url('<%=complementar%>imagens/box/baseesquerda.png'); width: 16px;"></td>
                    <td style="background-image: url('<%=complementar%>imagens/box/basecentro.png'); "></td>
                    <td style="background-image: url('<%=complementar%>imagens/box/basedireita.png'); width: 16px;"></td>
                </tr>
            </table>
            <%
                        }
            %>

            <br>
        </center>
    </div>
</div>
