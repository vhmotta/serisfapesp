<%@page import="seris.ontologia.redesemantica.Conceito" %>
<%@page import="seris.ontologia.OntologiaManager" %>
<%@page import="seris.ontologia.ClasseSinalFraco" %>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>

<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>

<%
    int tipoSemantica = 0;
    String strTipoSemantica = request.getParameter("tipoSemantica");
    if (strTipoSemantica != null && !strTipoSemantica.equals("")) {
        tipoSemantica = Integer.parseInt(strTipoSemantica);
    }
%>
<div style="background-color: #ffffff;">
    <div style="background-color: #bb3a34; height: 18px; border-top: 4px solid #bb3a34;"> <font color="#ffffff">
        <b><center> 
                <%                if (tipoSemantica == 0) {
                %>
                <bean:message key="label.resultadoSinaisFracos"/> 
                <%                } else if (tipoSemantica == 1) {
                %>
                <bean:message key="label.resultadoConhecimento"/> 
                <%                } else {
                %>
                <bean:message key="label.resultadoAmbos"/> 
                <%                }
                %>
            </center></b></font>
    </div>
    <div style="overflow: auto; height: 458px;">
        <center>
            <br>
            <%
                String complementar = request.getParameter("complementar");
                if (complementar == null) {
                    complementar = "";
                }

                String kit = request.getParameter("kit");
                if (kit == null) {
                    kit = "";
                }

                String texto = request.getParameter("texto");
                if (texto == null) {
                    texto = "";
                }

                String id = request.getParameter("id");
                if (id == null) {
                    id = "0";
                }

                String listaId = request.getParameter("listaId");
                if (listaId == null) {
                    listaId = "0";
                }

                String graficoIds = request.getParameter("graficoIds");
                if (graficoIds == null) {
                    graficoIds = "";
                }

                int idDominio = 0;
                String strIdDominio = request.getParameter("idDominio");
                if (strIdDominio != null && !strIdDominio.equals("")) {
                    idDominio = Integer.parseInt(strIdDominio);
                }

                int nivelConexao = 2;
                String strNivelConexao = request.getParameter("nivelConexao");
                if (strNivelConexao != null && !strNivelConexao.equals("")) {
                    nivelConexao = Integer.parseInt(strNivelConexao);
                }

                int rede = 0;
                String strRede = request.getParameter("rede");
                if (strRede != null && !strRede.equals("")) {
                    rede = Integer.parseInt(strRede);
                }

                Character marcarPalavras = 'N';
                String strMarcarPalavras = request.getParameter("marcarPalavras");
                if (strMarcarPalavras != null && strMarcarPalavras.equals("S")) {
                    marcarPalavras = 'S';
                }

                String dataInicial = request.getParameter("dataInicial");
                String dataFinal = request.getParameter("dataFinal");

                OntologiaManager om = new OntologiaManager(Integer.parseInt(id), texto, kit, Integer.parseInt(listaId), graficoIds, nivelConexao, idDominio, tipoSemantica, rede, marcarPalavras, dataInicial, dataFinal);
                om.carregarClasseSinalFraco();
                java.util.List<ClasseSinalFraco> g0list = om.getG0list();
                java.util.List<ClasseSinalFraco> g1list = om.getG1list();
                java.util.List<ClasseSinalFraco> g2list = om.getG2list();
                java.util.List<ClasseSinalFraco> g3list = om.getG3list();
                java.util.List<ClasseSinalFraco> g4list = om.getG4list();
                java.util.List<ClasseSinalFraco> g5list = om.getG5list();

                java.util.List<Conceito> conceitosRedeSemantica = om.getConceitosRedeSemantica();
            %>

            <bean:message key="label.escolhaRedeWikonx"/>: 
            <select name="selrede" id="selrede" style="width: 150px;">
                <option value="0"><bean:message key="label.todasRedes"/></option>
                <%
                    Conceito conceito;
                    String checked = "";
                    for (int i = 0; i < conceitosRedeSemantica.size(); i++) {
                        checked = "";
                        if ((i + 1) == rede) {
                            checked = " selected ";
                        }
                        conceito = conceitosRedeSemantica.get(i);
                        out.println("<option " + checked + "value=\"" + (i + 1) + "\">" + conceito.getNome() + "</option>");
                    }
                %>
            </select>
            <input type="button" value="<bean:message key="label.conectar"/>" onclick="exibirOntologia();">
            <hr>

            <table border="0" width="370px" cellpadding="0" cellspacing="0">
                <%
                    out.println(om.getHTML(g0list, "+++", "#640000"));
                    out.println(om.getHTML(g1list, "++", "#8e0000"));
                    out.println(om.getHTML(g2list, "+", "#bc0000"));
                    out.println(om.getHTML(g3list, "-", "#e80000"));
                    out.println(om.getHTML(g4list, "--", "#ff2424"));
                    out.println(om.getHTML(g5list, "---", "#ff4e4e"));
                %>
            </table>
            <script>
                limparPalavraGrifarSelected();
                <%
                    if (marcarPalavras == 'S') {
                        out.println(om.getHTMLGrifarSF());
                    }
                %>
            </script>

            <!-- table id="tableSFOntologia<=vsinaisFraco.getId()%>" border="0" onClick="adicionarSinalFracoOntologia(this, '<=vsinaisFraco.getDescricao().replace("\n", "<BR>").replace("\r", "<BR>").replace("'", "<ASPAS>")%>', '<=vsinaisFraco.getId()%>');" cellpadding="0" cellspacing="0" style="width: 200px; cursor: hand; cursor: pointer; visibility: visible; display: '';">
                <tr style="height: 16px">
                    <td style="background-image: url('<%=complementar%>imagens/box/topoesquerda.png'); width: 16px;"> &nbsp; </td>
                    <td style="background-image: url('<%=complementar%>imagens/box/topocentro.png'); "></td>
                    <td style="background-image: url('<%=complementar%>imagens/box/topodireita.png'); width: 16px;"></td>
                </tr>
                <tr style="height: 16px">
                    <td style="background-image: url('<%=complementar%>imagens/box/centroesquerda.png'); width: 16px;"></td>
                    <td bgcolor="#ffffff">
                        <=csf.getClasse()%><br>
                        <=vsinaisFraco.getDescricao()%>
                    </td>
                    <td style="background-image: url('<%=complementar%>imagens/box/centrodireita.png'); width: 16px;"></td>
                </tr>
                <tr style="height: 16px">
                    <td style="background-image: url('<%=complementar%>imagens/box/baseesquerda.png'); width: 16px;"></td>
                    <td style="background-image: url('<%=complementar%>imagens/box/basecentro.png'); "></td>
                    <td style="background-image: url('<%=complementar%>imagens/box/basedireita.png'); width: 16px;"></td>
                </tr>
            </table-->

            <br>
        </center>
    </div>
</div>
