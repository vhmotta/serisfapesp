<%--
    Document   : cadastrarCriacaoSentido
    Created on : jan/2011
    Author     : Vitor Hugo da Motta <vmotta@gmail.com>
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso8859-1" />
        <%
                    response.setHeader("Cache-Control", "no-cache");
                    response.setHeader("Pragma", "no-cache");
                    response.setDateHeader("Expires", 0);
        %>
        <title>Seris</title>
    </head>
    <body id="mybody">

        <script>
            <%
                        String showmsg = (String) request.getParameter("showmsg");

                        if (showmsg != null && showmsg.equals("TRUE")) {
                            String msg = (String) request.getAttribute("message");
                            if (msg != null && !msg.equals("")) {
                                out.println("alert('" + msg + "');");
                            }
                        }
            %>
        </script>

    </body>
</html>