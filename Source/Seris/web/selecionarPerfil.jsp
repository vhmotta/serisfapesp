<%--
    Document   : selecionarPerfil
    Created on : jul/2010
    Author     : Ricardo C Bull - rcbull@gmail.com
--%>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso8859-1" />
    <title>Seris</title>
    <%
                response.setHeader("Cache-Control", "no-cache");
                response.setHeader("Pragma", "no-cache");
                response.setDateHeader("Expires", 0);
    %>
    <link REL="SHORTCUT ICON" HREF="favicon2.ico">
        <style type="text/css" title="currentStyle">
            @import "media/css/demo_page.css";
            @import "media/css/demo_table.css";
        </style>
        <script type="text/javascript" language="javascript" src="media/js/jquery.js"></script>
        <script type="text/javascript" language="javascript" src="media/js/jquery.dataTables.js"></script>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function() {
                $('#datatable').dataTable( {
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": false,
                    "bAutoWidth": false } );
            } );
        </script>
        <link REL="SHORTCUT ICON" HREF="favicon2.ico">
            <link rel="stylesheet" type="text/css" href="seris.css">
                </head>
                <body>
                    <%@ include file="cabecalho.jsp" %>
                    <div>
                        <jsp:useBean id="EfetuarLoginActionForm" scope="session" class="seris.EfetuarLoginActionForm">
                        </jsp:useBean>
                        <%
                                    String vUsuario = (String) session.getAttribute("nome");
                                    //out.println("Usu?rio: " + vUsuario);
                                    //out.println("N?vel: " + session.getAttribute("nivel"));
                                    //out.println("N?vel: " + EfetuarLoginActionForm.getNivel());
                                    if (vUsuario == null) {
                        %>
                        <jsp:forward page="login.jsp"/>
                        <%        } else {
                                    int usuario_id = Integer.parseInt(String.valueOf(session.getAttribute("usuario_id")));
                                    seris.database.Usuario usuario = seris.database.DAO.UsuarioDAO.consultarUsuario(usuario_id);
                                    seris.database.Administrador vAdministrador = seris.database.DAO.AdministradorDAO.consultarUsuario(usuario_id);
                                    java.util.List vListaPatrocinador = seris.database.DAO.PatrocinadorDAO.consultarListaUsuario(usuario_id);
                                    seris.database.Suportedominio vSuportedominio = seris.database.DAO.SuportedominioDAO.consultarUsuario(usuario_id);
                                    seris.database.Altadirecao vAltadirecao = seris.database.DAO.AltadirecaoDAO.consultarUsuario(usuario_id);
                                    seris.database.Analista vAnalista = seris.database.DAO.AnalistaDAO.consultarUsuario(usuario_id);
                        %>
                        <form action="efetuarLogin.do?task=selecionarPerfil" method="post">
                            <div id="nomecont" align="center"><bean:message key="label.selecionarPerfil"/></div>
                            <table width="500px">
                                <%
                                            java.util.ResourceBundle resource = java.util.ResourceBundle.getBundle("seris.SerisIdioma");
                                            boolean vExistePerfil = false;
                                            if (vAdministrador != null && vAdministrador.getUsuarioId() != 0) {
                                                vExistePerfil = true;
                                                out.println("<tr><td width=\"10px\"><input type=\"radio\" name=\"nivel\" value=\"0\"/></td><td>"+ resource.getString("label.administrador") +"</td></tr>");
                                            }
                                            if (vSuportedominio != null && vSuportedominio.getUsuarioId() != 0) {
                                                vExistePerfil = true;
                                                out.println("<tr><td width=\"10px\"><input type=\"radio\" name=\"nivel\" value=\"2\"/></td><td>"+ resource.getString("label.suporteDominio") +"</td></tr>");
                                            }
                                            if (vListaPatrocinador.size() > 0) {
                                                vExistePerfil = true;
                                                out.println("<tr><td width=\"10px\"><input type=\"radio\" name=\"nivel\" value=\"1\"/></td><td>"+ resource.getString("label.patrocinador") +"</td></tr>");
                                            }
                                            if (vAltadirecao != null && vAltadirecao.getUsuarioId() != 0) {
                                                vExistePerfil = true;
                                                out.println("<tr><td width=\"10px\"><input type=\"radio\" name=\"nivel\" value=\"3\"/></td><td>"+ resource.getString("label.altaDirecao") +"</td></tr>");
                                            }
                                            if (vAnalista != null && vAnalista.getUsuarioId() != 0) {
                                                vExistePerfil = true;
                                                out.println("<tr><td width=\"10px\"><input type=\"radio\" name=\"nivel\" value=\"5\"/></td><td>"+ resource.getString("label.facilitador") +"</td></tr>");
                                            }
                                            if (usuario != null && usuario.getId() != 0) {
                                                vExistePerfil = true;
                                                out.println("<tr><td width=\"10px\"><input type=\"radio\" name=\"nivel\" value=\"4\"/></td><td>"+ resource.getString("label.captador") +"</td></tr>");
                                            }
                                            if (vExistePerfil == false) {
                                %>
                                <jsp:forward page="login.jsp"/>
                                <%            }
                                %>
                            </table>
                            <input type="submit" value="<bean:message key="label.confirmar"/>">
                        </form>
                        <%
                                    }
                        %>
                    </div>
                    <%@ include file="rodape.jsp" %>
                </body>
                </html>
