<%--
    Document   : cadastrarListaSinalFraco
    Created on : nov/2010
    Author     : Vitor Hugo da Motta <vmotta@gmail.com>
--%>

<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <!-- Importa??o de Taglib -->
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>

    <!-- EDITAR - Importa??o de classes que ser?o utilizadas -->
    <%@page import="seris2.database.ListaSinalFraco" %>
    <%@page import="seris2.database.ListasSinaisFracos" %>
    <%@page import="seris2.database.ListasUsuarios" %>
    <%@page import="seris.database.Usuario" %>
    <%@page import="seris.database.Sinalfraco" %>
    <%@page import="seris.database.Ator" %>
    <%@page import="seris.database.Produto" %>
    <%@page import="seris.database.Mercado" %>
    <%@page import="seris.database.Fonte" %>
    <%@page import="seris.database.Kit" %>
    <%@page import="seris.database.Dominio" %>

    <!-- Cabe?alho do site -->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso8859-1" />
        <%
                    response.setHeader("Cache-Control", "no-cache");
                    response.setHeader("Pragma", "no-cache");
                    response.setDateHeader("Expires", 0);
        %>
        <title>Seris</title>
        <link REL="SHORTCUT ICON" HREF="favicon2.ico">
        <style type="text/css" title="currentStyle">
            @import "media/css/demo_page.css";
            @import "media/css/demo_table.css";
        </style>
        <link rel="stylesheet" type="text/css" href="seris.css">

        <!-- Script para o grid -->
        <script type="text/javascript" language="javascript" src="javascript/ajax.js"></script>
        <script type="text/javascript" language="javascript" src="media/js/jquery.js"></script>
        <script type="text/javascript" language="javascript" src="media/js/jquery.dataTables.js"></script>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function() {
                $('#datatable').dataTable( {
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": false,
                    "bAutoWidth": false } );

                $('#datatable2').dataTable( {
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": false,
                    "bAutoWidth": false } );

                $('#datatable3').dataTable( {
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": false,
                    "bAutoWidth": false } );
            } );
            jQuery.fn.dataTableExt.oSort['string-asc']  = function(a,b) {
                var x = a.toLowerCase();
                var y = b.toLowerCase();
                return x.localeCompare(y);
            };

            jQuery.fn.dataTableExt.oSort['string-desc'] = function(a,b) {
                var x = a.toLowerCase();
                var y = b.toLowerCase();
                return y.localeCompare(x);
            };
        </script>
        <%
                    java.util.ResourceBundle resource = java.util.ResourceBundle.getBundle("seris.SerisIdioma");
        %>
        <script type="text/javascript">

            // EDITAR - Adicionar campos obrigat?rios
            function validaForm(){
                d = document.cadastrarListaSinalFraco;
                if (d.nome.value == ""){
                    alert("<bean:message key="message.preencherField" arg0="<%=resource.getString("label.nome")%>"/>");
                    d.nome.focus();
                    return false;
                }
                if (d.data.value == ""){
                    alert("<bean:message key="message.preencherField" arg0="<%=resource.getString("label.data")%>"/>");
                    d.data.focus();
                    return false;
                }
                if (d.dataInicial.value == ""){
                    alert("<bean:message key="message.preencherField" arg0="<%=resource.getString("label.dataInicial")%>"/>");
                    d.dataInicial.focus();
                    return false;
                }
                if (d.dataFinal.value == ""){
                    alert("<bean:message key="message.preencherField" arg0="<%=resource.getString("label.dataFinal")%>"/>");
                    d.dataFinal.focus();
                    return false;
                }
                return true;
            }

            function validaFormParticipante(){
                d = document.formAdicionarParticipante;
                //                if (d.participanteId.value == "" && (d.nomeParticipante.value == "" || d.funcaoParticipante.value == "" || d.participanteIdOutros.value == "")) {
                if (d.participanteId.value == "" && (d.funcaoParticipante.value == "" || d.participanteIdOutros.value == "")) {
                    alert("<bean:message key="message.selecionarParticipante"/>");
                    d.participanteId.focus();
                    return false;
                }
                return true;
            }

            // Mascara para campo do tipo data
            function format(value,data){
                value = value.replace(/\D/g,"");
                var result="";
                if(data.length < value.length)
                    return value;
                for(i=0,j=0;(i<data.length)&&(j<value.length);i++)
                {
                    var ch = data.charAt(i);
                    if(ch == '#')
                    {
                        result += value.charAt(j++);
                        continue;
                    }
                    result += ch;
                }
                return result;
            }
        </script>

    </head>
    <body id="mybody">

        <!-- Inclui Cabe?alho e Menu -->
        <%@ include file="cabecalho.jsp" %>
        <%@ include file="menu.jsp" %>

        <!-- DIV com o conte?do de todo site -->
        <div id="conteudo">

            <!-- Obt?m uma inst?ncia do domain da cria??o de sentido -->
            <jsp:useBean id="listaSinalFraco" scope="session" class="seris2.database.ListaSinalFraco"></jsp:useBean>

            <%
                        String vAbaSelecionada = request.getParameter("abaSelecionada");
                        if (vAbaSelecionada == null) {
                            vAbaSelecionada = "1";
                        }

                        String vKitId = request.getParameter("kitSelecionado");

                        String vId = request.getParameter("id");
                        String vTask = request.getParameter("task");
                        String vData = seris.utils.Utils.getDataSistema();
                        String vDataInicial = seris.utils.Utils.getDataSistema(-30);
                        String vDataFinal = seris.utils.Utils.getDataSistema();
                        if (vTask == null) {
                            vTask = "";
                        }

                        if (vId == null || vId.equals("0")) {
                            String vIdSalvo = (String) request.getAttribute("idSalvo");
                            if (vIdSalvo != null) {
                                vId = vIdSalvo;
                            }
                        }

                        String dominioName = "";
                        String kitName = "";

                        if (vKitId != null && !vKitId.equals("0")) {
                            Kit kit = seris.database.DAO.KitDAO.consultarKit(Integer.parseInt(vKitId));
                            Dominio dominio = seris.database.DAO.DominioDAO.consultarDominio(kit.getDominioId());

                            dominioName = dominio.getNome();
                            kitName = kit.getDescricao();
                        }

                        // EDITAR - Consulta ou seta os campos para branco
                        int id = 0;
                        if (vId != null && !vId.equals("0") && (!vTask.equals("delete"))) {
                            id = Integer.parseInt(vId);
                            listaSinalFraco = seris2.database.DAO.ListaSinalFracoDAO.consultarListaSinalFraco(id);
                            vData = seris.utils.Utils.ConverteDateParaString(listaSinalFraco.getData());
                            vDataInicial = seris.utils.Utils.ConverteDateParaString(listaSinalFraco.getDataInicial());
                            vDataFinal = seris.utils.Utils.ConverteDateParaString(listaSinalFraco.getDataFinal());
                        } else {
                            vId = "0";
                            listaSinalFraco.setNome("");
                        }

                        // Se houver um registro selecionado mostra o t?tulo editar e o Id
                        if (vId != null && !vId.equals("0")) {
                            out.print("<div id=\"nomecont\" align=\"center\">" + resource.getString("label.editarListaSF") + "</div>");
                        } else {
                            out.print("<div id=\"nomecont\" align=\"center\">" + resource.getString("label.cadastrarListaSF") + "</div>");
                        }
            %>

            <div>
                <b><bean:message key="label.dominio"/>:</b> <%=dominioName%> <BR>
                <b><bean:message key="label.kit"/>:</b> <%=kitName%>
            </div>
            <br>

            <form name="cadastrarListaSinalFraco" action="cadastrarListaSinalFraco.do" method="post" onSubmit="return validaForm()">

                &nbsp;
                <input type="button" value="<bean:message key="label.novo"/>" OnClick="document.location.href = 'cadastrarListaSinalFraco.do?task=novo&kitSelecionado=<%=vKitId%>';" style="width: 100px">

                <input type="submit" value="<bean:message key="label.salvar"/>" style="width: 100px">
                <%
                            if (vId != null && !vId.equals("0")) {
                %>
                <input type="button" value="<bean:message key="label.deletar"/>" OnClick="if (confirm('<bean:message key="message.deletarListaSF"/>')) { document.cadastrarListaSinalFraco.task.value='delete'; document.cadastrarListaSinalFraco.submit();}" style="width: 100px">
                <input type="button" value="<bean:message key="label.finalizar"/>" OnClick="document.cadastrarListaSinalFraco.task.value='finalizar'; document.cadastrarListaSinalFraco.submit();" style="width: 100px">
                <%           } // fim de "if (vId != null && !vId.equals("0")) {"
                %>

                <input type="button" value="<bean:message key="label.pesquisar"/>" OnClick="document.location.href = 'cadastrarListaSinalFraco.do?task=pesquisar&kitSelecionado=<%=vKitId%>';"  style="width: 100px">

                <br>
                <font color="blue">
                    <%
                                if (request.getAttribute("message") != null) {
                                    out.print("&nbsp;&nbsp;&nbsp;" + request.getAttribute("message"));
                                }
                    %>
                </font>

                <BR><BR>

                <%
                            if (vId != null && !vId.equals("0")) {
                                out.print("&nbsp;Id: " + vId + "<br>");
                            }
                %>

                <div style="background-color: #d69d92; height: 2px;"></div><br>
                <table width="600" border="0">
                    <tr>
                        <td> *<bean:message key="label.nome"/>: </td>
                        <td>  <input type="text" name="nome"  size="60" maxlength="100" value="<%=listaSinalFraco.getNome()%>"/> </td>
                    </tr><tr>
                        <td> *<bean:message key="label.data"/>: </td>
                        <td> <input type="text" name="data" size="10" onkeyup="this.value=format(this.value,'##/##/####');" value="<%=vData%>"/> DD/MM/AAAA  </td>
                    </tr><tr>
                        <td colspan="2" valign="top">
                            <BR>
                            <table cellpadding="0" cellspacing="0" width="740px">
                                <td valign="bottom"> <font color="#666666"><b> <bean:message key="label.categorizacao"/>: </b></font> </td>
                                <td valign="bottom" align="right"> <input type="submit" value="<bean:message key="label.atualizar"/>" style="width: 100px"> </td>
                            </table>
                            <div style="background-color: #d69d92; height: 2px;"></div>
                        </td>
                    </tr><tr>
                        <td> *<bean:message key="label.periodo"/>: </td>
                        <td>
                            <input type="text" name="dataInicial" size="10" onkeyup="this.value=format(this.value,'##/##/####');" value="<%=vDataInicial%>"/> &nbsp; ? &nbsp;
                            <input type="text" name="dataFinal" size="10" onkeyup="this.value=format(this.value,'##/##/####');" value="<%=vDataFinal%>"/> DD/MM/AAAA
                        </td>
                    </tr><tr>
                        <td> <bean:message key="label.ator"/>: </td>
                        <td>
                            <select name="atorId">
                                <option value="">--</option>
                                <%
                                            java.util.List list = seris.database.DAO.AtorDAO.consultarListaAtoresPorKit(vKitId);

                                            Ator vAtor;
                                            for (int i = 0; i < list.size(); i++) {
                                                vAtor = (Ator) list.get(i);
                                                if (listaSinalFraco.getAtorId() != null && vAtor.getId().intValue() == listaSinalFraco.getAtorId().intValue()) {
                                                    out.print("<option value=\"" + vAtor.getId() + "\" selected>" + vAtor.getNome() + "</option>");
                                                } else {
                                                    out.print("<option value=\"" + vAtor.getId() + "\">" + vAtor.getNome() + "</option>");
                                                }
                                            }
                                %>
                            </select>
                        </td>
                    </tr><tr>
                        <td> <bean:message key="label.produto"/>: </td>
                        <td>
                            <select name="produtoId">
                                <option value="">--</option>
                                <%
                                            list = seris.database.DAO.ProdutoDAO.consultarListaProdutosPorKit(vKitId);

                                            Produto vProduto;
                                            for (int i = 0; i < list.size(); i++) {
                                                vProduto = (Produto) list.get(i);
                                                if (listaSinalFraco.getProdutoId() != null && vProduto.getId().intValue() == listaSinalFraco.getProdutoId().intValue()) {
                                                    out.print("<option value=\"" + vProduto.getId() + "\" selected>" + vProduto.getDescricao() + "</option>");
                                                } else {
                                                    out.print("<option value=\"" + vProduto.getId() + "\">" + vProduto.getDescricao() + "</option>");
                                                }
                                            }
                                %>
                            </select>
                        </td>
                    </tr><tr>
                        <td> <bean:message key="label.mercado"/>: </td>
                        <td>
                            <select name="mercadoId">
                                <option value="">--</option>
                                <%
                                            list = seris.database.DAO.MercadoDAO.consultarListaMercadosPorKit(vKitId);

                                            Mercado vMercado;
                                            for (int i = 0; i < list.size(); i++) {
                                                vMercado = (Mercado) list.get(i);
                                                if (listaSinalFraco.getMercadoId() != null && vMercado.getId().intValue() == listaSinalFraco.getMercadoId().intValue()) {
                                                    out.print("<option value=\"" + vMercado.getId() + "\" selected>" + vMercado.getNome() + "</option>");
                                                } else {
                                                    out.print("<option value=\"" + vMercado.getId() + "\">" + vMercado.getNome() + "</option>");
                                                }
                                            }
                                %>
                            </select>
                        </td>
                    </tr><tr>
                        <td> <bean:message key="label.fonte"/>: </td>
                        <td>
                            <select name="fonteId">
                                <option value="">--</option>
                                <%
                                            list = seris.database.DAO.FonteDAO.consultarListaFontesPorKit(vKitId);

                                            Fonte vFonte;
                                            for (int i = 0; i < list.size(); i++) {
                                                vFonte = (Fonte) list.get(i);
                                                if (listaSinalFraco.getFonteId() != null && vFonte.getId().intValue() == listaSinalFraco.getFonteId().intValue()) {
                                                    out.print("<option value=\"" + vFonte.getId() + "\" selected>" + vFonte.getNome() + "</option>");
                                                } else {
                                                    out.print("<option value=\"" + vFonte.getId() + "\">" + vFonte.getNome() + "</option>");
                                                }
                                            }
                                %>
                            </select>
                        </td>
                    </tr>
                </table>

                <input type="hidden" name="id" value="<%=id%>" />
                <input type="hidden" name="kitSelecionado" value="<%=vKitId%>" />
                <input type="hidden" name="abaSelecionada" value="<%=vAbaSelecionada%>" />
                <input type="hidden" name="task" value="save" /><br /><br />
            </form>

            <%
                        // se est? em edi??o exibe o grid para adicionar sinal fraco na lista
                        if (vId != null && !vId.equals("0")) {

                            java.util.List sinalFracoList = seris.database.DAO.SinalfracoDAO.consultarListaSinaisFracosSemLista(Integer.parseInt(vId), listaSinalFraco.getDataInicial(), listaSinalFraco.getDataFinal(), listaSinalFraco.getKitId(), listaSinalFraco.getAtorId(), listaSinalFraco.getProdutoId(), listaSinalFraco.getMercadoId(), listaSinalFraco.getFonteId());
                            //java.util.List sinalFracoList = seris.database.DAO.SinalfracoDAO.consultarListaSinaisFracos();
                            java.util.List listasSinaisFracosList = seris2.database.DAO.ListasSinaisFracosDAO.consultarListasSinaisFracosByListaId(Integer.parseInt(vId));
            %>

            <A NAME="ABAS"></A>

            <table cellpadding="0" cellspacing="0" border="0" style="background-image: url('imagens/abafundo.png'); width: 100%; height: 30px;">
                <tr>
                    <td width="2"></td>
                    <td id="aba1" OnClick="ativarAba('1');" width="212" style="background-image: url('imagens/abaselecionada.png'); background-repeat: no-repeat; cursor: hand; cursor: pointer;" align="center" valign="top">
                        <font style="font-size: 4px;"><BR></font>
                        <b> <bean:message key="label.sinaisFracos"/> </b>
                    </td>
                    <td id="aba2" OnClick="ativarAba('2');" width="212" style="background-image: url('imagens/abanaoselecionada.png'); background-repeat: no-repeat; cursor: hand; cursor: pointer;" align="center" valign="top">
                        <font style="font-size: 4px;"><BR></font>
                        <b> <bean:message key="label.participantes"/> </b>
                    </td>
                    <td>&nbsp;</td>
                </tr>
            </table>

            <br>

            <div id="divListaSinaisFracos" style="visibility: visible; display: '';">
                <table>
                    <tr>
                        <td valign="top" width="400">

                            <div id="demo">
                                <font color="#666666"><B><bean:message key="label.sinaisFracosDisponiveis"/></B></font>
                                <div style="background-color: #d69d92; height: 2px;"></div>
                                <table cellpadding="0" cellspacing="0" border="0" class="display" width="600px" id="datatable">
                                    <thead>
                                        <tr>
                                            <th width="30px"><bean:message key="label.id"/></th>
                                            <th width="200"><bean:message key="label.descricao"/></th>
                                            <th width="30px"><bean:message key="label.inserir"/></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%
                                                                    Sinalfraco vSinalFraco;
                                                                    for (int i = 0; i < sinalFracoList.size(); i++) {
                                                                        vSinalFraco = (Sinalfraco) sinalFracoList.get(i);
                                                                        out.print("<tr class='seris'>");
                                                                        out.print("<td> <span style=\"cursor: hand; cursor: pointer;\" OnCLick=\"exibirinfo('" + vSinalFraco.getId() + "');\">" + vSinalFraco.getId() + "</span> </td>");
                                                                        out.print("<td>" + vSinalFraco.getDescricao() + "</td>");
                                                                        out.print("<td align='center'>");
                                                                        out.print("  <a href='cadastrarListaSinalFraco.do?task=inserirSinalFraco&id=" + vId + "&idSinalFraco=" + vSinalFraco.getId() + "&kitSelecionado=" + vKitId + "'><img src='imagens/insert-icon.png' border='0'></a>");
                                                                        out.print("</td>");
                                                                        out.print("</tr>");
                                                                    }
                                        %>
                                    </tbody>
                                </table>
                            </div>

                        </td>
                        <td width="10"></td>
                        <td valign="top" width="400">

                            <div id="demo">
                                <font color="#666666"><B><bean:message key="label.sinaisFracosLista"/></B></font>
                                <div style="background-color: #d69d92; height: 2px;"></div>
                                <table cellpadding="0" cellspacing="0" border="0" class="display" width="600px" id="datatable2">
                                    <thead>
                                        <tr>
                                            <th width="30px"><bean:message key="label.id"/></th>
                                            <th width="200px"><bean:message key="label.descricao"/></th>
                                            <th width="30px"><bean:message key="label.excluir"/></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <%
                                                                    ListasSinaisFracos vListasSinaisFracos;
                                                                    for (int i = 0; i < listasSinaisFracosList.size(); i++) {
                                                                        vListasSinaisFracos = (ListasSinaisFracos) listasSinaisFracosList.get(i);
                                                                        if (vListasSinaisFracos.getSinalFraco() != null) {
                                                                            out.print("<tr class='seris'>");
                                                                            out.print("<td> <span style=\"cursor: hand; cursor: pointer;\" OnCLick=\"exibirinfo('" + vListasSinaisFracos.getSinalFraco().getId() + "');\">" + vListasSinaisFracos.getSinalFraco().getId() + "</span> </td>");
                                                                            out.print("<td>" + vListasSinaisFracos.getSinalFraco().getDescricao() + "</td>");
                                                                            out.print("<td align='center'>");
                                                                            out.print("  <a href='cadastrarListaSinalFraco.do?task=excluirSinalFraco&id=" + vId + "&idSinalFraco=" + vListasSinaisFracos.getId() + "&kitSelecionado=" + vKitId + "'><img src='imagens/delete-icon.png' border='0'></a>");
                                                                            out.print("</td>");
                                                                            out.print("</tr>");
                                                                        }
                                                                    }
                                        %>
                                    </tbody>
                                </table>
                            </div>

                        </td>
                    </tr>
                </table>
            </div>
            <div id="divParticipantes" style="visibility: hidden; display: 'none';">
                <form name="formAdicionarParticipante" action="cadastrarListaSinalFraco.do" method="post" onSubmit="return validaFormParticipante()">
                    <table width="600" border="0">
                        <tr>
                            <td> <bean:message key="label.usuario"/>: </td>
                            <td>
                                <select name="participanteId">
                                    <option value="">--</option>
                                    <%
                                                                //list = seris.database.DAO.UsuarioDAO.consultarListaUsuarios();
                                                                list = seris.database.DAO.UsuarioDAO.consultarListaUsuariosByDominio(vKitId);

                                                                Usuario vUsuario;
                                                                for (int i = 0; i < list.size(); i++) {
                                                                    vUsuario = (Usuario) list.get(i);
                                                                    out.print("<option value=\"" + vUsuario.getId() + "\">" + vUsuario.getNome() + "</option>");
                                                                }
                                    %>
                                </select>
                                <input type="submit" value="<bean:message key="label.adicionar"/>"  style="width: 100px;">
                            </td>
                            <!-- /tr><tr>
                                <td> Nome: </td>
                                <td>  <input type="text" name="nomeParticipante"  size="60" maxlength="50" value=""/> </td -->
                        </tr><tr>
                            <td> <bean:message key="label.outros"/>: </td>
                            <td> 
                                <select name="funcaoParticipante">
                                    <option value="Convidado"> <bean:message key="label.convidado"/> </option>
                                    <option value="Especialista"> <bean:message key="label.especialista"/> </option>
                                </select>

                                <select name="participanteIdOutros">
                                    <option value="">--</option>
                                    <%
                                                                //list = seris.database.DAO.UsuarioDAO.consultarListaUsuarios();
                                                                list = seris.database.DAO.UsuarioDAO.consultarListaUsuarios();

                                                                for (int i = 0; i < list.size(); i++) {
                                                                    vUsuario = (Usuario) list.get(i);
                                                                    out.print("<option value=\"" + vUsuario.getId() + "\">" + vUsuario.getNome() + "</option>");
                                                                }
                                    %>
                                </select>

                            </td>
                            <!-- /tr><tr>
                                <td> E-mail: </td>
                                <td>  <input type="text" name="emailParticipante"  size="60" maxlength="100" value=""/> </td -->
                        </tr>
                    </table>

                    <input type="hidden" name="id" value="<%=id%>" />
                    <input type="hidden" name="kitSelecionado" value="<%=vKitId%>" />
                    <input type="hidden" name="abaSelecionada" value="2" />
                    <input type="hidden" name="task" value="adicionarParticipante" />
                </form>

                <div id="demo">
                    <table cellpadding="0" cellspacing="0" border="0" class="display" width="600px" id="datatable3">
                        <thead>
                            <tr>
                                <th width="150px"><bean:message key="label.participante"/></th>
                                <th width="100px"><bean:message key="label.funcao"/></th>
                                <th width="150px"><bean:message key="label.email"/></th>
                                <th width="30px"><bean:message key="label.excluir2"/></th>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                                        java.util.List participantesList = seris2.database.DAO.ListasUsuariosDAO.consultarListasUsuariosByListaId(Integer.parseInt(vId));
                                                        ListasUsuarios vListasUsuarios;
                                                        String funcao;
                                                        for (int i = 0; i < participantesList.size(); i++) {
                                                            vListasUsuarios = (ListasUsuarios) participantesList.get(i);
                                                            funcao = vListasUsuarios.getFuncao();

                                                            if (funcao.indexOf("Facilitador") >= 0) {
                                                                funcao = resource.getString("label.facilitador");
                                                            } else if (funcao.indexOf("Patrocinador") >= 0) {
                                                                funcao = resource.getString("label.patrocinador");
                                                            } else if (funcao.indexOf("Captador") >= 0) {
                                                                funcao = resource.getString("label.captador");
                                                            } else if (funcao.indexOf("Alta Dire??o") >= 0) {
                                                                funcao = resource.getString("label.altaDirecao");
                                                            } else if (funcao.indexOf("Convidado") >= 0) {
                                                                funcao = resource.getString("label.convidado");
                                                            } else if (funcao.indexOf("Especialista") >= 0) {
                                                                funcao = resource.getString("label.especialista");
                                                            }

                                                            out.print("<tr class='seris'>");
                                                            out.print("<td>" + vListasUsuarios.getNome() + "</td>");
                                                            out.print("<td>" + funcao + "</td>");
                                                            out.print("<td>" + ((vListasUsuarios.getEmail() == null) ? "" : vListasUsuarios.getEmail()) + "</td>");
                                                            out.print("<td align='center'>");
                                                            out.print("  <a href='cadastrarListaSinalFraco.do?task=excluirParticipante&id=" + vId + "&listaUsuarioId=" + vListasUsuarios.getId() + "&kitSelecionado=" + vKitId + "&abaSelecionada=2'><img src='imagens/delete-icon.png' border='0'></a>");
                                                            out.print("</td>");
                                                            out.print("</tr>");
                                                        }
                            %>
                        </tbody>
                    </table>
                </div>

            </div>
            <%
                        } // if (vId != null && !vId.equals("0")) {

                        if (vTask != null) {
                            if (vTask.equals("pesquisar")) {

                                // EDITAR - Obt?m lista de ListaSinalFraco
                                java.util.List listaSinalFracoList = seris2.database.DAO.ListaSinalFracoDAO.consultarListaSinalFracoPorKit(vKitId);
            %>
            <div id="demo">
                <table cellpadding="0" cellspacing="0" border="0" class="display" width="600px" id="datatable">
                    <thead>
                        <tr>
                            <th width="30px"><bean:message key="label.id"/></th>
                            <th><bean:message key="label.nome"/></th>
                            <th><bean:message key="label.data"/></th>
                            <th><bean:message key="label.dataInicial"/></th>
                            <th><bean:message key="label.dataFinal"/></th>
                            <th><bean:message key="label.status"/></th>
                            <th width="30px"><bean:message key="label.editar"/></th>
                        </tr>
                    </thead>
                    <tbody>
                        <%
                                                        ListaSinalFraco vListaSinalFraco;
                                                        for (int i = 0; i < listaSinalFracoList.size(); i++) {
                                                            vListaSinalFraco = (ListaSinalFraco) listaSinalFracoList.get(i);
                                                            out.print("<tr class='seris'>");
                                                            out.print("<td>" + vListaSinalFraco.getId() + "</td>");
                                                            out.print("<td>" + vListaSinalFraco.getNome() + "</td>");
                                                            out.print("<td>" + seris.utils.Utils.ConverteDateParaString(vListaSinalFraco.getData()) + "</td>");
                                                            out.print("<td>" + seris.utils.Utils.ConverteDateParaString(vListaSinalFraco.getDataInicial()) + "</td>");
                                                            out.print("<td>" + seris.utils.Utils.ConverteDateParaString(vListaSinalFraco.getDataFinal()) + "</td>");
                                                            out.print("<td>" + ((vListaSinalFraco.getStatus() == 'A') ? resource.getString("label.aberta") : resource.getString("label.encerrada")) + "</td>");
                                                            out.print("<td align='center'>");
                                                            out.print("  <a href='cadastrarListaSinalFraco.jsp?id=" + vListaSinalFraco.getId() + "&kitSelecionado=" + vListaSinalFraco.getKitId() + "'><img src='imagens/edit-icon.png' border='0'></a>");
                                                            out.print("</td>");
                                                            out.print("</tr>");
                                                        }
                        %>
                    </tbody>
                </table>
            </div>
            <%
                            } // fim de "if (vTask.equals("pesquisar")) {"
                        } // fim de "if (vTask != null) {"
            %>

        </div>

        <!-- Inclui Rodap? -->
        <%@ include file="rodape.jsp" %>

        <div id="mainDivInfoSinalFraco" name="mainDivInfoSinalFraco" style="position: absolute; top: 10px; left: 10px; z-index: 1000; visibility: hidden; display: none; width: 500px;">
            <div style="border: solid 2px #bb3a34; background-color: #eeeeee; cursor: hand; cursor: pointer;" OnClick="esconderInfo();"><center><b><bean:message key="label.fechar"/></b></center></div>
            <div id="divInfoSinalFraco" name="divInfoSinalFraco" style="background-color: #bb3a34;">
                <font color="#ffffff"><center><b>Carregando ... Aguarde !!!</b></center></font>
            </div>
        </div>

        <script>
            var isIE = (navigator.appName.indexOf('Microsoft') !=-1);
            var mouseX=0, mouseY=0;

            if (!isIE) document.captureEvents(Event.MOUSEMOVE | Event.MOUSEUP);
            document.onmousemove = handlerMM;

            function handlerMM(e){
                mouseX = (!isIE) ? e.pageX : event.clientX ;
                mouseY = (!isIE) ? e.pageY : event.clientY ;
            }

            function exibirinfo(id){

                esconderInfo();

                var url = 'infoSinalFraco.jsp?id=' + id;
                ajaxLink("divInfoSinalFraco",url);

                var objdiv = document.getElementById("mainDivInfoSinalFraco");

                var posX = mouseX - 250;
                var posY = mouseY - 250;
                if (posX <= 0) posX = 10;
                if (posY <= 0) posY = 10;

                /*
                var winH = 0, winW;
                
                if (parseInt(navigator.appVersion)>3) {
                    if (navigator.appName=="Netscape") {
                        winH = window.innerHeight;
                        winW = window.innerWidth;
                    }
                    if (navigator.appName.indexOf("Microsoft")!=-1) {
                        winH = document.body.offsetHeight-80;
                        winW = document.body.offsetWidth;
                    }
                }
                 */

                /*
                if (document.documentElement && !document.documentElement.scrollTop) {
                    posSroll = document.documentElement.scrollTop;
                }else if (document.documentElement && document.documentElement.scrollTop){
                    posSroll = document.documentElement.scrollTop;
                }else if (document.body && document.body.scrollTop){
                    posSroll = document.body.scrollTop;
                }
                 */

                var topRolagem = document.getElementById('mybody').scrollTop;
                if (topRolagem == 0) {
                    topRolagem = document.documentElement.scrollTop;
                }
                var leftRolagem = document.getElementById('mybody').scrollLeft;
                if (leftRolagem == 0) {
                    leftRolagem = document.documentElement.scrollLeft;
                }
                
                winH = screen.availHeight-80;
                winW = screen.availWidth;

                posY = topRolagem + (winH/2) - 250;
                posX = leftRolagem + (winW/2) - 250;

                objdiv.style.top = posY + 'px';
                objdiv.style.left = posX + 'px';

                objdiv.style.visibility = 'visible';
                objdiv.style.display = '';
            }

            function esconderInfo(){
                document.getElementById('mainDivInfoSinalFraco').style.visibility = 'hidden';
                document.getElementById('mainDivInfoSinalFraco').style.display = 'none';
                var content = '<font color="#ffffff"><center><b>Carregando ... Aguarde !!!</b></center></font>';
                document.getElementById('divInfoSinalFraco').innerHTML=content;
            }

            function ativarAba(numero){

                document.cadastrarListaSinalFraco.abaSelecionada.value = numero;

                var obj1 = document.getElementById('aba1');
                var obj2 = document.getElementById('aba2');

                obj1.style.backgroundImage = "url('imagens/abanaoselecionada.png')";
                obj2.style.backgroundImage = "url('imagens/abanaoselecionada.png')";

                var div1 = document.getElementById('divListaSinaisFracos');
                var div2 = document.getElementById('divParticipantes');

                div1.style.visibility = 'hidden';
                div1.style.display = 'none';
                div2.style.visibility = 'hidden';
                div2.style.display = 'none';

                if (numero == '1') {
                    obj1.style.backgroundImage = "url('imagens/abaselecionada.png')";
                    div1.style.visibility = 'visible';
                    div1.style.display = '';
                } else {
                    obj2.style.backgroundImage = "url('imagens/abaselecionada.png')";
                    div2.style.visibility = 'visible';
                    div2.style.display = '';
                }

            }

            ativarAba('<%=vAbaSelecionada%>');

            location.href='#ABAS';

        </script>

    </body>
</html>