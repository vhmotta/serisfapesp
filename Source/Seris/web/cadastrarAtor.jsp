<%--
    Document   : cadastrarAtor
    Created on : jul/2010
    Author     : Ricardo C Bull - rcbull@gmail.com
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><!-- InstanceBegin template="/Templates/tempateSeris.dwt.jsp" codeOutsideHTMLIsLocked="false" -->
    <!-- InstanceBeginEditable name="taglibEimport" -->
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
    <%@page import="seris.database.Ator" %>
    <!-- InstanceEndEditable -->

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso8859-1" />
        <%
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Pragma", "no-cache");
            response.setDateHeader("Expires", 0);
        %>
        <!-- InstanceBeginEditable name="doctitle" -->
        <title>Seris</title>
        <!-- InstanceEndEditable -->
        <link REL="SHORTCUT ICON" HREF="favicon2.ico">
        <style type="text/css" title="currentStyle">
            @import "media/css/demo_page.css";
            @import "media/css/demo_table.css";
        </style>
        <script type="text/javascript" language="javascript" src="javascript/functions.js"></script>
        <script type="text/javascript" language="javascript" src="media/js/jquery.js"></script>
        <script type="text/javascript" language="javascript" src="media/js/jquery.dataTables.js"></script>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function() {
                $('#datatable').dataTable( {
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": false,
                    "bAutoWidth": false } );
            } );
            jQuery.fn.dataTableExt.oSort['string-asc']  = function(a,b) {
                var x = a.toLowerCase();
                var y = b.toLowerCase();
                return x.localeCompare(y);
            };

            jQuery.fn.dataTableExt.oSort['string-desc'] = function(a,b) {
                var x = a.toLowerCase();
                var y = b.toLowerCase();
                return y.localeCompare(x);
            };
        </script>
        <link REL="SHORTCUT ICON" HREF="favicon2.ico">
        <link rel="stylesheet" type="text/css" href="seris.css">
        <!-- InstanceBeginEditable name="head" -->
        <%
            java.util.ResourceBundle resource = java.util.ResourceBundle.getBundle("seris.SerisIdioma");
        %>
        <script type="text/javascript">
            function validaForm(){
                d = document.cadastrarAtor;
                if (d.nome.value == ""){
                    alert("<bean:message key="message.preencherField" arg0="<%=resource.getString("label.ator")%>"/>");
                    d.nome.focus();
                    return false;
                }
                return true;
            }
            
            function validaFormImportar(){
                d = document.importarAtor;
                if (d.arquivoImportar.value == ""){
                    alert("<bean:message key="message.preencherField" arg0="<%=resource.getString("label.importar")%>"/>");
                    d.arquivoImportar.focus();
                    return false;
                }
                return true;
            }
        </script>
        <!-- InstanceEndEditable -->
    </head>

    <body>
        <%@ include file="cabecalho.jsp" %>
        <%@ include file="menu.jsp" %>
        <div id="conteudo">
            <!-- InstanceBeginEditable name="conteudo" -->
            <jsp:useBean id="Ator" scope="session" class="seris.database.Ator"></jsp:useBean>
            <%
                String vId = request.getParameter("id");
                String vTask = request.getParameter("task");
                if (vTask == null) {
                    vTask = "";
                }
                int id = 0;
                if (vId != null && !vId.equals("0") && (!vTask.equals("delete"))) {
                    id = Integer.parseInt(vId);
                    Ator = seris.database.DAO.AtorDAO.consultarAtor(id);
                } else {
                    Ator.setNome("");
                }
            %>
            <jsp:setProperty name="Ator" property="id" value="<%= id%>"></jsp:setProperty>
            <jsp:setProperty name="Ator" property="nome" value="<%= Ator.getNome()%>"></jsp:setProperty>
            <%
                if (vId != null && !vId.equals("0")) {
                    out.print("<div id=\"nomecont\" align=\"center\">" + resource.getString("label.editarAtor") + "</div>");
                    out.print("Id: " + vId + "<br>");
                } else {
                    out.print("<div id=\"nomecont\" align=\"center\">" + resource.getString("label.cadastrarAtor") + "</div>");
                }
            %>

            <form name="cadastrarAtor" action="cadastrarAtor.do" method="post" onSubmit="return validaForm()">
                <bean:message key="label.ator"/>: <input type="text" name="nome"  size="60" maxlength="100" value="<jsp:getProperty name="Ator" property="nome"></jsp:getProperty>"/>
                <input type="hidden" name="id" value="<jsp:getProperty name="Ator" property="id"></jsp:getProperty>" />
                <input type="hidden" name="task" value="save" /><br /><br />
                <input type="submit" value="<bean:message key="label.salvar"/>" />&nbsp&nbsp
            </form>
            <%
                if (vId != null && !vId.equals("0")) {
            %>
            <form name="deletarAtor" action="cadastrarAtor.do" method="post">
                <input type="hidden" name="id" value="<jsp:getProperty name="Ator" property="id"></jsp:getProperty>" />
                <input type="hidden" name="task" value="delete" /><br />
                <input type="submit" value="<bean:message key="label.deletar"/>" />&nbsp&nbsp
            </form>
            <%        }
            %>
            <br><br>
            <form name="importarAtor" action="cadastrarAtor.do" method="post" onSubmit="return validaFormImportar()">
                <bean:message key="label.arquivo"/>: 
                <input type="text" name="arquivoImportar" id="arquivoImportar" style="width: 500px;">
                <input type="button" name="btnImportar" value="<bean:message key="label.arquivo"/>" onclick="abrirArquivo('arquivoImportar');">&nbsp&nbsp
                <input type="submit" value="<bean:message key="label.importar"/>" />&nbsp&nbsp

                <input type="hidden" name="id" value="<jsp:getProperty name="Ator" property="id"></jsp:getProperty>" />
                <input type="hidden" name="task" value="importar" /><br /><br />
            </form>
            <p>&nbsp;</p><p><font color="blue">
                    <% if (request.getAttribute("message") != null) {
                            out.print(request.getAttribute("message"));
                        }
                    %>
                </font></p>
            <br /><br />
            <html:link page="/cadastrarAtor.do?task=novo"><bean:message key="label.novo"/></html:link>&nbsp<html:link page="/cadastrarAtor.do?task=pesquisar"><bean:message key="label.pesquisar"/></html:link>
            <%
                if (vTask != null) {
                    if (vTask.equals("pesquisar")) {
            %>
            <jsp:scriptlet>
                session.setAttribute("Atores", seris.database.DAO.AtorDAO.consultarListaAtores());
            </jsp:scriptlet>
            <jsp:useBean id="Atores" scope="session" type="java.util.List"></jsp:useBean>
            <div id="demo">
                <table cellpadding="0" cellspacing="0" border="0" class="display" width="600px" id="datatable">
                    <thead>
                        <tr>
                            <th width="30px"><bean:message key="label.id"/></th>
                            <th width="540px"><bean:message key="label.nome"/></th>
                            <th width="30px"><bean:message key="label.editar"/></th>
                        </tr>
                    </thead>
                    <tbody>
                        <%
                            for (int i = 0; i < Atores.size(); i++) {
                                Ator vAtor = (Ator) Atores.get(i);
                                out.print("<tr class='seris'><td>" + vAtor.getId() + "</td><td>" + vAtor.getNome() + "</td><td align='center'><a href='cadastrarAtor.jsp?id=" + vAtor.getId() + "'><img src='imagens/edit-icon.png' border='0'></a></td></tr>");
                            }
                        %>
                    </tbody>
                </table>
            </div>
            <%
                    }
                }
            %>
            <!-- InstanceEndEditable -->
        </div>
        <%@ include file="rodape.jsp" %>
    </body>
    <!-- InstanceEnd --></html>