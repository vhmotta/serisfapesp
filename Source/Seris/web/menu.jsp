<%@page import="seris.database.Usuario"%>
<%@page import="seris.database.DAO.UsuarioDAO"%>
<style type="text/css">
    <!--
    a, a:hover, a:active, a:focus {
        outline:0;
        direction:ltr;
    }

    .wrapper {
        position:relative; height:25px;
    }

    .mainmenu {
        position:absolute;
        z-index:100;
        font-family:Verdana, Geneva, sans-serif;
        font-weight:normal;
        font-size:90%;
        line-height:25px;
        width:100%;
    }

    ul.menu {
        padding:0;
        margin:0;
        list-style:none;
        width:125px;
        overflow:hidden;
        float:left;
        margin-right:1px;
        background:#800000;
        z-index:50;
        border-right: solid 1px #ffffff;
    }

    ul.menu a {
        background:#800000;
        text-decoration:none;
        color:#fff;
        padding-left:5px;
        z-index:50;
    }

    ul.menu li.list {
        float:left;
        width:250px;
        margin:-32767px -125px 0px 0px;
        z-index:50;
    }

    ul.menu li.list a.category {
        position:relative;
        z-index:50;
        display:block;
        float:left;
        width:120px;
        margin-top:32767px;
        background:transparent;
    }

    ul.menu li.list a.category:hover,
    ul.menu li.list a.category:focus,
    ul.menu li.list a.category:active {
        margin-right:1px;
        background-repeat:no-repeat;
        background-position:left top;
        z-index:50;
    }

    ul.submenu {
        float:left;
        padding:25px 0px 0px 0px;
        margin:0;
        list-style:none;
        background-position:left top;
        margin:-25px 0px 0px 0px;
        z-index:50;
    }

    ul.submenu li a {
        float:left;
        width:120px;
        background:#800000;
        clear:left;
        color:#fff;
        z-index:50;
    }

    ul.submenu a:hover,
    ul.submenu a:focus,
    ul.submenu a:active {
        background:#CD5C5C;
        margin-right:1px;
        color:#fff;
        z-index:50;
    }
    -->
</style>
<div class="wrapper">
    <%
        int vNivel = -1;
        //out.print("N�vel selecionado=" + session.getAttribute("nivel"));
        if (session.getAttribute("nivel") != null) {
            vNivel = Integer.parseInt(String.valueOf(session.getAttribute("nivel")));
        }
        if (vNivel == -1) {
    %>
    <jsp:forward page="login.jsp"/>
    <%        }
    %>
    <div class="mainmenu">
        <ul class="menu">
            <li class="list"><a class="category" href="home.jsp"><bean:message key="label.inicio"/></a></li>
        </ul>
        <ul class="menu">
            <li class="list">
                <% if (vNivel == 0 || vNivel == 1 || vNivel == 2) {
                %>
                <a class="category" href="#"><bean:message key="label.planejamento"/></a>
                <ul class="submenu"/>
                <% } else {
                %>
                <a class="category" href="#">&nbsp;</a>
                <ul class="submenu">
                    <%        }
                        if (vNivel == 0 || vNivel == 2) {
                    %>
                    <li><a href="cadastrarDepartamento.jsp"><bean:message key="label.departamento"/></a></li>
                    <li><a href="cadastrarDominio.jsp"><bean:message key="label.dominio"/></a></li>
                    <li><a href="cadastrarGrupo.jsp"><bean:message key="label.grupo"/></a></li>
                    <li><a href="cadastrarPerfil.jsp"><bean:message key="label.perfil"/></a></li>
                    <li><a href="cadastrarUsuario.jsp"><bean:message key="label.usuario"/></a></li>
                    <li><a href="cadastrarLocal.jsp"><bean:message key="label.local"/></a></li>
                    <%        }
                        if (vNivel == 1 || vNivel == 2) {
                    %>
                    <li><a href="cadastrarFonte.jsp"><bean:message key="label.fonte"/></a></li>
                    <li><a href="cadastrarProduto.jsp"><bean:message key="label.produto"/></a></li>
                    <li><a href="cadastrarAtor.jsp"><bean:message key="label.ator"/></a></li>
                    <li><a href="cadastrarMercado.jsp"><bean:message key="label.mercado"/></a></li>
                    <%        }
                    %>
                </ul>
            </li>
        </ul>
        <ul class="menu">
            <li class="list">
                <a class="category" href="#"><bean:message key="label.captacao"/></a>
                <ul class="submenu">
                    <li><a href="selecionarDominio.jsp"><bean:message key="label.sinaisFracos"/></a></li>
                    <%
                        if (vNivel == 1 || vNivel == 2) {
                    %>
                    <li><a href="selecionarDominioKitGeral.jsp?formaction=importarSinalFraco.jsp"><bean:message key="label.importar"/></a></li>
                    <% }
                    %>
                </ul>
            </li>
        </ul>
        <ul class="menu">
            <li class="list">
                <a class="category" href="#"><bean:message key="label.conhecimento"/></a>

                <ul class="submenu">
                    <%
                        if (vNivel == 1 || vNivel == 2 || vNivel == 3 || vNivel == 5) {
                    %>
                    <li><a href="selecionarTipoRelatorioSinaisFracos.jsp"><bean:message key="label.memoriaFuturo"/></a></li>
                    <% }
                        if (vNivel == 1 || vNivel == 3 || vNivel == 5) {
                    %>
                    <li><a href="#"><bean:message key="label.serisMaps"/></a></li>
                    <% }
                    %>
                </ul>
            </li>
        </ul>
        <ul class="menu">
            <li class="list">
                <%
                    if (vNivel == 1 || vNivel == 3 || vNivel == 5) {
                %>
                <a class="category" href="#"><bean:message key="label.inteligencia"/></a>
                <ul class="submenu">
                    <li><a href="selecionarDominioKitGeral.jsp?formaction=cadastrarListaSinalFraco.jsp"><bean:message key="label.criarLista"/></a></li>
                    <li><a href="selecionarDominioKitGeral.jsp?formaction=cadastrarCriacaoSentido.jsp"><bean:message key="label.criarSerisMaps"/></a></li>
                    <%                    } else {
                    %>
                    <a class="category" href="#">&nbsp;</a>
                    <ul class="submenu">

                        <%                        }
                        %>
                    </ul>
            </li>
        </ul>
        <ul class="menu">
            <li class="list"><a class="category" href="efetuarLogin.do?task=logout"><bean:message key="label.desconectar"/></a></li>
        </ul>
        <DIV style="background:#800000; border: solid 0px #FF0000;"> &nbsp; </DIV>
    </div>
</div>
<table width="98%" align="center" cellpadding="0" cellspacing="0">
    <td style="font-family: verdana; font-size: 10px; color: #800000;">
        <%
            java.text.SimpleDateFormat formatter = new java.text.SimpleDateFormat("dd/MM/yyyy");
            out.println(formatter.format(new java.util.Date()));

            String vusuario_idMenu = String.valueOf(request.getSession().getAttribute("usuario_id"));
            Usuario usuario_menu = UsuarioDAO.consultarUsuario(Integer.parseInt(vusuario_idMenu));
        %>
    </td>
    <td style="font-family: verdana; font-size: 10px; color: #800000;" align="right">Usu�rio conectado: <%=usuario_menu.getNome()%></td>
</table>