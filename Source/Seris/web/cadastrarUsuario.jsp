<%--
    Document   : cadastrarUsuario
    Created on : jul/2010
    Author     : Ricardo C Bull - rcbull@gmail.com
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><!-- InstanceBegin template="/Templates/tempateSeris.dwt.jsp" codeOutsideHTMLIsLocked="false" -->
    <!-- InstanceBeginEditable name="taglibEimport" -->
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
    <%@page import="seris.database.Departamento" %>
    <%@page import="seris.database.Grupo" %>
    <%@page import="seris.database.Local" %>
    <%@page import="seris.database.Perfil" %>
    <%@page import="seris.database.Usuario" %>
    <%@page import="seris.database.Dominio" %>
    <!-- InstanceEndEditable -->

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso8859-1" />
        <%
                    response.setHeader("Cache-Control", "no-cache");
                    response.setHeader("Pragma", "no-cache");
                    response.setDateHeader("Expires", 0);
        %>
        <!-- InstanceBeginEditable name="doctitle" -->
        <title>Seris</title>
        <!-- InstanceEndEditable -->
        <link REL="SHORTCUT ICON" HREF="favicon2.ico">
        <style type="text/css" title="currentStyle">
            @import "media/css/demo_page.css";
            @import "media/css/demo_table.css";
        </style>
        <script type="text/javascript" language="javascript" src="media/js/jquery.js"></script>
        <script type="text/javascript" language="javascript" src="media/js/jquery.dataTables.js"></script>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function() {
                $('#datatable').dataTable( {
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": false,
                    "bAutoWidth": false } );
            } );
            jQuery.fn.dataTableExt.oSort['string-asc']  = function(a,b) {
                var x = a.toLowerCase();
                var y = b.toLowerCase();
                return x.localeCompare(y);
            };

            jQuery.fn.dataTableExt.oSort['string-desc'] = function(a,b) {
                var x = a.toLowerCase();
                var y = b.toLowerCase();
                return y.localeCompare(x);
            };
        </script>
        <link REL="SHORTCUT ICON" HREF="favicon2.ico">
        <link rel="stylesheet" type="text/css" href="seris.css">
        <!-- InstanceBeginEditable name="head" -->
        <%
                    java.util.ResourceBundle resource = java.util.ResourceBundle.getBundle("seris.SerisIdioma");
        %>
        <script type="text/javascript">
            function validaForm(){
                d = document.cadastrarUsuario;
                if (d.nome.value == ""){
                    alert("<bean:message key="message.preencherField" arg0="<%=resource.getString("label.nome")%>"/>");
                    d.nome.focus();
                    return false;
                }
                if (d.email.value == ""){
                    alert("<bean:message key="message.preencherField" arg0="<%=resource.getString("label.email")%>"/>");
                    d.email.focus();
                    return false;
                }
                if (d.login.value == ""){
                    alert("<bean:message key="message.preencherField" arg0="<%=resource.getString("label.login")%>"/>");
                    d.login.focus();
                    return false;
                }
                if(d.senha.value == "" && d.id.value == 0){
                    alert("<bean:message key="message.preencherField" arg0="<%=resource.getString("label.senha")%>"/>");
                    d.senha.focus();
                    return false;
                }
                if (d.senha.value != d.senha2.value){
                    alert("<bean:message key="message.senhaNaoConfere"/>");
                    d.senha.focus();
                    return false;
                }
                if (d.localId.value == ""){
                    alert("<bean:message key="message.selecionarFieldA" arg0="<%=resource.getString("label.cidade")%>"/>");
                    d.localId.focus();
                    return false;
                }
                if (d.grupo.value == ""){
                    alert("<bean:message key="message.selecionarField" arg0="<%=resource.getString("label.grupo")%>"/>");
                    d.grupo.focus();
                    return false;
                }
                if (d.departamento.value == ""){
                    alert("<bean:message key="message.selecionarField" arg0="<%=resource.getString("label.departamento")%>"/>");
                    d.departamento.focus();
                    return false;
                }
                if (d.perfil.value == ""){
                    alert("<bean:message key="message.selecionarField" arg0="<%=resource.getString("label.perfil")%>"/>");
                    d.perfil.focus();
                    return false;
                }
                var dominio = new Boolean(); dominio = false;
                if(typeof d.dominio.value == "undefined"){
                    for (i=0;i<d.dominio.length;i++){
                        if (d.dominio[i].checked) {
                            dominio = true;
                            break;
                        }
                    }
                } else{
                    if (d.dominio.checked == false){
                        dominio = false;
                    }else{
                        dominio = true;
                    }
                }
                if (dominio == false){
                    alert("<bean:message key="message.selecionarVariosField" arg0="<%=resource.getString("label.dominio")%>"/>");
                    return false;
                }
                var nivel = new Boolean(); nivel = false;
                if(typeof d.nivel.value == "undefined"){
                    for (i=0;i<d.nivel.length;i++){
                        if (d.nivel[i].checked) {
                            nivel = true;
                            break;
                        }
                    }
                } else{
                    if (d.nivel.checked == false){
                        nivel = false;
                    }else{
                        nivel = true;
                    }
                }
                if (nivel == false){
                    alert("<bean:message key="message.selecionarVariosField" arg0="<%=resource.getString("label.nivel")%>"/>");
                    return false;
                }
                return true;
            }
        </script>
        <!-- InstanceEndEditable -->
    </head>

    <body>
        <%@ include file="cabecalho.jsp" %>
        <%@ include file="menu.jsp" %>
        <div id="conteudo">
            <!-- InstanceBeginEditable name="conteudo" -->
            <jsp:useBean id="Usuario" scope="session" class="seris.database.Usuario"></jsp:useBean>
            <%
                        String vUsuario = (String) session.getAttribute("nome");
                        if (vUsuario == null) {
            %>
            <jsp:forward page="login.jsp"/>
            <%        } else {
                                        String vId = request.getParameter("id");
                                        String vTask = request.getParameter("task");
                                        if (vTask == null) {
                                            vTask = "";
                                        }
                                        int id = 0;
                                        if (vId != null && !vId.equals("0") && (!vTask.equals("delete"))) {
                                            id = Integer.parseInt(vId);
                                            Usuario = seris.database.DAO.UsuarioDAO.consultarUsuario(id);
                                        } else {
                                            Usuario.setNome("");
                                            Usuario.setEmail("");
                                            Usuario.setCelular("");
                                            Usuario.setLogin("");
                                            Usuario.setSenha("");
                                            Usuario.setLocalId(0);
                                            Usuario.setDepartamento(null);
                                            Usuario.setGrupo(null);
                                            Usuario.setPerfil(null);
                                        }
                                        //lista de parametros para o deletar
                                        java.util.HashMap params = new java.util.HashMap();
                                        params.put("id", Usuario.getId());
                                        params.put("nome", Usuario.getNome());
                                        pageContext.setAttribute("delete", params);
            %>
            <jsp:setProperty name="Usuario" property="id" value="<%= id%>"></jsp:setProperty>
            <jsp:setProperty name="Usuario" property="nome" value="<%= Usuario.getNome()%>"></jsp:setProperty>
            <jsp:setProperty name="Usuario" property="email" value="<%= Usuario.getEmail()%>"></jsp:setProperty>
            <jsp:setProperty name="Usuario" property="celular" value="<%= Usuario.getCelular()%>"></jsp:setProperty>
            <jsp:setProperty name="Usuario" property="login" value="<%= Usuario.getLogin()%>"></jsp:setProperty>
            <jsp:setProperty name="Usuario" property="senha" value="<%= Usuario.getSenha()%>"></jsp:setProperty>
            <jsp:setProperty name="Usuario" property="departamento" value="<%= Usuario.getDepartamento()%>"></jsp:setProperty>
            <jsp:setProperty name="Usuario" property="grupo" value="<%= Usuario.getGrupo()%>"></jsp:setProperty>
            <jsp:setProperty name="Usuario" property="perfil" value="<%= Usuario.getPerfil()%>"></jsp:setProperty>
            <jsp:setProperty name="Usuario" property="localId" value="<%= Usuario.getLocalId()%>"></jsp:setProperty>

            <div id="nomecont" align="center"><bean:message key="label.cadastrarUsuario"/></div>
            <form name="cadastrarUsuario" action="cadastrarUsuario.do" method="post" onSubmit="return validaForm()">
                <table width="600" border="0">
                    <tr>
                        <td width="100px"><bean:message key="label.nome"/>:</td>
                        <td width="450px"><input type="text" name="nome" value="<jsp:getProperty name="Usuario" property="nome"></jsp:getProperty>" size="80" maxlength="255"/></td>
                    </tr>
                    <tr>
                        <td width="100px"><bean:message key="label.email"/>:</td>
                        <td width="450px"><input type="text" name="email" value="<jsp:getProperty name="Usuario" property="email"></jsp:getProperty>" size="80" maxlength="255"/></td>
                    </tr>
                    <tr>
                        <td width="100px"><bean:message key="label.celular"/>:</td>
                        <td width="450px"><input type="text" name="celular" value="<jsp:getProperty name="Usuario" property="celular"></jsp:getProperty>" size="50" maxlength="255"/></td>
                    </tr>
                    <tr>
                        <td width="100px"><bean:message key="label.login"/>:</td>
                        <td width="450px"><input type="text" name="login" value="<jsp:getProperty name="Usuario" property="login"></jsp:getProperty>" size="20" maxlength="255"/></td>
                    </tr>
                    <!-- %
                if (id == 0) {
                    <input type="password" name="senha" value="<jsp:getProperty name="Usuario" property="senha"></jsp:getProperty>" size="20" maxlength="10"/>&nbsp;&nbsp;
                    % -->
                    <tr>
                        <td width="100px"><bean:message key="label.senha"/>:</td>
                        <td width="450px">
                            <input type="password" name="senha" value="" size="20" maxlength="10"/>&nbsp;&nbsp;
                            <bean:message key="label.redigitarSenha"/>:
                            <input type="password" name="senha2" value="" size="20" maxlength="10"/>
                            <input type="hidden" name="senhaUsuario" value="<jsp:getProperty name="Usuario" property="senha"></jsp:getProperty>" />
                        </td>
                    </tr>
                    <!-- %                } else {
                    % --><!-- input type="hidden" name="senha" value="<jsp:getProperty name="Usuario" property="senha"></jsp:getProperty>" / -->
                    <!--%}
                    % -->
                    <tr>
                        <td width="100px"><bean:message key="label.cidade"/>:</td>
                        <td width="450px">
                            <jsp:scriptlet>
                                                        session.setAttribute("Locais", seris.database.DAO.LocalDAO.consultarListaLocais());
                            </jsp:scriptlet>
                            <jsp:useBean id="Locais" scope="session" type="java.util.List"></jsp:useBean>
                            <select name="localId">
                                <option value="">--</option>
                                <%
                                                            for (int i = 0; i < Locais.size(); i++) {
                                                                Local vLocal = (Local) Locais.get(i);
                                                                if (vLocal.getId() == Usuario.getLocalId()) {
                                                                    out.print("<option value=\"" + vLocal.getId() + "\" selected>" + vLocal.getCidade() + " (" + vLocal.getUf() + ") - Pa?s: " + vLocal.getPais() + "</option>");
                                                                } else {
                                                                    out.print("<option value=\"" + vLocal.getId() + "\">" + vLocal.getCidade() + " (" + vLocal.getUf() + ") - Pa?s: " + vLocal.getPais() + "</option>");
                                                                }
                                                            }
                                %>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td width="100px"><bean:message key="label.grupo"/>:</td>
                        <td width="450px">
                            <jsp:scriptlet>
                                                        session.setAttribute("Grupos", seris.database.DAO.GrupoDAO.consultarListaGrupos());
                            </jsp:scriptlet>
                            <jsp:useBean id="Grupos" scope="session" type="java.util.List"></jsp:useBean>
                            <select name="grupo">
                                <option value="">--</option>>
                                <%
                                                            for (int i = 0; i < Grupos.size(); i++) {
                                                                Grupo vGrupo = (Grupo) Grupos.get(i);
                                                                if (Usuario.getGrupo() != null && Usuario.getGrupo().getId() == vGrupo.getId()) {
                                                                    out.print("<option value=\"" + vGrupo.getId() + "\" SELECTED>" + vGrupo.getNome() + "</option>");
                                                                } else {
                                                                    out.print("<option value=\"" + vGrupo.getId() + "\">" + vGrupo.getNome() + "</option>");
                                                                }
                                                            }
                                %>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td width="100px"><bean:message key="label.departamento"/>:</td>
                        <td width="450px">
                            <jsp:scriptlet>
                                                        session.setAttribute("Departamentos", seris.database.DAO.DepartamentoDAO.consultarListaDepartamentos());
                            </jsp:scriptlet>
                            <jsp:useBean id="Departamentos" scope="session" type="java.util.List"></jsp:useBean>
                            <select name="departamento">
                                <option value="">--</option>>
                                <%
                                                            for (int i = 0; i < Departamentos.size(); i++) {
                                                                Departamento vDepartamento = (Departamento) Departamentos.get(i);
                                                                if (Usuario.getDepartamento() != null && Usuario.getDepartamento().getId() == vDepartamento.getId()) {
                                                                    out.print("<option value=\"" + vDepartamento.getId() + "\" SELECTED>" + vDepartamento.getNome() + "</option>");
                                                                } else {
                                                                    out.print("<option value=\"" + vDepartamento.getId() + "\">" + vDepartamento.getNome() + "</option>");
                                                                }
                                                            }
                                %>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td width="100px"><bean:message key="label.dominios"/>:</td>
                        <td width="450px">
                            <jsp:scriptlet>
                                                        session.setAttribute("Dominios", seris.database.DAO.DominioDAO.consultarListaDominios());
                            </jsp:scriptlet>
                            <jsp:useBean id="Dominios" scope="session" type="java.util.List"></jsp:useBean>
                            <%
                                                        for (int i = 0; i < Dominios.size(); i++) {
                                                            Dominio vDominio = (Dominio) Dominios.get(i);
                                                            boolean vChecagemDominio = seris.database.DAO.DominioUsuarioDAO.consultarDominioUsuario(id, vDominio.getId());
                                                            String vChecked = "checked";
                                                            if (vChecagemDominio == false) {
                                                                vChecked = "";
                                                            }
                                                            out.print("<input type=\"checkbox\" name=\"dominio\" value=\"" + vDominio.getId() + "\" " + vChecked + " /> " + vDominio.getNome() + "<br>");
                                                        }
                            %>
                        </td>
                    </tr>
                    <tr>
                        <td width="100px"><bean:message key="label.perfil"/>:</td>
                        <td width="450px">
                            <jsp:scriptlet>
                                                        session.setAttribute("Perfis", seris.database.DAO.PerfilDAO.consultarListaPerfis());
                            </jsp:scriptlet>
                            <jsp:useBean id="Perfis" scope="session" type="java.util.List"></jsp:useBean>
                            <select name="perfil">
                                <option value="">--</option>>
                                <%
                                                            for (int i = 0; i < Perfis.size(); i++) {
                                                                Perfil vPerfil = (Perfil) Perfis.get(i);
                                                                if (Usuario.getPerfil() != null && Usuario.getPerfil().getId() == vPerfil.getId()) {
                                                                    out.print("<option value=\"" + vPerfil.getId() + "\" SELECTED>" + vPerfil.getNome() + "</option>");
                                                                } else {
                                                                    out.print("<option value=\"" + vPerfil.getId() + "\">" + vPerfil.getNome() + "</option>");
                                                                }
                                                            }
                                %>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td width="100px"><bean:message key="label.niveis"/>:</td>
                        <%
                                                    seris.database.Administrador vAdministrador = seris.database.DAO.AdministradorDAO.consultarUsuario(id);
                                                    java.util.List vListaPatrocinador = seris.database.DAO.PatrocinadorDAO.consultarListaUsuario(id);
                                                    seris.database.Suportedominio vSuportedominio = seris.database.DAO.SuportedominioDAO.consultarUsuario(id);
                                                    seris.database.Altadirecao vAltadirecao = seris.database.DAO.AltadirecaoDAO.consultarUsuario(id);
                                                    seris.database.Analista vAnalista = seris.database.DAO.AnalistaDAO.consultarUsuario(id);

                                                    out.print("<td width=\"450px\">");
                                                    String vChecked = "";
                                                    if (vAdministrador.getUsuarioId() != 0) {
                                                        vChecked = "checked";
                                                    }
                                                    out.print("<input  type = \"checkbox\" name = \"nivel\" value = \"0\" " + vChecked + "/> " + resource.getString("label.administrador") + " <br>");
                                                    vChecked = "";
                                                    if (vListaPatrocinador.size() > 0) {
                                                        vChecked = "checked";
                                                    }
                                                    out.print("<input type = \"checkbox\" name = \"nivel\" value = \"1\" " + vChecked + "/>  " + resource.getString("label.patrocinador") + " <br>");
                                                    vChecked = "";
                                                    if (vSuportedominio.getUsuarioId() != 0) {
                                                        vChecked = "checked";
                                                    }
                                                    out.print("<input type = \"checkbox\" name = \"nivel\" value = \"2\" " + vChecked + "/>  " + resource.getString("label.suporteDominio") + " <br>");
                                                    vChecked = "";
                                                    if (vAltadirecao.getUsuarioId() != 0) {
                                                        vChecked = "checked";
                                                    }
                                                    out.print("<input type = \"checkbox\" name = \"nivel\" value = \"3\" " + vChecked + "/>  " + resource.getString("label.altaDirecao") + " <br>");

                                                    vChecked = "";
                                                    if (vAnalista.getUsuarioId() != 0) {
                                                        vChecked = "checked";
                                                    }
                                                    out.print("<input type = \"checkbox\" name = \"nivel\" value = \"5\" " + vChecked + "/>  " + resource.getString("label.facilitador") + " <br>");


                                                    out.print("<input type = \"checkbox\" name = \"nivel\" value = \"4\" checked/>  " + resource.getString("label.captador") + " </td>");
                        %>
                    </tr>
                </table>
                <input type="hidden" name="id" value="<jsp:getProperty name="Usuario" property="id"></jsp:getProperty>" />
                <input type="hidden" name="task" value="save" /><br />
                <input type="submit" value="<bean:message key="label.salvar"/>" />
            </form>
            <%
                                        if (vId != null && !vId.equals("0")) {
            %>
            <form name="deletarUsuario" action="cadastrarUsuario.do" method="post">
                <input type="hidden" name="id" value="<jsp:getProperty name="Usuario" property="id"></jsp:getProperty>" />
                <input type="hidden" name="task" value="delete" /><br />
                <input type="submit" value="<bean:message key="label.deletar"/>" />&nbsp&nbsp</form>
                <%        }
                %>
            <p>&nbsp;</p><p><font color="blue">
                    <% if (request.getAttribute("message") != null) {
                                                    out.print(request.getAttribute("message"));
                                                }
                    %>
                </font></p>
            <br /><br />
            <html:link page="/cadastrarUsuario.do?task=novo"><bean:message key="label.novo"/></html:link>&nbsp<html:link page="/cadastrarUsuario.do?task=pesquisar"><bean:message key="label.pesquisar"/></html:link>
            <%
                                        if (vTask != null) {
                                            if (vTask.equals("pesquisar")) {
            %>
            <jsp:scriptlet>
                                                            session.setAttribute("Usuarios", seris.database.DAO.UsuarioDAO.consultarListaUsuarios());
            </jsp:scriptlet>
            <jsp:useBean id="Usuarios" scope="session" type="java.util.List"></jsp:useBean>
            <div id="demo">
                <table cellpadding="0" cellspacing="0" border="0" class="display" width="600px" id="datatable">
                    <thead>
                        <tr>
                            <th width="30px"><bean:message key="label.id"/></th>
                            <th width="540px"><bean:message key="label.nome"/></th>
                            <th width="200px"><bean:message key="label.login"/></th>
                            <th width="30px"><bean:message key="label.editar"/></th>
                        </tr>
                    </thead>
                    <tbody>
                        <%
                                                                        for (int i = 0; i < Usuarios.size(); i++) {
                                                                            Usuario usuario = (Usuario) Usuarios.get(i);
                                                                            out.print("<tr class='seris'><td>" + usuario.getId() + "</td><td>" + usuario.getNome() + "</td><td>" + usuario.getLogin() + "</td><td align='center'><a href='cadastrarUsuario.jsp?id=" + usuario.getId() + "'><img src='imagens/edit-icon.png' border='0'></a></td></tr>");
                                                                        }
                        %>
                    </tbody>
                </table>
            </div>
            <%
                                }
                            }
                        }
            %>
            <!-- InstanceEndEditable -->
        </div>
        <%@ include file="rodape.jsp" %>
    </body>
    <!-- InstanceEnd --></html>