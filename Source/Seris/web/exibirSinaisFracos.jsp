<%-- 
    Document   : exibirSinaisFracos
    Created on : jul/2010
    Author     : Ricardo C Bull - rcbull@gmail.com
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">

<html>
    <!-- InstanceBeginEditable name="taglibEimport" -->
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso8859-1">
        <title>Relat?rio de Sinais Fracos</title>
        <style type="text/css" title="currentStyle">
            @import "media/css/demo_page.css";
            @import "media/css/demo_table.css";
        </style>
        <script type="text/javascript" language="javascript" src="media/js/jquery.js"></script>
        <script type="text/javascript" language="javascript" src="media/js/jquery.dataTables.js"></script>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function() {
                $('#datatable').dataTable( {
                    "bPaginate": false,
                    "bLengthChange": false,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": false,
                    "bAutoWidth": false } );
            } );
            jQuery.fn.dataTableExt.oSort['string-asc']  = function(a,b) {
                var x = a.toLowerCase();
                var y = b.toLowerCase();
                return x.localeCompare(y);
            };

            jQuery.fn.dataTableExt.oSort['string-desc'] = function(a,b) {
                var x = a.toLowerCase();
                var y = b.toLowerCase();
                return y.localeCompare(x);
            };
        </script>
        <link REL="SHORTCUT ICON" HREF="favicon2.ico">
        <link rel="stylesheet" type="text/css" href="seris.css">
    </head>
    <body>
        <p><b><bean:message key="label.relatorioSinaisFracos"/></b><br><bean:message key="label.scanSlogan"/></p>
        <div id="conteudo">
            <%
                        java.util.ResourceBundle resource = java.util.ResourceBundle.getBundle("seris.SerisIdioma");

                        if (session.getAttribute("dominioSelecionado") != null && session.getAttribute("tipoConsulta") != null) {
                            int dominio_id = Integer.parseInt(String.valueOf(session.getAttribute("dominioSelecionado")));
                            int tipoConsulta = Integer.parseInt(String.valueOf(session.getAttribute("tipoConsulta")));

                            if (tipoConsulta == 1) {
                                //
                                // Consulta para cria??o de sentido
                                //
                                int kit_id = Integer.parseInt(String.valueOf(session.getAttribute("kitSelecionado")));
                                String[][] vRelatorioSinaisFracos = seris.database.DAO.SinalfracoDAO.consultarRelatorioSinaisFracos(dominio_id, kit_id, Integer.parseInt(String.valueOf(session.getAttribute("ator"))), Integer.parseInt(String.valueOf(session.getAttribute(("produto")))), Integer.parseInt(String.valueOf(session.getAttribute(("fonte")))), Integer.parseInt(String.valueOf(session.getAttribute(("mercado")))), Integer.parseInt(String.valueOf(session.getAttribute(("avaliacao")))), Integer.parseInt(String.valueOf(session.getAttribute(("usuario")))), String.valueOf(session.getAttribute(("dataInicio"))), String.valueOf(session.getAttribute(("dataFinal"))));
                                if (vRelatorioSinaisFracos.length > 0) {
                                    out.print("<p> <b>"+resource.getString("label.dominio")+":</b> " + vRelatorioSinaisFracos[0][0] + "<br>"
                                            + "<b>"+resource.getString("label.kit")+":</b> " + vRelatorioSinaisFracos[0][1] + "</p>"
                                            + "<p><b>"+resource.getString("label.totalKit")+":</b> " + vRelatorioSinaisFracos[0][12] + "<br>"
                                            + "<b>"+resource.getString("label.totalConsulta")+":</b> " + vRelatorioSinaisFracos[0][13] + " <b>(" + vRelatorioSinaisFracos[0][11] + " %)</b></p>");
            %>
            <div id="demo"><table cellpadding="0" cellspacing="0" border="0" class="display" width="800px" id="datatable">
                    <thead><tr>
                            <th align="center"><b><bean:message key="label.captador"/></b></th>
                            <th align="center"><b><bean:message key="label.mercado"/></b></th>
                            <th align="center"><b><bean:message key="label.produto"/></b></th>
                            <th align="center"><b><bean:message key="label.ator"/></b></th>
                            <th align="center"><b><bean:message key="label.fonte"/></b></th>
                            <th align="center"><b><bean:message key="label.relevancia"/></b></th>
                            <th align="center"><b><bean:message key="label.data"/></b></th>
                            <th align="center"><b><bean:message key="label.sinalFraco"/></b></th>
                            <th align="center"><b><bean:message key="label.informacoesAdicionais"/></b></th>
                            <th align="center"><b><bean:message key="label.seusComentarios"/></b></th>
                        </tr></thead>

                    <%
                                        for (int i = 0; i < vRelatorioSinaisFracos.length; i++) {
                                            out.print("<tr class='seris'>"
                                                    + "<td>" + vRelatorioSinaisFracos[i][8] + "</td>"
                                                    + "<td>" + vRelatorioSinaisFracos[i][2] + "</td>"
                                                    + "<td>" + vRelatorioSinaisFracos[i][3] + "</td>"
                                                    + "<td>" + vRelatorioSinaisFracos[i][4] + "</td>"
                                                    + "<td>" + vRelatorioSinaisFracos[i][5] + "</td>"
                                                    + "<td>" + vRelatorioSinaisFracos[i][6] + "</td>"
                                                    + "<td>" + vRelatorioSinaisFracos[i][14] + "</td>"
                                                    + "<td>" + vRelatorioSinaisFracos[i][7] + "</td>"
                                                    + "<td>" + vRelatorioSinaisFracos[i][9] + "</td>"
                                                    + "<td>" + vRelatorioSinaisFracos[i][10] + "</td>"
                                                    + "</tr>");
                                        }
                    %>
                    </tbody></table></div>
                    <%
                                    } else {
                                        out.print("<p><b>n?o foram encontrados registros com os crit?rios selecionados</b></p>");
                                    }
                                } else if (tipoConsulta == 2) {
                                    //
                                    // Consulta para distribui??o de sinais fracos
                                    //
                                    int kit_id = Integer.parseInt(String.valueOf(session.getAttribute("kitSelecionado")));
                                    String[][] vRelatorioSinaisFracos = seris.database.DAO.SinalfracoDAO.consultarRelatorioSinaisFracos(dominio_id, kit_id, Integer.parseInt(String.valueOf(session.getAttribute("usuario"))), Integer.parseInt(String.valueOf(session.getAttribute(("perfil")))), Integer.parseInt(String.valueOf(session.getAttribute(("grupo")))), Integer.parseInt(String.valueOf(session.getAttribute(("departamento")))), String.valueOf(session.getAttribute(("dataInicio"))), String.valueOf(session.getAttribute(("dataFinal"))));
                                    if (vRelatorioSinaisFracos.length > 0) {
                                        out.print("<p> <b>"+resource.getString("label.dominio")+":</b> " + vRelatorioSinaisFracos[0][0] + "</p>"
                                                + "<p><b>"+resource.getString("label.totalDominio")+":</b> " + vRelatorioSinaisFracos[0][11] + "<br>"
                                                + "<b>"+resource.getString("label.totalConsulta")+":</b> " + vRelatorioSinaisFracos[0][12] + " <b>(" + vRelatorioSinaisFracos[0][10] + " %)</b></p>");

                                        out.print("<div id=\"demo\"><table cellpadding=\"0\" cellspacing=\"0\" border=\"0\" class=\"display\" width=\"800px\" id=\"datatable\">"
                                                + "<thead><tr>"
                                                + "<th align=\"center\"><b>"+resource.getString("label.kit")+"</b></td>"
                                                + "<th align=\"center\"><b>"+resource.getString("label.captador")+"</b></td>"
                                                + "<th align=\"center\"><b>"+resource.getString("label.emailCaptador")+" </b></td>"
                                                + "<th align=\"center\"><b>");
                    %>
                    <bean:message key="label.perfil"/>
                    <%
                                        out.print("</b></th>"
                                                + "<th align=\"center\"><b>");
                    %>
                    <bean:message key="label.grupo"/>
                    <%
                                            out.print("</b></th>"
                                                    + "<th align=\"center\"><b>"+resource.getString("label.departamento")+"</b></td>"
                                                    + "<th align=\"center\"><b>"+resource.getString("label.data")+"</b></td>"
                                                    + "<th align=\"center\"><b>"+resource.getString("label.sinalFraco")+"</b></td>"
                                                    + "<th align=\"center\"><b>"+resource.getString("label.informacoesAdicionais")+"</b></td>"
                                                    + "<th align=\"center\"><b>"+resource.getString("label.seusComentarios")+"</b></td>"
                                                    + "</tr></thead><tbody>");
                                            for (int i = 0; i < vRelatorioSinaisFracos.length; i++) {
                                                out.print("<tr class='seris'>"
                                                        + "<td>" + vRelatorioSinaisFracos[i][1] + "</td>"
                                                        + "<td>" + vRelatorioSinaisFracos[i][2] + "</td>"
                                                        + "<td>" + vRelatorioSinaisFracos[i][3] + "</td>"
                                                        + "<td>" + vRelatorioSinaisFracos[i][4] + "</td>"
                                                        + "<td>" + vRelatorioSinaisFracos[i][5] + "</td>"
                                                        + "<td>" + vRelatorioSinaisFracos[i][6] + "</td>"
                                                        + "<td>" + vRelatorioSinaisFracos[i][13] + "</td>"
                                                        + "<td>" + vRelatorioSinaisFracos[i][7] + "</td>"
                                                        + "<td>" + vRelatorioSinaisFracos[i][8] + "</td>"
                                                        + "<td>" + vRelatorioSinaisFracos[i][9] + "</td>"
                                                        + "</tr>");
                                            }
                                            out.print("</tbody></table></div>");

                                        } else {
                                            out.print("<p><b>n?o foram encontrados registros com os crit?rios selecionados</b></p>");
                                        }
                                    }
                                }
                    %>
        </div>
    </body>
</html>
