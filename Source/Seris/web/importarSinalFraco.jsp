<%--
    Document   : cadastrarSinaisFracos
    Created on : jul/2010
    Author     : Ricardo C Bull - rcbull@gmail.com
--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><!-- InstanceBegin template="/Templates/tempateSeris.dwt.jsp" codeOutsideHTMLIsLocked="false" -->
    <!-- InstanceBeginEditable name="taglibEimport" -->
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
    <%@page import="seris.database.Ator" %>
    <%@page import="seris.database.Local" %>
    <%@page import="seris.database.Sinalfraco" %>
    <%@page import="seris.database.Fonte" %>
    <%@page import="seris.database.Produto" %>
    <%@page import="seris.database.Mercado" %>
    <%@page import="seris.database.Avaliacao" %>
    <%@page import="seris.database.Kit" %>
    <%@page import="seris.database.Kiq" %>
    <%@page import="seris.database.Dominio" %>
    <!-- InstanceEndEditable -->

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso8859-1" />
        <%
                    response.setHeader("Cache-Control", "no-cache");
                    response.setHeader("Pragma", "no-cache");
                    response.setDateHeader("Expires", 0);
        %>
        <!-- InstanceBeginEditable name="doctitle" -->
        <title>Seris</title>
        <!-- InstanceEndEditable -->
        <link REL="SHORTCUT ICON" HREF="favicon2.ico">
        <style type="text/css" title="currentStyle">
            @import "media/css/demo_page.css";
            @import "media/css/demo_table.css";
        </style>
        <script type="text/javascript" language="javascript" src="javascript/functions.js"></script>
        <script type="text/javascript" language="javascript" src="media/js/jquery.js"></script>
        <script type="text/javascript" language="javascript" src="media/js/jquery.dataTables.js"></script>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function() {
                $('#datatable').dataTable( {
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": false,
                    "bAutoWidth": false } );
            } );
            jQuery.fn.dataTableExt.oSort['string-asc']  = function(a,b) {
                var x = a.toLowerCase();
                var y = b.toLowerCase();
                return x.localeCompare(y);
            };

            jQuery.fn.dataTableExt.oSort['string-desc'] = function(a,b) {
                var x = a.toLowerCase();
                var y = b.toLowerCase();
                return y.localeCompare(x);
            };
        </script>
        <link REL="SHORTCUT ICON" HREF="favicon2.ico">
        <link rel="stylesheet" type="text/css" href="seris.css">
        <!-- InstanceBeginEditable name="head" -->
        <%
                    java.util.ResourceBundle resource = java.util.ResourceBundle.getBundle("seris.SerisIdioma");
        %>
        <script type="text/javascript">
            function validaForm(){
                d = document.importarSinaisFracos;
                if(d.kiqSelecionado.value ==0){
                    alert("<bean:message key="message.selecionarField" arg0="<%=resource.getString("label.kiq")%>"/>");
                    d.kiqSelecionado.focus();
                    return false;
                }
                if(d.avaliacao.value == 0){
                    alert("<bean:message key="message.selecionarFieldA" arg0="<%=resource.getString("label.relevancia")%>"/>");
                    d.avaliacao.focus();
                    return false;
                }
                if (d.local.value == ""){
                    alert("<bean:message key="message.selecionarFieldA" arg0="<%=resource.getString("label.cidade")%>"/>");
                    d.local.focus();
                    return false;
                }
                if(d.fonte.value == 0){
                    alert("<bean:message key="message.selecionarFieldA" arg0="<%=resource.getString("label.fonte")%>"/>");
                    d.fonte.focus();
                    return false;
                }
                if(d.ator.value == 0){
                    alert("<bean:message key="message.selecionarField" arg0="<%=resource.getString("label.ator")%>"/>");
                    d.ator.focus();
                    return false;
                }
                if (d.arquivoImportar.value == ""){
                    alert("<bean:message key="message.preencherField" arg0="<%=resource.getString("label.arquivo")%>"/>");
                    d.arquivoImportar.focus();
                    return false;
                }
                return true;
            }

            function format(value,data)
            {
                value = value.replace(/\D/g,"");
                var result="";
                if(data.length < value.length)
                    return value;
                for(i=0,j=0;(i<data.length)&&(j<value.length);i++)
                {
                    var ch = data.charAt(i);
                    if(ch == '#')
                    {
                        result += value.charAt(j++);
                        continue;
                    }
                    result += ch;
                }
                return result;
            }
            function limitaTextArea(campo){
                d = document.cadastrarSinaisFracos;
                var tamanho = d[campo].value.length;
                var tex=d[campo].value;
                if (tamanho>=200) {
                    d[campo].value=tex.substring(0,200);
                }
                return true;
            }
            
        </script>
        <!-- InstanceEndEditable -->
    </head>
    <body>
        <%@ include file="cabecalho.jsp" %>
        <%@ include file="menu.jsp" %>
        <div id="conteudo">
            <!-- InstanceBeginEditable name="conteudo" -->
            <jsp:useBean id="Sinalfraco" scope="session" class="seris.database.Sinalfraco"></jsp:useBean>
            <%
                        String vUsuario = (String) session.getAttribute("nome");
                        if (vUsuario == null) {
            %>
            <jsp:forward page="login.jsp"/>
            <%        } else {
                                        String vId = request.getParameter("id");
                                        String vTask = request.getParameter("task");

                                        String vKitId = request.getParameter("kitSelecionado");

                                        String dominioName = "";
                                        String kitName = "";

                                        if (vKitId != null && !vKitId.equals("0")) {
                                            Kit kit = seris.database.DAO.KitDAO.consultarKit(Integer.parseInt(vKitId));
                                            Dominio dominio = seris.database.DAO.DominioDAO.consultarDominio(kit.getDominioId());

                                            dominioName = dominio.getNome();
                                            kitName = kit.getDescricao();
                                        }

                                        if (vTask == null) {
                                            vTask = "";
                                        }
                                        int id = 0;
                                        if (vId != null) {
                                            id = Integer.parseInt(vId);
                                            Sinalfraco = seris.database.DAO.SinalfracoDAO.consultarSinalfraco(id);
                                        } else {
                                            Sinalfraco.setDescricao("");
                                        }

            %>
            <jsp:setProperty name="Sinalfraco" property="id" value="<%= id%>"></jsp:setProperty>
            <jsp:setProperty name="Sinalfraco" property="descricao" value="<%= Sinalfraco.getDescricao()%>"></jsp:setProperty>
            <div id="nomecont" align="center"><bean:message key="label.importarSinalFraco"/></div>

            <div>
                <b><bean:message key="label.dominio"/>:</b> <%=dominioName%> <BR>
                <b><bean:message key="label.kit"/>:</b> <%=kitName%>
            </div>
            <br>

            <form name="importarSinaisFracos" action="importarSinaisFracos.do" method="post" onSubmit="return validaForm()">

                <input type="submit" value="<bean:message key="label.importar"/>" />

                <br>
                <font color="blue">
                    <%
                                                if (request.getAttribute("message") != null) {
                                                    out.print("<br>" + request.getAttribute("message") + "<BR><BR>");
                                                }
                    %>
                </font>

                <table width="600" border="0">
                    <tr><td colspan="2" height="50px"><i><bean:message key="message.avisoObrigatorio"/></i></td></tr>
                    <tr>
                        <td width="100px">*<bean:message key="label.selecioneKiq"/>:</td>
                        <td width="450px">
                            <jsp:scriptlet>
                                                        session.setAttribute("Kiqs", seris.database.DAO.KiqDAO.consultarListaKiqsPorKit(vKitId));
                            </jsp:scriptlet>
                            <jsp:useBean id="Kiqs" scope="session" type="java.util.List"></jsp:useBean>
                            <select name="kiqSelecionado">
                                <%
                                                            out.print("<option value=\"0\" />--</option>");
                                                            for (int i = 0; i < Kiqs.size(); i++) {
                                                                Kiq vKiq = (Kiq) Kiqs.get(i);
                                                                out.print("<option value=\"" + vKiq.getId() + "\">" + vKiq.getDescricao() + "</option>");
                                                            }
                                %>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td width="100px">*<bean:message key="label.relevancia"/>:</td>
                        <td width="450px">
                            <jsp:scriptlet>
                                                        session.setAttribute("Avaliacoes", seris.database.DAO.AvaliacaoDAO.consultarListaAvaliacoes());
                            </jsp:scriptlet>
                            <jsp:useBean id="Avaliacoes" scope="session" type="java.util.List"></jsp:useBean>
                            <br><br>
                            <select name="avaliacao">
                                <%
                                                            out.print("<option value=\"0\" />--</option>");
                                                            for (int i = 0; i < Avaliacoes.size(); i++) {
                                                                Avaliacao vAvaliacao = (Avaliacao) Avaliacoes.get(i);
                                                                out.print("<option value=\"" + vAvaliacao.getId() + "\" />" + vAvaliacao.getRelevancia() + "</option>");
                                                            }
                                %>
                            </select>
                            <br><br>
                        </td>
                    </tr>
                    <tr>
                        <td width="100px">*<bean:message key="label.cidade"/>:</td>
                        <td width="450px">
                            <jsp:scriptlet>
                                                        session.setAttribute("Locais", seris.database.DAO.LocalDAO.consultarListaLocais());
                            </jsp:scriptlet>
                            <jsp:useBean id="Locais" scope="session" type="java.util.List"></jsp:useBean>
                            <select name="local">
                                <option value="">--</option>
                                <%
                                                            for (int i = 0; i < Locais.size(); i++) {
                                                                Local vLocal = (Local) Locais.get(i);
                                                                out.print("<option value=\"" + vLocal.getId() + "\">" + vLocal.getCidade() + " (" + vLocal.getUf() + ") - Pa?s: " + vLocal.getPais() + "</option>");
                                                            }
                                %>
                            </select>
                            <br>Nome do local de captura:<br>
                            <input type="text" name="nomeLocalCaptura" value="" size="95" maxlength="120"/>
                            <br>Descri??o do local de captura:<br>
                            <input type="text" name="descricaoLocalCaptura" value="" size="95" maxlength="120"/>
                            <!--<textarea name="descricaoLocalCaptura" rows="2" cols="80"></textarea>-->
                            <br><br>
                        </td>
                    </tr>
                    <tr>
                        <td width="100px">*<bean:message key="label.fonte"/>:</td>
                        <td width="450px">
                            <jsp:scriptlet>
                                                        session.setAttribute("Fontes", seris.database.DAO.FonteDAO.consultarListaFontesPorKit(vKitId));
                            </jsp:scriptlet>
                            <jsp:useBean id="Fontes" scope="session" type="java.util.List"></jsp:useBean>
                            <select name="fonte">
                                <%
                                                            out.print("<option value=\"0\" />--</option>");
                                                            for (int i = 0; i < Fontes.size(); i++) {
                                                                Fonte vFonte = (Fonte) Fontes.get(i);
                                                                out.print("<option value=\"" + vFonte.getId() + "\" />" + vFonte.getNome() + "</option>");
                                                            }
                                %>
                            </select>
                            <br><br>
                        </td>
                    </tr>
                    <tr>
                        <td width="100px"><bean:message key="label.produto"/>:</td>
                        <td width="450px">
                            <jsp:scriptlet>
                                                        session.setAttribute("Produtos", seris.database.DAO.ProdutoDAO.consultarListaProdutosPorKit(vKitId));
                            </jsp:scriptlet>
                            <jsp:useBean id="Produtos" scope="session" type="java.util.List"></jsp:useBean>
                            <select name="produto">
                                <%
                                                            out.print("<option value=\"0\" />--</option>");
                                                            for (int i = 0; i < Produtos.size(); i++) {
                                                                Produto vProduto = (Produto) Produtos.get(i);
                                                                out.print("<option value=\"" + vProduto.getId() + "\" />" + vProduto.getDescricao() + "</option>");
                                                            }
                                %>
                            </select>
                            <br><br>
                        </td>
                    </tr>
                    <tr>
                        <td width="100px"><bean:message key="label.mercado"/>:</td>
                        <td width="450px">
                            <jsp:scriptlet>
                                                        session.setAttribute("Mercados", seris.database.DAO.MercadoDAO.consultarListaMercadosPorKit(vKitId));
                            </jsp:scriptlet>
                            <jsp:useBean id="Mercados" scope="session" type="java.util.List"></jsp:useBean>
                            <select name="mercado">
                                <%
                                                            out.print("<option value=\"0\" />--</option>");
                                                            for (int i = 0; i < Mercados.size(); i++) {
                                                                Mercado vMercado = (Mercado) Mercados.get(i);
                                                                out.print("<option value=\"" + vMercado.getId() + "\" />" + vMercado.getNome() + "</option>");
                                                            }
                                %>
                            </select>
                            <br><br>
                        </td>
                    </tr>
                    <tr>
                        <td width="100px">*<bean:message key="label.ator"/>:</td>
                        <td width="450px">
                            <jsp:scriptlet>
                                                        session.setAttribute("Atores", seris.database.DAO.AtorDAO.consultarListaAtoresPorKit(vKitId));
                            </jsp:scriptlet>
                            <jsp:useBean id="Atores" scope="session" type="java.util.List"></jsp:useBean>
                            <select name="ator">
                                <%
                                                            out.print("<option value=\"0\" />--</option>");
                                                            for (int i = 0; i < Atores.size(); i++) {
                                                                Ator vAtor = (Ator) Atores.get(i);
                                                                out.print("<option value=\"" + vAtor.getId() + "\" />" + vAtor.getNome() + "</option>");
                                                            }
                                %>
                            </select>
                            <br><br>
                        </td>
                    </tr>
                    <tr>
                        <td width="100px">*<bean:message key="label.arquivo"/>:</td>
                        <td width="450px">
                            <%--<input type="file" name="arquivoImportar" style="width: 500px;">--%>
                            <input type="text" name="arquivoImportar" id="arquivoImportar" style="width: 500px;">
                            <input type="button" name="btnImportar" value="<bean:message key="label.arquivo"/>" onclick="abrirArquivo('arquivoImportar');">
                        </td>
                    </tr>
                </table>
                <input type="hidden" name="kitSelecionado" value="<%=vKitId%>" />
                <input type="hidden" name="task" value="save" /><br />
            </form>
            <br /><br />
            <!--<html:link page="/cadastrarSinalfraco.do?task=novo">novo</html:link>&nbsp<html:link page="/cadastrarSinalfraco.do?task=pesquisar">Pesquisar</html:link>-->
            <%
                        }
            %>
            <!-- InstanceEndEditable -->
        </div>
        <%@ include file="rodape.jsp" %>
    </body>
    <!-- InstanceEnd --></html>
