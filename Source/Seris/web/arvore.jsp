<%--
Document   : selecionarTipoRelatorioSinaisFracos
Created on : jul/2010
Author     : Ricardo C Bull - rcbull@gmail.com
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><!-- InstanceBegin template="/Templates/tempateSeris.dwt.jsp" codeOutsideHTMLIsLocked="false" -->
    <!-- InstanceBeginEditable name="taglibEimport" -->
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
    <!-- InstanceEndEditable -->

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso8859-1" />
        <%
                    response.setHeader("Cache-Control", "no-cache");
                    response.setHeader("Pragma", "no-cache");
                    response.setDateHeader("Expires", 0);
        %>
        <!-- InstanceBeginEditable name="doctitle" -->
        <title>Seris</title>
        <!-- InstanceEndEditable -->
        <link REL="SHORTCUT ICON" HREF="favicon2.ico">
        <style type="text/css" title="currentStyle">
            @import "media/css/demo_page.css";
            @import "media/css/demo_table.css";
        </style>
        <script type="text/javascript" language="javascript" src="media/js/jquery.js"></script>
        <script type="text/javascript" language="javascript" src="media/js/jquery.dataTables.js"></script>
        <link REL="SHORTCUT ICON" HREF="favicon2.ico">
        <link rel="stylesheet" type="text/css" href="seris.css">
        <!-- InstanceBeginEditable name="head" -->
        <!-- InstanceEndEditable -->
    </head>

    <body>
        <%@ include file="cabecalho.jsp" %>
        <%@ include file="menu.jsp" %>

            <DIV name="divApplet" id="divApplet" style="border: outset 2px #ad3831;width: 96%; height: 550px;">
                <applet id="Treebolic" archive="arvore/Treebolic.jar" code="treebolic.applet.Treebolic.class" width="100%"height="100%">
                    <param name="xml" value="arvore/scan01.xml">
                </applet>
            </DIV>

    </body>

</html>

<script>
    function startDraw() {

        //var tela = document.getElementById('mybody');

        var winW = 630, winH = 460;

        if (parseInt(navigator.appVersion)>3) {
            if (navigator.appName=="Netscape") {
                winW = window.innerWidth;
                winH = window.innerHeight;
            }
            if (navigator.appName.indexOf("Microsoft")!=-1) {
                winW = document.body.offsetWidth;
                winH = document.body.offsetHeight-80;
            }
        }

        var objDivApplet = document.getElementById("divApplet");
       // objDivApplet.style.width = (winW-250) + 'px';
        objDivApplet.style.height = (winH-180) + 'px';

        //var jsdraw = document.draw;
        //jsdraw.style.width = (winW-250) + 'px';
        //jsdraw.style.height = (winH-140) + 'px';
    }
    window.setTimeout('startDraw();', 1000);

</script>