<%--
    Document   : cadastrarKiq
    Created on : jul/2010
    Author     : Ricardo C Bull - rcbull@gmail.com
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><!-- InstanceBegin template="/Templates/tempateSeris.dwt.jsp" codeOutsideHTMLIsLocked="false" -->
    <!-- InstanceBeginEditable name="taglibEimport" -->
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
    <%@page import="seris.database.Kit" %>
    <%@page import="seris.database.Kiq" %>
    <!-- InstanceEndEditable -->

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso8859-1" />
        <%
                    response.setHeader("Cache-Control", "no-cache");
                    response.setHeader("Pragma", "no-cache");
                    response.setDateHeader("Expires", 0);
        %>
        <!-- InstanceBeginEditable name="doctitle" -->
        <title>Seris</title>
        <!-- InstanceEndEditable -->
        <link REL="SHORTCUT ICON" HREF="favicon2.ico">
        <style type="text/css" title="currentStyle">
            @import "media/css/demo_page.css";
            @import "media/css/demo_table.css";
        </style>
        <script type="text/javascript" language="javascript" src="media/js/jquery.js"></script>
        <script type="text/javascript" language="javascript" src="media/js/jquery.dataTables.js"></script>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function() {
                $('#datatable').dataTable( {
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": false,
                    "bAutoWidth": false } );
            } );
            jQuery.fn.dataTableExt.oSort['string-asc']  = function(a,b) {
                var x = a.toLowerCase();
                var y = b.toLowerCase();
                return x.localeCompare(y);
            };

            jQuery.fn.dataTableExt.oSort['string-desc'] = function(a,b) {
                var x = a.toLowerCase();
                var y = b.toLowerCase();
                return y.localeCompare(x);
            };
        </script>
        <link REL="SHORTCUT ICON" HREF="favicon2.ico">
        <link rel="stylesheet" type="text/css" href="seris.css">
        <!-- InstanceBeginEditable name="head" -->
        <%
                    java.util.ResourceBundle resource = java.util.ResourceBundle.getBundle("seris.SerisIdioma");
        %>
        <script type="text/javascript">
            function validaForm(){
                d = document.cadastrarKiq;
                if (d.descricao.value == ""){
                    alert("<bean:message key="message.preencherField" arg0="<%=resource.getString("label.kiq")%>"/>");
                    d.descricao.focus();
                    return false;
                }
                if(d.kitSelecionado.value ==0){
                    alert("<bean:message key="message.selecionarDominioRelatorio"/>");
                    d.kitSelecionado.focus();
                    return false;
                }
                return true;
            }
        </script>
        <!-- InstanceEndEditable -->
    </head>

    <body>
        <%@ include file="cabecalho.jsp" %>
        <%@ include file="menu.jsp" %>
        <div id="conteudo">
            <!-- InstanceBeginEditable name="conteudo" -->
            <jsp:useBean id="Kiq" scope="session" class="seris.database.Kiq"></jsp:useBean>
            <%
                        String vId = request.getParameter("id");
                        String vTask = request.getParameter("task");
                        if (vTask == null) {
                            vTask = "";
                        }
                        int id = 0;
                        if (vId != null && !vId.equals("0") && (!vTask.equals("delete"))) {
                            id = Integer.parseInt(vId);
                            Kiq = seris.database.DAO.KiqDAO.consultarKiq(id);
                        } else {
                            Kiq.setDescricao("");
                        }
                        //lista de parametros para o deletar
                        java.util.HashMap params = new java.util.HashMap();
                        params.put("id", Kiq.getId());
                        params.put("descricao", Kiq.getDescricao());
                        pageContext.setAttribute("delete", params);
            %>
            <jsp:setProperty name="Kiq" property="id" value="<%= id%>"></jsp:setProperty>
            <jsp:setProperty name="Kiq" property="descricao" value="<%= Kiq.getDescricao()%>"></jsp:setProperty>
            <jsp:setProperty name="Kiq" property="kit" value="<%= Kiq.getKit()%>"></jsp:setProperty>
            <%
                        if (vId != null && !vId.equals("0")) {
                            out.print("<div id=\"nomecont\" align=\"center\">" + resource.getString("label.editarKiq") + "</div>");
                            out.print("Id: " + vId + "<br>");
                        } else {
                            out.print("<div id=\"nomecont\" align=\"center\">" + resource.getString("label.cadastrarKiq") + "</div>");
                        }
            %>
            <form name="cadastrarKiq" action="cadastrarKiq.do" method="post" onsubmit="return validaForm()">
                <p id="textoLabel">
                    <bean:message key="label.selecioneKit"/>:
                    <jsp:scriptlet>
                                session.setAttribute("Kits", seris.database.DAO.KitDAO.consultarListaKits());
                    </jsp:scriptlet>
                    <jsp:useBean id="Kits" scope="session" type="java.util.List"></jsp:useBean>
                    <select name="kitSelecionado">
                        <%
                                    out.print("<option value=\"0\" />--</option>");
                                    for (int i = 0; i < Kits.size(); i++) {
                                        Kit vKit = (Kit) Kits.get(i);
                                        if (Kiq.getKit() != null && Kiq.getKit().getId() == vKit.getId()) {
                                            out.print("<option value=\"" + vKit.getId() + "\" SELECTED>" + vKit.getDescricao() + "</option>");
                                        } else {
                                            out.print("<option value=\"" + vKit.getId() + "\">" + vKit.getDescricao() + "</option>");
                                        }
                                    }
                        %>
                    </select>
                </p>
                <p id="textoLabel">
                    <bean:message key="label.kiq"/>:
                    <input type="text" name="descricao"  size="60" maxlength="100" value="<jsp:getProperty name="Kiq" property="descricao"></jsp:getProperty>"/>
                </p>
                <input type="hidden" name="id" value="<jsp:getProperty name="Kiq" property="id"></jsp:getProperty>" />
                <input type="hidden" name="task" value="save" /><br />
                <input type="submit" value="<bean:message key="label.salvar"/>" />
            </form>
            <html:errors/>
            <logic:present name="CadastrarKiqActionForm" property="status">
                <br>
                <bean:write name="CadastrarKiqActionForm" property="status"/>
            </logic:present>
            <br /><br />
            <html:link page="/cadastrarKiq.do?task=novo"><bean:message key="label.novo"/></html:link>&nbsp<html:link page="/cadastrarKiq.do?task=pesquisar"><bean:message key="label.pesquisar"/></html:link>
            <%
                        if (vTask != null) {
                            if (vTask.equals("pesquisar")) {
            %>
            <jsp:scriptlet>
                            session.setAttribute("Kiqs", seris.database.DAO.KiqDAO.consultarListaKiqs());
            </jsp:scriptlet>
            <jsp:useBean id="Kiqs" scope="session" type="java.util.List"></jsp:useBean>
            <div id="demo">
                <table cellpadding="0" cellspacing="0" border="0" class="display" width="600px" id="datatable">
                    <thead>
                        <tr>
                            <th width="30px"><bean:message key="label.id"/></th>
                            <th width="540px"><bean:message key="label.nome"/></th>
                            <th width="30px"><bean:message key="label.editar"/></th>
                        </tr>
                    </thead>
                    <tbody>
                        <%
                                        for (int i = 0; i < Kiqs.size(); i++) {
                                            Kiq vKiq = (Kiq) Kiqs.get(i);
                                            out.print("<tr class='seris'><td>" + vKiq.getId() + "</td><td>" + vKiq.getDescricao() + "</td><td align='center'><a href='cadastrarKiq.jsp?id=" + vKiq.getId() + "'><img src='imagens/edit-icon.png' border='0'></a></td></tr>");
                                        }
                        %>
                    </tbody>
                </table>
            </div>
            <%
                            }
                        }
            %>
            <!-- InstanceEndEditable -->
        </div>
        <%@ include file="rodape.jsp" %>
    </body>
    <!-- InstanceEnd --></html>