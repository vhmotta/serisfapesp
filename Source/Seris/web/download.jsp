<%@ page import="java.io.*" %>
<%@ page import="java.util.ResourceBundle" %>
<html>
    <head>
        <title>Donwload File</title>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
    </head>

    <body bgcolor="#FFFFFF">
        <%

                    ResourceBundle resourceConfig = ResourceBundle.getBundle("seris.seris");

                    String file = resourceConfig.getString("SystemPath") + request.getParameter("RemoteFile");
                    String fileName = file.substring(file.lastIndexOf("/") + 1);

                    response.setContentType("APPLICATION/OCTET-STREAM");
                    response.setHeader("Content-Disposition", "attachment; filename=" + fileName);
                    java.io.InputStream is = null;
                    OutputStream out2 = null;
                    try {
                        out2 = response.getOutputStream();
                        is = new FileInputStream(file);
                        int i;
                        while ((i = is.read()) != -1) {
                            out2.write(i);
                        }
                    } finally {
                        if (out2 != null) {
                            out2.flush();
                            out2.close();
                        }
                        if (is != null) {
                            is.close();
                        }
                    }

        %>
    </body>
</html>
