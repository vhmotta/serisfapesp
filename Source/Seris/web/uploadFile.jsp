
<%@ page import="seris.functions.BasicUploadFile" %>
<%@ page import="javazoom.upload.MultipartFormDataRequest" %>

<%

            String objName = request.getParameter("objName");

            String status = "";
            String fileName = "";

            if (MultipartFormDataRequest.isMultipartFormData(request)) {

                java.util.ResourceBundle resource = java.util.ResourceBundle.getBundle("seris.seris");

                BasicUploadFile basicUploadFile = new BasicUploadFile(request, resource.getString("SystemPath") + "/tmp");
                basicUploadFile.setDiretory(resource.getString("SystemPath") + "/tmp");
                status = basicUploadFile.uploadFile("arquivo");
                fileName = basicUploadFile.getFileName();

                MultipartFormDataRequest myrequest = basicUploadFile.getMyrequest();
                objName = myrequest.getParameter("objName");
            }

            if (status == null) {
%>

<script>
    if (window.opener.document.getElementById('<%=objName%>')) {
        window.opener.document.getElementById('<%=objName%>').value='<%=fileName%>';
    } else if (window.parent.document.getElementById('<%=objName%>')) {
        window.parent.document.getElementById('<%=objName%>').value='<%=fileName%>';
    }
    window.close();
</script>

<%
            } else {
                status = "<font size=1 face=verdana color=#ff0000>" + status + "</font>";
            }
%>

<html>
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel='stylesheet' href='style/1024x768/default/form.css' type='text/css'>
        <title>Upload File</title>
    </head>
    <body bgcolor="#EEEEEE" >

        <%=status%>

        <FORM name="frmUpload" method=post action="uploadFile.jsp" enctype="multipart/form-data">
            <CENTER>
                <TABLE HEIGHT="100%">
                    <TR>
                        <TD class="fieldCaption" colspan=2>
                            <bean:message key="label.selecioneArquivo"/>:
                        </TD>
                    </TR>
                    <TR>
                        <TD colspan=2 ALIGN="center">
                            <input class="textField" type='file' id='arquivo' name='arquivo' value='' size='30'>
                        </TD>
                    </TR>
                    <TR>
                        <TD ALIGN="left">
                            <input class="textField" type="reset" name="btnCancelar" value="<bean:message key="label.cancelar"/>" onClick="window.close()">
                        </TD>
                        <TD ALIGN="right">
                            <input class="textField" type="submit" name="btnEnviar" value="<bean:message key="label.ok"/>">
                                   <input type="hidden" name="valida" value="1">
                        </TD>
                    </TR>
                </TABLE>
            </CENTER>

            <INPUT type=hidden name="objName" value="<%=objName%>">

        </FORM>

    </body>
</html>
