<%--
    Document   : cadastrarListaSinalFraco
    Created on : nov/2010
    Author     : Vitor Hugo da Motta <vmotta@gmail.com>
--%>
<%@page contentType="text/html" pageEncoding="ISO-8859-1"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <!-- Importa??o de Taglib -->
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>

    <!-- EDITAR - Importa??o de classes que ser?o utilizadas -->
    <%@page import="seris.database.Sinalfraco" %>
    <%@page import="seris.database.KiqSinalfraco" %>
    <%@page import="seris.database.Kiq" %>
    <%@page import="seris.database.Kit" %>
    <%@page import="seris.database.Dominio" %>

    <!-- Cabe?alho do site -->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
        <%
                    response.setHeader("Cache-Control", "no-cache");
                    response.setHeader("Pragma", "no-cache");
                    response.setDateHeader("Expires", 0);
        %>
        <title>Seris</title>
        <link REL="SHORTCUT ICON" HREF="favicon2.ico">
        <style type="text/css" title="currentStyle">
            @import "media/css/demo_page.css";
            @import "media/css/demo_table.css";
        </style>
        <link rel="stylesheet" type="text/css" href="seris.css">

    </head>
    <body bgcolor="#ffffff">

        <!-- DIV com o conte?do de todo site -->
        <div id="conteudo" style="width: 500px;">

            <%
                        String vScroll = request.getParameter("scroll");
                        if (vScroll == null) {
                            vScroll = "0";
                        }

                        String vId = request.getParameter("id");

                        Object[] infoSinalFraco = {new Sinalfraco(), "", "", "", "", "", "", ""};

                        Sinalfraco sinalFraco = new Sinalfraco();

                        // EDITAR - Consulta ou seta os campos para branco
                        int id = 0;
                        if (vId != null && !vId.equals("0")) {
                            id = Integer.parseInt(vId);
                            infoSinalFraco = seris.database.DAO.SinalfracoDAO.consultarInfoSinalFracoById(id);
                            sinalFraco = (Sinalfraco) infoSinalFraco[0];
                        }
            %>

            <table width="500" cellpadding="4" cellspacing="0" style="border: solid 2px #bb3a34; background-color: #ffffff;">
                <tr>
                    <th colspan="2" bgcolor="#bb3a34" height="25"> <font color="#FFFFFF"> Informa??es do Sinal Fraco </font> </th>
                </tr>
                <tr>
                    <td>
                        <table width="100%" cellpadding="4" cellspacing="0">
                            <tr>
                                <td align="right" valign="top"><b> Id: </b></td>
                                <td> <%=sinalFraco.getId()%> </td>
                            </tr>
                            <tr>
                                <td align="right" valign="top"><b> Descri??o: </b></td>
                                <td> <%=sinalFraco.getDescricao()%> </td>
                            </tr>
                            <tr>
                                <td align="right" valign="top"><b> Data: </b></td>
                                <td> <%=seris.utils.Utils.ConverteDateParaString(sinalFraco.getData())%> </td>
                            </tr>
                            <tr>
                                <td align="right" valign="top"><b> Usu?rio: </b></td>
                                <td> <%=infoSinalFraco[1]%> </td>
                            </tr>
                            <tr>
                                <td align="right" valign="top"><b> Dom?nio: </b></td>
                                <td> <%=infoSinalFraco[2]%> </td>
                            </tr>
                            <tr>
                                <td align="right" valign="top"><b> Kit: </b></td>
                                <td> <%=infoSinalFraco[7]%> </td>
                            </tr>
                            <tr>
                                <td align="right" valign="top"><b> Ator: </b></td>
                                <td> <%=infoSinalFraco[3]%> </td>
                            </tr>
                            <tr>
                                <td align="right" valign="top"><b> Produto: </b></td>
                                <td> <%=infoSinalFraco[4]%> </td>
                            </tr>
                            <tr>
                                <td align="right" valign="top"><b> <bean:message key="label.mercado"/>: </b></td>
                                <td> <%=infoSinalFraco[5]%> </td>
                            </tr>
                            <tr>
                                <td align="right" valign="top"><b> Fonte: </b></td>
                                <td> <%=infoSinalFraco[6]%> </td>
                            </tr>
                            <tr>
                                <td align="right" valign="top"><b> Coment?rios: </b></td>
                                <td> <%=sinalFraco.getComentarios()%> </td>
                            </tr>
                            <tr>
                                <td align="right" valign="top"><b> Informa??es: </b></td>
                                <td> <%=sinalFraco.getInformacoes()%> </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>

        </div>

    </body>
</html>