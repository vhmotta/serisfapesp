<%--
    Document   : cadastrarSinaisFracos
    Created on : jul/2010
    Author     : Ricardo C Bull - rcbull@gmail.com
--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><!-- InstanceBegin template="/Templates/tempateSeris.dwt.jsp" codeOutsideHTMLIsLocked="false" -->
    <!-- InstanceBeginEditable name="taglibEimport" -->
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
    <%@page import="seris.database.Ator" %>
    <%@page import="seris.database.Local" %>
    <%@page import="seris.database.Sinalfraco" %>
    <%@page import="seris.database.Fonte" %>
    <%@page import="seris.database.Produto" %>
    <%@page import="seris.database.Mercado" %>
    <%@page import="seris.database.Avaliacao" %>
    <%@page import="seris.database.Kit" %>
    <%@page import="seris.database.Kiq" %>
    <%@page import="seris.database.Dominio" %>
    <!-- InstanceEndEditable -->

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso8859-1" />
        <%
                    response.setHeader("Cache-Control", "no-cache");
                    response.setHeader("Pragma", "no-cache");
                    response.setDateHeader("Expires", 0);
        %>
        <!-- InstanceBeginEditable name="doctitle" -->
        <title>Seris</title>
        <!-- InstanceEndEditable -->
        <link REL="SHORTCUT ICON" HREF="favicon2.ico">
        <style type="text/css" title="currentStyle">
            @import "media/css/demo_page.css";
            @import "media/css/demo_table.css";
        </style>
        <script type="text/javascript" language="javascript" src="media/js/jquery.js"></script>
        <script type="text/javascript" language="javascript" src="media/js/jquery.dataTables.js"></script>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function() {
                $('#datatable').dataTable( {
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": false,
                    "bAutoWidth": false } );
            } );
            jQuery.fn.dataTableExt.oSort['string-asc']  = function(a,b) {
                var x = a.toLowerCase();
                var y = b.toLowerCase();
                return x.localeCompare(y);
            };

            jQuery.fn.dataTableExt.oSort['string-desc'] = function(a,b) {
                var x = a.toLowerCase();
                var y = b.toLowerCase();
                return y.localeCompare(x);
            };
        </script>
        <link REL="SHORTCUT ICON" HREF="favicon2.ico">
        <link rel="stylesheet" type="text/css" href="seris.css">
        <!-- InstanceBeginEditable name="head" -->
        <%
                    java.util.ResourceBundle resource = java.util.ResourceBundle.getBundle("seris.SerisIdioma");
        %>
        <script type="text/javascript">
            function validaForm(){
                d = document.cadastrarSinaisFracos;
                if (d.descricao.value == ""){
                    alert("<bean:message key="message.preencherField" arg0="<%=resource.getString("label.sinalFraco")%>"/>");
                    d.descricao.focus();
                    return false;
                }
                if(d.kiqSelecionado.value ==0){
                    alert("<bean:message key="message.selecionarField" arg0="<%=resource.getString("label.kiq")%>"/>");
                    d.kiqSelecionado.focus();
                    return false;
                }
                if(d.avaliacao.value == 0){
                    alert("<bean:message key="message.selecionarFieldA" arg0="<%=resource.getString("label.relevancia")%>"/>");
                    d.avaliacao.focus();
                    return false;
                }
                if (d.local.value == ""){
                    alert("<bean:message key="message.selecionarFieldA" arg0="<%=resource.getString("label.cidade")%>"/>");
                    d.local.focus();
                    return false;
                }
                if(d.fonte.value == 0){
                    alert("<bean:message key="message.selecionarFieldA" arg0="<%=resource.getString("label.fonte")%>"/>");
                    d.fonte.focus();
                    return false;
                }
                if(d.ator.value == 0){
                    alert("<bean:message key="message.selecionarField" arg0="<%=resource.getString("label.ator")%>"/>");
                    d.ator.focus();
                    return false;
                }
                if (d.data.value == ""){
                    alert("<bean:message key="message.preencherField" arg0="<%=resource.getString("label.data")%>"/>");
                    d.data.focus();
                    return false;
                }
                return true;
            }

            function format(value,data)
            {
                value = value.replace(/\D/g,"");
                var result="";
                if(data.length < value.length)
                    return value;
                for(i=0,j=0;(i<data.length)&&(j<value.length);i++)
                {
                    var ch = data.charAt(i);
                    if(ch == '#')
                    {
                        result += value.charAt(j++);
                        continue;
                    }
                    result += ch;
                }
                return result;
            }
            function limitaTextArea(campo){
                d = document.cadastrarSinaisFracos;
                var tamanho = d[campo].value.length;
                var tex=d[campo].value;
                if (tamanho>200) {
                    d[campo].value=tex.substring(0,200);
                }
                return true;
            }
        </script>
        <!-- InstanceEndEditable -->
    </head>

    <body>
        <%@ include file="cabecalho.jsp" %>
        <%@ include file="menu.jsp" %>
        <div id="conteudo">
            <!-- InstanceBeginEditable name="conteudo" -->
            <jsp:useBean id="Sinalfraco" scope="session" class="seris.database.Sinalfraco"></jsp:useBean>
            <%
                        String vUsuario = (String) session.getAttribute("nome");
                        if (vUsuario == null) {
            %>
            <jsp:forward page="login.jsp"/>
            <%        } else {
                        String vId = request.getParameter("id");
                        String vTask = request.getParameter("task");

                        String dominioSelecionado = String.valueOf(session.getAttribute("dominioSelecionado"));
                        int vdominioSelecionado = 0;
                        if (dominioSelecionado != null && !dominioSelecionado.equals("")) {
                            vdominioSelecionado = Integer.parseInt(String.valueOf(dominioSelecionado));
                        }
                        if (vdominioSelecionado == 0) {
            %>
            <jsp:forward page="selecionarDominio.jsp"/>
            <%                }
                        Dominio d = seris.database.DAO.DominioDAO.consultarDominio(vdominioSelecionado);

                        String kitSelecionado = String.valueOf(session.getAttribute("kitSelecionado"));
                        int vkitSelecionado = 0;
                        if (kitSelecionado != null && !kitSelecionado.equals("")) {
                            vkitSelecionado = Integer.parseInt(String.valueOf(kitSelecionado));

                        }
                        if (vkitSelecionado == 0) {
            %>
            <jsp:forward page="selecionarKit.jsp"/>
            <%                }
                        Kit k = k = seris.database.DAO.KitDAO.consultarKit(vkitSelecionado);

                        if (vTask == null) {
                            vTask = "";
                        }
                        int id = 0;
                        if (vId != null) {
                            id = Integer.parseInt(vId);
                            Sinalfraco = seris.database.DAO.SinalfracoDAO.consultarSinalfraco(id);
                        } else {
                            Sinalfraco.setDescricao("");
                        }
                        //lista de parametros para o deletar
                        java.util.HashMap params = new java.util.HashMap();
                        params.put("id", Sinalfraco.getId());
                        //params.put("nome", Sinalfraco.getDescricao);
                        pageContext.setAttribute("delete", params);
            %>
            <jsp:setProperty name="Sinalfraco" property="id" value="<%= id%>"></jsp:setProperty>
            <jsp:setProperty name="Sinalfraco" property="descricao" value="<%= Sinalfraco.getDescricao()%>"></jsp:setProperty>
            <div id="nomecont" align="center"><bean:message key="label.cadastroSinalFraco"/></div>
            <form name="cadastrarSinaisFracos" action="cadastrarSinalfraco.do" method="post" onSubmit="return validaForm()">
                <table width="600" border="0">
                    <tr>
                        <td colspan="2">
                            <p id="textoLabel">
                                <bean:message key="label.dominio"/>: <% out.print(d.getNome());%>
                            </p>
                            <p id="textoLabel">
                                <bean:message key="label.kit"/>: <% out.print(k.getDescricao());%>
                            </p>
                        </td>
                    </tr>
                    <tr><td colspan="2" height="50px"><i><bean:message key="message.avisoObrigatorio"/></i></td></tr>
                    <tr>
                        <td width="100px">*<bean:message key="label.selecioneKiq"/>:</td>
                        <td width="450px">
                            <jsp:scriptlet>
                                        session.setAttribute("Kiqs", seris.database.DAO.KiqDAO.consultarListaKiqsPorKit(kitSelecionado));
                            </jsp:scriptlet>
                            <jsp:useBean id="Kiqs" scope="session" type="java.util.List"></jsp:useBean>
                            <select name="kiqSelecionado">
                                <%
                                            out.print("<option value=\"0\" />--</option>");
                                            for (int i = 0; i < Kiqs.size(); i++) {
                                                Kiq vKiq = (Kiq) Kiqs.get(i);
                                                out.print("<option value=\"" + vKiq.getId() + "\">" + vKiq.getDescricao() + "</option>");
                                            }
                                %>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td width="100px">*<bean:message key="label.data"/>:</td>
                        <td width="450px"><input type="text" name="data" size="10" onkeyup="this.value=format(this.value,'##/##/####');" value="<%= seris.utils.Utils.getDataSistema()%>"/> DD/MM/AAAA</td>
                    </tr>
                    <tr>
                        <td width="100px">*<bean:message key="label.relevancia"/>:</td>
                        <td width="450px">
                            <jsp:scriptlet>
                                        session.setAttribute("Avaliacoes", seris.database.DAO.AvaliacaoDAO.consultarListaAvaliacoes());
                            </jsp:scriptlet>
                            <jsp:useBean id="Avaliacoes" scope="session" type="java.util.List"></jsp:useBean>
                            <br><br>
                            <select name="avaliacao">
                                <%
                                            out.print("<option value=\"0\" />--</option>");
                                            for (int i = 0; i < Avaliacoes.size(); i++) {
                                                Avaliacao vAvaliacao = (Avaliacao) Avaliacoes.get(i);
                                                out.print("<option value=\"" + vAvaliacao.getId() + "\" />" + vAvaliacao.getRelevancia() + "</option>");
                                            }
                                %>
                            </select>
                            <br><br>
                        </td>
                    </tr>
                    <tr>
                        <td width="100px">*<bean:message key="label.cidade"/>:</td>
                        <td width="450px">
                            <jsp:scriptlet>
                                        session.setAttribute("Locais", seris.database.DAO.LocalDAO.consultarListaLocais());
                            </jsp:scriptlet>
                            <jsp:useBean id="Locais" scope="session" type="java.util.List"></jsp:useBean>
                            <select name="local">
                                <option value="">--</option>
                                <%
                                            for (int i = 0; i < Locais.size(); i++) {
                                                Local vLocal = (Local) Locais.get(i);
                                                out.print("<option value=\"" + vLocal.getId() + "\">" + vLocal.getCidade() + " (" + vLocal.getUf() + ") - Pa?s: " + vLocal.getPais() + "</option>");
                                            }
                                %>
                            </select>
                            <br>Nome do local de captura:<br>
                            <input type="text" name="nomeLocalCaptura" value="" size="95" maxlength="120"/>
                            <br>Descri??o do local de captura:<br>
                            <input type="text" name="descricaoLocalCaptura" value="" size="95" maxlength="120"/>
                            <!--<textarea name="descricaoLocalCaptura" rows="2" cols="80"></textarea>-->
                            <br><br>
                        </td>
                    </tr>
                    <tr>
                        <td width="100px">*<bean:message key="label.fonte"/>:</td>
                        <td width="450px">
                            <jsp:scriptlet>
                                        session.setAttribute("Fontes", seris.database.DAO.FonteDAO.consultarListaFontesPorKit(kitSelecionado));
                            </jsp:scriptlet>
                            <jsp:useBean id="Fontes" scope="session" type="java.util.List"></jsp:useBean>
                            <select name="fonte">
                                <%
                                            out.print("<option value=\"0\" />--</option>");
                                            for (int i = 0; i < Fontes.size(); i++) {
                                                Fonte vFonte = (Fonte) Fontes.get(i);
                                                out.print("<option value=\"" + vFonte.getId() + "\" />" + vFonte.getNome() + "</option>");
                                            }
                                %>
                            </select>
                            <br><br>
                        </td>
                    </tr>
                    <tr>
                        <td width="100px"><bean:message key="label.produto"/>:</td>
                        <td width="450px">
                            <jsp:scriptlet>
                                        session.setAttribute("Produtos", seris.database.DAO.ProdutoDAO.consultarListaProdutosPorKit(kitSelecionado));
                            </jsp:scriptlet>
                            <jsp:useBean id="Produtos" scope="session" type="java.util.List"></jsp:useBean>
                            <select name="produto">
                                <%
                                            out.print("<option value=\"0\" />--</option>");
                                            for (int i = 0; i < Produtos.size(); i++) {
                                                Produto vProduto = (Produto) Produtos.get(i);
                                                out.print("<option value=\"" + vProduto.getId() + "\" />" + vProduto.getDescricao() + "</option>");
                                            }
                                %>
                            </select>
                            <br><br>
                        </td>
                    </tr>
                    <tr>
                        <td width="100px"><bean:message key="label.mercado"/>:</td>
                        <td width="450px">
                            <jsp:scriptlet>
                                        session.setAttribute("Mercados", seris.database.DAO.MercadoDAO.consultarListaMercadosPorKit(kitSelecionado));
                            </jsp:scriptlet>
                            <jsp:useBean id="Mercados" scope="session" type="java.util.List"></jsp:useBean>
                            <select name="mercado">
                                <%
                                            out.print("<option value=\"0\" />--</option>");
                                            for (int i = 0; i < Mercados.size(); i++) {
                                                Mercado vMercado = (Mercado) Mercados.get(i);
                                                out.print("<option value=\"" + vMercado.getId() + "\" />" + vMercado.getNome() + "</option>");
                                            }
                                %>
                            </select>
                            <br><br>
                        </td>
                    </tr>
                    <tr>
                        <td width="100px">*<bean:message key="label.ator"/>:</td>
                        <td width="450px">
                            <jsp:scriptlet>
                                        session.setAttribute("Atores", seris.database.DAO.AtorDAO.consultarListaAtoresPorKit(kitSelecionado));
                            </jsp:scriptlet>
                            <jsp:useBean id="Atores" scope="session" type="java.util.List"></jsp:useBean>
                            <select name="ator">
                                <%
                                            out.print("<option value=\"0\" />--</option>");
                                            for (int i = 0; i < Atores.size(); i++) {
                                                Ator vAtor = (Ator) Atores.get(i);
                                                out.print("<option value=\"" + vAtor.getId() + "\" />" + vAtor.getNome() + "</option>");
                                            }
                                %>
                            </select>
                            <br><br>
                        </td>
                    </tr>
                    <tr>
                        <td width="100px">*<bean:message key="label.sinalFraco"/>:</td>
                        <td width="450px"><textarea name="descricao" rows="4" cols="80" onKeyUp="javascript:limitaTextArea('descricao');"><jsp:getProperty name="Sinalfraco" property="descricao"></jsp:getProperty></textarea></td>
                    </tr>
                    <tr>
                        <td width="100px"><bean:message key="label.informacoesAdicionais"/>:</td>
                        <td width="450px"><textarea name="informacoes" rows="4" cols="80" onKeyUp="javascript:limitaTextArea('informacoes');"><jsp:getProperty name="Sinalfraco" property="descricao"></jsp:getProperty></textarea></td>
                    </tr>
                    <tr>
                        <td width="100px"><bean:message key="label.seusComentarios"/>:</td>
                        <td width="450px"><textarea name="comentarios" rows="4" cols="80" onKeyUp="javascript:limitaTextArea('comentarios');"><jsp:getProperty name="Sinalfraco" property="descricao"></jsp:getProperty></textarea></td>
                    </tr>
                </table>
                <input type="hidden" name="task" value="save" /><br />
                <input type="submit" value="<bean:message key="label.salvar"/>" />
            </form>
            <br /><br />
            <!--<html:link page="/cadastrarSinalfraco.do?task=novo">novo</html:link>&nbsp<html:link page="/cadastrarSinalfraco.do?task=pesquisar">Pesquisar</html:link>-->
            <%
                        }
            %>
            <!-- InstanceEndEditable -->
        </div>
        <%@ include file="rodape.jsp" %>
    </body>
    <!-- InstanceEnd --></html>