<%--
    Document   : cadastrarGrupo
    Created on : jul/2010
    Author     : Ricardo C Bull - rcbull@gmail.com
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><!-- InstanceBegin template="/Templates/tempateSeris.dwt.jsp" codeOutsideHTMLIsLocked="false" -->
    <!-- InstanceBeginEditable name="taglibEimport" -->
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
    <%@page import="seris.database.Grupo" %>
    <!-- InstanceEndEditable -->

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso8859-1" />
        <%
                    response.setHeader("Cache-Control", "no-cache");
                    response.setHeader("Pragma", "no-cache");
                    response.setDateHeader("Expires", 0);
        %>
        <!-- InstanceBeginEditable name="doctitle" -->
        <title>Seris</title>
        <!-- InstanceEndEditable -->
        <link REL="SHORTCUT ICON" HREF="favicon2.ico">
        <style type="text/css" title="currentStyle">
            @import "media/css/demo_page.css";
            @import "media/css/demo_table.css";
        </style>
        <script type="text/javascript" language="javascript" src="media/js/jquery.js"></script>
        <script type="text/javascript" language="javascript" src="media/js/jquery.dataTables.js"></script>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function() {
                $('#datatable').dataTable( {
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": false,
                    "bAutoWidth": false } );
            } );
            jQuery.fn.dataTableExt.oSort['string-asc']  = function(a,b) {
                var x = a.toLowerCase();
                var y = b.toLowerCase();
                return x.localeCompare(y);
            };

            jQuery.fn.dataTableExt.oSort['string-desc'] = function(a,b) {
                var x = a.toLowerCase();
                var y = b.toLowerCase();
                return y.localeCompare(x);
            };
        </script>
        <link REL="SHORTCUT ICON" HREF="favicon2.ico">
        <link rel="stylesheet" type="text/css" href="seris.css">
        <!-- InstanceBeginEditable name="head" -->
        <%
                    java.util.ResourceBundle resource = java.util.ResourceBundle.getBundle("seris.SerisIdioma");
        %>
        <script type="text/javascript">
            function validaForm(){
                d = document.cadastrarGrupo;
                if (d.nome.value == ""){
                    alert("<bean:message key="message.preencherField" arg0="<%=resource.getString("label.grupo")%>"/>");
                    d.nome.focus();
                    return false;
                }
                return true;
            }
        </script>
        <!-- InstanceEndEditable -->
    </head>

    <body>
        <%@ include file="cabecalho.jsp" %>
        <%@ include file="menu.jsp" %>
        <div id="conteudo">
            <!-- InstanceBeginEditable name="conteudo" -->
            <jsp:useBean id="Grupo" scope="session" class="seris.database.Grupo"></jsp:useBean>
            <%
                        String vId = request.getParameter("id");
                        String vTask = request.getParameter("task");
                        if (vTask == null) {
                            vTask = "";
                        }
                        int id = 0;
                        if (vId != null && !vId.equals("0") && (!vTask.equals("delete"))) {
                            id = Integer.parseInt(vId);
                            Grupo = seris.database.DAO.GrupoDAO.consultarGrupo(id);
                        } else {
                            Grupo.setNome("");
                        }
            %>
            <jsp:setProperty name="Grupo" property="id" value="<%= id%>"></jsp:setProperty>
            <jsp:setProperty name="Grupo" property="nome" value="<%= Grupo.getNome()%>"></jsp:setProperty>
            <%
                        if (vId != null && !vId.equals("0")) {
                            out.print("<div id=\"nomecont\" align=\"center\">" + resource.getString("label.editarGrupo") + "</div>");
                            out.print("Id: " + vId + "<br>");
                        } else {
                            out.print("<div id=\"nomecont\" align=\"center\">" + resource.getString("label.cadastrarGrupo") + "</div>");
                        }
            %>
            <form name="cadastrarGrupo" action="cadastrarGrupo.do" method="post" onSubmit="return validaForm()">
                <bean:message key="label.grupo"/>:<input type="text" name="nome" size="60" maxlength="100" value="<jsp:getProperty name="Grupo" property="nome"></jsp:getProperty>"/>
                <input type="hidden" name="id" value="<jsp:getProperty name="Grupo" property="id"></jsp:getProperty>" />
                <input type="hidden" name="task" value="save" /><br /><br />
                <input type="submit" value="<bean:message key="label.salvar"/>" />&nbsp;&nbsp;</form>
                <%
                            if (vId != null && !vId.equals("0")) {
                %>
            <form name="deletarGrupo" action="cadastrarGrupo.do" method="post">
                <input type="hidden" name="id" value="<jsp:getProperty name="Grupo" property="id"></jsp:getProperty>" />
                <input type="hidden" name="task" value="delete" /><br />
                <input type="submit" value="<bean:message key="label.deletar"/>" />&nbsp&nbsp</form>
                <%        }
                %>
            <p>&nbsp;</p><p><font color="blue">
                    <% if (request.getAttribute("message") != null) {
                                    out.print(request.getAttribute("message"));
                                }
                    %>
                </font></p>
            <br /><br />
            <html:link page="/cadastrarGrupo.do?task=novo"><bean:message key="label.novo"/></html:link>&nbsp;<html:link page="/cadastrarGrupo.do?task=pesquisar"><bean:message key="label.pesquisar"/></html:link>
            <%
                        if (vTask != null) {
                            if (vTask.equals("pesquisar")) {
            %>
            <jsp:scriptlet>
                                            session.setAttribute("Grupos", seris.database.DAO.GrupoDAO.consultarListaGrupos());
            </jsp:scriptlet>
            <jsp:useBean id="Grupos" scope="session" type="java.util.List"></jsp:useBean>
            <div id="demo">
                <table cellpadding="0" cellspacing="0" border="0" class="display" width="600px" id="datatable">
                    <thead>
                        <tr>
                            <th width="30px"><bean:message key="label.id"/></th>
                            <th width="540px"><bean:message key="label.nome"/></th>
                            <th width="30px"><bean:message key="label.editar"/></th>
                        </tr>
                    </thead>
                    <tbody>
                        <%
                                                        for (int i = 0; i < Grupos.size(); i++) {
                                                            Grupo vGrupo = (Grupo) Grupos.get(i);
                                                            out.print("<tr class='seris'><td>" + vGrupo.getId() + "</td><td>" + vGrupo.getNome() + "</td><td align='center'><a href='cadastrarGrupo.jsp?id=" + vGrupo.getId() + "'><img src='imagens/edit-icon.png' border='0'></a></td></tr>");
                                                        }
                        %>
                    </tbody>
                </table>
            </div>
            <%
                            }
                        }
            %>
            <!-- InstanceEndEditable -->
        </div>
        <%@ include file="rodape.jsp" %>
    </body>
    <!-- InstanceEnd --></html>