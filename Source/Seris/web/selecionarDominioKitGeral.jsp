<%--
    Document   : selecionarDominio
    Created on : nov/2010
    Author     : Vitor Hugo da Motta <vmotta@gmail.com>
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><!-- InstanceBegin template="/Templates/tempateSeris.dwt.jsp" codeOutsideHTMLIsLocked="false" -->
    <!-- InstanceBeginEditable name="taglibEimport" -->
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
    <%@page import="seris.database.Usuario" %>
    <%@page import="seris.database.Dominio" %>
    <%@page import="seris.database.Kit" %>
    <!-- InstanceEndEditable -->

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso8859-1" />
        <%
                    response.setHeader("Cache-Control", "no-cache");
                    response.setHeader("Pragma", "no-cache");
                    response.setDateHeader("Expires", 0);
        %>
        <!-- InstanceBeginEditable name="doctitle" -->
        <title>Seris</title>
        <!-- InstanceEndEditable -->
        <link REL="SHORTCUT ICON" HREF="favicon2.ico">
        <style type="text/css" title="currentStyle">
            @import "media/css/demo_page.css";
            @import "media/css/demo_table.css";
        </style>
        <script type="text/javascript" language="javascript" src="media/js/jquery.js"></script>
        <script type="text/javascript" language="javascript" src="media/js/jquery.dataTables.js"></script>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function() {
                $('#datatable').dataTable( {
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": false,
                    "bAutoWidth": false } );
            } );
            jQuery.fn.dataTableExt.oSort['string-asc']  = function(a,b) {
                var x = a.toLowerCase();
                var y = b.toLowerCase();
                return x.localeCompare(y);
            };

            jQuery.fn.dataTableExt.oSort['string-desc'] = function(a,b) {
                var x = a.toLowerCase();
                var y = b.toLowerCase();
                return y.localeCompare(x);
            };
        </script>
        <link REL="SHORTCUT ICON" HREF="favicon2.ico">
        <link rel="stylesheet" type="text/css" href="seris.css">
        <!-- InstanceBeginEditable name="head" -->
        <script type="text/javascript">
            function validaFormDominio(){
                d = document.selecionarDominio;
                if(d.dominioSelecionado.value ==0){
                    alert("<bean:message key="message.selecionarDominioRelatorio"/>");
                    d.dominioSelecionado.focus();
                    return false;
                }
                if(d.kitSelecionado.value ==0){
                    alert("<bean:message key="message.selecionarKitRelatorio"/>");
                    d.kitSelecionado.focus();
                    return false;
                }
                return true;
            }
        </script>
        <!-- InstanceEndEditable -->
    </head>

    <body>
        <%@ include file="cabecalho.jsp" %>
        <%@ include file="menu.jsp" %>
        <div id="conteudo">
            <!-- InstanceBeginEditable name="conteudo" -->
            <%
                        String formaction = String.valueOf(request.getParameter("formaction"));
                        String vusuario_id = String.valueOf(session.getAttribute("usuario_id"));
                        int usuario_id = 0;
                        if (vusuario_id != null && !vusuario_id.equals("")) {
                            usuario_id = Integer.parseInt(vusuario_id);
                        }
                        if (usuario_id == 0) {
            %>
            <jsp:forward page="login.jsp"/>
            <%                        }
                        java.util.List dominios = seris.database.DAO.DominioUsuarioDAO.consultarListaDominiosPorUsuario(usuario_id);
                        java.util.List kits = seris.database.DAO.KitDAO.consultarListaKitsPorUsuario(usuario_id);

                        Kit vKit;
                        String arrayScript = "var kitvalues=[]; \n";
                        int countkit = 0;
                        for (int i = 0; i < kits.size(); i++) {
                            vKit = (Kit) kits.get(i);
                            arrayScript += "kitvalues[" + countkit++ + "] = {dominioId: '" + vKit.getDominio().getId() + "', id: '" + vKit.getId() + "', descricao: '" + vKit.getDescricao() + "'};\n";
                        }

            %>
            <script>
                <%=arrayScript%>

                    function limparSelect(objName) {
                        var obj=document.getElementById(objName);
                        var options = obj.getElementsByTagName("option");
                        while (options.length > 0) obj.remove(0);
                    }

                    function insertSelect(objName, value, title){
                        var obj = document.getElementById(objName);

                        var opt = document.createElement('option');
                        opt.text = title;
                        opt.value = value;
                        try {
                            obj.add(opt,null);
                        } catch(ex) {
                            obj.add(opt);
                        }
                    }

                    function carregarKit(objDominio){
                        limparSelect("kitSelecionado")
                        insertSelect("kitSelecionado", "", "--");
                        var kits;
                        for (i=0; i<kitvalues.length; i++){
                            kits = kitvalues[i];
                            if (kits.dominioId == objDominio.value){
                                insertSelect("kitSelecionado", kits.id, kits.descricao);
                            }
                        }
                    }
            </script>
            <p><b><bean:message key="label.selecaoDominioKit"/>:</b></p>
            <form name="selecionarDominio" action="<%=formaction%>" method="post" onsubmit="return validaFormDominio()">
                <br><bean:message key="label.selecioneDominio"/></br>
                <select name="dominioSelecionado" OnChange="carregarKit(this);">
                    <%
                                out.print("<option value=\"0\" />--</option>");
                                Dominio vDominio;
                                for (int i = 0; i < dominios.size(); i++) {
                                    vDominio = (Dominio) dominios.get(i);
                                    out.print("<option value=\"" + vDominio.getId() + "\"> " + vDominio.getNome() + "</option><br>");
                                }
                    %>
                </select>
                <br><bean:message key="label.selecioneKit"/></br>
                <select name="kitSelecionado" id="kitSelecionado">
                    <%
                                out.print("<option value=\"0\" />--</option>");
                    %>
                </select><BR>
                <input type="submit" value="<bean:message key="label.selecionar"/>">
            </form>
            <!-- InstanceEndEditable -->
        </div>
        <%@ include file="rodape.jsp" %>
    </body>
    <!-- InstanceEnd --></html>