<%--
    Document   : home
    Created on : jul/2010
    Author     : Ricardo C Bull - rcbull@gmail.com
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>

<html>
    <%@page import="seris2.database.ConceitoSemantico" %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Seris</title>
        <%
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Pragma", "no-cache");
            response.setDateHeader("Expires", 0);
        %>
        <link REL="SHORTCUT ICON" HREF="favicon2.ico">
        <link rel="stylesheet" type="text/css" href="seris.css">
    </head>
    <body>
        <%@ include file="cabecalho.jsp" %>
        <%@ include file="menu.jsp" %>
        <!-- inicio conteudo -->

        <br>
        <table cellpadding="0" cellspacing="0" border="0">
            <tr>
            <td valign="top" style="width: 720px;">
                <div align="center">
                    <div style="width: 675px; height: 432px; background: url('img/centrosistema.png') no-repeat; text-align: left;">
                        <div style="width:1px; position: relative; top: 17px; left: 530px; font-weight: bold; font-family: verdana; font-size: 14px; color: #fff;">
                            <bean:message key="label.inteligenciaCA"/>
                        </div>
                        <div style="width:1px; position: relative; top: 190px; left: 44px; font-weight: bold; font-family: verdana; font-size: 14px; color: #fff;">
                            <bean:message key="label.captacaoCA"/>
                        </div>
                        <div style="width:1px; position: relative; top: 355px; left: 420px; font-weight: bold; font-family: verdana; font-size: 14px; color: #fff;">
                            <bean:message key="label.conhecimentoCA"/>
                        </div>                

                        <!--
                        0 - Administrador
                        1 - Patrocinador
                        2 - Suporte de Domínio
                        3 - Analista (ou alta direção precisa verificar, mais provável)
                        4 - Captador
                        5 - Alta direção (ou facilitador precisa verificar, mais provável)
                        
                        -->
                        <% if (vNivel == 1 || vNivel == 2) {%>
                        <div style="width:150px; position: relative; top: 18px; left: 235px; text-align: center;">
                            <a href="selecionarDominioCadastrarKit.jsp" style="font-weight: bold; font-family: verdana; font-size: 12px; color: #b62900; text-decoration: none;">
                                <bean:message key="label.criarSerisKitsBR"/>
                            </a>
                        </div>            
                        <%} else {
                        %>
                        <div style="width:150px; position: relative; top: 18px; left: 235px; font-weight: bold; font-family: verdana; font-size: 12px; color: #ccc; text-align: center;">
                            <bean:message key="label.criarSerisKitsBR"/>
                        </div>            
                        <%}
                            if (vNivel == 0 || vNivel == 1 || vNivel == 2 || vNivel == 3 || vNivel == 4 || vNivel == 5) {%>
                        <div style="width:150px; position: relative; top: 44px; left: 155px; text-align: center;">
                            <a href="selecionarDominio.jsp" style="font-weight: bold; font-family: verdana; font-size: 12px; color: #b62900; text-decoration: none;">
                                <bean:message key="label.lancarSinaisFracosBR"/>
                            </a>
                        </div>            
                        <%} else {
                        %>
                        <div style="width:150px; position: relative; top: 44px; left: 155px; font-weight: bold; font-family: verdana; font-size: 12px; color: #ccc; text-align: center;">
                            <bean:message key="label.lancarSinaisFracosBR"/>
                        </div>            
                        <%}
                            if (vNivel == 1 || vNivel == 3 || vNivel == 5) {%>
                        <div style="width:150px; position: relative; top: 10px; left: 435px; text-align: center;">
                            <a href="selecionarDominioKitGeral.jsp?formaction=cadastrarCriacaoSentido.jsp" style="font-weight: bold; font-family: verdana; font-size: 12px; color: #b62900; text-decoration: none;">
                                <bean:message key="label.criacaoSentidoBR"/>
                            </a>
                        </div>            
                        <%} else {
                        %>
                        <div style="width:150px; position: relative; top: 10px; left: 435px; font-weight: bold; font-family: verdana; font-size: 12px; color: #ccc; text-align: center;">
                            <bean:message key="label.criacaoSentidoBR"/>
                        </div>            
                        <%}
                            if (vNivel == -1) {%>
                        <div style="width:150px; position: relative; top: 57px; left: 400px; text-align: center;">
                            <a href="#" style="font-weight: bold; font-family: verdana; font-size: 12px; color: #b62900; text-decoration: none;">
                                <bean:message key="label.rastreamentoAtoresBR"/>
                            </a>
                        </div>            
                        <%} else {
                        %>
                        <div style="width:150px; position: relative; top: 57px; left: 400px; font-weight: bold; font-family: verdana; font-size: 12px; color: #ccc; text-align: center;">
                            <bean:message key="label.rastreamentoAtoresBR"/>
                        </div>            
                        <%}
                            if (vNivel == 1 || vNivel == 2 || vNivel == 3 || vNivel == 5) {%>
                        <div style="width:150px; position: relative; top: 175px; left: 72px; text-align: center;">
                            <a href="selecionarTipoRelatorioSinaisFracos.jsp" style="font-weight: bold; font-family: verdana; font-size: 12px; color: #b62900; text-decoration: none;">
                                <bean:message key="label.memoriaFuturoBR"/>
                            </a>
                        </div>            
                        <%} else {
                        %>
                        <div style="width:150px; position: relative; top: 175px; left: 72px; font-weight: bold; font-family: verdana; font-size: 12px; color: #ccc; text-align: center;">
                            <bean:message key="label.memoriaFuturoBR"/>
                        </div>            
                        <%}
                            if (vNivel == 1 || vNivel == 2 || vNivel == 3 || vNivel == 5) {%>
                        <div style="width:150px; position: relative; top: 165px; left: 250px; text-align: center;">
                            <a href="exibirCriacaoSentido.jsp" style="font-weight: bold; font-family: verdana; font-size: 12px; color: #b62900; text-decoration: none;">
                                <bean:message key="label.consultaSerisMapsBR"/>
                            </a>
                        </div>            
                        <%} else {
                        %>
                        <div style="width:150px; position: relative; top: 165px; left: 250px; font-weight: bold; font-family: verdana; font-size: 12px; color: #ccc; text-align: center;">
                            <bean:message key="label.consultaSerisMapsBR"/>
                        </div>            
                        <%}%>

                    </div>
                </div>
                        
            </td><td valign="top" width="520px" style="width: 520px;">

                <div>
                    <bean:message key="label.rastrearRedeAtor"/>
                    <select onChange="window.location = 'home.jsp?idConceitoNews=' + this.value;">
                        <option></option>
                        <%
                            String idConceitoNews = request.getParameter("idConceitoNews");

                            java.util.List vConceitos = seris2.database.DAO.ConceitoSemanticoDAO.consultarConceitosDasInstanciasClassificadas();
                            ConceitoSemantico cs;
                            for (int i = 0; i < vConceitos.size(); i++) {
                                cs = (ConceitoSemantico) vConceitos.get(i);
                                if (idConceitoNews != null && idConceitoNews.equals(cs.getId().toString())) {
                                    out.println("<OPTION selected value=\"" + cs.getId() + "\">" + cs.getConceito() + "</OPTION>");
                                } else {
                                    out.println("<OPTION value=\"" + cs.getId() + "\">" + cs.getConceito() + "</OPTION>");
                                }
                            }
                            out.println("</select>");

                            java.util.ResourceBundle resource = java.util.ResourceBundle.getBundle("seris.SerisIdioma");
                            //
                            // Consulta para distribuição de sinais fracos
                            //
                            String[][] vRelatorioSinaisFracos = seris.database.DAO.SinalfracoDAO.consultarListaSinaisFracosNews(idConceitoNews);
                            if (vRelatorioSinaisFracos.length > 0) {

                                //out.print("<div><table width=\"100%\"><td width=725></td><td align=center><table width=\"500\" cellpadding=\"0\" cellspacing=\"1\" border=\"0\">"
                                //        + "<thead><tr bgcolor=\"#bb3a34\" height=\"16\">"
                                //        + "<th width=\"100\" align=\"center\"><font color=\"#FFFFFF\"><b>Data</b></font></td>"
                                //        + "<th width=\"100\" align=\"center\"><font color=\"#FFFFFF\"><b>Domínio</b></font></td>"
                                //        + "<th align=\"center\"><font color=\"#FFFFFF\"><b>Sinal Fraco</b></font></td>"
                                //        + "</tr></thead><tbody>");
                                out.print("<table width=\"500\" cellpadding=\"0\" cellspacing=\"1\" border=\"0\">"
                                        + "<thead><tr bgcolor=\"#bb3a34\" height=\"16\">"
                                        + "<th width=\"100\" align=\"center\"><font color=\"#FFFFFF\"><b>" + resource.getString("label.data") + "</b></font></td>"
                                        + "<th width=\"100\" align=\"center\"><font color=\"#FFFFFF\"><b>" + resource.getString("label.dominio") + "</b></font></td>"
                                        + "<th align=\"center\"><font color=\"#FFFFFF\"><b>" + resource.getString("label.sinalFraco") + "</b></font></td>"
                                        + "</tr></thead><tbody>");

                                int total = 5;
                                if (total > vRelatorioSinaisFracos.length) {
                                    total = vRelatorioSinaisFracos.length;
                                }
                                for (int i = 0; i < total; i++) {
                                    out.print("<tr>"
                                            + "<td align=\"center\">" + vRelatorioSinaisFracos[i][5] + "</td>"
                                            + "<td>" + vRelatorioSinaisFracos[i][9] + "</td>"
                                            + "<td title=\"" + vRelatorioSinaisFracos[i][7] + "\">" + vRelatorioSinaisFracos[i][6] + "</td>"
                                            + "</tr>");
                                    out.print("<tr><td colspan=3 height=\"1\" bgcolor=\"#DDDDDD\"></td></tr>");
                                }
                                out.print("</tbody></table></div>");

                            } else {
                                //out.print("<p><b>não foram encontrados registros com os critérios selecionados</b></p>");
                            }
                        %>
                </div>
            </td>
            </tr>
            <tr>
                <td colspan="2" valign="bottom">
        <%@ include file="rodape.jsp" %>
                </td>
            </tr>
        </table>


    </body>
    <%
        String agradecimento = (String) request.getAttribute("AGRADECIMENTO");
        if (agradecimento != null && agradecimento.equals("TRUE")) {
            String nomeUsuario = (String) request.getAttribute("NOMEUSUARIO");
    %>
    <script>
        alert("<%=nomeUsuario%> <bean:message key="message.agradecimentoSinalFraco"/>");
    </script>
    <%                }
    %>
</html>