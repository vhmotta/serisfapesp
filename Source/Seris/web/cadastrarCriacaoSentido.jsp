<%--
    Document   : cadastrarCriacaoSentido
    Created on : nov/2010
    Author     : Vitor Hugo da Motta <vmotta@gmail.com>
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <!-- Importa??o de Taglib -->
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>

    <!-- EDITAR - Importa??o de classes que ser?o utilizadas -->
    <%@page import="seris2.database.CriacaoSentido" %>
    <%@page import="seris2.database.ListaSinalFraco" %>
    <%@page import="seris.database.Local" %>
    <%@page import="seris.database.Kit" %>
    <%@page import="seris.database.Dominio" %>

    <!-- Cabe?alho do site -->
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso8859-1" />
        <%
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Pragma", "no-cache");
            response.setDateHeader("Expires", 0);
        %>
        <title>Seris</title>
        <link REL="SHORTCUT ICON" HREF="favicon2.ico">
        <style type="text/css" title="currentStyle">
            @import "media/css/demo_page.css";
            @import "media/css/demo_table.css";
        </style>
        <link rel="stylesheet" type="text/css" href="seris.css">

        <!-- Script para o grid -->
        <script type="text/javascript" language="javascript" src="javascript/ajax.js"></script>
        <script type="text/javascript" language="javascript" src="media/js/jquery.js"></script>
        <script type="text/javascript" language="javascript" src="media/js/jquery.dataTables.js"></script>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function() {
                $('#datatable').dataTable( {
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": false,
                    "bAutoWidth": false } );
            } );
            jQuery.fn.dataTableExt.oSort['string-asc']  = function(a,b) {
                var x = a.toLowerCase();
                var y = b.toLowerCase();
                return x.localeCompare(y);
            };

            jQuery.fn.dataTableExt.oSort['string-desc'] = function(a,b) {
                var x = a.toLowerCase();
                var y = b.toLowerCase();
                return y.localeCompare(x);
            };
        </script>
        <%
            java.util.ResourceBundle resource = java.util.ResourceBundle.getBundle("seris.SerisIdioma");
        %>
        <script type="text/javascript">

            // EDITAR - Adicionar campos obrigat?rios
            function validaForm(){
                d = document.cadastrarCriacaoSentido;
                if (d.nome.value == ""){
                    alert("<bean:message key="message.preencherField" arg0="<%=resource.getString("label.criacaoSentido")%>"/>");
                    d.nome.focus();
                    return false;
                }
                if (d.data.value == ""){
                    alert("<bean:message key="message.preencherField" arg0="<%=resource.getString("label.data")%>"/>");
                    d.data.focus();
                    return false;
                }
                if (d.listaId.value == ""){
                    alert("<bean:message key="message.preencherField" arg0="<%=resource.getString("label.lista")%>"/>");
                    d.listaId.focus();
                    return false;
                }
                if (d.empresa.value == ""){
                    alert("<bean:message key="message.preencherField" arg0="<%=resource.getString("label.empresa")%>"/>");
                    d.empresa.focus();
                    return false;
                }
                if (d.localId.value == ""){
                    alert("<bean:message key="message.preencherField" arg0="<%=resource.getString("label.cidade")%>"/>");
                    d.localId.focus();
                    return false;
                }
                if (d.lugar.value == ""){
                    alert("<bean:message key="message.preencherField" arg0="<%=resource.getString("label.lugar")%>"/>");
                    d.lugar.focus();
                    return false;
                }
                return true;
            }

            // Mascara para campo do tipo data
            function format(value,data){
                value = value.replace(/\D/g,"");
                var result="";
                if(data.length < value.length)
                    return value;
                for(i=0,j=0;(i<data.length)&&(j<value.length);i++)
                {
                    var ch = data.charAt(i);
                    if(ch == '#')
                    {
                        result += value.charAt(j++);
                        continue;
                    }
                    result += ch;
                }
                return result;
            }
        </script>

    </head>
    <body id="mybody">

        <!-- Inclui Cabe?alho e Menu -->
        <%@ include file="cabecalho.jsp" %>
        <%@ include file="menu.jsp" %>

        <!-- DIV com o conte?do de todo site -->
        <div id="conteudo">

            <!-- Obt?m uma inst?ncia do domain da cria??o de sentido -->
            <jsp:useBean id="criacaoSentido" scope="session" class="seris2.database.CriacaoSentido"></jsp:useBean>

            <%
                String vKitId = request.getParameter("kitSelecionado");

                String vId = request.getParameter("id");
                String vTask = request.getParameter("task");
                //String vData = seris.utils.Utils.getDataSistema();
                String vData = "";
                if (vTask == null) {
                    vTask = "";
                }

                String dominioName = "";
                String kitName = "";

                if (vKitId != null && !vKitId.equals("0")) {
                    Kit kit = seris.database.DAO.KitDAO.consultarKit(Integer.parseInt(vKitId));
                    Dominio dominio = seris.database.DAO.DominioDAO.consultarDominio(kit.getDominioId());

                    dominioName = dominio.getNome();
                    kitName = kit.getDescricao();
                }

                // EDITAR - Consulta ou seta os campos para branco
                int id = 0;
                if (vId != null && !vId.equals("0") && (!vTask.equals("delete"))) {
                    id = Integer.parseInt(vId);
                    criacaoSentido = seris2.database.DAO.CriacaoSentidoDAO.consultarCriacaoSentido(id);
                    vData = seris.utils.Utils.ConverteDateParaString(criacaoSentido.getData());
                } else {
                    vId = "0";
                    criacaoSentido.setNome("");
                    criacaoSentido.setEmpresa("");
                    criacaoSentido.setLugar("");
                }

                // Se houver um registro selecionado mostra o t?tulo editar e o Id
                if (vId != null && !vId.equals("0")) {
                    out.print("<div id=\"nomecont\" align=\"center\">" + resource.getString("label.editarCriacaoSentido") + "</div>");
                } else {
                    out.print("<div id=\"nomecont\" align=\"center\">" + resource.getString("label.cadastrarCriacaoSentido") + "</div>");
                }
            %>

            <div>
                <b><bean:message key="label.dominio"/>:</b> <%=dominioName%> <BR>
                <b><bean:message key="label.kit"/>:</b> <%=kitName%>
            </div>
            <br>

            <form name="cadastrarCriacaoSentido" action="cadastrarCriacaoSentido.do" method="post" onSubmit="return validaForm()">

                &nbsp;
                <input type="button" value="<bean:message key="label.novo"/>" OnClick="document.location.href = 'cadastrarCriacaoSentido.do?task=novo&kitSelecionado=<%=vKitId%>';" style="width: 100px">

                <input type="submit" value="<bean:message key="label.salvar"/>" style="width: 100px">
                <%
                    if (vId != null && !vId.equals("0")) {
                %>
                <input type="button" value="<bean:message key="label.deletar"/>" OnClick="document.cadastrarCriacaoSentido.task.value='delete'; document.cadastrarCriacaoSentido.submit();" style="width: 100px">
                <input type="button" value="<bean:message key="label.finalizar"/>" OnClick="document.cadastrarCriacaoSentido.task.value='finalizar'; document.cadastrarCriacaoSentido.submit();" style="width: 100px">
                <%           } // fim de "if (vId != null && !vId.equals("0")) {"
%>
                <input type="button" value="<bean:message key="label.pesquisar"/>" OnClick="document.location.href = 'cadastrarCriacaoSentido.do?task=pesquisar&kitSelecionado=<%=vKitId%>';"  style="width: 100px">

                <br>
                <font color="blue">
                    <%
                        if (request.getAttribute("message") != null) {
                            out.print("&nbsp;&nbsp;&nbsp;" + request.getAttribute("message"));
                        }
                    %>
                </font>

                <BR><BR>

                <%
                    if (vId != null && !vId.equals("0")) {
                        out.print("&nbsp;Id: " + vId + "<br>");
                    }
                %>


                <table width="600" border="0">
                    <tr>
                        <td> *<bean:message key="label.lista"/>: </td>
                        <td>
                            <select name="listaId">
                                <option value="">--</option>
                                <%
                                    java.util.List listas = seris2.database.DAO.ListaSinalFracoDAO.consultarListaSinalFracoEncerrada(vKitId);

                                    ListaSinalFraco vlista;
                                    for (int i = 0; i < listas.size(); i++) {
                                        vlista = (ListaSinalFraco) listas.get(i);
                                        if (criacaoSentido.getListaSinalFracoId() != null && vlista.getId().intValue() == criacaoSentido.getListaSinalFracoId().intValue()) {
                                            out.print("<option value=\"" + vlista.getId() + "\" selected>" + vlista.getNome() + "</option>");
                                        } else {
                                            out.print("<option value=\"" + vlista.getId() + "\">" + vlista.getNome() + "</option>");
                                        }
                                    }
                                %>
                            </select>
                        </td>
                    </tr><tr>
                        <td> *<bean:message key="label.nome"/>: </td>
                        <td>  <input type="text" name="nome"  size="60" maxlength="100" value="<%=criacaoSentido.getNome()%>"/> </td>
                    </tr><tr>
                        <td> *<bean:message key="label.dataSessao"/>: </td>
                        <td> <input type="text" name="data" size="10" onkeyup="this.value=format(this.value,'##/##/####');" value="<%=vData%>"/> DD/MM/AAAA  </td>
                    </tr><tr>
                        <td> *<bean:message key="label.empresa"/>: </td>
                        <td> <input type="text" name="empresa"  size="60" maxlength="100" value="<%=criacaoSentido.getEmpresa()%>"/> </td>
                    </tr><tr>
                        <td> *<bean:message key="label.cidade"/>: </td>
                        <td>
                            <jsp:scriptlet>
                                session.setAttribute("Locais", seris.database.DAO.LocalDAO.consultarListaLocais());
                            </jsp:scriptlet>
                            <jsp:useBean id="Locais" scope="session" type="java.util.List"></jsp:useBean>
                            <select name="localId">
                                <option value="">--</option>
                                <%
                                    for (int i = 0; i < Locais.size(); i++) {
                                        Local vLocal = (Local) Locais.get(i);
                                        if (criacaoSentido.getLocal() != null && vLocal.getId().intValue() == criacaoSentido.getLocal().getId().intValue()) {
                                            out.print("<option value=\"" + vLocal.getId() + "\" selected>" + vLocal.getCidade() + " (" + vLocal.getUf() + ") - Pa?s: " + vLocal.getPais() + "</option>");
                                        } else {
                                            out.print("<option value=\"" + vLocal.getId() + "\">" + vLocal.getCidade() + " (" + vLocal.getUf() + ") - Pa?s: " + vLocal.getPais() + "</option>");
                                        }
                                    }
                                %>
                            </select>
                        </td>
                    </tr><tr>
                        <td> *<bean:message key="label.lugar"/>: </td>
                        <td> <input type="text" name="lugar"  size="60" maxlength="100" value="<%=criacaoSentido.getLugar()%>"/> </td>
                    </tr>
                </table>

                <input type="hidden" name="kitSelecionado" value="<%=vKitId%>" />
                <input type="hidden" name="id" value="<%=id%>" />
                <input type="hidden" name="task" value="save" /><br /><br />
            </form>

            <%
                if (vTask != null) {
                    if (vTask.equals("pesquisar")) {

                        // EDITAR - Obt?m lista de criacao de sentido
                        java.util.List criacaoSentidoList = seris2.database.DAO.CriacaoSentidoDAO.consultarListaCriacaoSentidoByKit(vKitId);
            %>
            <div id="demo">
                <table cellpadding="0" cellspacing="0" border="0" class="display" width="600px" id="datatable">
                    <thead>
                        <tr>
                            <th width="30px"><bean:message key="label.id"/></th>
                            <th><bean:message key="label.nome"/></th>
                            <th><bean:message key="label.data"/></th>
                            <th><bean:message key="label.empresa"/></th>
                            <th><bean:message key="label.lugar"/></th>
                            <th><bean:message key="label.cidade"/></th>
                            <th width="30px"><bean:message key="label.editar"/></th>
                            <th width="30px"><bean:message key="label.lista"/></th>
                            <th width="30px"><bean:message key="label.abrir"/></th>
                        </tr>
                    </thead>
                    <tbody>
                        <%
                            CriacaoSentido vCriacaoSentido;
                            for (int i = 0; i < criacaoSentidoList.size(); i++) {
                                vCriacaoSentido = (CriacaoSentido) criacaoSentidoList.get(i);
                                out.print("<tr class='seris'>");
                                out.print("<td>" + vCriacaoSentido.getId() + "</td>");
                                out.print("<td>" + vCriacaoSentido.getNome() + "</td>");
                                out.print("<td align=center>" + seris.utils.Utils.ConverteDateParaString(vCriacaoSentido.getData()) + "</td>");
                                out.print("<td>" + vCriacaoSentido.getEmpresa() + "</td>");
                                out.print("<td>" + vCriacaoSentido.getLugar() + "</td>");
                                out.print("<td>" + vCriacaoSentido.getLocal().getCidade() + " / " + vCriacaoSentido.getLocal().getUf() + "</td>");
                                out.print("<td align='center'>");
                                out.print("  <a href='cadastrarCriacaoSentido.jsp?id=" + vCriacaoSentido.getId() + "&kitSelecionado=" + vKitId + "'><img src='imagens/edit-icon.png' border='0'></a>");
                                out.print("</td>");
                                out.print("<td align='center'>");
                                out.print("  <a target='baixarPDF' href='baixarlista.jsp?id=" + vCriacaoSentido.getId() + "&listaId=" + vCriacaoSentido.getListaSinalFracoId() + "'> " + resource.getString("label.baixar") + " </a>");
                                out.print("</td>");
                                out.print("<td align='center'>");
                                //out.print("  <a target='_blank' href='applet/criacaosentido.jsp?id=" + vCriacaoSentido.getId() + "&listaId=" + vCriacaoSentido.getListaSinalFracoId() + "'> " + resource.getString("label.abrir") + " </a>");
                                out.print("  <a href='#' onClick=\"exibirSCS(" + vCriacaoSentido.getId() + "," + vCriacaoSentido.getListaSinalFracoId() + ", '" + vCriacaoSentido.getNome() + "');\"> " + resource.getString("label.abrir") + " </a>");
                                out.print("</td>");
                                out.print("</tr>");
                            }
                        %>
                    </tbody>
                </table>
            </div>
            <%
                    } // fim de "if (vTask.equals("pesquisar")) {"
                } // fim de "if (vTask != null) {"
            %>

        </div>

        <!-- Inclui Rodap? -->
        <%@ include file="rodape.jsp" %>

        <IFRAME id="baixarPDF" name="baixarPDF" style="visibility: hidden; display: 'none';" src="" scrolling=no marginwidth=0 marginheight=0 frameborder=0 vspace=0 hspace=0></IFRAME>

        <div id="divAbrirGraficos" name="divAbrirGraficos" style="position: absolute; top: 10px; left: 10px; z-index: 1000; visibility: hidden; display: none; width: 400px;">
            <div style="border: solid 2px #bb3a34; background-color: #eeeeee; cursor: hand; cursor: pointer;" OnClick="esconderSCS();"><center><b><bean:message key="label.fechar"/></b></center></div>
            <div id="divInfoAbrirGraficos" name="divInfoAbrirGraficos" style="background-color: #bb3a34; border: solid 2px #bb3a34; border-top: solid 0px; overflow: auto;">
                <font color="#ffffff"><center><b>Carregando ... Aguarde !!!</b></center></font>
            </div>
        </div>

    </body>
</html>
<script>
    function exibirSCS(idSCS, idLista, nome){

        esconderSCS();

        var url = 'applet/graficosabrir.jsp?idSCS=' + idSCS + '&idLista='+ idLista + '&nome='+ nome;
        ajaxLink("divInfoAbrirGraficos",url);

        var objdiv = document.getElementById("divAbrirGraficos");

        var topRolagem = document.getElementById('mybody').scrollTop;
        if (topRolagem == 0) {
            topRolagem = document.documentElement.scrollTop;
        }
        var leftRolagem = document.getElementById('mybody').scrollLeft;
        if (leftRolagem == 0) {
            leftRolagem = document.documentElement.scrollLeft;
        }
        
        var winH = screen.availHeight-80;
        var winW = screen.availWidth;

        var posY = topRolagem + (winH/2) - 100;
        var posX = leftRolagem + (winW/2) - 200;

        objdiv.style.top = posY + 'px';
        objdiv.style.left = posX + 'px';

        objdiv.style.visibility = 'visible';
        objdiv.style.display = '';
    }
    
    function esconderSCS(){
        var objdiv = document.getElementById("divAbrirGraficos");
        objdiv.style.visibility = 'hidden';
        objdiv.style.display = 'none';
    }
</script>