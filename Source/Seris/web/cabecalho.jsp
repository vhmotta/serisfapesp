<table width="100%" height="130" cellpadding="0" cellspacing="0" border="0">
    <td width="160" valign="top">
        <div id="divLogoEmpresa">
            <span style="position: relative; top: 50px; left: -8px; color: #d69d92; font-weight: bold; font-family: verdana; font-size: 11px;">
                Vers�o <bean:message key="label.versao"/>
            </span>
            <span style="position: relative; top: 65px; left: 80px; color: #b03432; font-weight: bold; font-family: verdana; font-size: 18px;">
                <i><bean:message key="label.tema"/></i>
            </span>
        </div>
    </td>
    <td style="background: url('img/logocentro.png') no-repeat center; text-align: center;" valign="middle">
        <table border="0" align="center" height="60">
            <tr>
                <td width="160" valign="top" style="font-size: 14px; color: #ffffff;"><b><bean:message key="label.inteligencia"/></b></td>
                <td width="160" valign="top" style="font-size: 14px;"><b><bean:message key="label.antecipativa"/></b></td>
            </tr>
            <tr>
                <td valign="bottom" style="font-size: 14px;"><b><bean:message key="label.conhecimento"/></b></td>
                <td valign="bottom" style="font-size: 14px; color: #ffffff;"><b><bean:message key="label.coletivo"/></b></td>
            </tr>
        </table>
    </td>
    <td width="250" valign="top" style="background: url('img/direitatopo.png') no-repeat left;" >
        <div id="divLogoCliente">&nbsp;</div>
    </td>
</table>