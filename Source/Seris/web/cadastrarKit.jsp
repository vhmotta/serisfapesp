<%--
    Document   : cadastrarKit
    Created on : jul/2010
    Author     : Ricardo C Bull - rcbull@gmail.com
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><!-- InstanceBegin template="/Templates/tempateSeris.dwt.jsp" codeOutsideHTMLIsLocked="false" -->
    <!-- InstanceBeginEditable name="taglibEimport" -->
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
    <%@page import="seris.database.Kit" %>
    <%@page import="seris.database.Kiq" %>
    <%@page import="seris.database.Patrocinador" %>
    <%@page import="seris.database.Ator" %>
    <%@page import="seris.database.Fonte" %>
    <%@page import="seris.database.Produto" %>
    <%@page import="seris.database.Mercado" %>
    <%@page import="seris.database.Dominio" %>
    <!-- InstanceEndEditable -->

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso8859-1" />
        <%
                    response.setHeader("Cache-Control", "no-cache");
                    response.setHeader("Pragma", "no-cache");
                    response.setDateHeader("Expires", 0);
        %>
        <!-- InstanceBeginEditable name="doctitle" -->
        <title>Seris</title>
        <!-- InstanceEndEditable -->
        <link REL="SHORTCUT ICON" HREF="favicon2.ico">
        <style type="text/css" title="currentStyle">
            @import "media/css/demo_page.css";
            @import "media/css/demo_table.css";
        </style>
        <script type="text/javascript" language="javascript" src="media/js/jquery.js"></script>
        <script type="text/javascript" language="javascript" src="media/js/jquery.dataTables.js"></script>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function() {
                $('#datatable').dataTable( {
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": false,
                    "bAutoWidth": false } );
            } );
            jQuery.fn.dataTableExt.oSort['string-asc']  = function(a,b) {
                var x = a.toLowerCase();
                var y = b.toLowerCase();
                return x.localeCompare(y);
            };

            jQuery.fn.dataTableExt.oSort['string-desc'] = function(a,b) {
                var x = a.toLowerCase();
                var y = b.toLowerCase();
                return y.localeCompare(x);
            };
        </script>
        <link REL="SHORTCUT ICON" HREF="favicon2.ico">
        <link rel="stylesheet" type="text/css" href="seris.css">
        <!-- InstanceBeginEditable name="head" -->
        <%
                    java.util.ResourceBundle resource = java.util.ResourceBundle.getBundle("seris.SerisIdioma");
        %>
        <script type="text/javascript">
            function validaForm(){
                d = document.cadastrarKit;
                if (d.nomeKit.value == ""){
                    alert("<bean:message key="message.preencherField" arg0="<%=resource.getString("label.kit")%>"/>");
                    d.nomeKit.focus();
                    return false;
                }
                var mercado = new Boolean(); mercado = false;
                if(typeof d.mercado.value == "undefined"){
                    for (i=0;i<d.mercado.length;i++){
                        if (d.mercado[i].checked) {
                            mercado = true;
                            break;
                        }
                    }
                } else{
                    if (d.mercado.checked == false){
                        mercado = false;
                    }else{
                        mercado = true;
                    }
                }
                if (mercado == false){
                    alert("<bean:message key="message.selecionarVariosField" arg0="<%=resource.getString("label.mercado")%>"/>");
                    return false;
                }
                var fonte = new Boolean(); fonte = false;
                if(typeof d.fonte.value == "undefined"){
                    for (i=0;i<d.fonte.length;i++){
                        if (d.fonte[i].checked) {
                            fonte = true;
                            break;
                        }
                    }
                } else{
                    if (d.fonte.checked == false){
                        fonte = false;
                    }else{
                        fonte = true;
                    }
                }
                if (fonte == false){
                    alert("<bean:message key="message.selecionarVariosFieldA" arg0="<%=resource.getString("label.fonte")%>"/>");
                    return false;
                }
                var produto = new Boolean(); produto = false;
                if(typeof d.produto.value == "undefined"){
                    for (i=0;i<d.produto.length;i++){
                        if (d.produto[i].checked) {
                            produto = true;
                            break;
                        }
                    }
                } else{
                    if (d.produto.checked == false){
                        produto = false;
                    }else{
                        produto = true;
                    }
                }
                if (produto == false){
                    alert("<bean:message key="message.selecionarVariosField" arg0="<%=resource.getString("label.produto")%>"/>");
                    return false;
                }

                var ator = new Boolean(); ator = false;
                if(typeof d.ator.value == "undefined"){
                    for (i=0;i<d.ator.length;i++){
                        if (d.ator[i].checked) {
                            ator = true;
                            break;
                        }
                    }
                } else{
                    if (d.ator.checked == false){
                        ator = false;
                    }else{
                        ator = true;
                    }
                }
                if (ator == false){
                    alert("<bean:message key="message.selecionarVariosField" arg0="<%=resource.getString("label.ator")%>"/>");
                    return false;
                }
                return true;
            }
        </script>
        <!-- InstanceEndEditable -->
    </head>

    <body>
        <%@ include file="cabecalho.jsp" %>
        <%@ include file="menu.jsp" %>
        <div id="conteudo">
            <!-- InstanceBeginEditable name="conteudo" -->
            <jsp:useBean id="Kit" scope="session" class="seris.database.Kit"></jsp:useBean>
            <%
                        String vId = request.getParameter("id");
                        String vTask = request.getParameter("task");
                        if (vTask == null) {
                            vTask = "";
                        }
                        int id = 0;
                        if (vId != null && !vId.equals("0") && (!vTask.equals("delete"))) {
                            id = Integer.parseInt(vId);
                            Kit = seris.database.DAO.KitDAO.consultarKit(id);
                        } else {
                            Kit.setDescricao("");
                        }
                        //pega o usuario_id da sess?o
                        String vusuario_id = String.valueOf(session.getAttribute("usuario_id"));
                        int usuario_id = 0;
                        if (vusuario_id != null && !vusuario_id.equals("")) {
                            usuario_id = Integer.parseInt(vusuario_id);
                        }
                        //lista de parametros para o deletar
                        java.util.HashMap params = new java.util.HashMap();
                        params.put("id", Kit.getId());
                        pageContext.setAttribute("delete", params);
            %>
            <jsp:setProperty name="Kit" property="id" value="<%= id%>"></jsp:setProperty>
            <jsp:setProperty name="Kit" property="descricao" value="<%= Kit.getDescricao()%>"></jsp:setProperty>
            <jsp:setProperty name="Kit" property="mercadoKits" value="<%= Kit.getMercadoKits()%>"></jsp:setProperty>
            <jsp:setProperty name="Kit" property="kiqs" value="<%= Kit.getKiqs()%>"></jsp:setProperty>
            <jsp:setProperty name="Kit" property="atorKits" value="<%= Kit.getAtorKits()%>"></jsp:setProperty>
            <jsp:setProperty name="Kit" property="produtoKits" value="<%= Kit.getProdutoKits()%>"></jsp:setProperty>
            <jsp:setProperty name="Kit" property="fonteKits" value="<%= Kit.getFonteKits()%>"></jsp:setProperty>
            <%
                        if (vId != null && !vId.equals("0")) {
                            out.print("<div id=\"nomecont\" align=\"center\">" + resource.getString("label.editarKit") + "</div>");
                            out.print("Id: " + vId + "<br>");
                        } else {
                            out.print("<div id=\"nomecont\" align=\"center\">" + resource.getString("label.cadastrarKit") + "</div>");
                        }
            %>
            <form name="cadastrarKit" action="cadastrarKit.do" method="post" onSubmit="return validaForm()">
                <p id="textoLabel">
                    <bean:message key="label.usuarioLogado"/>:
                    <%
                                out.println((String) session.getAttribute("nome"));
                    %>
                </p>
                <%
                            String dominioSelecionado = String.valueOf(session.getAttribute("dominioSelecionado"));
                            int vdominioSelecionado = 0;
                            Dominio d = new Dominio();
                            if (dominioSelecionado != null && !dominioSelecionado.equals("")) {
                                vdominioSelecionado = Integer.parseInt(String.valueOf(dominioSelecionado));
                                d = seris.database.DAO.DominioDAO.consultarDominio(vdominioSelecionado);
                            }
                            if (vdominioSelecionado == 0) {
                %>
                <jsp:forward page="selecionarDominioCadastrarKit.jsp"/>
                <%        }
                %>
                <jsp:scriptlet>
                            session.setAttribute("Dominios", seris.database.DAO.DominioUsuarioDAO.consultarListaDominiosPorUsuario(usuario_id));
                </jsp:scriptlet>
                <jsp:useBean id="Dominios" scope="session" type="java.util.List"></jsp:useBean>
                <p id="textoLabel">
                    <bean:message key="label.dominio"/>: <% out.print(d.getNome());%>
                </p>
                <p id="textoLabel">
                    <bean:message key="label.kit"/>:
                    <input type="text" name="nomeKit"  size="60" maxlength="100" value="<jsp:getProperty name="Kit" property="descricao"></jsp:getProperty>"/>
                </p>
                <p><bean:message key="label.selecionarAtores"/>:<br>
                    <jsp:scriptlet>
                                session.setAttribute("Atores", seris.database.DAO.AtorDAO.consultarListaAtores());
                    </jsp:scriptlet>
                    <jsp:useBean id="Atores" scope="session" type="java.util.List"></jsp:useBean>
                    <%
                                for (int i = 0; i < Atores.size(); i++) {
                                    Ator vAtor = (Ator) Atores.get(i);
                                    boolean vChecagemAtor = seris.database.DAO.AtorDAO.consultarAtorKit(vAtor.getId(), id);
                                    String vChecked = "checked";
                                    if (vChecagemAtor == false) {
                                        vChecked = "";
                                    }
                                    out.print("<input type=\"checkbox\" name=\"ator\" value=\"" + vAtor.getId() + "\" " + vChecked + " />" + vAtor.getNome() + "<br>");
                                }
                    %>
                </p>
                <p><bean:message key="label.selecionarFontes"/>:<br>
                    <jsp:scriptlet>
                                session.setAttribute("Fontes", seris.database.DAO.FonteDAO.consultarListaFontes());
                    </jsp:scriptlet>
                    <jsp:useBean id="Fontes" scope="session" type="java.util.List"></jsp:useBean>
                    <%
                                for (int i = 0; i < Fontes.size(); i++) {
                                    Fonte vFonte = (Fonte) Fontes.get(i);
                                    boolean vChecagemFonte = seris.database.DAO.FonteDAO.consultarFonteKit(vFonte.getId(), id);
                                    String vChecked = "checked";
                                    if (vChecagemFonte == false) {
                                        vChecked = "";
                                    }
                                    out.print("<input type=\"checkbox\" name=\"fonte\" value=\"" + vFonte.getId() + "\" " + vChecked + " />" + vFonte.getNome() + "<br>");
                                }
                    %>
                </p>
                <p><bean:message key="label.selecionarProdutos"/>:<br>
                    <jsp:scriptlet>
                                session.setAttribute("Produtos", seris.database.DAO.ProdutoDAO.consultarListaProdutos());
                    </jsp:scriptlet>
                    <jsp:useBean id="Produtos" scope="session" type="java.util.List"></jsp:useBean>
                    <%
                                for (int i = 0; i < Produtos.size(); i++) {
                                    Produto vProduto = (Produto) Produtos.get(i);
                                    boolean vChecagemProduto = seris.database.DAO.ProdutoDAO.consultarProdutoKit(vProduto.getId(), id);
                                    String vChecked = "checked";
                                    if (vChecagemProduto == false) {
                                        vChecked = "";
                                    }
                                    out.print("<input type=\"checkbox\" name=\"produto\" value=\"" + vProduto.getId() + "\" " + vChecked + " />" + vProduto.getDescricao() + "<br>");
                                }
                    %>
                </p>
                <p><bean:message key="label.selecionarMercados"/>:<br>
                    <jsp:scriptlet>
                                session.setAttribute("Mercados", seris.database.DAO.MercadoDAO.consultarListaMercados());
                    </jsp:scriptlet>
                    <jsp:useBean id="Mercados" scope="session" type="java.util.List"></jsp:useBean>
                    <%
                                for (int i = 0; i < Mercados.size(); i++) {
                                    Mercado vMercado = (Mercado) Mercados.get(i);
                                    boolean vChecagemMercado = seris.database.DAO.MercadoDAO.consultarMercadoKit(vMercado.getId(), id);
                                    String vChecked = "checked";
                                    if (vChecagemMercado == false) {
                                        vChecked = "";
                                    }
                                    out.print("<input type=\"checkbox\" name=\"mercado\" value=\"" + vMercado.getId() + "\" " + vChecked + " />" + vMercado.getNome() + "<br>");
                                }
                    %>
                </p>
                <input type="hidden" name="task" value="save" />
                <p>&nbsp;</p><p><font color="blue">
                        <% if (request.getAttribute("message") != null) {
                                        out.print(request.getAttribute("message"));
                                    }
                        %>
                    </font></p>
                <input type="hidden" name="id" value="<jsp:getProperty name="Kit" property="id"></jsp:getProperty>" />
                <input type="hidden" name="task" value="save" /><br />
                <input type="submit" value="<bean:message key="label.salvar"/>" />
            </form>
            <br /><br />
            <html:link page="/cadastrarKit.do?task=novo"><bean:message key="label.novo"/></html:link>&nbsp<html:link page="/cadastrarKit.do?task=pesquisar"><bean:message key="label.pesquisar"/></html:link>
            <%
                        if (vTask != null) {
                            if (vTask.equals("pesquisar")) {
            %>
            <jsp:scriptlet>
                                            session.setAttribute("Kits", seris.database.DAO.KitDAO.consultarListaKitsPorDominiosUsuario(vdominioSelecionado));
            </jsp:scriptlet>
            <jsp:useBean id="Kits" scope="session" type="java.util.List"></jsp:useBean>
            <div id="demo">
                <table cellpadding="0" cellspacing="0" border="0" class="display" width="600px" id="datatable">
                    <thead>
                        <tr>
                            <th width="30px"><bean:message key="label.id"/></th>
                            <th width="540px"><bean:message key="label.nome"/></th>
                            <th width="30px"><bean:message key="label.editar"/></th>
                        </tr>
                    </thead>
                    <tbody>
                        <%
                                                        for (int i = 0; i < Kits.size(); i++) {
                                                            Kit vKit = (Kit) Kits.get(i);
                                                            out.print("<tr class='seris'><td>" + vKit.getId() + "</td><td>" + vKit.getDescricao() + "</td><td align='center'><a href='cadastrarKit.jsp?id=" + vKit.getId() + "'><img src='imagens/edit-icon.png' border='0'></a></td></tr>");
                                                        }
                        %>
                    </tbody>
                </table>
            </div>
            <%
                            }
                        }
            %>
            <!-- InstanceEndEditable -->
        </div>
        <%@ include file="rodape.jsp" %>
    </body>
    <!-- InstanceEnd --></html>