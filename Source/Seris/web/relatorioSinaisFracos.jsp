<%--
    Document   : relatorioSinaisFracos
    Created on : jul/2010
    Author     : Ricardo C Bull - rcbull@gmail.com
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><!-- InstanceBegin template="/Templates/tempateSeris.dwt.jsp" codeOutsideHTMLIsLocked="false" -->
    <!-- InstanceBeginEditable name="taglibEimport" -->
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
    <%@page import="seris.database.Kit" %>
    <%@page import="seris.database.Ator" %>
    <%@page import="seris.database.Fonte" %>
    <%@page import="seris.database.Produto" %>
    <%@page import="seris.database.Mercado" %>
    <%@page import="seris.database.Dominio" %>
    <%@page import="seris.database.Avaliacao" %>
    <%@page import="seris.database.Usuario" %>
    <%@page import="seris.database.Departamento" %>
    <%@page import="seris.database.Grupo" %>
    <%@page import="seris.database.Perfil" %>
    <!-- InstanceEndEditable -->

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso8859-1" />
        <%
                    response.setHeader("Cache-Control", "no-cache");
                    response.setHeader("Pragma", "no-cache");
                    response.setDateHeader("Expires", 0);
        %>
        <!-- InstanceBeginEditable name="doctitle" -->
        <title>Seris</title>
        <!-- InstanceEndEditable -->
        <link REL="SHORTCUT ICON" HREF="favicon2.ico">
        <style type="text/css" title="currentStyle">
            @import "media/css/demo_page.css";
            @import "media/css/demo_table.css";
        </style>
        <script type="text/javascript" language="javascript" src="media/js/jquery.js"></script>
        <script type="text/javascript" language="javascript" src="media/js/jquery.dataTables.js"></script>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function() {
                $('#datatable').dataTable( {
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": false,
                    "bAutoWidth": false } );
            } );
            jQuery.fn.dataTableExt.oSort['string-asc']  = function(a,b) {
                var x = a.toLowerCase();
                var y = b.toLowerCase();
                return x.localeCompare(y);
            };

            jQuery.fn.dataTableExt.oSort['string-desc'] = function(a,b) {
                var x = a.toLowerCase();
                var y = b.toLowerCase();
                return y.localeCompare(x);
            };
        </script>
        <link REL="SHORTCUT ICON" HREF="favicon2.ico">
        <link rel="stylesheet" type="text/css" href="seris.css">
        <!-- InstanceBeginEditable name="head" -->
        <script type="text/javascript">
            function format(value,data)
            {
                value = value.replace(/\D/g,"");
                var result="";
                if(data.length < value.length)
                    return value;
                for(i=0,j=0;(i<data.length)&&(j<value.length);i++)
                {
                    var ch = data.charAt(i);
                    if(ch == '#')
                    {
                        result += value.charAt(j++);
                        continue;
                    }
                    result += ch;
                }
                return result;
            }
        </script>
        <!-- InstanceEndEditable -->
    </head>

    <body>
        <%@ include file="cabecalho.jsp" %>
        <%@ include file="menu.jsp" %>
        <div id="conteudo">
            <!-- InstanceBeginEditable name="conteudo" -->
            <%
                        //pega o usuario_id da sess?o
                        String vusuario_id = String.valueOf(session.getAttribute("usuario_id"));
                        int usuario_id = 0;
                        if (vusuario_id != null && !vusuario_id.equals("")) {
                            usuario_id = Integer.parseInt(vusuario_id);
                        }
            %>
            <div id="nomecont" align="center"><bean:message key="label.consultaSinaisFracos"/></div>
            <p id="textoLabel">
                <bean:message key="label.usuarioLogado"/>:
                <%
                            out.println((String) session.getAttribute("nome"));
                %>
            </p>
            <%
                        String dominioSelecionado = String.valueOf(session.getAttribute("dominioSelecionado"));
                        int vdominioSelecionado = 0;
                        Dominio d = new Dominio();
                        if (dominioSelecionado != null && !dominioSelecionado.equals("")) {
                            vdominioSelecionado = Integer.parseInt(String.valueOf(dominioSelecionado));
                            d = seris.database.DAO.DominioDAO.consultarDominio(vdominioSelecionado);
                        }
                        if (vdominioSelecionado == 0) {
            %>
            <jsp:forward page="selecionarDominioRelatorioSinaisFracos.jsp"/>
            <%        }
                        String tipoConsulta = String.valueOf(session.getAttribute("tipoConsulta"));
                        int vtipoConsulta = 0;
                        if (tipoConsulta != null && !tipoConsulta.equals("")) {
                            vtipoConsulta = Integer.parseInt(String.valueOf(tipoConsulta));
                        }
                        if (vtipoConsulta == 0) {
            %>
            <jsp:forward page="selecionarTipoRelatorioSinaisFracos.jsp"/>
            <%            }
            %>
            <jsp:scriptlet>
                        session.setAttribute("Dominios", seris.database.DAO.DominioUsuarioDAO.consultarListaDominiosPorUsuario(usuario_id));
            </jsp:scriptlet>
            <jsp:useBean id="Dominios" scope="session" type="java.util.List"></jsp:useBean>
            <p id="textoLabel">
                <bean:message key="label.dominio"/>: <% out.print(d.getNome());%>
            </p>
            <%
                        //
                        // Consulta para cria??o de sentido
                        //
                        if (vtipoConsulta == 1) {
                            String kitSelecionado = String.valueOf(session.getAttribute("kitSelecionado"));
                            int vkitSelecionado = 0;
                            Kit k = new Kit();
                            if (kitSelecionado != null && !kitSelecionado.equals("")) {
                                vkitSelecionado = Integer.parseInt(String.valueOf(kitSelecionado));
                                k = seris.database.DAO.KitDAO.consultarKit(vkitSelecionado);
                            }
                            if (vkitSelecionado == 0) {
            %>
            <jsp:forward page="selecionarKitRelatorioSinaisFracos.jsp"/>
            <%            }
            %>
            <p id="textoLabel">
                <bean:message key="label.kit"/>: <% out.print(k.getDescricao());%>
            </p>
            <p id="textoLabel"><bean:message key="label.consultaCriacaoSentido"/></p>
            <form action="relatorioSinaisFracos.do" method="post" name="relatorioSinaisFracos" target="_blanck">
                <p><bean:message key="label.dataInicio"/>: <input type="text" name="dataInicio" size="10" onkeyup="this.value=format(this.value,'##/##/####');" value="<%= seris.utils.Utils.getDataSistema()%>"/> DD/MM/AAAA</p>
                <p><bean:message key="label.dataTermino"/>: <input type="text" name="dataFinal" size="10" onkeyup="this.value=format(this.value,'##/##/####');" value="<%= seris.utils.Utils.getDataSistema()%>"/> DD/MM/AAAA</p>
                <p><bean:message key="label.relevancia"/>:<br>
                    <jsp:scriptlet>
                                                session.setAttribute("Avaliacoes", seris.database.DAO.AvaliacaoDAO.consultarListaAvaliacoes());
                    </jsp:scriptlet>
                    <jsp:useBean id="Avaliacoes" scope="session" type="java.util.List"></jsp:useBean>
                    <select name="avaliacao">
                        <%
                                                    out.print("<option value=\"0\" />--</option>");
                                                    for (int i = 0; i < Avaliacoes.size(); i++) {
                                                        Avaliacao vAvaliacao = (Avaliacao) Avaliacoes.get(i);
                                                        out.print("<option value=\"" + vAvaliacao.getId() + "\" />" + vAvaliacao.getRelevancia() + "</option>");
                                                    }
                        %>
                    </select>
                <p><bean:message key="label.selecionarAtor"/>:<br>
                    <jsp:scriptlet>
                                                session.setAttribute("Atores", seris.database.DAO.AtorDAO.consultarListaAtoresPorKit(kitSelecionado));
                    </jsp:scriptlet>
                    <jsp:useBean id="Atores" scope="session" type="java.util.List"></jsp:useBean>
                    <select name="ator">
                        <%
                                                    out.print("<option value=\"0\" />--</option>");
                                                    for (int i = 0; i < Atores.size(); i++) {
                                                        Ator vAtor = (Ator) Atores.get(i);
                                                        out.print("<option value=\"" + vAtor.getId() + "\" />" + vAtor.getNome() + "</option>");
                                                    }
                        %>
                    </select>
                </p>
                <p><bean:message key="label.selecionarFonte"/>:<br>
                    <jsp:scriptlet>
                                                session.setAttribute("Fontes", seris.database.DAO.FonteDAO.consultarListaFontesPorKit(kitSelecionado));
                    </jsp:scriptlet>
                    <jsp:useBean id="Fontes" scope="session" type="java.util.List"></jsp:useBean>
                    <select name="fonte">
                        <%
                                                    out.print("<option value=\"0\" />--</option>");
                                                    for (int i = 0; i < Fontes.size(); i++) {
                                                        Fonte vFonte = (Fonte) Fontes.get(i);
                                                        out.print("<option value=\"" + vFonte.getId() + "\" />" + vFonte.getNome() + "</option>");
                                                    }
                        %>
                    </select>
                </p>
                <p><bean:message key="label.selecionarProduto"/>:<br>
                    <jsp:scriptlet>
                                                session.setAttribute("Produtos", seris.database.DAO.ProdutoDAO.consultarListaProdutosPorKit(kitSelecionado));
                    </jsp:scriptlet>
                    <jsp:useBean id="Produtos" scope="session" type="java.util.List"></jsp:useBean>
                    <select name="produto">
                        <%
                                                    out.print("<option value=\"0\" />--</option>");
                                                    for (int i = 0; i < Produtos.size(); i++) {
                                                        Produto vProduto = (Produto) Produtos.get(i);
                                                        out.print("<option value=\"" + vProduto.getId() + "\" />" + vProduto.getDescricao() + "</option>");
                                                    }
                        %>
                    </select>
                </p>
                <p><bean:message key="label.selecionarMercado"/>:<br>
                    <jsp:scriptlet>
                                                session.setAttribute("Mercados", seris.database.DAO.MercadoDAO.consultarListaMercadosPorKit(kitSelecionado));
                    </jsp:scriptlet>
                    <jsp:useBean id="Mercados" scope="session" type="java.util.List"></jsp:useBean>
                    <select name="mercado">
                        <%
                                                    out.print("<option value=\"0\" />--</option>");
                                                    for (int i = 0; i < Mercados.size(); i++) {
                                                        Mercado vMercado = (Mercado) Mercados.get(i);
                                                        out.print("<option value=\"" + vMercado.getId() + "\" />" + vMercado.getNome() + "</option>");
                                                    }
                        %>
                    </select>
                </p>
                <p><bean:message key="label.selecionarCaptador"/>:<br>
                    <jsp:scriptlet>
                                                session.setAttribute("Usuarios", seris.database.DAO.UsuarioDAO.consultarListaUsuarios());
                    </jsp:scriptlet>
                    <jsp:useBean id="Usuarios" scope="session" type="java.util.List"></jsp:useBean>
                    <select name="usuario">
                        <%
                                                    out.print("<option value=\"0\" />--</option>");
                                                    for (int i = 0; i < Usuarios.size(); i++) {
                                                        Usuario vUsuario = (Usuario) Usuarios.get(i);
                                                        out.print("<option value=\"" + vUsuario.getId() + "\" />" + vUsuario.getNome() + "</option>");
                                                    }
                        %>
                    </select>
                </p>
                <input type="submit" value="<bean:message key="label.confirmar"/>">
            </form>
            <%
                                    } else if (vtipoConsulta == 2) {
            %>
            <p id="textoLabel"><bean:message key="label.consultaDistribuicaoSF"/></p>
            <form action="relatorioSinaisFracos.do" method="post" name="relatorioSinaisFracos" target="_blanck">
                <p><bean:message key="label.dataInicio"/>: <input type="text" name="dataInicio" size="10" onkeyup="this.value=format(this.value,'##/##/####');" value="<%= seris.utils.Utils.getDataSistema()%>"/> DD/MM/AAAA</p>
                <p><bean:message key="label.dataTermino"/>: <input type="text" name="dataFinal" size="10" onkeyup="this.value=format(this.value,'##/##/####');" value="<%= seris.utils.Utils.getDataSistema()%>"/> DD/MM/AAAA</p>
                <p><bean:message key="label.selecionarCaptador"/>:<br>
                    <jsp:scriptlet>
                                                session.setAttribute("Usuariost2", seris.database.DAO.DominioUsuarioDAO.consultarListaUsuariosPorDominio(vdominioSelecionado));
                    </jsp:scriptlet>
                    <jsp:useBean id="Usuariost2" scope="session" type="java.util.List"></jsp:useBean>
                    <select name="usuario">
                        <%
                                                    out.print("<option value=\"0\" />--</option>");
                                                    for (int i = 0; i < Usuariost2.size(); i++) {
                                                        Usuario vUsuario = (Usuario) Usuariost2.get(i);
                                                        out.print("<option value=\"" + vUsuario.getId() + "\" />" + vUsuario.getNome() + "</option>");
                                                    }
                        %>
                    </select>
                </p>
                <p>
                    <bean:message key="label.selecionarKit"/>:
                    <jsp:scriptlet>
                                                session.setAttribute("Kits", seris.database.DAO.KitDAO.consultarListaKitsPorDominiosUsuario(vdominioSelecionado));
                    </jsp:scriptlet>
                    <jsp:useBean id="Kits" scope="session" type="java.util.List"></jsp:useBean>
                    <select name="kitSelecionado">
                        <%
                                                    out.print("<option value=\"0\" />--</option>");
                                                    for (int i = 0; i < Kits.size(); i++) {
                                                        Kit vKit = (Kit) Kits.get(i);
                                                        out.print("<option value=\"" + vKit.getId() + "\">" + vKit.getDescricao() + "</option>");
                                                    }
                        %>
                    </select>
                </p>
                <p><bean:message key="label.perfil"/>:
                    <jsp:scriptlet>
                                                session.setAttribute("Perfis", seris.database.DAO.PerfilDAO.consultarListaPerfis());
                    </jsp:scriptlet>
                    <jsp:useBean id="Perfis" scope="session" type="java.util.List"></jsp:useBean>
                    <select name="perfil">
                        <%
                                                    out.print("<option value=\"0\" />--</option>");
                                                    for (int i = 0; i < Perfis.size(); i++) {
                                                        Perfil vPerfil = (Perfil) Perfis.get(i);
                                                        out.print("<option value=\"" + vPerfil.getId() + "\">" + vPerfil.getNome() + "</option>");
                                                    }
                        %>
                    </select>
                </p>
                <p><bean:message key="label.grupo"/>
                    <jsp:scriptlet>
                                                session.setAttribute("Grupos", seris.database.DAO.GrupoDAO.consultarListaGrupos());
                    </jsp:scriptlet>
                    <jsp:useBean id="Grupos" scope="session" type="java.util.List"></jsp:useBean>
                    <select name="grupo">
                        <%
                                                    out.print("<option value=\"0\" />--</option>");
                                                    for (int i = 0; i < Grupos.size(); i++) {
                                                        Grupo vGrupo = (Grupo) Grupos.get(i);
                                                        out.print("<option value=\"" + vGrupo.getId() + "\">" + vGrupo.getNome() + "</option>");
                                                    }
                        %>
                    </select>
                </p>
                <p><bean:message key="label.departamento"/>:
                    <jsp:scriptlet>
                                                session.setAttribute("Departamentos", seris.database.DAO.DepartamentoDAO.consultarListaDepartamentos());
                    </jsp:scriptlet>
                    <jsp:useBean id="Departamentos" scope="session" type="java.util.List"></jsp:useBean>
                    <select name="departamento">
                        <%
                                                    out.print("<option value=\"0\" />--</option>");
                                                    for (int i = 0; i < Departamentos.size(); i++) {
                                                        Departamento vDepartamento = (Departamento) Departamentos.get(i);
                                                        out.print("<option value=\"" + vDepartamento.getId() + "\">" + vDepartamento.getNome() + "</option>");
                                                    }
                        %>
                    </select>
                </p>
                <input type="submit" value="<bean:message key="label.confirmar"/>">
            </form>
            <%
                                    }
            %>

            <!-- InstanceEndEditable -->
        </div>
        <%@ include file="rodape.jsp" %>
    </body>
    <!-- InstanceEnd --></html>
