<%--
    Document   : selecionarKit
    Created on : jul/2010
    Author     : Ricardo C Bull - rcbull@gmail.com
--%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html><!-- InstanceBegin template="/Templates/tempateSeris.dwt.jsp" codeOutsideHTMLIsLocked="false" -->
    <!-- InstanceBeginEditable name="taglibEimport" -->
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
    <%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>
    <%@page import="seris.database.Kit" %>
    <!-- InstanceEndEditable -->

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso8859-1" />
        <%
                    response.setHeader("Cache-Control", "no-cache");
                    response.setHeader("Pragma", "no-cache");
                    response.setDateHeader("Expires", 0);
        %>
        <!-- InstanceBeginEditable name="doctitle" -->
        <title>Seris</title>
        <!-- InstanceEndEditable -->
        <link REL="SHORTCUT ICON" HREF="favicon2.ico">
        <style type="text/css" title="currentStyle">
            @import "media/css/demo_page.css";
            @import "media/css/demo_table.css";
        </style>
        <script type="text/javascript" language="javascript" src="media/js/jquery.js"></script>
        <script type="text/javascript" language="javascript" src="media/js/jquery.dataTables.js"></script>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function() {
                $('#datatable').dataTable( {
                    "bPaginate": true,
                    "bLengthChange": false,
                    "bFilter": true,
                    "bSort": true,
                    "bInfo": false,
                    "bAutoWidth": false } );
            } );
            jQuery.fn.dataTableExt.oSort['string-asc']  = function(a,b) {
                var x = a.toLowerCase();
                var y = b.toLowerCase();
                return x.localeCompare(y);
            };

            jQuery.fn.dataTableExt.oSort['string-desc'] = function(a,b) {
                var x = a.toLowerCase();
                var y = b.toLowerCase();
                return y.localeCompare(x);
            };
        </script>
        <link REL="SHORTCUT ICON" HREF="favicon2.ico">
        <link rel="stylesheet" type="text/css" href="seris.css">
        <!-- InstanceBeginEditable name="head" -->
        <script type="text/javascript">
            function validaForm(){
                d = document.selecionarKit;
                if(d.kitSelecionado.value ==0){
                    alert("<bean:message key="message.selecionarKitRelatorio"/>");
                    d.kitSelecionado.focus();
                    return false;
                }
                return true;
            }
        </script>
        <!-- InstanceEndEditable -->
    </head>

    <body>
        <%@ include file="cabecalho.jsp" %>
        <%@ include file="menu.jsp" %>
        <div id="conteudo">
            <!-- InstanceBeginEditable name="conteudo" -->
            <p><b><bean:message key="label.cadastroSinalFracoKit"/>:</b></p>
            <%
                        String dominioSelecionado = String.valueOf(request.getParameter("dominioSelecionado"));
                        int vdominioSelecionado = 0;
                        if (dominioSelecionado != null && !dominioSelecionado.equals("")) {
                            vdominioSelecionado = Integer.parseInt(String.valueOf(dominioSelecionado));
                        }
                        if (vdominioSelecionado == 0) {
            %>
            <jsp:forward page="selecionarDominio.jsp"/>
            <%        }
            %>
            <jsp:scriptlet>
                        session.setAttribute("Kits", seris.database.DAO.KitDAO.consultarListaKitsPorDominiosUsuario(vdominioSelecionado));
            </jsp:scriptlet>
            <jsp:useBean id="Kits" scope="session" type="java.util.List"></jsp:useBean>
            <form name="selecionarKit" action="selecionarKit.do" method="post" onsubmit="return validaForm()">
                <br><bean:message key="label.selecioneKit"/>:</br>
                <select name="kitSelecionado">
                    <%
                                out.print("<option value=\"0\" />--</option>");
                                for (int i = 0; i < Kits.size(); i++) {
                                    Kit vKit = (Kit) Kits.get(i);
                                    out.print("<option value=\"" + vKit.getId() + "\"> " + vKit.getDescricao() + "</option><br>");
                                }
                    %>
                </select>
                <input type="submit" value="<bean:message key="label.selecionar"/>">
            </form>
            <!-- InstanceEndEditable -->
        </div>
        <%@ include file="rodape.jsp" %>
    </body>
    <!-- InstanceEnd --></html>