<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://jakarta.apache.org/struts/tags-bean" prefix="bean" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-html" prefix="html" %>
<%@ taglib uri="http://jakarta.apache.org/struts/tags-logic" prefix="logic" %>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=iso8859-1">
        <title>Seris</title>
        <%
            response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Pragma", "no-cache");
            response.setDateHeader("Expires", 0);
            
            /*
             function flyCookie(name,value,days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime()+(days*24*60*60*1000));
        var expires = "; expires="+date.toGMTString();
    }else{
        var expires = "";
    }
    document.cookie = name+"="+value+expires+"; path=/";
}

function lerFlyCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while(c.charAt(0)==' '){
            c = c.substring(1,c.length);
        }
        if(c.indexOf(nameEQ) == 0){
            return c.substring(nameEQ.length,c.length);
        }
    }
    return null;
}
            */
            
            String language = request.getParameter("language");
            if (language == null || language.isEmpty()) {
                language = "SerisIdiomaPt";
            }
            request.getSession().setAttribute("language", language);
        %>
        <link REL="SHORTCUT ICON" HREF="favicon2.ico">
        <link rel="stylesheet" type="text/css" href="seris.css">
    </head>
    <body>
        <div id="login" align="center">
            <table width="100%" height="130" cellpadding="0" cellspacing="0" border="0">
                <td width="160" valign="top">
                    <div id="divLogoEmpresa">
                        <span style="position: relative; top: 50px; left: -8px; color: #d69d92; font-weight: bold; font-family: verdana; font-size: 11px;">
                            Versão <bean:message bundle="<%=((String) request.getSession().getAttribute("language"))%>" key="label.versao"/>
                        </span>
                        <span style="position: relative; top: 65px; left: 80px; color: #b03432; font-weight: bold; font-family: verdana; font-size: 18px;">
                            <i><bean:message bundle="<%=((String) request.getSession().getAttribute("language"))%>" key="label.tema"/></i>
                        </span>
                    </div>
                </td>
                <td style="background: url('img/logocentro.png') no-repeat center; text-align: center;" valign="middle">
                    <table border="0" align="center" height="60">
                        <tr>
                            <td width="160" valign="top" style="font-size: 14px; color: #ffffff;"><b><bean:message bundle="<%=((String) request.getSession().getAttribute("language"))%>" key="label.inteligencia"/></b></td>
                            <td width="160" valign="top" style="font-size: 14px;"><b><bean:message bundle="<%=((String) request.getSession().getAttribute("language"))%>" key="label.antecipativa"/></b></td>
                        </tr>
                        <tr>
                            <td valign="bottom" style="font-size: 14px;"><b><bean:message bundle="<%=((String) request.getSession().getAttribute("language"))%>" key="label.conhecimento"/></b></td>
                            <td valign="bottom" style="font-size: 14px; color: #ffffff;"><b><bean:message bundle="<%=((String) request.getSession().getAttribute("language"))%>" key="label.coletivo"/></b></td>
                        </tr>
                    </table>
                </td>
                <td width="250" valign="top" style="background: url('img/direitatopo.png') no-repeat left;" >
                    <div id="divLogoCliente">&nbsp;</div>
                </td>
            </table>
            <div style="background: url('img/barralogin.png'); height: 32px;">&nbsp;</div>

            <div style="text-align: right;">
                <a href="?language=SerisIdiomaPt"><img src="img/idiomas/pt.png" style="border: 0px; height: 20px;"></a>
                <a href="?language=SerisIdiomaEn"><img src="img/idiomas/en.png" style="border: 0px; height: 20px;"></a>
                <a href="?language=SerisIdiomaFr"><img src="img/idiomas/fr.png" style="border: 0px; height: 20px;"></a>
                <a href="?language=SerisIdiomaDe"><img src="img/idiomas/de.png" style="border: 0px; height: 20px;"></a>
                &nbsp;&nbsp;&nbsp;
            </div>
            
            <br>

            <div style="background: url('img/centrologin.png'); width: 780px; height: 480px; text-align: left;">

                <span style="position: relative; top: 16px; left: 625px; font-weight: bold; font-family: verdana; font-size: 14px; color: #ffffff;">
                    <bean:message bundle="<%=((String) request.getSession().getAttribute("language"))%>" key="label.inteligenciaCA"/>
                </span><br>
                <span style="position: relative; top: 226px; left: 44px; font-weight: bold; font-family: verdana; font-size: 14px; color: #ffffff;">
                    <bean:message bundle="<%=((String) request.getSession().getAttribute("language"))%>" key="label.captacaoCA"/>
                </span><br>
                <span style="position: relative; top: 398px; left: 519px; font-weight: bold; font-family: verdana; font-size: 14px; color: #ffffff;">
                    <bean:message bundle="<%=((String) request.getSession().getAttribute("language"))%>" key="label.conhecimentoCA"/>
                </span>

                <div id="divLogin">
                    <html:form action="efetuarLogin.do?task=login">
                        <bean:message bundle="<%=((String) request.getSession().getAttribute("language"))%>" key="label.usuarioEntrar"/>: <html:text name="EfetuarLoginActionForm" property="login" />
                        <br>
                        <bean:message bundle="<%=((String) request.getSession().getAttribute("language"))%>" key="label.senhaEntrar"/>: <html:password name="EfetuarLoginActionForm" property="senha" />
                        <br>
                        <%
                            java.util.ResourceBundle resource = java.util.ResourceBundle.getBundle("seris." + request.getSession().getAttribute("language"));
                        %>
                        <html:submit property="entrar" value="<%=resource.getString("label.conectar")%>" />
                    </html:form>
                    <br>
                    <html:errors/>
                    <br>
                    <logic:present name="EfetuarLoginActionForm" property="mensagem">
                        <bean:write name="EfetuarLoginActionForm" property="mensagem"/>
                    </logic:present>
                    <img src="img/logoseris.png" width="100">
                </div>

            </div>
            <table width="98%" align="center" cellpadding="0" cellspacing="0">
                <td style="font-family: verdana; font-size: 10px; color: #b62900;" valign="bottom">
                    <bean:message bundle="<%=((String) request.getSession().getAttribute("language"))%>" key="label.direitosReservados"/>
                </td>
                <td align="right">
                    <img src="img/logoseris.png" width="100">
                </td>
            </table>


        </div>
    </body>
</html>
