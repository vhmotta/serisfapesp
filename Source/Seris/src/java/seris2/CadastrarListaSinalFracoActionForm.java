package seris2;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class CadastrarListaSinalFracoActionForm
        extends ActionForm {

    private int id = 0;
    private int idSinalFraco = 0;
    private int kitSelecionado;
    private int atorId;
    private int produtoId;
    private int mercadoId;
    private int fonteId;
    private String task = "";
    private String nome;
    private String data;
    private String dataInicial;
    private String dataFinal;
    private int listaUsuarioId = 0;
    private int participanteId = 0;
    private int participanteIdOutros = 0;

    private String funcaoParticipante;

    private List listaSinalFraco;

    public CadastrarListaSinalFracoActionForm() {
    }

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        return errors;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdSinalFraco() {
        return idSinalFraco;
    }

    public void setIdSinalFraco(int idSinalFraco) {
        this.idSinalFraco = idSinalFraco;
    }

    public int getKitSelecionado() {
        return kitSelecionado;
    }

    public void setKitSelecionado(int kitSelecionado) {
        this.kitSelecionado = kitSelecionado;
    }

    public int getAtorId() {
        return atorId;
    }

    public void setAtorId(int atorId) {
        this.atorId = atorId;
    }

    public int getProdutoId() {
        return produtoId;
    }

    public void setProdutoId(int produtoId) {
        this.produtoId = produtoId;
    }

    public int getMercadoId() {
        return mercadoId;
    }

    public void setMercadoId(int mercadoId) {
        this.mercadoId = mercadoId;
    }

    public int getFonteId() {
        return fonteId;
    }

    public void setFonteId(int fonteId) {
        this.fonteId = fonteId;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getDataInicial() {
        return dataInicial;
    }

    public void setDataInicial(String dataInicial) {
        this.dataInicial = dataInicial;
    }

    public String getDataFinal() {
        return dataFinal;
    }

    public void setDataFinal(String dataFinal) {
        this.dataFinal = dataFinal;
    }

    public List getListaSinalFraco() {
        return listaSinalFraco;
    }

    public void setListaSinalFraco(List listaSinalFraco) {
        this.listaSinalFraco = listaSinalFraco;
    }

    public int getParticipanteId() {
        return participanteId;
    }

    public void setParticipanteId(int participanteId) {
        this.participanteId = participanteId;
    }

    public String getFuncaoParticipante() {
        return funcaoParticipante;
    }

    public void setFuncaoParticipante(String funcaoParticipante) {
        this.funcaoParticipante = funcaoParticipante;
    }

    public int getListaUsuarioId() {
        return listaUsuarioId;
    }

    public void setListaUsuarioId(int listaUsuarioId) {
        this.listaUsuarioId = listaUsuarioId;
    }

    public int getParticipanteIdOutros() {
        return participanteIdOutros;
    }

    public void setParticipanteIdOutros(int participanteIdOutros) {
        this.participanteIdOutros = participanteIdOutros;
    }
}
