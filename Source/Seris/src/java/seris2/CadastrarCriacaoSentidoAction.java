package seris2;

import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import seris.HibernateUtil;
import seris2.database.CriacaoSentido;
import seris2.database.CriacaoSentidoGraficos;
import seris2.database.DAO.CriacaoSentidoDAO;
import seris2.database.DAO.CriacaoSentidoGraficosDAO;

public class CadastrarCriacaoSentidoAction
        extends DispatchAction {

    private static final String SUCCESS = "success";
    private static final String FAILURE = "failure";
    private static final String GRAFICO = "grafico";
    private ResourceBundle resource = ResourceBundle.getBundle("seris.SerisIdioma");

    public CadastrarCriacaoSentidoAction() {
    }

    public ActionForward save(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        int vNivel = -1;
        if (request.getSession().getAttribute("nivel") != null) {
            vNivel = Integer.parseInt(String.valueOf(request.getSession().getAttribute("nivel")));
        }
        if (vNivel == 5) {
            CadastrarCriacaoSentidoActionForm formBean = (CadastrarCriacaoSentidoActionForm) form;

            if ((formBean.getNome() != null) && (!formBean.getNome().equals(""))) {
                String vusuario_id = String.valueOf(request.getSession().getAttribute("usuario_id"));

                DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                Date data = formatter.parse(formBean.getData());

                CriacaoSentido criacaoSentido = new CriacaoSentido();
                criacaoSentido.setNome(formBean.getNome());
                criacaoSentido.setData(data);
                criacaoSentido.setEmpresa(formBean.getEmpresa());
                criacaoSentido.setListaSinalFracoId(Integer.valueOf(formBean.getListaId()));
                criacaoSentido.setLugar(formBean.getLugar());
                criacaoSentido.setLocalId(Integer.valueOf(formBean.getLocalId()));
                criacaoSentido.setUsuarioId(new Integer(vusuario_id));
                criacaoSentido.setStatus(Character.valueOf('A'));

                Session session = HibernateUtil.getSession();
                Transaction transaction = session.beginTransaction();
                transaction.begin();
                if (formBean.getId() > 0) {
                    CriacaoSentido criacaosentidobd = CriacaoSentidoDAO.consultarCriacaoSentido(formBean.getId());

                    if (criacaosentidobd.getStatus().charValue() != 'E') {
                        List vList = session.createQuery("from CriacaoSentido c where c.id <> " + formBean.getId() + " and c.nome = '" + formBean.getNome() + "'").list();

                        if (vList.size() == 0) {
                            criacaoSentido.setId(Integer.valueOf(formBean.getId()));
                            criacaoSentido.setGrafico(criacaosentidobd.getGrafico());
                            session.update(criacaoSentido);
                        } else {
                            transaction.rollback();
                            session.flush();
                            session.close();
                            request.setAttribute("message", resource.getString("message.criacaoSentidoExistente"));
                            return mapping.findForward("failure");
                        }
                    } else {
                        transaction.rollback();
                        session.flush();
                        session.close();
                        request.setAttribute("message", resource.getString("message.criacaoSentidoEncerrada"));
                        return mapping.findForward("failure");
                    }
                } else {
                    List vList = session.createQuery("from CriacaoSentido c where c.nome = '" + formBean.getNome() + "'").list();

                    if (vList.size() == 0) {
                        session.save(criacaoSentido);
                    } else {
                        transaction.rollback();
                        session.flush();
                        session.close();
                        request.setAttribute("message", resource.getString("message.criacaoSentidoExistente"));
                        return mapping.findForward("failure");
                    }
                }
                transaction.commit();
                session.flush();
                session.close();
            } else {
                request.setAttribute("message", resource.getString("message.infoFormInvalida"));
                return mapping.findForward("failure");
            }
            request.setAttribute("message", resource.getString("message.salvoSucesso"));
        } else {
            request.setAttribute("message", resource.getString("message.usuarioSemPermissao"));
        }

        return mapping.findForward("success");
    }

    public ActionForward delete(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        try {
            int vNivel = -1;

            if (request.getSession().getAttribute("nivel") != null) {
                vNivel = Integer.parseInt(String.valueOf(request.getSession().getAttribute("nivel")));
            }

            if (vNivel == 5) {
                CadastrarCriacaoSentidoActionForm formBean = (CadastrarCriacaoSentidoActionForm) form;

                if (formBean.getId() > 0) {
                    Session session = HibernateUtil.getSession();
                    Transaction transaction = session.beginTransaction();
                    CriacaoSentido criacaoSentido = (CriacaoSentido) session.createQuery("from CriacaoSentido where id=" + formBean.getId()).list().iterator().next();
                    session.delete(criacaoSentido);
                    transaction.commit();
                    session.flush();
                    session.close();

                    formBean.setId(0);
                    formBean.setNome(null);
                }

                request.setAttribute("message", resource.getString("message.deletadoSucesso"));
            } else {
                request.setAttribute("message", resource.getString("message.usuarioSemPermissao"));
            }
            return mapping.findForward("success");
        } catch (Exception ex) {
            request.setAttribute("message", resource.getString("message.falhaDeletar"));
        }
        return mapping.findForward("failure");
    }

    public ActionForward novo(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        return mapping.findForward("success");
    }

    public ActionForward pesquisar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        return mapping.findForward("success");
    }

    public ActionForward salvargrafico(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");

        System.out.println("SalvarGrafico INICIO (" + sdf.format(new Date()) + ") ###########");

        int vNivel = -1;
        if (request.getSession().getAttribute("nivel") != null) {
            vNivel = Integer.parseInt(String.valueOf(request.getSession().getAttribute("nivel")));
        }
        if (vNivel == 5) {
            System.out.println("SalvarGrafico NIVEL APROVADO ###########");

            CadastrarCriacaoSentidoActionForm formBean = (CadastrarCriacaoSentidoActionForm) form;

            if (formBean.getId() > 0) {
                System.out.println("SalvarGrafico FORMULARIO APROVADO ###########");

                CriacaoSentido criacaoSentido = CriacaoSentidoDAO.consultarCriacaoSentido(formBean.getId());

                if (criacaoSentido.getStatus().charValue() != 'E') {
                    System.out.println("SalvarGrafico STATUS APROVADO ###########");

                    String showmsg = request.getParameter("showmsg");

                    String grafico = formBean.getGrafico();
                    grafico = grafico.substring(0, grafico.indexOf("</LOADOBJECT>"));

                    if ((grafico.indexOf("</object>") > 0) || ((showmsg != null) && (showmsg.equals("TRUE")))) {
                        System.out.println("SalvarGrafico VALOR APROVADO ###########");

                        String salvarcomo = request.getParameter("salvarcomo");

                        Session session = HibernateUtil.getSession();
                        Transaction transaction = session.beginTransaction();
                        transaction.begin();

                        if ((salvarcomo != null) && (salvarcomo.equals("TRUE"))) {
                            String nomegrafico = request.getParameter("nomegrafico");

                            if ((nomegrafico != null) && (!nomegrafico.equals(""))) {
                                List list = CriacaoSentidoGraficosDAO.consultarListaCriacaoSentidoByNome(nomegrafico);

                                if (list.size() > 0) {
                                    System.out.println("SalvarGrafico Nome já existente ###########");
                                    request.setAttribute("message", "Nome do gráfico já existente.");
                                    return mapping.findForward("grafico");
                                }
                                CriacaoSentidoGraficos criacaoSentidoGraficos = new CriacaoSentidoGraficos();
                                criacaoSentidoGraficos.setCriacaoSentidoId(criacaoSentido.getId());
                                criacaoSentidoGraficos.setData(new Date());
                                criacaoSentidoGraficos.setNome(nomegrafico);
                                criacaoSentidoGraficos.setGrafico(formBean.getGrafico());
                                session.save(criacaoSentidoGraficos);
                            } else {
                                System.out.println("SalvarGrafico Nome não definido ###########");
                                request.setAttribute("message", "Nome do gráfico não definido.");
                                return mapping.findForward("grafico");
                            }
                        } else {
                            String vGraficoId = request.getParameter("graficoId");

                            if ((vGraficoId != null) && (!vGraficoId.equals("")) && (!vGraficoId.equals("0"))) {
                                CriacaoSentidoGraficos criacaoSentidoGraficos = CriacaoSentidoGraficosDAO.consultarCriacaoSentidoGraficos(vGraficoId);
                                criacaoSentidoGraficos.setGrafico(formBean.getGrafico());
                                session.update(criacaoSentidoGraficos);
                            } else {
                                criacaoSentido.setGrafico(formBean.getGrafico());
                                session.update(criacaoSentido);
                            }
                        }

                        transaction.commit();
                        session.flush();
                        session.close();
                    } else {
                        System.out.println("SalvarGrafico VALOR NAO APROVADO ###########");
                        return mapping.findForward("grafico");
                    }
                } else {
                    System.out.println("SalvarGrafico JA ENCERRADA ###########");

                    request.setAttribute("message", resource.getString("message.criacaoSentidoEncerrada"));
                    return mapping.findForward("grafico");
                }
            } else {
                System.out.println("SalvarGrafico FORMULARIO NAO APROVADO ###########");

                request.setAttribute("message", resource.getString("message.infoFormInvalida"));
                return mapping.findForward("grafico");
            }

            System.out.println("SalvarGrafico SALVO SUCESSO ###########");

            request.setAttribute("message", resource.getString("message.salvoSucesso"));
        } else {
            System.out.println("SalvarGrafico SEM PERMISSAO ###########");

            request.setAttribute("message", resource.getString("message.usuarioSemPermissao"));
        }

        System.out.println("SalvarGrafico ENCERRAR ###########");

        return mapping.findForward("grafico");
    }

    public ActionForward finalizar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        int vNivel = -1;
        if (request.getSession().getAttribute("nivel") != null) {
            vNivel = Integer.parseInt(String.valueOf(request.getSession().getAttribute("nivel")));
        }
        if (vNivel == 5) {
            CadastrarCriacaoSentidoActionForm formBean = (CadastrarCriacaoSentidoActionForm) form;

            if (formBean.getId() > 0) {
                Session session = HibernateUtil.getSession();
                Transaction transaction = session.beginTransaction();
                transaction.begin();

                CriacaoSentido criacaosentido = CriacaoSentidoDAO.consultarCriacaoSentido(formBean.getId());

                if (criacaosentido.getStatus().charValue() != 'E') {
                    criacaosentido.setStatus(Character.valueOf('E'));
                    session.update(criacaosentido);
                } else {
                    transaction.rollback();
                    session.flush();
                    session.close();
                    request.setAttribute("message", resource.getString("message.criacaoSentidoEncerrada"));
                    return mapping.findForward("failure");
                }

                transaction.commit();
                session.flush();
                session.close();
            } else {
                request.setAttribute("message", resource.getString("message.selecioneCriacaoSentido"));
                return mapping.findForward("failure");
            }

            request.setAttribute("message", resource.getString("message.finalizadaSucesso"));
        } else {
            request.setAttribute("message", resource.getString("message.usuarioSemPermissao"));
            return mapping.findForward("failure");
        }

        return mapping.findForward("success");
    }

    public ActionForward finalizargrafico(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        int vNivel = -1;
        if (request.getSession().getAttribute("nivel") != null) {
            vNivel = Integer.parseInt(String.valueOf(request.getSession().getAttribute("nivel")));
        }
        if (vNivel == 5) {
            CadastrarCriacaoSentidoActionForm formBean = (CadastrarCriacaoSentidoActionForm) form;

            if (formBean.getId() > 0) {
                Session session = HibernateUtil.getSession();
                Transaction transaction = session.beginTransaction();
                transaction.begin();

                CriacaoSentido criacaosentido = CriacaoSentidoDAO.consultarCriacaoSentido(formBean.getId());

                if (criacaosentido.getStatus().charValue() != 'E') {
                    criacaosentido.setStatus(Character.valueOf('E'));
                    session.update(criacaosentido);
                } else {
                    transaction.rollback();
                    session.flush();
                    session.close();
                    request.setAttribute("message", resource.getString("message.criacaoSentidoEncerrada"));
                    return mapping.findForward("failure");
                }

                transaction.commit();
                session.flush();
                session.close();
            } else {
                request.setAttribute("message", resource.getString("message.selecioneCriacaoSentido"));
                return mapping.findForward("failure");
            }

            request.setAttribute("message", resource.getString("message.finalizadaSucesso"));
        } else {
            request.setAttribute("message", resource.getString("message.usuarioSemPermissao"));
            return mapping.findForward("failure");
        }

        return mapping.findForward("grafico");
    }
}
