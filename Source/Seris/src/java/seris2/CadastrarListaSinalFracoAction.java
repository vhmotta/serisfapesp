package seris2;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import seris.HibernateUtil;
import seris.database.DAO.UsuarioDAO;
import seris.database.Usuario;
import seris.functions.BasicFunctions;
import seris2.database.DAO.ListaSinalFracoDAO;
import seris2.database.DAO.ListasSinaisFracosDAO;
import seris2.database.DAO.ListasUsuariosDAO;
import seris2.database.ListaSinalFraco;
import seris2.database.ListasSinaisFracos;
import seris2.database.ListasUsuarios;

public class CadastrarListaSinalFracoAction
        extends DispatchAction {

    private static final String SUCCESS = "success";
    private static final String FAILURE = "failure";
    private ResourceBundle resource = ResourceBundle.getBundle("seris.SerisIdioma");

    public CadastrarListaSinalFracoAction() {
    }

    public ActionForward save(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        int vNivel = -1;
        if (request.getSession().getAttribute("nivel") != null) {
            vNivel = Integer.parseInt(String.valueOf(request.getSession().getAttribute("nivel")));
        }
        if (vNivel == 5) {
            CadastrarListaSinalFracoActionForm formBean = (CadastrarListaSinalFracoActionForm) form;

            if ((formBean.getNome() != null) && (!formBean.getNome().equals(""))) {
                DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                Date data = formatter.parse(formBean.getData());
                Date dataInicial = formatter.parse(formBean.getDataInicial());
                Date dataFinal = formatter.parse(formBean.getDataFinal());

                ListaSinalFraco listaSinalFraco = new ListaSinalFraco();
                listaSinalFraco.setNome(formBean.getNome());
                listaSinalFraco.setData(data);
                listaSinalFraco.setDataInicial(dataInicial);
                listaSinalFraco.setDataFinal(dataFinal);
                listaSinalFraco.setKitId(corrigirId(formBean.getKitSelecionado()));
                listaSinalFraco.setAtorId(corrigirId(formBean.getAtorId()));
                listaSinalFraco.setProdutoId(corrigirId(formBean.getProdutoId()));
                listaSinalFraco.setMercadoId(corrigirId(formBean.getMercadoId()));
                listaSinalFraco.setFonteId(corrigirId(formBean.getFonteId()));
                listaSinalFraco.setStatus(Character.valueOf('A'));

                Session session = HibernateUtil.getSession();
                Transaction transaction = session.beginTransaction();
                transaction.begin();
                if (formBean.getId() > 0) {
                    ListaSinalFraco listaSF = ListaSinalFracoDAO.consultarListaSinalFraco(formBean.getId());

                    if (listaSF.getStatus().charValue() != 'E') {
                        List vList = session.createQuery("from ListaSinalFraco lsf where lsf.id <> " + formBean.getId() + " and lsf.nome = '" + formBean.getNome() + "'").list();

                        if (vList.size() == 0) {
                            listaSinalFraco.setId(Integer.valueOf(formBean.getId()));
                            session.update(listaSinalFraco);

                        } else {
                            transaction.rollback();
                            session.flush();
                            session.close();
                            request.setAttribute("message", resource.getString("message.listaSFexistente"));
                            return mapping.findForward("failure");
                        }
                    } else {
                        transaction.rollback();
                        session.flush();
                        session.close();
                        request.setAttribute("message", resource.getString("message.listaSFencerrada"));
                        return mapping.findForward("failure");
                    }
                } else {
                    List vList = session.createQuery("from ListaSinalFraco lsf where lsf.nome = '" + formBean.getNome() + "'").list();

                    if (vList.size() == 0) {
                        session.save(listaSinalFraco);
                        request.setAttribute("idSalvo", listaSinalFraco.getId().toString());

                        String vusuario_id = String.valueOf(request.getSession().getAttribute("usuario_id"));

                        Usuario usuario = UsuarioDAO.consultarUsuario(Integer.parseInt(vusuario_id));

                        String funcao = BasicFunctions.getFuncaoCriacaoSentido(request);

                        if (funcao != null) {
                            ListasUsuarios listasUsuarios = new ListasUsuarios();
                            listasUsuarios.setListaSinalFracoId(listaSinalFraco.getId());
                            listasUsuarios.setUsuarioId(Integer.valueOf(Integer.parseInt(vusuario_id)));
                            listasUsuarios.setNome(usuario.getNome());
                            listasUsuarios.setFuncao(funcao);
                            listasUsuarios.setEmail(usuario.getEmail());

                            session.save(listasUsuarios);
                        }
                    } else {
                        transaction.rollback();
                        session.flush();
                        session.close();
                        request.setAttribute("message", resource.getString("message.listaSFexistente"));
                        return mapping.findForward("failure");
                    }
                }
                transaction.commit();
                session.flush();
                session.close();
            } else {
                request.setAttribute("message", resource.getString("message.infoFormInvalida"));
                return mapping.findForward("failure");
            }
            request.setAttribute("message", resource.getString("message.salvoSucesso"));
        } else {
            request.setAttribute("message", resource.getString("message.usuarioSemPermissao"));
            return mapping.findForward("failure");
        }

        return mapping.findForward("success");
    }

    public ActionForward finalizar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        int vNivel = -1;
        if (request.getSession().getAttribute("nivel") != null) {
            vNivel = Integer.parseInt(String.valueOf(request.getSession().getAttribute("nivel")));
        }
        if (vNivel == 5) {
            CadastrarListaSinalFracoActionForm formBean = (CadastrarListaSinalFracoActionForm) form;

            if (formBean.getId() > 0) {
                Session session = HibernateUtil.getSession();
                Transaction transaction = session.beginTransaction();
                transaction.begin();

                List list = ListasSinaisFracosDAO.consultarListasSinaisFracosByListaId(formBean.getId());
                if ((list.size() < 8) || (list.size() > 12)) {
                    transaction.rollback();
                    session.flush();
                    session.close();
                    request.setAttribute("message", resource.getString("message.numeroSFinvalido"));
                    return mapping.findForward("failure");
                }

                for (int i = 0; i < list.size(); i++) {
                    ListasSinaisFracos lsf = (ListasSinaisFracos) list.get(i);
                    lsf.setNumero(Integer.valueOf(i + 1));
                    session.update(lsf);
                }

                list = ListasUsuariosDAO.consultarListasUsuariosByListaId(formBean.getId());

                if ((list.size() < 5) || (list.size() > 8)) {
                    transaction.rollback();
                    session.flush();
                    session.close();
                    request.setAttribute("message", resource.getString("message.numeroParticipanteInvalido"));
                    return mapping.findForward("failure");
                }

                int analista = 0;
                int patrocinador = 0;
                for (int i = 0; i < list.size(); i++) {
                    ListasUsuarios listaUsuarios = (ListasUsuarios) list.get(i);
                    if (listaUsuarios.getFuncao().equals("Facilitador")) {
                        analista++;
                    } else if (listaUsuarios.getFuncao().equals("Patrocinador")) {
                        patrocinador++;
                    }
                }
                if ((analista != 1) || (patrocinador != 1)) {
                    transaction.rollback();
                    session.flush();
                    session.close();
                    request.setAttribute("message", resource.getString("message.listaMinimoParticipante"));
                    return mapping.findForward("failure");
                }

                ListaSinalFraco listaSF = ListaSinalFracoDAO.consultarListaSinalFraco(formBean.getId());

                if (listaSF.getStatus().charValue() != 'E') {
                    listaSF.setStatus(Character.valueOf('E'));
                    session.update(listaSF);
                } else {
                    transaction.rollback();
                    session.flush();
                    session.close();
                    request.setAttribute("message", resource.getString("message.listaSFencerrada"));
                    return mapping.findForward("failure");
                }

                transaction.commit();
                session.flush();
                session.close();
            } else {
                request.setAttribute("message", resource.getString("message.selecioneLista"));
                return mapping.findForward("failure");
            }

            request.setAttribute("message", resource.getString("message.finalizadaSucesso"));
        } else {
            request.setAttribute("message", resource.getString("message.usuarioSemPermissao"));
            return mapping.findForward("failure");
        }

        return mapping.findForward("success");
    }

    public ActionForward delete(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        try {
            int vNivel = -1;

            if (request.getSession().getAttribute("nivel") != null) {
                vNivel = Integer.parseInt(String.valueOf(request.getSession().getAttribute("nivel")));
            }

            if (vNivel == 5) {
                CadastrarListaSinalFracoActionForm formBean = (CadastrarListaSinalFracoActionForm) form;

                if (formBean.getId() > 0) {
                    Session session = HibernateUtil.getSession();
                    Transaction transaction = session.beginTransaction();
                    ListaSinalFraco listaSinalFraco = (ListaSinalFraco) session.createQuery("from ListaSinalFraco where id=" + formBean.getId()).list().iterator().next();
                    session.delete(listaSinalFraco);
                    transaction.commit();
                    session.flush();
                    session.close();

                    formBean.setId(0);
                    formBean.setIdSinalFraco(0);
                    formBean.setNome(null);
                }

                request.setAttribute("message", resource.getString("message.deletadoSucesso"));
            } else {
                request.setAttribute("message", resource.getString("message.usuarioSemPermissao"));
            }
            return mapping.findForward("success");
        } catch (Exception ex) {
            request.setAttribute("message", resource.getString("message.falhaDeletar"));
        }
        return mapping.findForward("failure");
    }

    public ActionForward novo(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        return mapping.findForward("success");
    }

    public ActionForward adicionarParticipante(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        int vNivel = -1;
        if (request.getSession().getAttribute("nivel") != null) {
            vNivel = Integer.parseInt(String.valueOf(request.getSession().getAttribute("nivel")));
        }
        if (vNivel == 5) {
            CadastrarListaSinalFracoActionForm formBean = (CadastrarListaSinalFracoActionForm) form;

            if ((formBean.getId() != 0) && ((formBean.getParticipanteId() != 0) || (formBean.getParticipanteIdOutros() != 0))) {
                ListaSinalFraco listaSF = ListaSinalFracoDAO.consultarListaSinalFraco(formBean.getId());

                if (listaSF.getStatus().charValue() != 'E') {
                    ListasUsuarios listasUsuarios = new ListasUsuarios();
                    listasUsuarios.setListaSinalFracoId(Integer.valueOf(formBean.getId()));

                    if (formBean.getParticipanteId() != 0) {
                        List funcoes = BasicFunctions.getFuncaoCriacaoSentido(formBean.getParticipanteId(), request);

                        if (funcoes.size() > 0) {
                            int intValidarFuncao = -1;
                            int indice = 0;
                            String funcao = "";
                            do {
                                funcao = (String) funcoes.get(indice++);
                                intValidarFuncao = validarFuncao(formBean.getId(), funcao);
                            } while ((intValidarFuncao == 1) && (indice < funcoes.size()));

                            if (intValidarFuncao == 2) {
                                request.setAttribute("message", resource.getString("message.numeroMaximoParticipante"));
                                return mapping.findForward("failure");
                            }
                            if (intValidarFuncao == 1) {
                                String str = resource.getString("message.numeroLimiteFuncao");
                                str = str.replace("{0}", funcao);
                                request.setAttribute("message", str);
                                return mapping.findForward("failure");
                            }

                            Usuario usuario = UsuarioDAO.consultarUsuario(formBean.getParticipanteId());

                            listasUsuarios.setUsuarioId(Integer.valueOf(formBean.getParticipanteId()));
                            listasUsuarios.setNome(usuario.getNome());
                            listasUsuarios.setFuncao(funcao);
                            listasUsuarios.setEmail(usuario.getEmail());
                        } else {
                            request.setAttribute("message", resource.getString("message.usuarioSemFuncao"));
                            return mapping.findForward("failure");
                        }
                    } else {
                        int validarFuncao = validarFuncao(formBean.getId(), formBean.getFuncaoParticipante());
                        if (validarFuncao == 2) {
                            request.setAttribute("message", resource.getString("message.numeroMaximoParticipante"));
                            return mapping.findForward("failure");
                        }
                        if (validarFuncao == 1) {
                            String str = resource.getString("message.numeroLimiteFuncao");
                            str = str.replace("{0}", formBean.getFuncaoParticipante());
                            request.setAttribute("message", str);
                            return mapping.findForward("failure");
                        }

                        Usuario usuario = UsuarioDAO.consultarUsuario(formBean.getParticipanteIdOutros());

                        listasUsuarios.setUsuarioId(Integer.valueOf(formBean.getParticipanteIdOutros()));
                        listasUsuarios.setNome(usuario.getNome());
                        listasUsuarios.setFuncao(formBean.getFuncaoParticipante());
                        listasUsuarios.setEmail(usuario.getEmail());
                    }

                    Session session = HibernateUtil.getSession();
                    Transaction transaction = session.beginTransaction();
                    transaction.begin();

                    session.save(listasUsuarios);

                    transaction.commit();
                    session.flush();
                    session.close();
                } else {
                    request.setAttribute("message", resource.getString("message.listaSFencerrada"));
                    return mapping.findForward("failure");
                }
            } else {
                request.setAttribute("message", "Informações de formulário inválidas");
                return mapping.findForward("failure");
            }
            request.setAttribute("message", resource.getString("message.salvoSucesso"));
        } else {
            request.setAttribute("message", resource.getString("message.usuarioSemPermissao"));
            return mapping.findForward("failure");
        }

        return mapping.findForward("success");
    }

    public ActionForward inserirSinalFraco(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        int vNivel = -1;
        if (request.getSession().getAttribute("nivel") != null) {
            vNivel = Integer.parseInt(String.valueOf(request.getSession().getAttribute("nivel")));
        }
        if (vNivel == 5) {
            CadastrarListaSinalFracoActionForm formBean = (CadastrarListaSinalFracoActionForm) form;

            if ((formBean.getId() != 0) && (formBean.getIdSinalFraco() != 0)) {
                ListaSinalFraco listaSF = ListaSinalFracoDAO.consultarListaSinalFraco(formBean.getId());

                if (listaSF.getStatus().charValue() != 'E') {
                    List list = ListasSinaisFracosDAO.consultarListasSinaisFracosByListaId(formBean.getId());

                    if (list.size() >= 12) {
                        request.setAttribute("message", "Número máximo de sinais fracos excedido");
                        return mapping.findForward("failure");
                    }

                    int maxNumero = 0;

                    for (int i = 0; i < list.size(); i++) {
                        ListasSinaisFracos lsf = (ListasSinaisFracos) list.get(i);
                        if (maxNumero < lsf.getNumero().intValue()) {
                            maxNumero = lsf.getNumero().intValue();
                        }
                    }
                    maxNumero++;

                    ListasSinaisFracos listasSinaisFracos = new ListasSinaisFracos();
                    listasSinaisFracos.setListaSinalFracoId(Integer.valueOf(formBean.getId()));
                    listasSinaisFracos.setSinalFracoId(Integer.valueOf(formBean.getIdSinalFraco()));

                    Session session = HibernateUtil.getSession();
                    Transaction transaction = session.beginTransaction();
                    transaction.begin();

                    List vList = session.createQuery("from ListasSinaisFracos lsf where lsf.listaSinalFracoId = " + formBean.getId() + " and lsf.sinalFracoId = " + formBean.getIdSinalFraco()).list();

                    if (vList.size() == 0) {
                        listasSinaisFracos.setNumero(Integer.valueOf(maxNumero));
                        session.save(listasSinaisFracos);
                    } else {
                        transaction.rollback();
                        session.flush();
                        session.close();
                        request.setAttribute("message", resource.getString("message.sinalFracoExistente"));
                        return mapping.findForward("failure");
                    }
                    transaction.commit();
                    session.flush();
                    session.close();
                } else {
                    request.setAttribute("message", resource.getString("message.listaSFencerrada"));
                    return mapping.findForward("failure");
                }
            } else {
                request.setAttribute("message", resource.getString("message.selecioneListaOuSF"));
                return mapping.findForward("failure");
            }
            request.setAttribute("message", resource.getString("message.salvoSucesso"));
        } else {
            request.setAttribute("message", resource.getString("message.usuarioSemPermissao"));
            return mapping.findForward("failure");
        }

        return mapping.findForward("success");
    }

    public ActionForward excluirParticipante(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        try {
            int vNivel = -1;

            if (request.getSession().getAttribute("nivel") != null) {
                vNivel = Integer.parseInt(String.valueOf(request.getSession().getAttribute("nivel")));
            }

            if (vNivel == 5) {
                CadastrarListaSinalFracoActionForm formBean = (CadastrarListaSinalFracoActionForm) form;

                if (formBean.getListaUsuarioId() > 0) {
                    ListaSinalFraco listaSF = ListaSinalFracoDAO.consultarListaSinalFraco(formBean.getId());

                    if (listaSF.getStatus().charValue() != 'E') {
                        ListasUsuarios listaUsuarios = ListasUsuariosDAO.consultarListasUsuarios(formBean.getListaUsuarioId());
                        if ((listaUsuarios != null) && (listaUsuarios.getFuncao().equals("Facilitador"))) {
                            request.setAttribute("message", resource.getString("message.naoExcluiFacilitador"));
                            return mapping.findForward("failure");
                        }
                        Session session = HibernateUtil.getSession();
                        Transaction transaction = session.beginTransaction();
                        ListasUsuarios listasUsuarios = (ListasUsuarios) session.createQuery("from ListasUsuarios where id=" + formBean.getListaUsuarioId()).list().iterator().next();
                        session.delete(listasUsuarios);
                        transaction.commit();
                        session.flush();
                        session.close();

                        formBean.setIdSinalFraco(0);
                    } else {
                        request.setAttribute("message", resource.getString("message.listaSFencerrada"));
                        return mapping.findForward("failure");
                    }
                }

                request.setAttribute("message", resource.getString("message.deletadoSucesso"));
            } else {
                request.setAttribute("message", resource.getString("message.usuarioSemPermissao"));
                return mapping.findForward("failure");
            }
            return mapping.findForward("success");
        } catch (Exception ex) {
            request.setAttribute("message", resource.getString("message.falhaDeletar"));
        }
        return mapping.findForward("failure");
    }

    public ActionForward excluirSinalFraco(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        try {
            int vNivel = -1;

            if (request.getSession().getAttribute("nivel") != null) {
                vNivel = Integer.parseInt(String.valueOf(request.getSession().getAttribute("nivel")));
            }

            if (vNivel == 5) {
                CadastrarListaSinalFracoActionForm formBean = (CadastrarListaSinalFracoActionForm) form;

                if (formBean.getIdSinalFraco() > 0) {
                    ListaSinalFraco listaSF = ListaSinalFracoDAO.consultarListaSinalFraco(formBean.getId());

                    if (listaSF.getStatus().charValue() != 'E') {
                        Session session = HibernateUtil.getSession();
                        Transaction transaction = session.beginTransaction();
                        ListasSinaisFracos listasSinaisFracos = (ListasSinaisFracos) session.createQuery("from ListasSinaisFracos where id=" + formBean.getIdSinalFraco()).list().iterator().next();
                        session.delete(listasSinaisFracos);
                        transaction.commit();
                        session.flush();
                        session.close();

                        formBean.setIdSinalFraco(0);
                    } else {
                        request.setAttribute("message", resource.getString("message.listaSFencerrada"));
                        return mapping.findForward("failure");
                    }
                }

                request.setAttribute("message", resource.getString("message.deletadoSucesso"));
            } else {
                request.setAttribute("message", resource.getString("message.usuarioSemPermissao"));
                return mapping.findForward("failure");
            }
            return mapping.findForward("success");
        } catch (Exception ex) {
            request.setAttribute("message", resource.getString("message.falhaDeletar"));
        }
        return mapping.findForward("failure");
    }

    public ActionForward pesquisar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        return mapping.findForward("success");
    }

    private Integer corrigirId(int id) {
        if (id > 0) {
            return new Integer(id);
        }
        return null;
    }

    private int validarFuncao(int idLista, String funcao) {
        int result = 0;

        List list = ListasUsuariosDAO.consultarListasUsuariosByListaId(idLista);

        int total = 0;

        if (list.size() >= 8) {
            result = 2;
        } else {
            for (int i = 0; i < list.size(); i++) {
                ListasUsuarios listasUsuarios = (ListasUsuarios) list.get(i);
                if (funcao.equals(listasUsuarios.getFuncao())) {
                    total++;
                }
            }

            if ((funcao.equals("Facilitador")) && (total >= 1)) {
                result = 1;
            } else if (total >= 2) {
                result = 1;
            } else if ((!funcao.equals("Facilitador")) && (!funcao.equals("Patrocinador")) && (!funcao.equals("Captador")) && (!funcao.equals("Alta Direção")) && (!funcao.equals("Especialista")) && (!funcao.equals("Convidado"))) {
                result = 1;
            }
        }
        return result;
    }
}
