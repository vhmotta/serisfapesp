package seris2;

import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.hibernate.Session;
import org.hibernate.Transaction;
import seris.HibernateUtil;
import seris.database.Ator;
import seris.database.Avaliacao;
import seris.database.DAO.AtorDAO;
import seris.database.DAO.AvaliacaoDAO;
import seris.database.DAO.FonteDAO;
import seris.database.DAO.KiqDAO;
import seris.database.DAO.KitDAO;
import seris.database.DAO.LocalDAO;
import seris.database.DAO.MercadoDAO;
import seris.database.DAO.ProdutoDAO;
import seris.database.DAO.SinalfracoDAO;
import seris.database.DAO.UsuarioDAO;
import seris.database.Fonte;
import seris.database.KiqSinalfraco;
import seris.database.Kit;
import seris.database.Local;
import seris.database.Localcaptura;
import seris.database.Mercado;
import seris.database.Produto;
import seris.database.Sinalfraco;
import seris.database.Usuario;
import seris.functions.BasicFunctions;

public class ImportarSinalfracoAction
        extends DispatchAction {

    private static final String SUCCESS = "success";
    private static final String FAILURE = "failure";
    private ResourceBundle resource = ResourceBundle.getBundle("seris.SerisIdioma");
    private ResourceBundle resourceSeris = ResourceBundle.getBundle("seris.seris");

    public ImportarSinalfracoAction() {
    }

    public ActionForward save(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        ImportarSinalfracoActionForm formBean = (ImportarSinalfracoActionForm) form;

        if ((formBean.getArquivoImportar() != null) && (!formBean.getArquivoImportar().equals(""))) {

            String vusuario_id = String.valueOf(request.getSession().getAttribute("usuario_id"));
            int usuario_id = 0;
            if ((vusuario_id != null) && (!vusuario_id.equals(""))) {
                usuario_id = Integer.parseInt(vusuario_id);
            }

            Session session = HibernateUtil.getSession();
            Transaction transaction = session.beginTransaction();
            transaction.begin();

            String vAtor = request.getParameter("ator");
            formBean.setAtorSelecionado(vAtor);

            String vMercado = request.getParameter("mercado");
            if (vMercado.equals("0")) {
                vMercado = null;
            }
            formBean.setMercadoSelecionado(vMercado);

            String vFonte = request.getParameter("fonte");
            formBean.setFonteSelecionada(vFonte);

            String vProduto = request.getParameter("produto");
            if (vProduto.equals("0")) {
                vProduto = null;
            }
            formBean.setProdutoSelecionado(vProduto);

            formBean.setAvaliacaoSelecionada(request.getParameter("avaliacao"));

            formBean.setLocalSelecionado(request.getParameter("local"));

            formBean.setNomeLocalCaptura(request.getParameter("nomeLocalCaptura"));

            formBean.setDescricaoLocalCaptura(request.getParameter("descricaoLocalCaptura"));

            formBean.setKiqsSelecionados(request.getParameter("kiqSelecionado"));

            Local local = LocalDAO.consultarLocal(Integer.parseInt(formBean.getLocalSelecionado()));
            Localcaptura localcaptura = new Localcaptura(local, formBean.getNomeLocalCaptura(), formBean.getDescricaoLocalCaptura());

            session.saveOrUpdate(localcaptura);

            Avaliacao avaliacao = AvaliacaoDAO.consultarAvaliacao(Integer.parseInt(formBean.getAvaliacaoSelecionada()));
            Produto produto = null;
            if (vProduto != null) {
                produto = ProdutoDAO.consultarProduto(Integer.parseInt(formBean.getProdutoSelecionado()));
            }
            Ator ator = AtorDAO.consultarAtor(Integer.parseInt(formBean.getAtorSelecionado()));
            Fonte fonte = FonteDAO.consultarFonte(Integer.parseInt(formBean.getFonteSelecionada()));
            Mercado mercado = null;
            if (vMercado != null) {
                mercado = MercadoDAO.consultarMercado(Integer.parseInt(formBean.getMercadoSelecionado()));
            }
            Usuario usuario = UsuarioDAO.consultarUsuario(usuario_id);

            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");

            String fileName = resourceSeris.getString("SystemPath") + "/tmp/" + formBean.getArquivoImportar();

            List<String[]> sinaisFracos = extrairSinaisFracos(fileName);

            int total = 0;

            if ((sinaisFracos != null) && (sinaisFracos.size() > 0)) {
                for (int i = 0; i < sinaisFracos.size(); i++) {
                    String[] values = (String[]) sinaisFracos.get(i);

                    boolean cadastrar = SinalfracoDAO.consultarInfoSinalFracoByDescricao(values[1].trim());

                    if (!cadastrar) {
                        Date data = formatter.parse(values[0]);

                        Fonte fonteUtilizar = fonte;
                        if (!values[4].trim().equals("")) {
                            Fonte fnt = FonteDAO.consultarFonte(values[4], session);
                            if ((fnt != null) && (fnt.getId() != null)) {
                                int kitid = Integer.parseInt(formBean.getKitSelecionado());
                                boolean fontekit = FonteDAO.consultarFonteKit(fnt.getId().intValue(), kitid);
                                if (!fontekit) {
                                    Kit kit = KitDAO.consultarKit(kitid);
                                    fnt.getFonteKits().add(kit);
                                    session.saveOrUpdate(fnt);
                                }

                                fonteUtilizar = fnt;
                            } else {
                                fonteUtilizar = new Fonte(values[4]);
                                session.save(fonteUtilizar);
                            }
                        }

                        total++;
                        inserirSinalFraco(session, formBean, avaliacao, produto, localcaptura, fonteUtilizar, ator, mercado, values[1].trim(), data, values[3].trim(), values[2].trim(), usuario);
                    }
                }
            }
            if (total > 0) {
                String msg = resource.getString("message.importadoSucesso") + "<br>" + resource.getString("message.totalImportado") + " " + total;
                request.setAttribute("message", msg);
            } else {
                transaction.rollback();
                session.flush();
                session.close();
                request.setAttribute("message", resource.getString("message.nenhumSinalFracoImportar"));
                return mapping.findForward("success");
            }

            transaction.commit();
            session.flush();
            session.close();
        }
        return mapping.findForward("success");
    }

    public ActionForward novo(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        return mapping.findForward("success");
    }

    public ActionForward pesquisar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        return mapping.findForward("success");
    }

    private void inserirSinalFraco(Session session, ImportarSinalfracoActionForm formBean, Avaliacao avaliacao, Produto produto, Localcaptura localcaptura, Fonte fonte, Ator ator, Mercado mercado, String descricao, Date data, String comentario, String informacao, Usuario usuario) {
        Sinalfraco sinalfraco = new Sinalfraco(avaliacao, produto, localcaptura, fonte, ator, mercado, descricao, data, comentario, informacao, usuario);
        session.save(sinalfraco);

        if (sinalfraco.getId() != 0) {
            KiqSinalfraco kiqSinalfraco = new KiqSinalfraco(sinalfraco, KiqDAO.consultarKiq(Integer.parseInt(formBean.getKiqsSelecionados())));
            session.save(kiqSinalfraco);
        }
    }

    private List<String[]> extrairSinaisFracos(String fileName) {
        List<String[]> retorno = null;

        String ext = fileName.substring(fileName.lastIndexOf(".") + 1).trim().toUpperCase();

        if (ext.equals("WEBARCHIVE")) {
            retorno = extrairSinaisFracosHTML(fileName);
        } else if (ext.equals("TXT")) {
            retorno = extrairSinaisFracosTXT(fileName);
        }

        return retorno;
    }

    private static List<String[]> extrairSinaisFracosHTML(String fileName) {
        List<String[]> retorno = new ArrayList();

        String parts = BasicFunctions.readText(fileName);

        parts = BasicFunctions.obterValorTexto(parts, "<tbody>", "</tbody>", 1, true);

        do {
            String[] values = {"", "", "", ""};

            parts = BasicFunctions.obterValorTexto(parts, "<td style=\"BORDER-BOTTOM-STYLE: ridge; BORDER-RIGHT-STYLE: ridge; WIDTH: 30%; BORDER-TOP-STYLE: ridge; VERTICAL-ALIGN: middle; BORDER-LEFT-STYLE: ridge\">", "</tbody>", 1, true);

            String info = BasicFunctions.obterValorTexto(parts, null, "</td>", 1, true);

            int ind = info.lastIndexOf("(");
            if (ind > 0) {
                String data = info.substring(ind + 1, info.lastIndexOf(")"));

                info = info.substring(0, ind);

                String desc = BasicFunctions.obterValorTexto(parts, "<td style=\"BORDER-BOTTOM-STYLE: ridge; TEXT-ALIGN: justify; BORDER-RIGHT-STYLE: ridge; WIDTH: 70%; BORDER-TOP-STYLE: ridge; VERTICAL-ALIGN: middle; BORDER-LEFT-STYLE: ridge\">", "<button", 1, true);

                System.out.println("Data: " + data);
                System.out.println("Info: " + limparHTML(info));
                System.out.println("Desc: " + limparHTML(desc));

                values[0] = data;
                values[1] = limparHTML(desc);
                values[2] = limparHTML(info);

                retorno.add(values);
            }

        } while (parts.indexOf("<td style=\"BORDER-BOTTOM-STYLE: ridge; BORDER-RIGHT-STYLE: ridge; WIDTH: 30%; BORDER-TOP-STYLE: ridge; VERTICAL-ALIGN: middle; BORDER-LEFT-STYLE: ridge\">") > 0);

        return retorno;
    }

    private List<String[]> extrairSinaisFracosTXT(String fileName) {
        List<String[]> retorno = new ArrayList();

        String texto = BasicFunctions.readText(fileName);

        String[] linhas = texto.split("\n");

        int indData = -1;
        int indDesc = -1;
        int indInfo = -1;
        int indComent = -1;
        int indFonte = -1;

        if (linhas.length > 0) {
            String[] valoresLinha = linhas[0].split("\t");

            for (int i = 0; i < valoresLinha.length; i++) {
                String aux = valoresLinha[i].trim().toUpperCase();
                if (aux.equals("DATA")) {
                    indData = i;
                } else if (aux.equals("DESCRICAO")) {
                    indDesc = i;
                } else if (aux.equals("INFORMACAO")) {
                    indInfo = i;
                } else if (aux.equals("COMENTARIO")) {
                    indComent = i;
                } else if (aux.equals("FONTE")) {
                    indFonte = i;
                }
            }

            if ((indData >= 0) && (indDesc >= 0)) {
                for (int i = 1; i < linhas.length; i++) {
                    valoresLinha = linhas[i].split("\t");

                    String[] values = {"", "", "", "", ""};

                    if ((valoresLinha.length > indData) && (valoresLinha.length > indDesc)) {
                        values[0] = valoresLinha[indData];
                        values[1] = valoresLinha[indDesc];

                        if ((indInfo >= 0) && (valoresLinha.length > indInfo)) {
                            values[2] = valoresLinha[indInfo];
                        }
                        if ((indComent >= 0) && (valoresLinha.length > indComent)) {
                            values[3] = valoresLinha[indComent];
                        }
                        if ((indFonte >= 0) && (valoresLinha.length > indFonte)) {
                            values[4] = valoresLinha[indFonte];
                        }

                        retorno.add(values);
                    }
                }
            }
        }

        return retorno;
    }

    private static String limparHTML(String texto) {
        texto = texto.replace("<b>", "");
        texto = texto.replace("<u>", "");
        texto = texto.replace("<i>", "");
        texto = texto.replace("</b>", "");
        texto = texto.replace("</u>", "");
        texto = texto.replace("</i>", "");

        int ind = texto.indexOf("<i ");
        while (ind >= 0) {
            String aux = texto.substring(ind);
            int ind2 = aux.indexOf(">");
            texto = texto.substring(0, ind) + aux.substring(ind2 + 1);
            ind = texto.indexOf("<i ");
        }

        ind = texto.indexOf("<u ");
        while (ind >= 0) {
            String aux = texto.substring(ind);
            int ind2 = aux.indexOf(">");
            texto = texto.substring(0, ind) + aux.substring(ind2 + 1);
            ind = texto.indexOf("<u ");
        }

        ind = texto.indexOf("<b ");
        while (ind >= 0) {
            String aux = texto.substring(ind);
            int ind2 = aux.indexOf(">");
            texto = texto.substring(0, ind) + aux.substring(ind2 + 1);
            ind = texto.indexOf("<b ");
        }

        while (texto.indexOf("  ") >= 0) {
            texto = texto.replace("  ", " ");
        }

        return texto;
    }
}
