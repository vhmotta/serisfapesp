package seris2.database;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import seris.database.Sinalfraco;

@StaticMetamodel(ListasSinaisFracos.class)
public class ListasSinaisFracos_ {

    public static volatile SingularAttribute<ListasSinaisFracos, Integer> id;
    public static volatile SingularAttribute<ListasSinaisFracos, Sinalfraco> sinalFraco;
    public static volatile SingularAttribute<ListasSinaisFracos, Integer> sinalFracoId;
    public static volatile SingularAttribute<ListasSinaisFracos, ListaSinalFraco> listaSinalFraco;
    public static volatile SingularAttribute<ListasSinaisFracos, Integer> listaSinalFracoId;
    public static volatile SingularAttribute<ListasSinaisFracos, Integer> numero;

    public ListasSinaisFracos_() {
    }
}
