package seris2.database.DAO;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import seris.HibernateUtil;
import seris2.database.Sinonimos;

public class SinonimosDAO {

    public SinonimosDAO() {
    }

    public static List consultarListaSinonimos() {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        List vList = session.createQuery("from Sinonimos").list();
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }

    public static Sinonimos consultarSinonimos(int id) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        Sinonimos vSinonimos = new Sinonimos();
        try {
            vSinonimos = (Sinonimos) session.createQuery("from Sinonimos WHERE id=" + id + "").list().iterator().next();
        } catch (NoSuchElementException e) {
        }
        transaction.commit();
        session.flush();
        session.close();
        return vSinonimos;
    }
}
