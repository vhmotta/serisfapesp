package seris2.database.DAO;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import seris.HibernateUtil;
import seris2.database.ListasUsuarios;

public class ListasUsuariosDAO {

    public ListasUsuariosDAO() {
    }

    public static List consultarListasUsuarios() {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        List vList = session.createQuery("from ListasUsuarios").list();
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }

    public static ListasUsuarios consultarListasUsuarios(int id) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        ListasUsuarios vListasUsuarios = new ListasUsuarios();
        try {
            vListasUsuarios = (ListasUsuarios) session.createQuery("from ListasUsuarios WHERE id=" + id + "").list().iterator().next();
        } catch (NoSuchElementException e) {
        }
        transaction.commit();
        session.flush();
        session.close();
        return vListasUsuarios;
    }

    public static List consultarListasUsuariosByListaId(int listaId) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        List vList = session.createQuery("from ListasUsuarios where listaSinalFracoId = " + listaId).list();
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }
}
