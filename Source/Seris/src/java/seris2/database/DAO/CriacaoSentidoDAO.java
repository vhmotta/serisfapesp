package seris2.database.DAO;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import seris.HibernateUtil;
import seris2.database.CriacaoSentido;

public class CriacaoSentidoDAO {

    public CriacaoSentidoDAO() {
    }

    public static List consultarListaCriacaoSentido() {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        List vList = session.createQuery("from CriacaoSentido").list();
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }

    public static CriacaoSentido consultarCriacaoSentido(int id) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        CriacaoSentido vCriacaoSentido = new CriacaoSentido();
        try {
            vCriacaoSentido = (CriacaoSentido) session.createQuery("from CriacaoSentido WHERE id=" + id + "").list().iterator().next();
        } catch (NoSuchElementException e) {
        }
        transaction.commit();
        session.flush();
        session.close();
        return vCriacaoSentido;
    }

    public static List consultarListaCriacaoSentidoByKit(String kitId) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        List vList = session.createQuery("select cs from CriacaoSentido cs, ListaSinalFraco l where cs.listaSinalFracoId = l.id and l.kitId = " + kitId).list();
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }

    public static List consultarListaCriacaoSentidoByDominio(String usuarioId) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        List vList = session.createQuery("select cs, l.kit.dominio.nome, l.kit.descricao from CriacaoSentido cs, ListaSinalFraco l where cs.listaSinalFracoId = l.id and l.kit.dominio.id in (select du.dominio.id from DominioUsuario du where du.usuario.id = " + usuarioId + ")").list();
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }
}
