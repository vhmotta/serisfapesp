package seris2.database.DAO;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import seris.HibernateUtil;
import seris2.database.SinonimosRelacao;

public class SinonimosRelacaoDAO {

    public SinonimosRelacaoDAO() {
    }

    public static List consultarListaSinonimosRelacao() {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        List vList = session.createQuery("from SinonimosRelacao").list();
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }

    public static SinonimosRelacao consultarSinonimosRelacao(int id) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        SinonimosRelacao vSinonimosRelacao = new SinonimosRelacao();
        try {
            vSinonimosRelacao = (SinonimosRelacao) session.createQuery("from SinonimosRelacao WHERE id=" + id + "").list().iterator().next();
        } catch (NoSuchElementException e) {
        }
        transaction.commit();
        session.flush();
        session.close();
        return vSinonimosRelacao;
    }

    public static List consultarSinonimosPalavras(List palavras, int nivel) {
        System.out.println(palavras);

        List vList = new ArrayList();

        if (palavras.size() > 0) {
            StringBuilder where = new StringBuilder();
            for (int i = 0; i < palavras.size(); i++) {
                if (i == 0) {
                    where.append("'" + palavras.get(i) + "'");
                } else {
                    where.append(",'" + palavras.get(i) + "'");
                }
            }

            StringBuilder query = new StringBuilder();

            query.append("select distinct s.nome from SinonimosRelacao sr, Sinonimos s ");
            query.append("where sr.nivel <= " + nivel);
            query.append(" and ( ");
            query.append("  ( ");
            query.append("    s.id = sr.idSinonimo2 ");
            query.append("    and sr.idSinonimo1 in ");
            query.append("    ( ");
            query.append("      select distinct s2.id from Sinonimos s2 where ");
            query.append("      s2.nome in (" + where.toString() + ") ");
            query.append("    ) ");
            query.append("  ) OR  ( ");
            query.append("    s.id = sr.idSinonimo1 ");
            query.append("    and sr.idSinonimo2 in ");
            query.append("    ( ");
            query.append("      select distinct s2.id from Sinonimos s2 where ");
            query.append("      s2.nome in (" + where.toString() + ") ");
            query.append("    ) ");
            query.append("  ) ");
            query.append(")");

            System.out.println("QUERY: " + query);

            try {
                Session session = HibernateUtil.getSession();
                Transaction transaction = session.beginTransaction();
                transaction.begin();

                vList = session.createQuery(query.toString()).list();

                System.out.println(vList);

                List palavrasChaves = session.createQuery("select distinct nome from Sinonimos where nome in (" + where.toString() + ")").list();

                vList.addAll(palavrasChaves);

                System.out.println(vList);

                transaction.rollback();
                session.flush();
                session.close();
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }
        return vList;
    }
}
