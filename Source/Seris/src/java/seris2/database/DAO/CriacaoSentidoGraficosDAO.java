package seris2.database.DAO;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import seris.HibernateUtil;
import seris2.database.CriacaoSentidoGraficos;

public class CriacaoSentidoGraficosDAO {

    public CriacaoSentidoGraficosDAO() {
    }

    public static List consultarListaCriacaoSentido() {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        List vList = session.createQuery("from CriacaoSentidoGraficos").list();
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }

    public static CriacaoSentidoGraficos consultarCriacaoSentidoGraficos(String id) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        CriacaoSentidoGraficos vCriacaoSentidoGraficos = new CriacaoSentidoGraficos();
        try {
            vCriacaoSentidoGraficos = (CriacaoSentidoGraficos) session.createQuery("from CriacaoSentidoGraficos WHERE id=" + id + "").list().iterator().next();
        } catch (NoSuchElementException e) {
        }
        transaction.commit();
        session.flush();
        session.close();
        return vCriacaoSentidoGraficos;
    }

    public static List consultarListaCriacaoSentidoBySCSid(String idSCS) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        List vList = session.createQuery("from CriacaoSentidoGraficos where criacaoSentidoId = " + idSCS).list();
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }

    public static List consultarListaCriacaoSentidoByNome(String nome) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        List vList = session.createQuery("from CriacaoSentidoGraficos where nome = '" + nome + "'").list();
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }
}
