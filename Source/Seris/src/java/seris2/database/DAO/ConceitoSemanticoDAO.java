package seris2.database.DAO;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import seris.HibernateUtil;
import seris2.database.ConceitoRegra;
import seris2.database.ConceitoSemantico;

public class ConceitoSemanticoDAO {

    public ConceitoSemanticoDAO() {
    }

    public static List consultarConceitosSemantico() {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        List vList = session.createQuery("from ConceitoSemantico").list();
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }

    public static ConceitoSemantico consultarConceitoSemantico(int id) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        ConceitoSemantico vConceitoSemantico = new ConceitoSemantico();
        try {
            vConceitoSemantico = (ConceitoSemantico) session.createQuery("from ConceitoSemantico WHERE id=" + id + "").list().iterator().next();
        } catch (NoSuchElementException e) {
        }
        transaction.commit();
        session.flush();
        session.close();
        return vConceitoSemantico;
    }

    public static List consultarConceitosDasInstanciasClassificadas() {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();

        List vList = new ArrayList();
        Criteria criteria = session.createCriteria(ConceitoRegra.class);
        Criteria instancia = criteria.createCriteria("conceito1");
        instancia.add(Restrictions.eq("tipo", Character.valueOf('I')));
        instancia.add(Restrictions.eq("classificacao", Character.valueOf('A')));

        List list = criteria.list();

        for (int i = 0; i < list.size(); i++) {
            ConceitoRegra cr = (ConceitoRegra) list.get(i);
            ConceitoSemantico cs = cr.getConceito2();

            if (!vList.contains(cs)) {
                vList.add(cs);
            }
        }

        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }

    public static List consultarInstanciasClassificadasDosConceitos(Integer idConceito) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();

        List vList = new ArrayList();
        Criteria criteria = session.createCriteria(ConceitoRegra.class);
        Criteria instancia = criteria.createCriteria("conceito2");
        instancia.add(Restrictions.eq("id", idConceito));

        List list = criteria.list();

        for (int i = 0; i < list.size(); i++) {
            ConceitoRegra cr = (ConceitoRegra) list.get(i);
            ConceitoSemantico cs = cr.getConceito1();

            if (!vList.contains(cs)) {
                vList.add(cs);
            }
        }

        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }
}
