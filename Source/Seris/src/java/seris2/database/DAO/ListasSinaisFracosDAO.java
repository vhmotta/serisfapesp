package seris2.database.DAO;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import seris.HibernateUtil;
import seris2.database.ListasSinaisFracos;

public class ListasSinaisFracosDAO {

    public ListasSinaisFracosDAO() {
    }

    public static List consultarListasSinaisFracos() {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        List vList = session.createQuery("from ListasSinaisFracos").list();
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }

    public static ListasSinaisFracos consultarListasSinaisFracos(int id) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        ListasSinaisFracos vListasSinaisFracos = new ListasSinaisFracos();
        try {
            vListasSinaisFracos = (ListasSinaisFracos) session.createQuery("from ListasSinaisFracos WHERE id=" + id + "").list().iterator().next();
        } catch (NoSuchElementException e) {
        }
        transaction.commit();
        session.flush();
        session.close();
        return vListasSinaisFracos;
    }

    public static List consultarListasSinaisFracosByListaId(int listaId) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        List vList = session.createQuery("from ListasSinaisFracos where listaSinalFracoId = " + listaId + " order by numero,id").list();
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }

    public static List consultarIdsListasSinaisFracosByListaId(int listaId) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        List vList = session.createQuery("select sinalFracoId from ListasSinaisFracos where listaSinalFracoId = " + listaId + " order by numero,id").list();
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }

    public static void deleteListasSinaisFracos(int idLista) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        ListasSinaisFracos vListasSinaisFracos = new ListasSinaisFracos();
        try {
            session.createQuery("delete from ListasSinaisFracos WHERE listaSinalFracoId=" + idLista).executeUpdate();
        } catch (NoSuchElementException e) {
        }
        transaction.commit();
        session.flush();
        session.close();
    }
}
