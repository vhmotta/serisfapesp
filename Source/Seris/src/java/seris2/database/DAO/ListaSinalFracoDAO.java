package seris2.database.DAO;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import seris.HibernateUtil;
import seris2.database.ListaSinalFraco;

public class ListaSinalFracoDAO {

    public ListaSinalFracoDAO() {
    }

    public static List consultarListaSinalFraco() {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        List vList = session.createQuery("from ListaSinalFraco").list();
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }

    public static ListaSinalFraco consultarListaSinalFraco(int id) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        ListaSinalFraco vListaSinalFraco = new ListaSinalFraco();
        try {
            vListaSinalFraco = (ListaSinalFraco) session.createQuery("from ListaSinalFraco WHERE id=" + id + "").list().iterator().next();
        } catch (NoSuchElementException e) {
        }
        transaction.commit();
        session.flush();
        session.close();
        return vListaSinalFraco;
    }

    public static List consultarListaSinalFracoPorKit(String kitId) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        List vList = session.createQuery("from ListaSinalFraco where kitId = " + kitId).list();
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }

    public static List consultarListaSinalFracoEncerrada() {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        List vList = session.createQuery("from ListaSinalFraco where status = 'E'").list();
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }

    public static List consultarListaSinalFracoEncerrada(String kitId) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        List vList = session.createQuery("from ListaSinalFraco where status = 'E' and kitId = " + kitId).list();
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }
}
