package seris2.database;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import seris.database.Local;
import seris.database.Usuario;

@Entity
@Table(name = "criacao_sentido")
@NamedQueries({
    @javax.persistence.NamedQuery(name = "CriacaoSentido.findAll", query = "SELECT c FROM CriacaoSentido c")
    , @javax.persistence.NamedQuery(name = "CriacaoSentido.findById", query = "SELECT c FROM CriacaoSentido c WHERE c.id = :id")
    , @javax.persistence.NamedQuery(name = "CriacaoSentido.findByNome", query = "SELECT c FROM CriacaoSentido c WHERE c.nome = :nome")
    , @javax.persistence.NamedQuery(name = "CriacaoSentido.findByData", query = "SELECT c FROM CriacaoSentido c WHERE c.data = :data")
    , @javax.persistence.NamedQuery(name = "CriacaoSentido.findByEmpresa", query = "SELECT c FROM CriacaoSentido c WHERE c.empresa = :empresa")
    , @javax.persistence.NamedQuery(name = "CriacaoSentido.findByLugar", query = "SELECT c FROM CriacaoSentido c WHERE c.lugar = :lugar")
    , @javax.persistence.NamedQuery(name = "CriacaoSentido.findByGrafico", query = "SELECT c FROM CriacaoSentido c WHERE c.grafico = :grafico")})
public class CriacaoSentido
        implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "lista_sinal_fraco_id")
    private Integer listaSinalFracoId;
    @Basic(optional = false)
    @Column(name = "local_id")
    private Integer localId;
    @Basic(optional = false)
    @Column(name = "usuario_id")
    private Integer usuarioId;
    @Basic(optional = false)
    @Column(name = "nome")
    private String nome;
    @Basic(optional = false)
    @Column(name = "status")
    private Character status;
    @Basic(optional = false)
    @Column(name = "data")
    @Temporal(TemporalType.DATE)
    private Date data;
    @Basic(optional = false)
    @Column(name = "empresa")
    private String empresa;
    @Basic(optional = false)
    @Column(name = "lugar")
    private String lugar;
    @Column(name = "grafico")
    private String grafico;
    @JoinColumn(name = "local_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Local local;
    @JoinColumn(name = "lista_sinal_fraco_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private ListaSinalFraco listaSinalFraco;
    @JoinColumn(name = "usuario_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Usuario usuario;

    public CriacaoSentido() {
    }

    public CriacaoSentido(Integer id) {
        this.id = id;
    }

    public CriacaoSentido(Integer id, String nome, Date data, String empresa, String lugar, String grafico) {
        this.id = id;
        this.nome = nome;
        this.data = data;
        this.empresa = empresa;
        this.lugar = lugar;
        this.grafico = grafico;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public String getGrafico() {
        return grafico;
    }

    public void setGrafico(String grafico) {
        this.grafico = grafico;
    }

    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    public boolean equals(Object object) {
        if (!(object instanceof CriacaoSentido)) {
            return false;
        }
        CriacaoSentido other = (CriacaoSentido) object;
        if (((id == null) && (id != null)) || ((id != null) && (!id.equals(id)))) {
            return false;
        }
        return true;
    }

    public String toString() {
        return "seris2.database.CriacaoSentido[id=" + id + "]";
    }

    public Integer getListaSinalFracoId() {
        return listaSinalFracoId;
    }

    public void setListaSinalFracoId(Integer listaSinalFracoId) {
        this.listaSinalFracoId = listaSinalFracoId;
    }

    public Integer getLocalId() {
        return localId;
    }

    public void setLocalId(Integer localId) {
        this.localId = localId;
    }

    public Integer getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Integer usuarioId) {
        this.usuarioId = usuarioId;
    }

    public Local getLocal() {
        return local;
    }

    public void setLocal(Local local) {
        this.local = local;
    }

    public ListaSinalFraco getListaSinalFraco() {
        return listaSinalFraco;
    }

    public void setListaSinalFraco(ListaSinalFraco listaSinalFraco) {
        this.listaSinalFraco = listaSinalFraco;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }
}
