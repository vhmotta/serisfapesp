package seris2.database;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(ConceitoRegra.class)
public class ConceitoRegra_ {

    public static volatile SingularAttribute<ConceitoRegra, Integer> id;
    public static volatile SingularAttribute<ConceitoRegra, ConceitoSemantico> conceito1;
    public static volatile SingularAttribute<ConceitoRegra, RegraSemantica> idRegra;
    public static volatile SingularAttribute<ConceitoRegra, ConceitoSemantico> conceito2;

    public ConceitoRegra_() {
    }
}
