package seris2.database;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.Table;

@Entity
@Table(name = "regra_semantica")
@NamedQueries({
    @javax.persistence.NamedQuery(name = "RegraSemantica.findAll", query = "SELECT r FROM RegraSemantica r")
    , @javax.persistence.NamedQuery(name = "RegraSemantica.findById", query = "SELECT r FROM RegraSemantica r WHERE r.id = :id")
    , @javax.persistence.NamedQuery(name = "RegraSemantica.findByRegra", query = "SELECT r FROM RegraSemantica r WHERE r.regra = :regra")})
public class RegraSemantica
        implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "regra")
    private String regra;

    public RegraSemantica() {
    }

    public RegraSemantica(Integer id) {
        this.id = id;
    }

    public RegraSemantica(Integer id, String regra) {
        this.id = id;
        this.regra = regra;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRegra() {
        return regra;
    }

    public void setRegra(String regra) {
        this.regra = regra;
    }

    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    public boolean equals(Object object) {
        if (!(object instanceof RegraSemantica)) {
            return false;
        }
        RegraSemantica other = (RegraSemantica) object;
        if (((id == null) && (id != null)) || ((id != null) && (!id.equals(id)))) {
            return false;
        }
        return true;
    }

    public String toString() {
        return "seris2.database.RegraSemantica[ id=" + id + " ]";
    }
}
