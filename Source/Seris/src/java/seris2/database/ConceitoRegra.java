package seris2.database;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@Table(name = "conceito_regra")
@XmlRootElement
@NamedQueries({
    @javax.persistence.NamedQuery(name = "ConceitoRegra.findAll", query = "SELECT c FROM ConceitoRegra c")
    , @javax.persistence.NamedQuery(name = "ConceitoRegra.findById", query = "SELECT c FROM ConceitoRegra c WHERE c.id = :id")})
public class ConceitoRegra
        implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @JoinColumn(name = "id_conceito2", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private ConceitoSemantico conceito2;
    @JoinColumn(name = "id_conceito1", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private ConceitoSemantico conceito1;
    @JoinColumn(name = "id_regra", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private RegraSemantica idRegra;

    public ConceitoRegra() {
    }

    public ConceitoRegra(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public ConceitoSemantico getConceito2() {
        return conceito2;
    }

    public void setConceito2(ConceitoSemantico conceito2) {
        this.conceito2 = conceito2;
    }

    public ConceitoSemantico getConceito1() {
        return conceito1;
    }

    public void setConceito1(ConceitoSemantico conceito1) {
        this.conceito1 = conceito1;
    }

    public RegraSemantica getIdRegra() {
        return idRegra;
    }

    public void setIdRegra(RegraSemantica idRegra) {
        this.idRegra = idRegra;
    }

    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    public boolean equals(Object object) {
        if (!(object instanceof ConceitoRegra)) {
            return false;
        }
        ConceitoRegra other = (ConceitoRegra) object;
        if (((id == null) && (id != null)) || ((id != null) && (!id.equals(id)))) {
            return false;
        }
        return true;
    }

    public String toString() {
        return "seris2.database.ConceitoRegra[ id=" + id + " ]";
    }
}
