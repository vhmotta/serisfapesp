package seris2.database;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.Table;

@Entity
@Table(name = "sinonimos_relacao")
@NamedQueries({
    @javax.persistence.NamedQuery(name = "SinonimosRelacao.findAll", query = "SELECT s FROM SinonimosRelacao s")})
public class SinonimosRelacao
        implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "id_sinonimo1")
    private Integer idSinonimo1;
    @Basic(optional = false)
    @Column(name = "id_sinonimo2")
    private Integer idSinonimo2;
    @Basic(optional = false)
    @Column(name = "nivel")
    private int nivel;
    @JoinColumn(name = "id_sinonimo1", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Sinonimos sinonimo1;
    @JoinColumn(name = "id_sinonimo2", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Sinonimos sinonimo2;

    public SinonimosRelacao() {
    }

    public SinonimosRelacao(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    public boolean equals(Object object) {
        if (!(object instanceof SinonimosRelacao)) {
            return false;
        }
        SinonimosRelacao other = (SinonimosRelacao) object;
        if (((id == null) && (id != null)) || ((id != null) && (!id.equals(id)))) {
            return false;
        }
        return true;
    }

    public String toString() {
        return "seris2.database.SinonimosRelacao[id=" + id + "]";
    }

    public Integer getIdSinonimo1() {
        return idSinonimo1;
    }

    public void setIdSinonimo1(Integer idSinonimo1) {
        this.idSinonimo1 = idSinonimo1;
    }

    public Integer getIdSinonimo2() {
        return idSinonimo2;
    }

    public void setIdSinonimo2(Integer idSinonimo2) {
        this.idSinonimo2 = idSinonimo2;
    }

    public Sinonimos getSinonimo1() {
        return sinonimo1;
    }

    public void setSinonimo1(Sinonimos sinonimo1) {
        this.sinonimo1 = sinonimo1;
    }

    public Sinonimos getSinonimo2() {
        return sinonimo2;
    }

    public void setSinonimo2(Sinonimos sinonimo2) {
        this.sinonimo2 = sinonimo2;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }
}
