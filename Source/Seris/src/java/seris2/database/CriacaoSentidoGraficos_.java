package seris2.database;

import java.util.Date;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(CriacaoSentidoGraficos.class)
public class CriacaoSentidoGraficos_ {

    public static volatile SingularAttribute<CriacaoSentidoGraficos, Integer> id;
    public static volatile SingularAttribute<CriacaoSentidoGraficos, Integer> criacaoSentidoId;
    public static volatile SingularAttribute<CriacaoSentidoGraficos, Date> data;
    public static volatile SingularAttribute<CriacaoSentidoGraficos, String> grafico;
    public static volatile SingularAttribute<CriacaoSentidoGraficos, String> nome;
    public static volatile SingularAttribute<CriacaoSentidoGraficos, CriacaoSentido> criacaoSentido;

    public CriacaoSentidoGraficos_() {
    }
}
