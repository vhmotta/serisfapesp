package seris2.database;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(ConceitoSemantico.class)
public class ConceitoSemantico_ {

    public static volatile SingularAttribute<ConceitoSemantico, Integer> id;
    public static volatile SingularAttribute<ConceitoSemantico, Character> tipo;
    public static volatile SingularAttribute<ConceitoSemantico, Character> classificacao;
    public static volatile SingularAttribute<ConceitoSemantico, String> conceito;

    public ConceitoSemantico_() {
    }
}
