package seris2.database;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.Table;
import seris.database.Usuario;

@Entity
@Table(name = "listas_usuarios")
@NamedQueries({
    @javax.persistence.NamedQuery(name = "ListasUsuarios.findAll", query = "SELECT l FROM ListasUsuarios l")
    , @javax.persistence.NamedQuery(name = "ListasUsuarios.findById", query = "SELECT l FROM ListasUsuarios l WHERE l.id = :id")
    , @javax.persistence.NamedQuery(name = "ListasUsuarios.findByNome", query = "SELECT l FROM ListasUsuarios l WHERE l.nome = :nome")
    , @javax.persistence.NamedQuery(name = "ListasUsuarios.findByFuncao", query = "SELECT l FROM ListasUsuarios l WHERE l.funcao = :funcao")
    , @javax.persistence.NamedQuery(name = "ListasUsuarios.findByEmail", query = "SELECT l FROM ListasUsuarios l WHERE l.email = :email")})
public class ListasUsuarios
        implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "lista_sinal_fraco_id")
    private Integer listaSinalFracoId;
    @Column(name = "usuario_id")
    private Integer usuarioId;
    @Basic(optional = false)
    @Column(name = "nome")
    private String nome;
    @Basic(optional = false)
    @Column(name = "funcao")
    private String funcao;
    @Column(name = "email")
    private String email;
    @JoinColumn(name = "lista_sinal_fraco_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private ListaSinalFraco listaSinalFraco;
    @JoinColumn(name = "usuario_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private Usuario usuario;

    public ListasUsuarios() {
    }

    public ListasUsuarios(Integer id) {
        this.id = id;
    }

    public ListasUsuarios(Integer id, String nome, String funcao) {
        this.id = id;
        this.nome = nome;
        this.funcao = funcao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getFuncao() {
        return funcao;
    }

    public void setFuncao(String funcao) {
        this.funcao = funcao;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    public boolean equals(Object object) {
        if (!(object instanceof ListasUsuarios)) {
            return false;
        }
        ListasUsuarios other = (ListasUsuarios) object;
        if (((id == null) && (id != null)) || ((id != null) && (!id.equals(id)))) {
            return false;
        }
        return true;
    }

    public String toString() {
        return "seris2.database.ListasUsuarios[id=" + id + "]";
    }

    public Integer getListaSinalFracoId() {
        return listaSinalFracoId;
    }

    public void setListaSinalFracoId(Integer listaSinalFracoId) {
        this.listaSinalFracoId = listaSinalFracoId;
    }

    public Integer getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(Integer usuarioId) {
        this.usuarioId = usuarioId;
    }

    public ListaSinalFraco getListaSinalFraco() {
        return listaSinalFraco;
    }

    public void setListaSinalFraco(ListaSinalFraco listaSinalFraco) {
        this.listaSinalFraco = listaSinalFraco;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
