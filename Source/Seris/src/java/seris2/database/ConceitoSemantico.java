package seris2.database;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.Table;

@Entity
@Table(name = "conceito_semantico")
@NamedQueries({
    @javax.persistence.NamedQuery(name = "ConceitoSemantico.findAll", query = "SELECT cs FROM ConceitoSemantico cs")})
public class ConceitoSemantico
        implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "conceito")
    private String conceito;
    @Basic(optional = false)
    @Column(name = "tipo")
    private Character tipo;
    @Basic(optional = true)
    @Column(name = "classificacao")
    private Character classificacao;

    public ConceitoSemantico() {
    }

    public ConceitoSemantico(Integer id) {
        this.id = id;
    }

    public ConceitoSemantico(Integer id, String conceito) {
        this.id = id;
        this.conceito = conceito;
    }

    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    public boolean equals(Object object) {
        if (!(object instanceof ConceitoSemantico)) {
            return false;
        }
        ConceitoSemantico other = (ConceitoSemantico) object;
        if (((getId() == null) && (other.getId() != null)) || ((getId() != null) && (!id.equals(id)))) {
            return false;
        }
        return true;
    }

    public String toString() {
        return "seris2.database.ConceitoSemantico[id=" + getId() + "]";
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getConceito() {
        return conceito;
    }

    public void setConceito(String conceito) {
        this.conceito = conceito;
    }

    public Character getClassificacao() {
        return classificacao;
    }

    public void setClassificacao(Character classificacao) {
        this.classificacao = classificacao;
    }

    public char getTipo() {
        return tipo.charValue();
    }

    public void setTipo(Character tipo) {
        this.tipo = tipo;
    }
}
