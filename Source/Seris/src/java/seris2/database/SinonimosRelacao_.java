package seris2.database;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(SinonimosRelacao.class)
public class SinonimosRelacao_ {

    public static volatile SingularAttribute<SinonimosRelacao, Integer> id;
    public static volatile SingularAttribute<SinonimosRelacao, Sinonimos> sinonimo2;
    public static volatile SingularAttribute<SinonimosRelacao, Sinonimos> sinonimo1;
    public static volatile SingularAttribute<SinonimosRelacao, Integer> idSinonimo1;
    public static volatile SingularAttribute<SinonimosRelacao, Integer> nivel;
    public static volatile SingularAttribute<SinonimosRelacao, Integer> idSinonimo2;

    public SinonimosRelacao_() {
    }
}
