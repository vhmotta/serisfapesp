package seris2.database;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import seris.database.Usuario;

@StaticMetamodel(ListasUsuarios.class)
public class ListasUsuarios_ {

    public static volatile SingularAttribute<ListasUsuarios, Integer> id;
    public static volatile SingularAttribute<ListasUsuarios, String> funcao;
    public static volatile SingularAttribute<ListasUsuarios, String> email;
    public static volatile SingularAttribute<ListasUsuarios, Usuario> usuario;
    public static volatile SingularAttribute<ListasUsuarios, ListaSinalFraco> listaSinalFraco;
    public static volatile SingularAttribute<ListasUsuarios, String> nome;
    public static volatile SingularAttribute<ListasUsuarios, Integer> listaSinalFracoId;
    public static volatile SingularAttribute<ListasUsuarios, Integer> usuarioId;

    public ListasUsuarios_() {
    }
}
