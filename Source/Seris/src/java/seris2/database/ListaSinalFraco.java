package seris2.database;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import seris.database.Ator;
import seris.database.Fonte;
import seris.database.Kit;
import seris.database.Mercado;
import seris.database.Produto;

@Entity
@Table(name = "lista_sinal_fraco")
@NamedQueries({
    @javax.persistence.NamedQuery(name = "ListaSinalFraco.findAll", query = "SELECT l FROM ListaSinalFraco l")
    , @javax.persistence.NamedQuery(name = "ListaSinalFraco.findById", query = "SELECT l FROM ListaSinalFraco l WHERE l.id = :id")
    , @javax.persistence.NamedQuery(name = "ListaSinalFraco.findByNome", query = "SELECT l FROM ListaSinalFraco l WHERE l.nome = :nome")
    , @javax.persistence.NamedQuery(name = "ListaSinalFraco.findByData", query = "SELECT l FROM ListaSinalFraco l WHERE l.data = :data")
    , @javax.persistence.NamedQuery(name = "ListaSinalFraco.findByDataInicial", query = "SELECT l FROM ListaSinalFraco l WHERE l.dataInicial = :dataInicial")
    , @javax.persistence.NamedQuery(name = "ListaSinalFraco.findByDataFinal", query = "SELECT l FROM ListaSinalFraco l WHERE l.dataFinal = :dataFinal")})
public class ListaSinalFraco
        implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "kit_id")
    private Integer kitId;
    @Basic(optional = true)
    @Column(name = "ator_id")
    private Integer atorId;
    @Basic(optional = true)
    @Column(name = "produto_id")
    private Integer produtoId;
    @Basic(optional = true)
    @Column(name = "mercado_id")
    private Integer mercadoId;
    @Basic(optional = true)
    @Column(name = "fonte_id")
    private Integer fonteId;
    @Basic(optional = false)
    @Column(name = "nome")
    private String nome;
    @Basic(optional = false)
    @Column(name = "status")
    private Character status;
    @Basic(optional = false)
    @Column(name = "data")
    @Temporal(TemporalType.DATE)
    private Date data;
    @Basic(optional = false)
    @Column(name = "data_inicial")
    @Temporal(TemporalType.DATE)
    private Date dataInicial;
    @Basic(optional = false)
    @Column(name = "data_final")
    @Temporal(TemporalType.DATE)
    private Date dataFinal;
    @JoinColumn(name = "kit_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Kit kit;
    @JoinColumn(name = "ator_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private Ator ator;
    @JoinColumn(name = "produto_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private Produto produto;
    @JoinColumn(name = "mercado_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private Mercado mercado;
    @JoinColumn(name = "fonte_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne
    private Fonte fonte;
    @OneToMany(cascade = {javax.persistence.CascadeType.ALL}, mappedBy = "listaSinalFracoId")
    private List<ListasSinaisFracos> listasSinaisFracosList;
    @OneToMany(cascade = {javax.persistence.CascadeType.ALL}, mappedBy = "listaSinalFracoId")
    private List<ListasUsuarios> listasUsuariosList;

    public ListaSinalFraco() {
    }

    public ListaSinalFraco(Integer id) {
        this.id = id;
    }

    public ListaSinalFraco(Integer id, String nome, Date data, Date dataInicial, Date dataFinal) {
        this.id = id;
        this.nome = nome;
        this.data = data;
        this.dataInicial = dataInicial;
        this.dataFinal = dataFinal;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Date getDataInicial() {
        return dataInicial;
    }

    public void setDataInicial(Date dataInicial) {
        this.dataInicial = dataInicial;
    }

    public Date getDataFinal() {
        return dataFinal;
    }

    public void setDataFinal(Date dataFinal) {
        this.dataFinal = dataFinal;
    }

    public List<ListasSinaisFracos> getListasSinaisFracosList() {
        return listasSinaisFracosList;
    }

    public void setListasSinaisFracosList(List<ListasSinaisFracos> listasSinaisFracosList) {
        this.listasSinaisFracosList = listasSinaisFracosList;
    }

    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    public boolean equals(Object object) {
        if (!(object instanceof ListaSinalFraco)) {
            return false;
        }
        ListaSinalFraco other = (ListaSinalFraco) object;
        if (((id == null) && (id != null)) || ((id != null) && (!id.equals(id)))) {
            return false;
        }
        return true;
    }

    public String toString() {
        return "seris2.database.ListaSinalFraco[id=" + id + "]";
    }

    public Kit getKit() {
        return kit;
    }

    public void setKit(Kit kit) {
        this.kit = kit;
    }

    public Ator getAtor() {
        return ator;
    }

    public void setAtor(Ator ator) {
        this.ator = ator;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public Mercado getMercado() {
        return mercado;
    }

    public void setMercado(Mercado mercado) {
        this.mercado = mercado;
    }

    public Fonte getFonte() {
        return fonte;
    }

    public void setFonte(Fonte fonte) {
        this.fonte = fonte;
    }

    public Integer getKitId() {
        return kitId;
    }

    public void setKitId(Integer kitId) {
        this.kitId = kitId;
    }

    public Integer getAtorId() {
        return atorId;
    }

    public void setAtorId(Integer atorId) {
        this.atorId = atorId;
    }

    public Integer getProdutoId() {
        return produtoId;
    }

    public void setProdutoId(Integer produtoId) {
        this.produtoId = produtoId;
    }

    public Integer getMercadoId() {
        return mercadoId;
    }

    public void setMercadoId(Integer mercadoId) {
        this.mercadoId = mercadoId;
    }

    public Integer getFonteId() {
        return fonteId;
    }

    public void setFonteId(Integer fonteId) {
        this.fonteId = fonteId;
    }

    public List<ListasUsuarios> getListasUsuariosList() {
        return listasUsuariosList;
    }

    public void setListasUsuariosList(List<ListasUsuarios> listasUsuariosList) {
        this.listasUsuariosList = listasUsuariosList;
    }

    public Character getStatus() {
        return status;
    }

    public void setStatus(Character status) {
        this.status = status;
    }
}
