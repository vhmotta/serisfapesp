package seris2.database;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "criacao_sentido_graficos")
@NamedQueries({
    @javax.persistence.NamedQuery(name = "CriacaoSentidoGraficos.findAll", query = "SELECT c FROM CriacaoSentidoGraficos c")})
public class CriacaoSentidoGraficos
        implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @Column(name = "criacao_sentido_id")
    private Integer criacaoSentidoId;
    @Basic(optional = false)
    @Column(name = "nome")
    private String nome;
    @Basic(optional = false)
    @Column(name = "data")
    @Temporal(TemporalType.DATE)
    private Date data;
    @Column(name = "grafico")
    private String grafico;
    @JoinColumn(name = "criacao_sentido_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private CriacaoSentido criacaoSentido;

    public CriacaoSentidoGraficos() {
    }

    public CriacaoSentidoGraficos(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getGrafico() {
        return grafico;
    }

    public void setGrafico(String grafico) {
        this.grafico = grafico;
    }

    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    public boolean equals(Object object) {
        if (!(object instanceof CriacaoSentidoGraficos)) {
            return false;
        }
        CriacaoSentidoGraficos other = (CriacaoSentidoGraficos) object;
        if (((id == null) && (id != null)) || ((id != null) && (!id.equals(id)))) {
            return false;
        }
        return true;
    }

    public String toString() {
        return "seris2.database.CriacaoSentidoGraficos[id=" + id + "]";
    }

    public Integer getCriacaoSentidoId() {
        return criacaoSentidoId;
    }

    public void setCriacaoSentidoId(Integer criacaoSentidoId) {
        this.criacaoSentidoId = criacaoSentidoId;
    }

    public CriacaoSentido getCriacaoSentido() {
        return criacaoSentido;
    }

    public void setCriacaoSentido(CriacaoSentido criacaoSentido) {
        this.criacaoSentido = criacaoSentido;
    }
}
