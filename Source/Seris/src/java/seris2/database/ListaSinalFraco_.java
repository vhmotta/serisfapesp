package seris2.database;

import java.util.Date;
import javax.persistence.metamodel.ListAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import seris.database.Ator;
import seris.database.Fonte;
import seris.database.Kit;
import seris.database.Mercado;
import seris.database.Produto;

@StaticMetamodel(ListaSinalFraco.class)
public class ListaSinalFraco_ {

    public static volatile SingularAttribute<ListaSinalFraco, Produto> produto;
    public static volatile SingularAttribute<ListaSinalFraco, Integer> atorId;
    public static volatile ListAttribute<ListaSinalFraco, ListasUsuarios> listasUsuariosList;
    public static volatile SingularAttribute<ListaSinalFraco, Mercado> mercado;
    public static volatile SingularAttribute<ListaSinalFraco, Character> status;
    public static volatile SingularAttribute<ListaSinalFraco, Ator> ator;
    public static volatile SingularAttribute<ListaSinalFraco, Integer> fonteId;
    public static volatile SingularAttribute<ListaSinalFraco, Date> data;
    public static volatile SingularAttribute<ListaSinalFraco, Integer> kitId;
    public static volatile SingularAttribute<ListaSinalFraco, Integer> id;
    public static volatile ListAttribute<ListaSinalFraco, ListasSinaisFracos> listasSinaisFracosList;
    public static volatile SingularAttribute<ListaSinalFraco, Date> dataFinal;
    public static volatile SingularAttribute<ListaSinalFraco, Fonte> fonte;
    public static volatile SingularAttribute<ListaSinalFraco, Date> dataInicial;
    public static volatile SingularAttribute<ListaSinalFraco, String> nome;
    public static volatile SingularAttribute<ListaSinalFraco, Integer> produtoId;
    public static volatile SingularAttribute<ListaSinalFraco, Kit> kit;
    public static volatile SingularAttribute<ListaSinalFraco, Integer> mercadoId;

    public ListaSinalFraco_() {
    }
}
