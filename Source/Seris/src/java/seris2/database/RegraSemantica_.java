package seris2.database;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(RegraSemantica.class)
public class RegraSemantica_ {

    public static volatile SingularAttribute<RegraSemantica, Integer> id;
    public static volatile SingularAttribute<RegraSemantica, String> regra;

    public RegraSemantica_() {
    }
}
