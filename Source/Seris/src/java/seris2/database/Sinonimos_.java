package seris2.database;

import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@StaticMetamodel(Sinonimos.class)
public class Sinonimos_ {

    public static volatile SingularAttribute<Sinonimos, Integer> id;
    public static volatile SingularAttribute<Sinonimos, String> nome;

    public Sinonimos_() {
    }
}
