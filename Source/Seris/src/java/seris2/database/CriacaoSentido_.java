package seris2.database;

import java.util.Date;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import seris.database.Local;
import seris.database.Usuario;

@StaticMetamodel(CriacaoSentido.class)
public class CriacaoSentido_ {

    public static volatile SingularAttribute<CriacaoSentido, Usuario> usuario;
    public static volatile SingularAttribute<CriacaoSentido, Character> status;
    public static volatile SingularAttribute<CriacaoSentido, ListaSinalFraco> listaSinalFraco;
    public static volatile SingularAttribute<CriacaoSentido, Date> data;
    public static volatile SingularAttribute<CriacaoSentido, Integer> localId;
    public static volatile SingularAttribute<CriacaoSentido, Integer> listaSinalFracoId;
    public static volatile SingularAttribute<CriacaoSentido, Integer> usuarioId;
    public static volatile SingularAttribute<CriacaoSentido, Integer> id;
    public static volatile SingularAttribute<CriacaoSentido, String> lugar;
    public static volatile SingularAttribute<CriacaoSentido, String> empresa;
    public static volatile SingularAttribute<CriacaoSentido, String> grafico;
    public static volatile SingularAttribute<CriacaoSentido, String> nome;
    public static volatile SingularAttribute<CriacaoSentido, Local> local;

    public CriacaoSentido_() {
    }
}
