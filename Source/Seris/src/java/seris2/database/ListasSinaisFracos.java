package seris2.database;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.Table;
import seris.database.Sinalfraco;

@Entity
@Table(name = "listas_sinais_fracos")
@NamedQueries({
    @javax.persistence.NamedQuery(name = "ListasSinaisFracos.findAll", query = "SELECT l FROM ListasSinaisFracos l")
    , @javax.persistence.NamedQuery(name = "ListasSinaisFracos.findById", query = "SELECT l FROM ListasSinaisFracos l WHERE l.id = :id")})
public class ListasSinaisFracos
        implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic
    @Column(name = "numero")
    private Integer numero;
    @Basic(optional = false)
    @Column(name = "lista_sinal_fraco_id")
    private Integer listaSinalFracoId;
    @Basic(optional = false)
    @Column(name = "sinal_fraco_id")
    private Integer sinalFracoId;
    @JoinColumn(name = "lista_sinal_fraco_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private ListaSinalFraco listaSinalFraco;
    @JoinColumn(name = "sinal_fraco_id", referencedColumnName = "id", insertable = false, updatable = false)
    @ManyToOne(optional = false)
    private Sinalfraco sinalFraco;

    public ListasSinaisFracos() {
    }

    public ListasSinaisFracos(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    public boolean equals(Object object) {
        if (!(object instanceof ListasSinaisFracos)) {
            return false;
        }
        ListasSinaisFracos other = (ListasSinaisFracos) object;
        if (((id == null) && (id != null)) || ((id != null) && (!id.equals(id)))) {
            return false;
        }
        return true;
    }

    public String toString() {
        return "seris2.database.ListasSinaisFracos[id=" + id + "]";
    }

    public Integer getListaSinalFracoId() {
        return listaSinalFracoId;
    }

    public void setListaSinalFracoId(Integer listaSinalFracoId) {
        this.listaSinalFracoId = listaSinalFracoId;
    }

    public Integer getSinalFracoId() {
        return sinalFracoId;
    }

    public void setSinalFracoId(Integer sinalFracoId) {
        this.sinalFracoId = sinalFracoId;
    }

    public ListaSinalFraco getListaSinalFraco() {
        return listaSinalFraco;
    }

    public void setListaSinalFraco(ListaSinalFraco listaSinalFraco) {
        this.listaSinalFraco = listaSinalFraco;
    }

    public Sinalfraco getSinalFraco() {
        return sinalFraco;
    }

    public void setSinalFraco(Sinalfraco sinalFraco) {
        this.sinalFraco = sinalFraco;
    }

    public Integer getNumero() {
        return numero;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }
}
