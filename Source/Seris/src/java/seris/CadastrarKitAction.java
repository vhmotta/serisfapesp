package seris;

import java.io.PrintStream;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.ResourceBundle;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import seris.database.Ator;
import seris.database.DAO.ProdutoDAO;
import seris.database.Dominio;
import seris.database.Fonte;
import seris.database.Kit;
import seris.database.Mercado;
import seris.database.Produto;

public class CadastrarKitAction extends DispatchAction {

    private static final String SUCCESS = "success";
    private static final String FAILURE = "failure";
    private ResourceBundle resource = ResourceBundle.getBundle("seris.SerisIdioma");

    public CadastrarKitAction() {
    }

    public ActionForward save(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        try {
            int vNivel = -1;
            if (request.getSession().getAttribute("nivel") != null) {
                vNivel = Integer.parseInt(String.valueOf(request.getSession().getAttribute("nivel")));
            }
            if ((vNivel == 1) || (vNivel == 2)) {
                System.out.println("cadastrar Kit");
                CadastrarKitActionForm formBean = (CadastrarKitActionForm) form;
                request.getSession().setAttribute("nomeDoKit", formBean.getNomeKit());
                int usuario_id = Integer.parseInt(String.valueOf(request.getSession().getAttribute("usuario_id")));

                Session session = HibernateUtil.getSession();
                Transaction transaction = session.beginTransaction();
                transaction.begin();
                seris.database.Usuario usuario = null;
                String dominioSelecionado = String.valueOf(request.getSession().getAttribute("dominioSelecionado"));
                int vdominioSelecionado = 0;
                Dominio d = new Dominio();
                if ((dominioSelecionado != null) && (!dominioSelecionado.equals(""))) {
                    vdominioSelecionado = Integer.parseInt(String.valueOf(dominioSelecionado));
                    d = seris.database.DAO.DominioDAO.consultarDominio(vdominioSelecionado);
                }
                try {
                    usuario = seris.database.DAO.UsuarioDAO.consultarUsuario(usuario_id);
                } catch (NoSuchElementException e) {
                    e.printStackTrace();
                }
                formBean.setNomeDominio(d.getNome());
                formBean.setAtores(session.createQuery("from Ator").list());
                formBean.setFontes(session.createQuery("from Fonte").list());
                formBean.setMercados(session.createQuery("from Mercado").list());

                String[] vAtor = request.getParameterValues("ator");
                formBean.setAtoresSelecionados(vAtor);

                String[] vMercado = request.getParameterValues("mercado");
                formBean.setMercadosSelecionados(vMercado);

                String[] vFonte = request.getParameterValues("fonte");
                formBean.setFontesSelecionadas(vFonte);

                String[] vProduto = request.getParameterValues("produto");
                formBean.setProdutosSelecionados(vProduto);

                Kit kit = null;
                if (formBean.getId() == 0) {
                    kit = new Kit(usuario, formBean.getNomeKit(), new Date(), d);
                    session.save(kit);
                } else {
                    kit = seris.database.DAO.KitDAO.consultarKit(formBean.getId());
                    kit.setDescricao(formBean.getNomeKit());
                    kit.setId(formBean.getId());
                    session.update(kit);

                    List vListaAtores = seris.database.DAO.AtorDAO.consultarListaAtoresPorKit(String.valueOf(formBean.getId()));
                    for (int i = 0; i < vListaAtores.size(); i++) {
                        Ator ator = (Ator) vListaAtores.get(i);
                        ator = (Ator) session.createQuery("from Ator where id=" + ator.getId()).list().iterator().next();
                        ator.getAtorKits().remove(kit);
                        session.saveOrUpdate(ator);
                    }

                    List vListaFontes = seris.database.DAO.FonteDAO.consultarListaFontesPorKit(String.valueOf(formBean.getId()));
                    for (int i = 0; i < vListaFontes.size(); i++) {
                        Fonte fonte = (Fonte) vListaFontes.get(i);
                        fonte = (Fonte) session.createQuery("from Fonte where id=" + fonte.getId()).list().iterator().next();
                        fonte.getFonteKits().remove(kit);
                        session.saveOrUpdate(fonte);
                    }

                    List vListaMercados = seris.database.DAO.MercadoDAO.consultarListaMercadosPorKit(String.valueOf(formBean.getId()));
                    for (int i = 0; i < vListaMercados.size(); i++) {
                        Mercado mercado = (Mercado) vListaMercados.get(i);
                        mercado = (Mercado) session.createQuery("from Mercado where id=" + mercado.getId()).list().iterator().next();
                        mercado.getMercadoKits().remove(kit);
                        session.saveOrUpdate(mercado);
                    }

                    List vListaProdutos = ProdutoDAO.consultarListaProdutosPorKit(String.valueOf(formBean.getId()));
                    for (int i = 0; i < vListaProdutos.size(); i++) {
                        Produto produto = (Produto) vListaProdutos.get(i);
                        produto = (Produto) session.createQuery("from Produto where id=" + produto.getId()).list().iterator().next();
                        produto.getProdutoKits().remove(kit);
                        session.saveOrUpdate(produto);
                    }
                }

                Ator ator = null;
                String[] atoresSelecionados = formBean.getAtoresSelecionados();
                for (int i = 0; i < atoresSelecionados.length; i++) {
                    ator = (Ator) session.createQuery("from Ator where id=" + atoresSelecionados[i]).list().iterator().next();
                    ator.getAtorKits().add(kit);
                    session.saveOrUpdate(ator);
                }

                Fonte fonte = null;
                String[] fontesSelecionadas = formBean.getFontesSelecionadas();
                for (int i = 0; i < fontesSelecionadas.length; i++) {
                    fonte = (Fonte) session.createQuery("from Fonte where id=" + fontesSelecionadas[i]).list().iterator().next();
                    fonte.getFonteKits().add(kit);
                    session.saveOrUpdate(fonte);
                }

                Mercado mercado = null;
                String[] mercadosSelecionados = formBean.getMercadosSelecionados();
                for (int i = 0; i < mercadosSelecionados.length; i++) {
                    mercado = (Mercado) session.createQuery("from Mercado where id=" + mercadosSelecionados[i]).list().iterator().next();
                    mercado.getMercadoKits().add(kit);
                    session.saveOrUpdate(mercado);
                }

                Produto produto = null;
                String[] produtosSelecionados = formBean.getProdutosSelecionados();
                for (int i = 0; i < produtosSelecionados.length; i++) {
                    produto = (Produto) session.createQuery("from Produto where id=" + produtosSelecionados[i]).list().iterator().next();
                    produto.getProdutoKits().add(kit);
                    session.saveOrUpdate(produto);
                }

                transaction.commit();
                session.flush();
                session.close();

                if ((formBean.getNomeKit() == null) || (formBean.getNomeKit().equals(""))) {
                    request.setAttribute("message", "Um nome de Kit deve ser fornecido!");
                    return mapping.findForward("failure");
                }
                request.setAttribute("message", resource.getString("message.salvoSucesso"));
                return mapping.findForward("success");
            }

            request.setAttribute("message", resource.getString("message.usuarioSemPermissao"));
            return mapping.findForward("failure");
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return mapping.findForward("failure");
    }

    public ActionForward delete(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        return mapping.findForward("success");
    }

    public ActionForward novo(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        return mapping.findForward("success");
    }

    public ActionForward pesquisar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        return mapping.findForward("success");
    }
}
