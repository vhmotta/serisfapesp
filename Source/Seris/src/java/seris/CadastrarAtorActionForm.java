package seris;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class CadastrarAtorActionForm
        extends ActionForm {

    private int id = 0;
    private String nome;
    private List atores;
    private String task = " ";
    private String arquivoImportar;

    public List getAtores() {
        return atores;
    }

    public void setAtores(List atores) {
        this.atores = atores;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public CadastrarAtorActionForm() {
    }

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        return errors;
    }

    public String getArquivoImportar() {
        return arquivoImportar;
    }

    public void setArquivoImportar(String arquivoImportar) {
        this.arquivoImportar = arquivoImportar;
    }
}
