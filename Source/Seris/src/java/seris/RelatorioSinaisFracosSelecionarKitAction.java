package seris;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class RelatorioSinaisFracosSelecionarKitAction
        extends Action {

    private static final String SUCCESS = "success";

    public RelatorioSinaisFracosSelecionarKitAction() {
    }

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        RelatorioSinaisFracosActionForm formBean = (RelatorioSinaisFracosActionForm) form;
        String kitSelecionado = formBean.getKitSelecionado();
        HttpSession sessao = request.getSession(true);
        sessao.setAttribute("kitSelecionado", kitSelecionado);

        return mapping.findForward("success");
    }
}
