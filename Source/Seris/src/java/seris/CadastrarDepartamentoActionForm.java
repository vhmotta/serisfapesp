package seris;

import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class CadastrarDepartamentoActionForm
        extends ActionForm {

    private int id = 0;
    private String nome;
    private List departamentos;
    private String task = " ";

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List getDepartamentos() {
        return departamentos;
    }

    public void setDepartamentos(List departamentos) {
        this.departamentos = departamentos;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public CadastrarDepartamentoActionForm() {
    }

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        super.reset(mapping, request);
        id = 0;
        nome = "";
        departamentos = new ArrayList();
        task = " ";
    }

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        if ((getNome() == null) || (getNome().length() < 1)) {
            errors.add("departamento", new ActionMessage("error.departamento.required"));
        }
        return errors;
    }
}
