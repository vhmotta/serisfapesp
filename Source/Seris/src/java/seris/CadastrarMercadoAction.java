package seris;

import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import seris.database.Mercado;
import seris.functions.BasicFunctions;

public class CadastrarMercadoAction
        extends DispatchAction {

    private static final String SUCCESS = "success";
    private static final String FAILURE = "failure";
    private ResourceBundle resource = ResourceBundle.getBundle("seris.SerisIdioma");
    private ResourceBundle resourceSeris = ResourceBundle.getBundle("seris.seris");

    public CadastrarMercadoAction() {
    }

    public ActionForward save(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        int vNivel = -1;
        if (request.getSession().getAttribute("nivel") != null) {
            vNivel = Integer.parseInt(String.valueOf(request.getSession().getAttribute("nivel")));
        }
        if ((vNivel == 1) || (vNivel == 2)) {
            CadastrarMercadoActionForm formBean = (CadastrarMercadoActionForm) form;
            if ((formBean.getNome() != null) && (!formBean.getNome().equals(""))) {
                Mercado mercado = new Mercado(formBean.getNome());

                Session session = HibernateUtil.getSession();
                Transaction transaction = session.beginTransaction();
                transaction.begin();
                if (formBean.getId() > 0) {
                    List vList = session.createQuery("from Mercado m where m.id <> " + formBean.getId() + " and m.nome = '" + formBean.getNome() + "'").list();

                    if (vList.size() == 0) {
                        mercado.setId(Integer.valueOf(formBean.getId()));
                        session.update(mercado);
                    } else {
                        transaction.rollback();
                        session.flush();
                        session.close();
                        request.setAttribute("message", resource.getString("message.registroExistente"));
                        return mapping.findForward("failure");
                    }
                } else {
                    List vList = session.createQuery("from Mercado m where m.nome = '" + formBean.getNome() + "'").list();

                    if (vList.size() == 0) {
                        session.save(mercado);
                    } else {
                        transaction.rollback();
                        session.flush();
                        session.close();
                        request.setAttribute("message", resource.getString("message.registroExistente"));
                        return mapping.findForward("failure");
                    }
                }
                transaction.commit();
                session.flush();
                session.close();
            } else {
                Session session = HibernateUtil.getSession();
                Transaction transaction = session.beginTransaction();
                transaction.begin();
                List vList = session.createQuery("from Mercado").list();
                formBean.setMercados(vList);
                transaction.commit();
                session.flush();
                session.close();
                return mapping.findForward("failure");
            }
            request.setAttribute("message", resource.getString("message.salvoSucesso"));
        } else {
            request.setAttribute("message", resource.getString("message.usuarioSemPermissao"));
        }
        return mapping.findForward("success");
    }

    public ActionForward delete(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        try {
            int vNivel = -1;
            if (request.getSession().getAttribute("nivel") != null) {
                vNivel = Integer.parseInt(String.valueOf(request.getSession().getAttribute("nivel")));
            }
            if ((vNivel == 1) || (vNivel == 2)) {
                CadastrarMercadoActionForm formBean = (CadastrarMercadoActionForm) form;
                if (formBean.getId() > 0) {
                    Session session = HibernateUtil.getSession();
                    Transaction transaction = session.beginTransaction();
                    Mercado mercado = (Mercado) session.createQuery("from Mercado where id=" + formBean.getId()).list().iterator().next();
                    session.delete(mercado);
                    transaction.commit();
                    session.flush();
                    session.close();

                    formBean.setId(0);
                    formBean.setNome(null);
                }
                request.setAttribute("message", resource.getString("message.deletadoSucesso"));
            } else {
                request.setAttribute("message", resource.getString("message.usuarioSemPermissao"));
            }
            return mapping.findForward("success");
        } catch (Exception ex) {
            request.setAttribute("message", resource.getString("message.falhaDeletar"));
        }
        return mapping.findForward("failure");
    }

    public ActionForward importar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        int vNivel = -1;
        if (request.getSession().getAttribute("nivel") != null) {
            vNivel = Integer.parseInt(String.valueOf(request.getSession().getAttribute("nivel")));
        }
        if ((vNivel == 1) || (vNivel == 2)) {
            int total = 0;

            CadastrarMercadoActionForm formBean = (CadastrarMercadoActionForm) form;
            if ((formBean.getArquivoImportar() != null) && (!formBean.getArquivoImportar().equals(""))) {

                Session session = HibernateUtil.getSession();
                Transaction transaction = session.beginTransaction();
                transaction.begin();

                String fileName = BasicFunctions.readText(resourceSeris.getString("SystemPath") + "/tmp/" + formBean.getArquivoImportar());
                String[] linhas = fileName.split("\n");

                for (int i = 0; i < linhas.length; i++) {
                    String valor = linhas[i].trim();

                    if (!valor.equals("")) {
                        List vList = session.createQuery("from Mercado a where a.nome = '" + valor + "'").list();

                        if (vList.isEmpty()) {
                            total++;
                            Mercado mercado = new Mercado(valor);
                            session.save(mercado);
                        }
                    }
                }

                transaction.commit();
                session.flush();
                session.close();
            }
            String msg = resource.getString("message.importadoSucesso") + "<br>" + resource.getString("message.totalImportado") + " " + total;
            request.setAttribute("message", msg);
        } else {
            request.setAttribute("message", resource.getString("message.usuarioSemPermissao"));
        }

        return mapping.findForward("success");
    }

    public ActionForward novo(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        return mapping.findForward("success");
    }

    public ActionForward pesquisar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        return mapping.findForward("success");
    }
}
