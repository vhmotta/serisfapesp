package seris;

import java.io.PrintStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class RelatorioSinaisFracosAction
        extends Action {

    private static final String SUCCESS = "success";

    public RelatorioSinaisFracosAction() {
    }

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        RelatorioSinaisFracosActionForm formBean = (RelatorioSinaisFracosActionForm) form;

        request.getSession().setAttribute("ator", Integer.valueOf(formBean.getAtor()));
        request.getSession().setAttribute("fonte", Integer.valueOf(formBean.getFonte()));
        request.getSession().setAttribute("mercado", Integer.valueOf(formBean.getMercado()));
        request.getSession().setAttribute("produto", Integer.valueOf(formBean.getProduto()));
        request.getSession().setAttribute("avaliacao", Integer.valueOf(formBean.getAvaliacao()));
        request.getSession().setAttribute("usuario", Integer.valueOf(formBean.getUsuario()));
        request.getSession().setAttribute("perfil", Integer.valueOf(formBean.getPerfil()));
        request.getSession().setAttribute("grupo", Integer.valueOf(formBean.getGrupo()));
        request.getSession().setAttribute("departamento", Integer.valueOf(formBean.getDepartamento()));
        request.getSession().setAttribute("dataInicio", formBean.getDataInicio());
        request.getSession().setAttribute("dataFinal", formBean.getDataFinal());

        String tipoConsulta = String.valueOf(request.getSession().getAttribute("tipoConsulta"));
        System.out.println("tipoConsulta=" + tipoConsulta + " formBean.getKitSelecionado()=" + formBean.getKitSelecionado());
        if (tipoConsulta.equals("2")) {
            request.getSession().setAttribute("kitSelecionado", formBean.getKitSelecionado());
        }

        formBean.reset(mapping, request);

        return mapping.findForward("success");
    }
}
