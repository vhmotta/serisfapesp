package seris;

import java.util.ArrayList;
import java.util.List;

public class Nivel {

    public static final int ADMINISTRADOR = 0;
    public static final int PATROCINADOR = 1;
    public static final int SUPORTEDOMINIO = 2;
    public static final int ALTADIRECAO = 3;
    public static final int CAPTADOR = 4;
    public static final int ANALISTA = 5;

    public Nivel() {
    }

    public static int getNivel(String nivel) {
        if (nivel.equals("Administrador")) {
            return 0;
        }
        if (nivel.equals("Patrocinador")) {
            return 1;
        }
        if (nivel.equals("Suporte Domínio")) {
            return 2;
        }
        if (nivel.equals("Alta Direção")) {
            return 3;
        }
        if (nivel.equals("Captador")) {
            return 4;
        }
        if (nivel.equals("Facilitador")) {
            return 5;
        }
        return 100;
    }

    public static List getNiveis() {
        List niveis = new ArrayList();
        niveis.add("Administrador");
        niveis.add("Patrocinador");
        niveis.add("Suporte Domínio");
        niveis.add("Alta Direção");
        niveis.add("Captador");
        niveis.add("Facilitador");
        return niveis;
    }
}
