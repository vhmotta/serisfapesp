package seris;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class RelatorioSinaisFracosActionForm
        extends ActionForm {

    private String dominioSelecionado;
    private String kitSelecionado;
    private String tipoConsulta;
    private String[][] relatorioSinaisFracos;
    private int dominio = 0;
    private int kit = 0;
    private int ator = 0;
    private int produto = 0;
    private int fonte = 0;
    private int mercado = 0;
    private int avaliacao = 0;
    private int usuario = 0;
    private int perfil = 0;
    private int grupo = 0;
    private int departamento = 0;
    private String dataInicio = "";
    private String dataFinal = "";

    public String getDataFinal() {
        return dataFinal;
    }

    public void setDataFinal(String dataFinal) {
        this.dataFinal = dataFinal;
    }

    public String getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(String dataInicio) {
        this.dataInicio = dataInicio;
    }

    public int getUsuario() {
        return usuario;
    }

    public void setUsuario(int usuario) {
        this.usuario = usuario;
    }

    public int getDepartamento() {
        return departamento;
    }

    public void setDepartamento(int departamento) {
        this.departamento = departamento;
    }

    public int getGrupo() {
        return grupo;
    }

    public void setGrupo(int grupo) {
        this.grupo = grupo;
    }

    public int getPerfil() {
        return perfil;
    }

    public void setPerfil(int perfil) {
        this.perfil = perfil;
    }

    public String getTipoConsulta() {
        return tipoConsulta;
    }

    public void setTipoConsulta(String tipoConsulta) {
        this.tipoConsulta = tipoConsulta;
    }

    public int getAtor() {
        return ator;
    }

    public void setAtor(int ator) {
        this.ator = ator;
    }

    public int getAvaliacao() {
        return avaliacao;
    }

    public void setAvaliacao(int avaliacao) {
        this.avaliacao = avaliacao;
    }

    public int getDominio() {
        return dominio;
    }

    public void setDominio(int dominio) {
        this.dominio = dominio;
    }

    public String getDominioSelecionado() {
        return dominioSelecionado;
    }

    public void setDominioSelecionado(String dominioSelecionado) {
        this.dominioSelecionado = dominioSelecionado;
    }

    public int getFonte() {
        return fonte;
    }

    public void setFonte(int fonte) {
        this.fonte = fonte;
    }

    public int getKit() {
        return kit;
    }

    public void setKit(int kit) {
        this.kit = kit;
    }

    public String getKitSelecionado() {
        return kitSelecionado;
    }

    public void setKitSelecionado(String kitSelecionado) {
        this.kitSelecionado = kitSelecionado;
    }

    public int getMercado() {
        return mercado;
    }

    public void setMercado(int mercado) {
        this.mercado = mercado;
    }

    public int getProduto() {
        return produto;
    }

    public void setProduto(int produto) {
        this.produto = produto;
    }

    public String[][] getRelatorioSinaisFracos() {
        return relatorioSinaisFracos;
    }

    public void setRelatorioSinaisFracos(String[][] relatorioSinaisFracos) {
        this.relatorioSinaisFracos = relatorioSinaisFracos;
    }

    public RelatorioSinaisFracosActionForm() {
    }

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        return errors;
    }
}
