package seris;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class CadastrarProdutoActionForm
        extends ActionForm {

    private Integer id;
    private String descricao;
    private List produtos;
    private String task = " ";
    private String arquivoImportar;

    public CadastrarProdutoActionForm(Integer id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List getProdutos() {
        return produtos;
    }

    public void setProdutos(List produtos) {
        this.produtos = produtos;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public CadastrarProdutoActionForm() {
    }

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();

        return errors;
    }

    public String getArquivoImportar() {
        return arquivoImportar;
    }

    public void setArquivoImportar(String arquivoImportar) {
        this.arquivoImportar = arquivoImportar;
    }
}
