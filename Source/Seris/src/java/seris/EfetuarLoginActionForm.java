package seris;

import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class EfetuarLoginActionForm
        extends ActionForm {

    private int id;
    private String login;
    private String senha;
    private int nivel;
    private String nome;
    private String mensagem;
    private String task = " ";

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public EfetuarLoginActionForm() {
    }

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        super.reset(mapping, request);
        login = null;
        senha = null;
        nivel = 100;
        nome = null;
        mensagem = null;
    }

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        if ((getLogin() == null) || (getLogin().length() < 1)) {
            errors.add("login", new ActionMessage("error.login.required"));
        }
        if ((getSenha() == null) || (getSenha().length() < 1)) {
            errors.add("senha", new ActionMessage("error.senha.required"));
        }
        return errors;
    }
}
