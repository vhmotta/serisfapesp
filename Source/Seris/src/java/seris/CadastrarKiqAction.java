package seris;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import seris.database.Kiq;
import seris.database.Kit;
import seris.database.Usuario;

public class CadastrarKiqAction
        extends DispatchAction {

    private static final String SUCCESS = "success";
    private ResourceBundle resource = ResourceBundle.getBundle("seris.SerisIdioma");

    public CadastrarKiqAction() {
    }

    public ActionForward save(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        int vNivel = -1;
        if (request.getSession().getAttribute("nivel") != null) {
            vNivel = Integer.parseInt(String.valueOf(request.getSession().getAttribute("nivel")));
        }
        if ((vNivel == 1) || (vNivel == 2)) {
            CadastrarKiqActionForm formBean = (CadastrarKiqActionForm) form;

            Session session = HibernateUtil.getSession();
            Transaction transaction = session.beginTransaction();
            transaction.begin();
            Usuario usuario = null;
            try {
                int usuario_id = Integer.parseInt(String.valueOf(request.getSession().getAttribute("usuario_id")));
                usuario = (Usuario) session.createQuery("from Usuario where id=" + usuario_id).list().iterator().next();
            } catch (NoSuchElementException e) {
            }

            formBean.setKits(session.createQuery("from Kit where usuario_id=" + usuario.getId()).list());
            transaction.commit();
            session.flush();
            session.close();
            if ((formBean.getDescricao() == null) || (formBean.getDescricao().equals(""))) {
                formBean.setStatus("Um nome de Kiq deve ser fornecido!");
                return mapping.findForward("success");
            }

            session = HibernateUtil.getSession();
            transaction = session.beginTransaction();
            transaction.begin();

            if (formBean.getId() == 0) {
                Kit kit = (Kit) session.createQuery("from Kit where id=" + formBean.getKitSelecionado()).list().iterator().next();
                Kiq kiq = new Kiq(kit, formBean.getDescricao());
                session.save(kiq);
            } else {
                Kit kit = (Kit) session.createQuery("from Kit where id=" + formBean.getKitSelecionado()).list().iterator().next();
                Kiq kiq = new Kiq(formBean.getId(), kit, formBean.getDescricao());
                session.update(kiq);
            }

            transaction.commit();
            session.flush();
            session.close();
            formBean.setDescricao("");
            formBean.setStatus(resource.getString("message.salvoSucesso"));
            return mapping.findForward("success");
        }

        request.setAttribute("message", resource.getString("message.usuarioSemPermissao"));
        return mapping.findForward("success");
    }

    public ActionForward delete(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        return mapping.findForward("success");
    }

    public ActionForward novo(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        return mapping.findForward("success");
    }

    public ActionForward pesquisar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        return mapping.findForward("success");
    }
}
