package seris;

import java.io.PrintStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class RelatorioSinaisFracosSelecionarDominioAction
        extends Action {

    private static final String FAILURE = "failure";
    private static final String SELECIONARKIT = "selecionarKit";
    private static final String RELATORIOSINAISFRACOS = "relatorioSinaisFracos";

    public RelatorioSinaisFracosSelecionarDominioAction() {
    }

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        RelatorioSinaisFracosActionForm formBean = (RelatorioSinaisFracosActionForm) form;
        String dominioSelecionado = formBean.getDominioSelecionado();
        HttpSession sessao = request.getSession(true);
        sessao.setAttribute("dominioSelecionado", dominioSelecionado);

        String tipoConsulta = (String) sessao.getAttribute("tipoConsulta");

        System.out.println(tipoConsulta);

        if ((tipoConsulta != null) && (!tipoConsulta.equals(""))) {
            if (tipoConsulta.equals("1")) {
                return mapping.findForward("selecionarKit");
            }
            if (tipoConsulta.equals("2")) {
                return mapping.findForward("relatorioSinaisFracos");
            }
        }
        return mapping.findForward("failure");
    }
}
