package seris.utils;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Utils {

    public Utils() {
    }

    public static String getDataSistema() {
        Calendar ca = GregorianCalendar.getInstance();
        Date data_sistema = ca.getTime();

        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        String data = formato.format(data_sistema);
        return data;
    }

    public static String getDataSistema(long dias) {
        Calendar ca = GregorianCalendar.getInstance();

        long count = Math.abs(dias);
        for (int i = 0; i < count; i++) {
            if (dias > 0L) {
                ca.setTimeInMillis(ca.getTimeInMillis() + 86400000L);
            } else {
                ca.setTimeInMillis(ca.getTimeInMillis() - 86400000L);
            }
        }

        Date data_sistema = ca.getTime();

        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        String data = formato.format(data_sistema);
        return data;
    }

    public static String FormatarDataBD(String Data) {
        String DataModificada = null;

        String Dia = null;
        String Mes = null;
        String Ano = null;

        Dia = Data.substring(0, 2);
        Mes = Data.substring(3, 5);
        Ano = Data.substring(6, 10);

        DataModificada = Ano + "/" + Mes + "/" + Dia;
        return DataModificada;
    }

    public static String FormatarDataFrm(String Data) {
        String DataModificada = null;

        String Dia = null;
        String Mes = null;
        String Ano = null;

        Dia = Data.substring(8, 10);
        Mes = Data.substring(5, 7);
        Ano = Data.substring(0, 4);

        DataModificada = Dia + "/" + Mes + "/" + Ano;
        return DataModificada;
    }

    public static Date ConverteStringParaDate(String Data) {
        Date vData = new Date();
        try {
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            vData = df.parse(Data);
        } catch (ParseException ex) {
            ex.printStackTrace();
        }
        return vData;
    }

    public static String ConverteDateParaString(Date Data) {
        String vData = "";
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        vData = df.format(Data);

        return vData;
    }

    public static String ConverteDateParaStringSQL(Date Data) {
        String vData = "";
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        vData = df.format(Data);

        return vData;
    }

    public static String DoubleToString(double Valor) {
        DecimalFormat vDecimal = new DecimalFormat("#,##0.00");
        String vValor = null;

        vValor = vDecimal.format(Valor);

        return vValor;
    }
}
