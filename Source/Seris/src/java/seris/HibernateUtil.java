package seris;

import java.io.PrintStream;
import java.util.Calendar;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.hibernate.classic.Session;

public class HibernateUtil {

    private static SessionFactory sessionFactory;
    private static Calendar FsConectionTimeOut = Calendar.getInstance();

    static {
        setNewFactory();
    }

    private static void setNewFactory() {
        try {
            try {
                if (sessionFactory != null) {
                    sessionFactory.close();
                }
            } catch (Exception e) {
            }

            AnnotationConfiguration configuration = new AnnotationConfiguration().configure("hibernate.cfg.xml");
            System.out.println(configuration.getProperties());
            sessionFactory = configuration.buildSessionFactory();
        } catch (Throwable ex) {
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static Session getSession() {
        if (Calendar.getInstance().getTimeInMillis() > FsConectionTimeOut.getTimeInMillis() + 1800000L) {
            setNewFactory();
        }

        FsConectionTimeOut.setTimeInMillis(Calendar.getInstance().getTimeInMillis());

        if (sessionFactory.isClosed()) {
            setNewFactory();
        }

        Session session = sessionFactory.openSession();

        return session;
    }

    public HibernateUtil() {
    }
}
