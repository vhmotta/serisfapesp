package seris;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class CadastrarKitActionForm
        extends ActionForm {

    private int id = 0;
    private String dominioSelecionado;
    private String nomeDominio;
    private String nomeKit;
    private String novoAtor;
    private String[] atoresSelecionados = new String[0];
    private List atores;
    private String novaFonte;
    private String[] fontesSelecionadas = new String[0];
    private List fontes;
    private String novoProduto;
    private List produtos;
    private String[] produtosSelecionados = new String[0];
    private String novoMercado;
    private String[] mercadosSelecionados = new String[0];
    private List mercados;
    private String status;
    private String mensagem;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDominioSelecionado() {
        return dominioSelecionado;
    }

    public void setDominioSelecionado(String dominioSelecionado) {
        this.dominioSelecionado = dominioSelecionado;
    }

    public String getMensagem() {
        return mensagem;
    }

    public void setMensagem(String mensagem) {
        this.mensagem = mensagem;
    }

    public String[] getProdutosSelecionados() {
        return produtosSelecionados;
    }

    public void setProdutosSelecionados(String[] produtosSelecionados) {
        this.produtosSelecionados = produtosSelecionados;
    }

    public List getAtores() {
        return atores;
    }

    public void setAtores(List atores) {
        this.atores = atores;
    }

    public String[] getAtoresSelecionados() {
        return atoresSelecionados;
    }

    public void setAtoresSelecionados(String[] atoresSelecionados) {
        this.atoresSelecionados = atoresSelecionados;
    }

    public List getFontes() {
        return fontes;
    }

    public void setFontes(List fontes) {
        this.fontes = fontes;
    }

    public String[] getFontesSelecionadas() {
        return fontesSelecionadas;
    }

    public void setFontesSelecionadas(String[] fontesSelecionadas) {
        this.fontesSelecionadas = fontesSelecionadas;
    }

    public List getMercados() {
        return mercados;
    }

    public void setMercados(List mercados) {
        this.mercados = mercados;
    }

    public String[] getMercadosSelecionados() {
        return mercadosSelecionados;
    }

    public void setMercadosSelecionados(String[] mercadosSelecionados) {
        this.mercadosSelecionados = mercadosSelecionados;
    }

    public String getNomeDominio() {
        return nomeDominio;
    }

    public void setNomeDominio(String nomeDominio) {
        this.nomeDominio = nomeDominio;
    }

    public String getNomeKit() {
        return nomeKit;
    }

    public void setNomeKit(String nomeKit) {
        this.nomeKit = nomeKit;
    }

    public String getNovaFonte() {
        return novaFonte;
    }

    public void setNovaFonte(String novaFonte) {
        this.novaFonte = novaFonte;
    }

    public String getNovoAtor() {
        return novoAtor;
    }

    public void setNovoAtor(String novoAtor) {
        this.novoAtor = novoAtor;
    }

    public String getNovoMercado() {
        return novoMercado;
    }

    public void setNovoMercado(String novoMercado) {
        this.novoMercado = novoMercado;
    }

    public String getNovoProduto() {
        return novoProduto;
    }

    public void setNovoProduto(String novoProduto) {
        this.novoProduto = novoProduto;
    }

    public List getProdutos() {
        return produtos;
    }

    public void setProdutos(List produtos) {
        this.produtos = produtos;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        super.reset(mapping, request);
        nomeDominio = null;
        nomeKit = null;
        novoAtor = null;
        atoresSelecionados = new String[0];
        atores = null;
        novaFonte = null;
        fontesSelecionadas = new String[0];
        fontes = null;
        novoProduto = null;
        produtos = null;
        novoMercado = null;
        mercadosSelecionados = new String[0];
        mercados = null;
        status = null;
    }

    public CadastrarKitActionForm() {
    }

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        return errors;
    }
}
