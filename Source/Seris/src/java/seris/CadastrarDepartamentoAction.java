package seris;

import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import seris.database.Departamento;

public class CadastrarDepartamentoAction
        extends DispatchAction {

    private static final String SUCCESS = "success";
    private static final String FAILURE = "failure";
    private ResourceBundle resource = ResourceBundle.getBundle("seris.SerisIdioma");

    public CadastrarDepartamentoAction() {
    }

    public ActionForward save(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        int vNivel = -1;
        if (request.getSession().getAttribute("nivel") != null) {
            vNivel = Integer.parseInt(String.valueOf(request.getSession().getAttribute("nivel")));
        }
        if ((vNivel == 0) || (vNivel == 2)) {
            CadastrarDepartamentoActionForm formBean = (CadastrarDepartamentoActionForm) form;
            if ((formBean.getNome() != null) && (!formBean.getNome().equals(""))) {
                Departamento departamento = new Departamento(formBean.getNome());

                Session session = HibernateUtil.getSession();
                Transaction transaction = session.beginTransaction();
                transaction.begin();
                if (formBean.getId() > 0) {
                    List vList = session.createQuery("from Departamento d where d.id <> " + formBean.getId() + " and d.nome = '" + formBean.getNome() + "'").list();

                    if (vList.size() == 0) {
                        departamento.setId(formBean.getId());
                        session.update(departamento);
                    } else {
                        transaction.rollback();
                        session.flush();
                        session.close();
                        request.setAttribute("message", resource.getString("message.registroExistente"));
                        return mapping.findForward("failure");
                    }
                } else {
                    List vList = session.createQuery("from Departamento d where d.nome = '" + formBean.getNome() + "'").list();

                    if (vList.size() == 0) {
                        session.save(departamento);
                    } else {
                        transaction.rollback();
                        session.flush();
                        session.close();
                        request.setAttribute("message", resource.getString("message.registroExistente"));
                        return mapping.findForward("failure");
                    }
                }
                transaction.commit();
                session.flush();
                session.close();
            } else {
                Session session = HibernateUtil.getSession();
                Transaction transaction = session.beginTransaction();
                transaction.begin();
                List vList = session.createQuery("from Departamento").list();
                formBean.setDepartamentos(vList);
                transaction.commit();
                session.flush();
                session.close();
                return mapping.findForward("failure");
            }

            request.setAttribute("message", resource.getString("message.salvoSucesso"));
        } else {
            request.setAttribute("message", resource.getString("message.usuarioSemPermissao"));
        }
        return mapping.findForward("success");
    }

    public ActionForward delete(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        try {
            int vNivel = -1;
            if (request.getSession().getAttribute("nivel") != null) {
                vNivel = Integer.parseInt(String.valueOf(request.getSession().getAttribute("nivel")));
            }
            if ((vNivel == 0) || (vNivel == 2)) {
                CadastrarDepartamentoActionForm formBean = (CadastrarDepartamentoActionForm) form;
                if (formBean.getId() > 0) {
                    Session session = HibernateUtil.getSession();
                    Transaction transaction = session.beginTransaction();
                    Departamento departamento = (Departamento) session.createQuery("from Departamento where id=" + formBean.getId()).list().iterator().next();
                    session.delete(departamento);
                    transaction.commit();
                    session.flush();
                    session.close();

                    formBean.setId(0);
                    formBean.setNome(null);
                }
                formBean.reset(mapping, request);
                request.setAttribute("message", resource.getString("message.deletadoSucesso"));
            } else {
                request.setAttribute("message", resource.getString("message.usuarioSemPermissao"));
            }
            return mapping.findForward("success");
        } catch (Exception ex) {
            request.setAttribute("message", resource.getString("message.falhaDeletar"));
        }
        return mapping.findForward("failure");
    }

    public ActionForward novo(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        return mapping.findForward("success");
    }

    public ActionForward pesquisar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        return mapping.findForward("success");
    }
}
