package seris;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class CadastrarSinalfracoActionForm
        extends ActionForm {

    private int id = 0;
    private String dominioSelecionado;
    private List dominios;
    private String dominio;
    private String kitSelecionado;
    private List kits;
    private String kit;
    private String data = "";
    private String kiqsSelecionados;
    private List kiqs;
    private String fonteSelecionada;
    private List fontes;
    private String mercadoSelecionado;
    private List mercados;
    private String avaliacaoSelecionada;
    private List avaliacoes;
    private String produtoSelecionado;
    private List produtos;
    private String atorSelecionado;
    private List atores;
    private String descricao;
    private String comentarios;
    private String informacoes;
    private String status;
    private String localSelecionado;
    private List locais;
    private String nomeLocalCaptura;
    private String descricaoLocalCaptura;

    public String getAtorSelecionado() {
        return atorSelecionado;
    }

    public void setAtorSelecionado(String atorSelecionado) {
        this.atorSelecionado = atorSelecionado;
    }

    public List getAtores() {
        return atores;
    }

    public void setAtores(List atores) {
        this.atores = atores;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricaoLocalCaptura() {
        return descricaoLocalCaptura;
    }

    public void setDescricaoLocalCaptura(String descricaoLocalCaptura) {
        this.descricaoLocalCaptura = descricaoLocalCaptura;
    }

    public String getNomeLocalCaptura() {
        return nomeLocalCaptura;
    }

    public void setNomeLocalCaptura(String nomeLocalCaptura) {
        this.nomeLocalCaptura = nomeLocalCaptura;
    }

    public String getAvaliacaoSelecionada() {
        return avaliacaoSelecionada;
    }

    public void setAvaliacaoSelecionada(String avaliacaoSelecionada) {
        this.avaliacaoSelecionada = avaliacaoSelecionada;
    }

    public List getAvaliacoes() {
        return avaliacoes;
    }

    public void setAvaliacoes(List avaliacoes) {
        this.avaliacoes = avaliacoes;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getDominio() {
        return dominio;
    }

    public void setDominio(String dominio) {
        this.dominio = dominio;
    }

    public String getDominioSelecionado() {
        return dominioSelecionado;
    }

    public void setDominioSelecionado(String dominioSelecionado) {
        this.dominioSelecionado = dominioSelecionado;
    }

    public List getDominios() {
        return dominios;
    }

    public void setDominios(List dominios) {
        this.dominios = dominios;
    }

    public String getFonteSelecionada() {
        return fonteSelecionada;
    }

    public void setFonteSelecionada(String fonteSelecionada) {
        this.fonteSelecionada = fonteSelecionada;
    }

    public List getFontes() {
        return fontes;
    }

    public void setFontes(List fontes) {
        this.fontes = fontes;
    }

    public String getInformacoes() {
        return informacoes;
    }

    public void setInformacoes(String informacoes) {
        this.informacoes = informacoes;
    }

    public List getKiqs() {
        return kiqs;
    }

    public void setKiqs(List kiqs) {
        this.kiqs = kiqs;
    }

    public String getKiqsSelecionados() {
        return kiqsSelecionados;
    }

    public void setKiqsSelecionados(String kiqsSelecionados) {
        this.kiqsSelecionados = kiqsSelecionados;
    }

    public String getKit() {
        return kit;
    }

    public void setKit(String kit) {
        this.kit = kit;
    }

    public String getKitSelecionado() {
        return kitSelecionado;
    }

    public void setKitSelecionado(String kitSelecionado) {
        this.kitSelecionado = kitSelecionado;
    }

    public List getKits() {
        return kits;
    }

    public void setKits(List kits) {
        this.kits = kits;
    }

    public List getLocais() {
        return locais;
    }

    public void setLocais(List locais) {
        this.locais = locais;
    }

    public String getLocalSelecionado() {
        return localSelecionado;
    }

    public void setLocalSelecionado(String localSelecionado) {
        this.localSelecionado = localSelecionado;
    }

    public String getMercadoSelecionado() {
        return mercadoSelecionado;
    }

    public void setMercadoSelecionado(String mercadoSelecionado) {
        this.mercadoSelecionado = mercadoSelecionado;
    }

    public List getMercados() {
        return mercados;
    }

    public void setMercados(List mercados) {
        this.mercados = mercados;
    }

    public String getProdutoSelecionado() {
        return produtoSelecionado;
    }

    public void setProdutoSelecionado(String produtoSelecionado) {
        this.produtoSelecionado = produtoSelecionado;
    }

    public List getProdutos() {
        return produtos;
    }

    public void setProdutos(List produtos) {
        this.produtos = produtos;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        super.reset(mapping, request);
    }

    public CadastrarSinalfracoActionForm() {
    }

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        return errors;
    }
}
