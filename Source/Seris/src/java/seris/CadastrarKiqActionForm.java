package seris;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class CadastrarKiqActionForm
        extends ActionForm {

    private int id = 0;
    private String nomeDominio;
    private String kitSelecionado;
    private List kits;
    private String descricao;
    private String status;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getKitSelecionado() {
        return kitSelecionado;
    }

    public void setKitSelecionado(String kitSelecionado) {
        this.kitSelecionado = kitSelecionado;
    }

    public List getKits() {
        return kits;
    }

    public void setKits(List kits) {
        this.kits = kits;
    }

    public String getNomeDominio() {
        return nomeDominio;
    }

    public void setNomeDominio(String nomeDominio) {
        this.nomeDominio = nomeDominio;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        super.reset(mapping, request);
        id = 0;
        nomeDominio = null;
        kitSelecionado = null;
        kits = null;
        descricao = null;
        status = null;
    }

    public CadastrarKiqActionForm() {
    }

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        return errors;
    }
}
