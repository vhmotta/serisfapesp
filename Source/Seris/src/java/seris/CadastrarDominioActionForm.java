package seris;

import java.util.Date;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class CadastrarDominioActionForm
        extends ActionForm {

    private int id;
    private String nome;
    private Date dataCriacao;
    private List dominios;
    private String task;

    public List getDominios() {
        return dominios;
    }

    public void setDominios(List dominios) {
        this.dominios = dominios;
    }

    public Date getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(Date dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public CadastrarDominioActionForm() {
    }

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        if ((getNome() == null) || (getNome().length() < 1)) {
            errors.add("name", new ActionMessage("error.name.required"));
        }

        return errors;
    }
}
