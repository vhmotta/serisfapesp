package seris;

import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import seris.database.Local;

public class CadastrarLocalAction
        extends DispatchAction {

    private static final String SUCCESS = "success";
    private static final String FAILURE = "failure";
    private ResourceBundle resource = ResourceBundle.getBundle("seris.SerisIdioma");

    public CadastrarLocalAction() {
    }

    public ActionForward save(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        int vNivel = -1;
        if (request.getSession().getAttribute("nivel") != null) {
            vNivel = Integer.parseInt(String.valueOf(request.getSession().getAttribute("nivel")));
        }
        if ((vNivel == 0) || (vNivel == 2)) {
            CadastrarLocalActionForm formBean = (CadastrarLocalActionForm) form;
            if ((formBean.getCidade() != null) && (!formBean.getCidade().equals(""))) {
                Local local = new Local();
                if (formBean.getCidade() == null) {
                    formBean.setCidade("");
                }
                local.setCidade(formBean.getCidade());
                if (formBean.getUf() == null) {
                    formBean.setUf("");
                }
                local.setUf(formBean.getUf());
                if (formBean.getPais() == null) {
                    formBean.setPais("");
                }
                local.setPais(formBean.getPais());

                Session session = HibernateUtil.getSession();
                Transaction transaction = session.beginTransaction();
                transaction.begin();
                if (formBean.getId().intValue() > 0) {
                    List vList = session.createQuery("from Local l where l.id <> " + formBean.getId() + " and l.cidade = '" + formBean.getCidade() + "'" + " and l.uf = '" + formBean.getUf() + "'").list();

                    if (vList.size() == 0) {
                        local.setId(formBean.getId());
                        session.update(local);
                    } else {
                        transaction.rollback();
                        session.flush();
                        session.close();
                        request.setAttribute("message", resource.getString("message.registroExistente"));
                        return mapping.findForward("failure");
                    }
                } else {
                    List vList = session.createQuery("from Local l where l.cidade = '" + formBean.getCidade() + "'" + " and l.uf = '" + formBean.getUf() + "'").list();

                    if (vList.size() == 0) {
                        session.save(local);
                    } else {
                        transaction.rollback();
                        session.flush();
                        session.close();
                        request.setAttribute("message", resource.getString("message.registroExistente"));
                        return mapping.findForward("failure");
                    }
                }
                transaction.commit();
                session.flush();
                session.close();
            }
            request.setAttribute("message", resource.getString("message.salvoSucesso"));
        } else {
            request.setAttribute("message", resource.getString("message.usuarioSemPermissao"));
        }
        return mapping.findForward("success");
    }

    public ActionForward delete(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        try {
            int vNivel = -1;
            if (request.getSession().getAttribute("nivel") != null) {
                vNivel = Integer.parseInt(String.valueOf(request.getSession().getAttribute("nivel")));
            }
            if ((vNivel == 0) || (vNivel == 2)) {
                CadastrarLocalActionForm formBean = (CadastrarLocalActionForm) form;
                if (formBean.getId().intValue() > 0) {
                    Session session = HibernateUtil.getSession();
                    Transaction transaction = session.beginTransaction();
                    Local local = (Local) session.createQuery("from Local where id=" + formBean.getId()).list().iterator().next();
                    session.delete(local);
                    transaction.commit();
                    session.flush();
                    session.close();
                }
                formBean.reset(mapping, request);
                request.setAttribute("message", resource.getString("message.deletadoSucesso"));
            } else {
                request.setAttribute("message", resource.getString("message.usuarioSemPermissao"));
            }
            return mapping.findForward("success");
        } catch (Exception ex) {
            request.setAttribute("message", resource.getString("message.falhaDeletar"));
        }
        return mapping.findForward("failure");
    }

    public ActionForward novo(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        return mapping.findForward("success");
    }

    public ActionForward pesquisar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        return mapping.findForward("success");
    }
}
