package seris;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.hibernate.Session;
import org.hibernate.Transaction;
import seris.database.Ator;
import seris.database.Avaliacao;
import seris.database.DAO.AtorDAO;
import seris.database.DAO.AvaliacaoDAO;
import seris.database.DAO.FonteDAO;
import seris.database.DAO.KiqDAO;
import seris.database.DAO.LocalDAO;
import seris.database.DAO.MercadoDAO;
import seris.database.DAO.ProdutoDAO;
import seris.database.DAO.UsuarioDAO;
import seris.database.Fonte;
import seris.database.KiqSinalfraco;
import seris.database.Local;
import seris.database.Localcaptura;
import seris.database.Mercado;
import seris.database.Produto;
import seris.database.Sinalfraco;
import seris.database.Usuario;

public class CadastrarSinalfracoAction
        extends DispatchAction {

    private static final String SUCCESS = "success";
    private static final String FAILURE = "failure";
    private ResourceBundle resource = ResourceBundle.getBundle("seris.SerisIdioma");

    public CadastrarSinalfracoAction() {
    }

    public ActionForward save(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        CadastrarSinalfracoActionForm formBean = (CadastrarSinalfracoActionForm) form;
        if ((formBean.getDescricao() != null) && (!formBean.getDescricao().equals(""))) {

            String vusuario_id = String.valueOf(request.getSession().getAttribute("usuario_id"));
            int usuario_id = 0;
            if ((vusuario_id != null) && (!vusuario_id.equals(""))) {
                usuario_id = Integer.parseInt(vusuario_id);
            }

            Session session = HibernateUtil.getSession();
            Transaction transaction = session.beginTransaction();
            transaction.begin();

            String vAtor = request.getParameter("ator");
            formBean.setAtorSelecionado(vAtor);

            String vMercado = request.getParameter("mercado");
            if (vMercado.equals("0")) {
                vMercado = null;
            }
            formBean.setMercadoSelecionado(vMercado);

            String vFonte = request.getParameter("fonte");
            formBean.setFonteSelecionada(vFonte);

            String vProduto = request.getParameter("produto");
            if (vProduto.equals("0")) {
                vProduto = null;
            }
            formBean.setProdutoSelecionado(vProduto);

            formBean.setAvaliacaoSelecionada(request.getParameter("avaliacao"));

            formBean.setLocalSelecionado(request.getParameter("local"));

            formBean.setDescricao(request.getParameter("descricao"));

            formBean.setComentarios(request.getParameter("comentarios"));

            formBean.setInformacoes(request.getParameter("informacoes"));

            formBean.setNomeLocalCaptura(request.getParameter("nomeLocalCaptura"));

            formBean.setDescricaoLocalCaptura(request.getParameter("descricaoLocalCaptura"));

            formBean.setKiqsSelecionados(request.getParameter("kiqSelecionado"));

            Local local = LocalDAO.consultarLocal(Integer.parseInt(formBean.getLocalSelecionado()));
            Localcaptura localcaptura = new Localcaptura(local, formBean.getNomeLocalCaptura(), formBean.getDescricaoLocalCaptura());

            session.saveOrUpdate(localcaptura);

            Avaliacao avaliacao = AvaliacaoDAO.consultarAvaliacao(Integer.parseInt(formBean.getAvaliacaoSelecionada()));
            Produto produto = null;
            if (vProduto != null) {
                produto = ProdutoDAO.consultarProduto(Integer.parseInt(formBean.getProdutoSelecionado()));
            }
            Ator ator = AtorDAO.consultarAtor(Integer.parseInt(formBean.getAtorSelecionado()));
            Fonte fonte = FonteDAO.consultarFonte(Integer.parseInt(formBean.getFonteSelecionada()));
            Mercado mercado = null;
            if (vMercado != null) {
                mercado = MercadoDAO.consultarMercado(Integer.parseInt(formBean.getMercadoSelecionado()));
            }
            Usuario usuario = UsuarioDAO.consultarUsuario(usuario_id);

            DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
            Date data = formatter.parse(formBean.getData());

            Sinalfraco sinalfraco = new Sinalfraco(avaliacao, produto, localcaptura, fonte, ator, mercado, formBean.getDescricao(), data, formBean.getComentarios(), formBean.getInformacoes(), usuario);
            session.save(sinalfraco);

            if (sinalfraco.getId() != 0) {
                KiqSinalfraco kiqSinalfraco = new KiqSinalfraco(sinalfraco, KiqDAO.consultarKiq(Integer.parseInt(formBean.getKiqsSelecionados())));
                session.save(kiqSinalfraco);
            }

            transaction.commit();
            session.flush();
            session.close();

            request.setAttribute("AGRADECIMENTO", "TRUE");

            String nomeUsuario = usuario.getNome();
            if (nomeUsuario.indexOf(" ") >= 0) {
                nomeUsuario = nomeUsuario.substring(0, nomeUsuario.indexOf(" "));
            }
            request.setAttribute("NOMEUSUARIO", nomeUsuario);
        }
        request.setAttribute("message", resource.getString("message.salvoSucesso"));
        return mapping.findForward("success");
    }

    public ActionForward novo(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        return mapping.findForward("success");
    }

    public ActionForward pesquisar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        return mapping.findForward("success");
    }
}
