package seris;

import java.io.PrintStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class CadastrarKitSelecionarDominioAction
        extends Action {

    private static final String SUCCESS = "success";

    public CadastrarKitSelecionarDominioAction() {
    }

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        CadastrarKitActionForm formBean = (CadastrarKitActionForm) form;
        String dominioSelecionado = formBean.getDominioSelecionado();
        HttpSession sessao = request.getSession(true);
        sessao.setAttribute("dominioSelecionado", dominioSelecionado);

        System.out.println("dominioSelecionado >>>> " + dominioSelecionado);
        return mapping.findForward("success");
    }
}
