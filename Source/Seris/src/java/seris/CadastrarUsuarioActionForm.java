package seris;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;

public class CadastrarUsuarioActionForm
        extends ActionForm {

    private int id = 0;
    private String departamento;
    private List departamentos;
    private String grupo;
    private List grupos;
    private String perfil;
    private List perfis;
    private String nivel = "100";
    private List niveis;
    private String nome = "";
    private String email = "";
    private String celular = "";
    private String login = "";
    private String senha = "";
    private int localId;
    private List dominios;
    private int captador = 0;
    private String[] dominiosSelecionados = new String[0];

    public int getCaptador() {
        return captador;
    }

    public void setCaptador(int captador) {
        this.captador = captador;
    }

    public List getDominios() {
        return dominios;
    }

    public void setDominios(List dominios) {
        this.dominios = dominios;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getDepartamento() {
        return departamento;
    }

    public void setDepartamento(String departamento) {
        this.departamento = departamento;
    }

    public List getDepartamentos() {
        return departamentos;
    }

    public void setDepartamentos(List departamentos) {
        this.departamentos = departamentos;
    }

    public String[] getDominiosSelecionados() {
        return dominiosSelecionados;
    }

    public void setDominiosSelecionados(String[] dominiosSelecionados) {
        this.dominiosSelecionados = dominiosSelecionados;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGrupo() {
        return grupo;
    }

    public void setGrupo(String grupo) {
        this.grupo = grupo;
    }

    public List getGrupos() {
        return grupos;
    }

    public void setGrupos(List grupos) {
        this.grupos = grupos;
    }

    public Integer getId() {
        return Integer.valueOf(id);
    }

    public void setId(Integer id) {
        this.id = id.intValue();
    }

    public int getLocalId() {
        return localId;
    }

    public void setLocalId(int localId) {
        this.localId = localId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public List getNiveis() {
        return niveis;
    }

    public void setNiveis(List niveis) {
        this.niveis = niveis;
    }

    public String getNivel() {
        return nivel;
    }

    public void setNivel(String nivel) {
        this.nivel = nivel;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    public List getPerfis() {
        return perfis;
    }

    public void setPerfis(List perfis) {
        this.perfis = perfis;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public void reset(ActionMapping mapping, HttpServletRequest request) {
        super.reset(mapping, request);
    }

    public CadastrarUsuarioActionForm() {
    }

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();

        return errors;
    }
}
