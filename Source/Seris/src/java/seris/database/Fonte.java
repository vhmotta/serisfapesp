package seris.database;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Fonte
        implements Serializable {

    private Integer id;
    private String nome;
    private Set sinalfracos = new HashSet(0);
    private Set fonteKits = new HashSet(0);

    public Fonte() {
    }

    public Fonte(Integer id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Fonte(String nome) {
        this.nome = nome;
    }

    public Fonte(String nome, Set sinalfracos, Set fonteKits) {
        this.nome = nome;
        this.sinalfracos = sinalfracos;
        this.fonteKits = fonteKits;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Set getSinalfracos() {
        return sinalfracos;
    }

    public void setSinalfracos(Set sinalfracos) {
        this.sinalfracos = sinalfracos;
    }

    public Set getFonteKits() {
        return fonteKits;
    }

    public void setFonteKits(Set fonteKits) {
        this.fonteKits = fonteKits;
    }
}
