package seris.database;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Kiq
        implements Serializable {

    private int id;
    private Kit kit;
    private String descricao;
    private Set sinalfracos = new HashSet(0);
    private Set kiqSinalfracos = new HashSet(0);

    public Kiq() {
    }

    public Kiq(int id, Kit kit, String descricao) {
        this.kit = kit;
        this.id = id;
        this.descricao = descricao;
    }

    public Kiq(Kit kit, String descricao) {
        this.kit = kit;
        this.descricao = descricao;
    }

    public Kiq(Kit kit, String descricao, Set sinalfracos, Set kiqSinalfracos) {
        this.kit = kit;
        this.descricao = descricao;
        this.sinalfracos = sinalfracos;
        this.kiqSinalfracos = kiqSinalfracos;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Kit getKit() {
        return kit;
    }

    public void setKit(Kit kit) {
        this.kit = kit;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Set getSinalfracos() {
        return sinalfracos;
    }

    public void setSinalfracos(Set sinalfracos) {
        this.sinalfracos = sinalfracos;
    }

    public Set getKiqSinalfracos() {
        return kiqSinalfracos;
    }

    public void setKiqSinalfracos(Set kiqSinalfracos) {
        this.kiqSinalfracos = kiqSinalfracos;
    }
}
