package seris.database;

import java.io.Serializable;

public class Suportedominio
        implements Serializable {

    private int usuarioId;
    private Usuario usuario;

    public Suportedominio() {
    }

    public Suportedominio(int usuarioId, Usuario usuario) {
        this.usuarioId = usuarioId;
        this.usuario = usuario;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
