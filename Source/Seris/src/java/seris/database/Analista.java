package seris.database;

import java.io.Serializable;

public class Analista
        implements Serializable {

    private int usuarioId;

    public Analista() {
    }

    public Analista(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }
}
