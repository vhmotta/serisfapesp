package seris.database;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Sinalfraco
        implements Serializable {

    private int id;
    private Avaliacao avaliacao;
    private Produto produto;
    private Localcaptura localcaptura;
    private Fonte fonte;
    private Ator ator;
    private Mercado mercado;
    private String descricao;
    private Date data;
    private String comentarios;
    private String informacoes;
    private Set kiqSinalfracos = new HashSet(0);
    private Set grupoSinalfracos = new HashSet(0);
    private int usuarioId;
    private Usuario usuario;

    public Sinalfraco() {
    }

    public Sinalfraco(Avaliacao avaliacao, Produto produto, Localcaptura localcaptura, Fonte fonte, Ator ator, Mercado mercado, String descricao, Date data, String comentarios, String informacoes, Usuario usuario) {
        this.avaliacao = avaliacao;
        this.produto = produto;
        this.localcaptura = localcaptura;
        this.fonte = fonte;
        this.ator = ator;
        this.mercado = mercado;
        this.descricao = descricao;
        this.data = data;
        this.comentarios = comentarios;
        this.informacoes = informacoes;
        this.usuario = usuario;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Ator getAtor() {
        return ator;
    }

    public void setAtor(Ator ator) {
        this.ator = ator;
    }

    public Avaliacao getAvaliacao() {
        return avaliacao;
    }

    public void setAvaliacao(Avaliacao avaliacao) {
        this.avaliacao = avaliacao;
    }

    public String getComentarios() {
        return comentarios;
    }

    public void setComentarios(String comentarios) {
        this.comentarios = comentarios;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Fonte getFonte() {
        return fonte;
    }

    public void setFonte(Fonte fonte) {
        this.fonte = fonte;
    }

    public Set getGrupoSinalfracos() {
        return grupoSinalfracos;
    }

    public void setGrupoSinalfracos(Set grupoSinalfracos) {
        this.grupoSinalfracos = grupoSinalfracos;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getInformacoes() {
        return informacoes;
    }

    public void setInformacoes(String informacoes) {
        this.informacoes = informacoes;
    }

    public Set getKiqSinalfracos() {
        return kiqSinalfracos;
    }

    public void setKiqSinalfracos(Set kiqSinalfracos) {
        this.kiqSinalfracos = kiqSinalfracos;
    }

    public Localcaptura getLocalcaptura() {
        return localcaptura;
    }

    public void setLocalcaptura(Localcaptura localcaptura) {
        this.localcaptura = localcaptura;
    }

    public Mercado getMercado() {
        return mercado;
    }

    public void setMercado(Mercado mercado) {
        this.mercado = mercado;
    }

    public Produto getProduto() {
        return produto;
    }

    public void setProduto(Produto produto) {
        this.produto = produto;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }
}
