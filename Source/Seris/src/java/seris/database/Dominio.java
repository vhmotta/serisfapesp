package seris.database;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Dominio
        implements Serializable {

    private int id;
    private String nome;
    private Date dataCriacao;
    private Set usuarios = new HashSet(0);

    public Dominio() {
    }

    public Dominio(int id, String nome, Date dataCriacao) {
        this.id = id;
        this.nome = nome;
        this.dataCriacao = dataCriacao;
    }

    public Dominio(String nome, Date dataCriacao) {
        this.nome = nome;
        this.dataCriacao = dataCriacao;
    }

    public Dominio(String nome, Date dataCriacao, Set usuarios) {
        this.nome = nome;
        this.dataCriacao = dataCriacao;
        this.usuarios = usuarios;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Date getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(Date dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public Set getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(Set usuarios) {
        this.usuarios = usuarios;
    }
}
