package seris.database;

import java.io.Serializable;

public class KiqSinalfraco
        implements Serializable {

    private Integer id;
    private Sinalfraco sinalfraco;
    private Kiq kiq;

    public KiqSinalfraco() {
    }

    public KiqSinalfraco(Sinalfraco sinalfraco, Kiq kiq) {
        this.sinalfraco = sinalfraco;
        this.kiq = kiq;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Sinalfraco getSinalfraco() {
        return sinalfraco;
    }

    public void setSinalfraco(Sinalfraco sinalfraco) {
        this.sinalfraco = sinalfraco;
    }

    public Kiq getKiq() {
        return kiq;
    }

    public void setKiq(Kiq kiq) {
        this.kiq = kiq;
    }
}
