package seris.database;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Produto
        implements Serializable {

    private Integer id;
    private String descricao;
    private Set sinalfracos = new HashSet(0);
    private Set produtoKits = new HashSet(0);

    public Produto() {
    }

    public Produto(Integer id, String descricao) {
        this.id = id;
        this.descricao = descricao;
    }

    public Produto(String descricao) {
        this.descricao = descricao;
    }

    public Produto(String descricao, Set sinalfracos, Set produtoKits) {
        this.descricao = descricao;
        this.sinalfracos = sinalfracos;
        this.produtoKits = produtoKits;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Set getSinalfracos() {
        return sinalfracos;
    }

    public void setSinalfracos(Set sinalfracos) {
        this.sinalfracos = sinalfracos;
    }

    public Set getProdutoKits() {
        return produtoKits;
    }

    public void setProdutoKits(Set produtoKits) {
        this.produtoKits = produtoKits;
    }
}
