package seris.database;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Ator
        implements Serializable {

    private Integer id;
    private String nome;
    private Set atorKits = new HashSet(0);
    private Set sinalfracos = new HashSet(0);

    public Ator() {
    }

    public Ator(Integer id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Ator(String nome) {
        this.nome = nome;
    }

    public Ator(String nome, Set atorKits, Set sinalfracos) {
        this.nome = nome;
        this.atorKits = atorKits;
        this.sinalfracos = sinalfracos;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Set getAtorKits() {
        return atorKits;
    }

    public void setAtorKits(Set atorKits) {
        this.atorKits = atorKits;
    }

    public Set getSinalfracos() {
        return sinalfracos;
    }

    public void setSinalfracos(Set sinalfracos) {
        this.sinalfracos = sinalfracos;
    }
}
