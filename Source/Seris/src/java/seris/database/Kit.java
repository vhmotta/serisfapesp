package seris.database;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

public class Kit
        implements Serializable {

    private int id;
    private int dominioId;
    private Usuario usuario;
    private String descricao;
    private Date dataCriacao;
    private Set mercadoKits = new HashSet(0);
    private Set kiqs = new HashSet(0);
    private Set atorKits = new HashSet(0);
    private Set produtoKits = new HashSet(0);
    private Set fonteKits = new HashSet(0);
    private Dominio dominio;

    public Kit() {
    }

    public Kit(Usuario usuario, String descricao, Date dataCriacao, Dominio dominio) {
        this.usuario = usuario;
        this.descricao = descricao;
        this.dataCriacao = dataCriacao;
        this.dominio = dominio;
    }

    public Kit(Usuario usuario, String descricao, Date dataCriacao, Set mercadoKits, Set kiqs, Set atorKits, Set produtoKits, Set fonteKits, Dominio dominio) {
        this.usuario = usuario;
        this.descricao = descricao;
        this.dataCriacao = dataCriacao;
        this.mercadoKits = mercadoKits;
        this.kiqs = kiqs;
        this.atorKits = atorKits;
        this.produtoKits = produtoKits;
        this.fonteKits = fonteKits;
        this.dominio = dominio;
    }

    public Dominio getDominio() {
        return dominio;
    }

    public void setDominio(Dominio dominio) {
        this.dominio = dominio;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Date getDataCriacao() {
        return dataCriacao;
    }

    public void setDataCriacao(Date dataCriacao) {
        this.dataCriacao = dataCriacao;
    }

    public Set getMercadoKits() {
        return mercadoKits;
    }

    public void setMercadoKits(Set mercadoKits) {
        this.mercadoKits = mercadoKits;
    }

    public Set getKiqs() {
        return kiqs;
    }

    public void setKiqs(Set kiqs) {
        this.kiqs = kiqs;
    }

    public Set getAtorKits() {
        return atorKits;
    }

    public void setAtorKits(Set atorKits) {
        this.atorKits = atorKits;
    }

    public Set getProdutoKits() {
        return produtoKits;
    }

    public void setProdutoKits(Set produtoKits) {
        this.produtoKits = produtoKits;
    }

    public Set getFonteKits() {
        return fonteKits;
    }

    public void setFonteKits(Set fonteKits) {
        this.fonteKits = fonteKits;
    }

    public int getDominioId() {
        return dominioId;
    }

    public void setDominioId(int dominioId) {
        this.dominioId = dominioId;
    }
}
