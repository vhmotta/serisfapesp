package seris.database;

import java.io.Serializable;

public class DominioUsuario
        implements Serializable {

    private Integer id;
    private Usuario usuario;
    private Dominio dominio;

    public DominioUsuario() {
    }

    public DominioUsuario(Usuario usuario, Dominio dominio) {
        this.usuario = usuario;
        this.dominio = dominio;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Dominio getDominio() {
        return dominio;
    }

    public void setDominio(Dominio dominio) {
        this.dominio = dominio;
    }
}
