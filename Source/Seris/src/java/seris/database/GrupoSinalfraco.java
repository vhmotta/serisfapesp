package seris.database;

import java.io.Serializable;

public class GrupoSinalfraco
        implements Serializable {

    private Integer id;
    private Sinalfraco sinalfraco;
    private Grupo grupo;

    public GrupoSinalfraco() {
    }

    public GrupoSinalfraco(Sinalfraco sinalfraco, Grupo grupo) {
        this.sinalfraco = sinalfraco;
        this.grupo = grupo;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Sinalfraco getSinalfraco() {
        return sinalfraco;
    }

    public void setSinalfraco(Sinalfraco sinalfraco) {
        this.sinalfraco = sinalfraco;
    }

    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }
}
