package seris.database;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Usuario
        implements Serializable {

    private int id;
    private Departamento departamento;
    private Grupo grupo;
    private Perfil perfil;
    private int nivel = 100;
    private String nome;
    private String email;
    private String celular;
    private String login;
    private String senha;
    private int localId;
    private int captador;
    private Set dominios = new HashSet(0);
    private Set dominioUsuarios = new HashSet(0);

    public Usuario() {
    }

    public Usuario(Departamento departamento, Grupo grupo, Perfil perfil, int nivel, String nome, String login, String senha, String email, String celular, int localId, int captador) {
        this.departamento = departamento;
        this.grupo = grupo;
        this.perfil = perfil;
        this.nivel = nivel;
        this.nome = nome;
        this.login = login;
        this.senha = senha;
        this.email = email;
        this.celular = celular;
        this.localId = localId;
        this.captador = captador;
    }

    public Usuario(int id, Departamento departamento, Grupo grupo, Perfil perfil, int nivel, String nome, String login, String senha, String email, String celular, int localId, int captador) {
        this.id = id;
        this.departamento = departamento;
        this.grupo = grupo;
        this.perfil = perfil;
        this.nivel = nivel;
        this.nome = nome;
        this.login = login;
        if (senha != null) {
            this.senha = senha;
        }
        this.email = email;
        this.celular = celular;
        this.localId = localId;
        this.captador = captador;
    }

    public Usuario(Departamento departamento, Grupo grupo, Perfil perfil, int nivel, String nome, String email, String celular, String login, String senha, int localId, Set dominios, int captador) {
        this.departamento = departamento;
        this.grupo = grupo;
        this.perfil = perfil;
        this.nivel = nivel;
        this.nome = nome;
        this.email = email;
        this.celular = celular;
        this.login = login;
        this.senha = senha;
        this.localId = localId;
        this.dominios = dominios;
        this.captador = captador;
    }

    public int getCaptador() {
        return captador;
    }

    public void setCaptador(int captador) {
        this.captador = captador;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public Set getDominioUsuarios() {
        return dominioUsuarios;
    }

    public void setDominioUsuarios(Set dominioUsuarios) {
        this.dominioUsuarios = dominioUsuarios;
    }

    public Set getDominios() {
        return dominios;
    }

    public void setDominios(Set dominios) {
        this.dominios = dominios;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLocalId() {
        return localId;
    }

    public void setLocalId(int localId) {
        this.localId = localId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Perfil getPerfil() {
        return perfil;
    }

    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
}
