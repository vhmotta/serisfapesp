package seris.database;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Mercado
        implements Serializable {

    private Integer id;
    private String nome;
    private Set mercadoKits = new HashSet(0);
    private Set sinalfracos = new HashSet(0);

    public Mercado() {
    }

    public Mercado(Integer id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Mercado(String nome) {
        this.nome = nome;
    }

    public Mercado(String nome, Set mercadoKits, Set sinalfracos) {
        this.nome = nome;
        this.mercadoKits = mercadoKits;
        this.sinalfracos = sinalfracos;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Set getMercadoKits() {
        return mercadoKits;
    }

    public void setMercadoKits(Set mercadoKits) {
        this.mercadoKits = mercadoKits;
    }

    public Set getSinalfracos() {
        return sinalfracos;
    }

    public void setSinalfracos(Set sinalfracos) {
        this.sinalfracos = sinalfracos;
    }
}
