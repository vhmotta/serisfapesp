package seris.database;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Local
        implements Serializable {

    private Integer id;
    private String cidade;
    private String uf;
    private String pais;
    private Set localcapturas = new HashSet(0);

    public Local() {
    }

    public Local(Integer id) {
        this.id = id;
    }

    public Local(String cidade, String uf, String pais, Set localcapturas) {
        this.cidade = cidade;
        this.uf = uf;
        this.pais = pais;
        this.localcapturas = localcapturas;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String cidade) {
        this.cidade = cidade;
    }

    public String getUf() {
        return uf;
    }

    public void setUf(String uf) {
        this.uf = uf;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public Set getLocalcapturas() {
        return localcapturas;
    }

    public void setLocalcapturas(Set localcapturas) {
        this.localcapturas = localcapturas;
    }
}
