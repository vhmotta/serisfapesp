package seris.database;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Localcaptura
        implements Serializable {

    private Integer id;
    private Local local;
    private String nome;
    private String descricao;
    private Set sinalfracos = new HashSet(0);

    public Localcaptura() {
    }

    public Localcaptura(String nome, String descricao) {
        this.nome = nome;
        this.descricao = descricao;
    }

    public Localcaptura(Local local, String nome, String descricao) {
        this.local = local;
        this.nome = nome;
        this.descricao = descricao;
    }

    public Localcaptura(String nome) {
        this.nome = nome;
    }

    public Localcaptura(Local local, String nome, String descricao, Set sinalfracos) {
        this.local = local;
        this.nome = nome;
        this.descricao = descricao;
        this.sinalfracos = sinalfracos;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Local getLocal() {
        return local;
    }

    public void setLocal(Local local) {
        this.local = local;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Set getSinalfracos() {
        return sinalfracos;
    }

    public void setSinalfracos(Set sinalfracos) {
        this.sinalfracos = sinalfracos;
    }
}
