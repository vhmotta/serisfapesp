package seris.database;

import java.io.Serializable;

public class Administrador
        implements Serializable {

    private int usuarioId;

    public Administrador() {
    }

    public Administrador(int usuarioId) {
        this.usuarioId = usuarioId;
    }

    public int getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(int usuarioId) {
        this.usuarioId = usuarioId;
    }
}
