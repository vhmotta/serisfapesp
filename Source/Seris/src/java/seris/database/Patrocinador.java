package seris.database;

import java.io.Serializable;

public class Patrocinador
        implements Serializable {

    private int id;
    private Usuario usuario;
    private Dominio dominio;

    public Patrocinador() {
    }

    public Patrocinador(Usuario usuario, Dominio dominio) {
        this.usuario = usuario;
        this.dominio = dominio;
    }

    public Dominio getDominio() {
        return dominio;
    }

    public void setDominio(Dominio dominio) {
        this.dominio = dominio;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
}
