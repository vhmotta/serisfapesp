package seris.database;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Avaliacao
        implements Serializable {

    private Integer id;
    private String relevancia;
    private Set sinalfracos = new HashSet(0);

    public Avaliacao() {
    }

    public Avaliacao(String relevancia) {
        this.relevancia = relevancia;
    }

    public Avaliacao(String relevancia, Set sinalfracos) {
        this.relevancia = relevancia;
        this.sinalfracos = sinalfracos;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRelevancia() {
        return relevancia;
    }

    public void setRelevancia(String relevancia) {
        this.relevancia = relevancia;
    }

    public Set getSinalfracos() {
        return sinalfracos;
    }

    public void setSinalfracos(Set sinalfracos) {
        this.sinalfracos = sinalfracos;
    }
}
