package seris.database;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Perfil
        implements Serializable {

    private int id;
    private String nome;
    private Set usuarios = new HashSet(0);

    public Perfil() {
    }

    public Perfil(int id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Perfil(String nome) {
        this.nome = nome;
    }

    public Perfil(String nome, Set usuarios) {
        this.nome = nome;
        this.usuarios = usuarios;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Set getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(Set usuarios) {
        this.usuarios = usuarios;
    }
}
