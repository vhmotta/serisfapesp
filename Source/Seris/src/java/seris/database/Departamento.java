package seris.database;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

public class Departamento
        implements Serializable {

    private int id;
    private String nome;
    private Set usuarios = new HashSet(0);

    public Departamento() {
    }

    public Departamento(Integer id, String nome) {
        this.id = id.intValue();
        this.nome = nome;
    }

    public Departamento(String nome) {
        this.nome = nome;
    }

    public Departamento(String nome, Set usuarios) {
        this.nome = nome;
        this.usuarios = usuarios;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Set getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(Set usuarios) {
        this.usuarios = usuarios;
    }
}
