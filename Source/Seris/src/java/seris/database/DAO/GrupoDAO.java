package seris.database.DAO;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import seris.HibernateUtil;
import seris.database.Grupo;

public class GrupoDAO {

    public GrupoDAO() {
    }

    public static List consultarListaGrupos() {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        List vList = session.createQuery("from Grupo g order by g.nome").list();
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }

    public static Grupo consultarGrupo(int id) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        Grupo vGrupo = new Grupo();
        try {
            vGrupo = (Grupo) session.createQuery("from Grupo WHERE id=" + id + "").list().iterator().next();
        } catch (NoSuchElementException e) {
        }
        transaction.commit();
        session.flush();
        session.close();
        return vGrupo;
    }
}
