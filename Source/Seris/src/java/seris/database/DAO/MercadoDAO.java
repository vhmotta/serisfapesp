package seris.database.DAO;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import seris.HibernateUtil;
import seris.database.Mercado;

public class MercadoDAO {

    public MercadoDAO() {
    }

    public static List consultarListaMercados() {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        List vList = session.createQuery("from Mercado").list();
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }

    public static List consultarListaMercadosPorKit(String kitSelecionado) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        Criteria criteria = session.createCriteria(Mercado.class);
        criteria.addOrder(Order.asc("nome"));
        Criteria mercadoKitCriteria = criteria.createCriteria("mercadoKits");
        mercadoKitCriteria.add(Restrictions.like("id", Integer.valueOf(Integer.parseInt(kitSelecionado))));
        List vList = criteria.list();
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }

    public static Mercado consultarMercado(int id) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        Mercado vMercado = new Mercado();
        try {
            vMercado = (Mercado) session.createQuery("from Mercado WHERE id=" + id + "").list().iterator().next();
        } catch (NoSuchElementException e) {
        }
        transaction.commit();
        session.flush();
        session.close();
        return vMercado;
    }

    public static boolean consultarMercadoKit(int mercado_id, int kit_id) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        boolean vEncontrouMercado = false;
        try {
            Criteria criteria = session.createCriteria(Mercado.class);
            Criteria mercadoKitCriteria = criteria.createCriteria("mercadoKits");
            mercadoKitCriteria.add(Restrictions.like("id", Integer.valueOf(kit_id)));
            List vList = criteria.list();
            for (int i = 0; i < vList.size(); i++) {
                Mercado vMercado = (Mercado) vList.get(i);
                if (vMercado.getId().intValue() == mercado_id) {
                    vEncontrouMercado = true;
                }
            }
        } catch (NoSuchElementException e) {
        }
        transaction.commit();
        session.flush();
        session.close();
        return vEncontrouMercado;
    }
}
