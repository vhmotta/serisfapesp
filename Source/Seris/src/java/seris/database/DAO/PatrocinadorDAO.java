package seris.database.DAO;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import seris.HibernateUtil;

public class PatrocinadorDAO {

    public PatrocinadorDAO() {
    }

    public static List consultarListaUsuario(int usuario_id) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        List vListaPatrocinador = new ArrayList();
        try {
            vListaPatrocinador = session.createQuery("from Patrocinador WHERE usuario_id=" + usuario_id + "").list();
        } catch (NoSuchElementException e) {
        }
        transaction.commit();
        session.flush();
        session.close();
        return vListaPatrocinador;
    }
}
