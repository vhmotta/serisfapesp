package seris.database.DAO;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import seris.HibernateUtil;
import seris.database.Perfil;

public class PerfilDAO {

    public PerfilDAO() {
    }

    public static List consultarListaPerfis() {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        List vList = session.createQuery("from Perfil p order by p.nome").list();
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }

    public static Perfil consultarPerfil(int id) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        Perfil vPerfil = new Perfil();
        try {
            vPerfil = (Perfil) session.createQuery("from Perfil WHERE id=" + id + "").list().iterator().next();
        } catch (NoSuchElementException e) {
        }
        transaction.commit();
        session.flush();
        session.close();
        return vPerfil;
    }
}
