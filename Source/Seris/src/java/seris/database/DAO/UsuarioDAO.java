package seris.database.DAO;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import seris.HibernateUtil;
import seris.database.Usuario;

public class UsuarioDAO {

    public UsuarioDAO() {
    }

    public static List consultarListaUsuarios() {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        List vList = session.createQuery("from Usuario u order by u.nome").list();
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }

    public static Usuario consultarUsuario(int id) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        Usuario vUsuario = new Usuario();
        try {
            vUsuario = (Usuario) session.createQuery("from Usuario WHERE id=" + id + "").list().iterator().next();
        } catch (NoSuchElementException e) {
        }
        transaction.commit();
        session.flush();
        session.close();
        return vUsuario;
    }

    public static List consultarListaUsuariosByDominio(String kit_id) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();

        String query = "select u from Usuario u, DominioUsuario du, Dominio d, Kit k where u.id = du.usuario.id and du.dominio.id = d.id and d.id = k.dominio.id and k.id = " + kit_id + " order by u.nome";

        List vList = session.createQuery(query).list();
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }
}
