package seris.database.DAO;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import seris.HibernateUtil;
import seris.database.Dominio;

public class DominioDAO {

    public DominioDAO() {
    }

    public static List consultarListaDominios() {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        List vList = session.createQuery("from Dominio d order by d.nome").list();
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }

    public static Dominio consultarDominio(int id) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        Dominio vDominio = new Dominio();
        try {
            vDominio = (Dominio) session.createQuery("from Dominio WHERE id=" + id + "").list().iterator().next();
        } catch (NoSuchElementException e) {
        }
        transaction.commit();
        session.flush();
        session.close();
        return vDominio;
    }
}
