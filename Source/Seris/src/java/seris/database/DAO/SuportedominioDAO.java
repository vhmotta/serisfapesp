package seris.database.DAO;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import seris.HibernateUtil;
import seris.database.Suportedominio;

public class SuportedominioDAO {

    public SuportedominioDAO() {
    }

    public static Suportedominio consultarUsuario(int usuario_id) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        Suportedominio vSuportedominio = new Suportedominio();
        try {
            vSuportedominio = (Suportedominio) session.createQuery("from Suportedominio WHERE usuario_id=" + usuario_id + "").list().iterator().next();
        } catch (NoSuchElementException e) {
        }
        transaction.commit();
        session.flush();
        session.close();
        return vSuportedominio;
    }
}
