package seris.database.DAO;

import java.util.Iterator;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import seris.HibernateUtil;
import seris.database.Kiq;

public class KiqDAO {

    public KiqDAO() {
    }

    public static List consultarListaKiqs() {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        List vList = session.createQuery("from Kiq").list();
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }

    public static Kiq consultarKiq(int kiq_id) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        Kiq kiq = (Kiq) session.createQuery("from Kiq where id=" + kiq_id).list().iterator().next();
        transaction.commit();
        session.flush();
        session.close();
        return kiq;
    }

    public static List consultarListaKiqsPorKit(String kit_id) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();

        List vList = session.createQuery("from Kiq where kit_id=" + Integer.parseInt(kit_id) + " order by descricao").list();
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }
}
