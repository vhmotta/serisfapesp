package seris.database.DAO;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import seris.HibernateUtil;
import seris.database.Dominio;
import seris.database.Usuario;

public class DominioUsuarioDAO {

    public DominioUsuarioDAO() {
    }

    public static List consultarListaDominiosPorUsuario(int usuario_id) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        List vList = new ArrayList();
        Usuario vUsuario = new Usuario();
        try {
            vUsuario = (Usuario) session.createQuery("from Usuario WHERE id=" + usuario_id + "").list().iterator().next();
            Set dominios = vUsuario.getDominioUsuarios();
            Iterator iterator = dominios.iterator();
            while (iterator.hasNext()) {
                Dominio d = (Dominio) iterator.next();
                vList.add(d);
            }
        } catch (NoSuchElementException e) {
        }
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }

    public static List consultarListaUsuariosPorDominio(int dominio_id) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        List vList = new ArrayList();
        try {
            Criteria criteria = session.createCriteria(Usuario.class);
            criteria.addOrder(Order.asc("nome"));
            Criteria dominioUsuarioCriteria = criteria.createCriteria("dominioUsuarios");
            dominioUsuarioCriteria.add(Restrictions.like("id", Integer.valueOf(dominio_id)));
            vList = criteria.list();
            System.out.println("nr registros de Usuário para o dominio=" + dominio_id + " " + vList.size());
            for (int i = 0; i < vList.size(); i++) {
                Usuario Usuario = (Usuario) vList.get(i);
                System.out.println(Usuario.getNome());
            }
        } catch (NoSuchElementException e) {
        }
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }

    public static boolean consultarDominioUsuario(int usuario_id, int dominio_id) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        boolean vEncontrouDominio = false;
        Usuario vUsuario = new Usuario();
        try {
            vUsuario = (Usuario) session.createQuery("from Usuario WHERE id=" + usuario_id + "").list().iterator().next();
            Set dominios = vUsuario.getDominioUsuarios();
            Iterator iterator = dominios.iterator();
            while (iterator.hasNext()) {
                Dominio d = (Dominio) iterator.next();
                if (d.getId() == dominio_id) {
                    vEncontrouDominio = true;
                }
            }
        } catch (NoSuchElementException e) {
        }
        transaction.commit();
        session.flush();
        session.close();
        return vEncontrouDominio;
    }
}
