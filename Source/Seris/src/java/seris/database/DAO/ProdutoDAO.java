package seris.database.DAO;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import seris.HibernateUtil;
import seris.database.Produto;

public class ProdutoDAO {

    public ProdutoDAO() {
    }

    public static List consultarListaProdutos() {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        List vList = session.createQuery("from Produto").list();
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }

    public static List consultarListaProdutosPorKit(String kitSelecionado) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        Criteria criteria = session.createCriteria(Produto.class);
        criteria.addOrder(Order.asc("descricao"));
        Criteria produtoKitCriteria = criteria.createCriteria("produtoKits");
        produtoKitCriteria.add(Restrictions.like("id", Integer.valueOf(Integer.parseInt(kitSelecionado))));
        List vList = criteria.list();
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }

    public static Produto consultarProduto(int id) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        Produto vProduto = new Produto();
        try {
            vProduto = (Produto) session.createQuery("from Produto WHERE id=" + id + "").list().iterator().next();
        } catch (NoSuchElementException e) {
        }
        transaction.commit();
        session.flush();
        session.close();
        return vProduto;
    }

    public static boolean consultarProdutoKit(int produto_id, int kit_id) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        boolean vEncontrouProduto = false;
        try {
            Criteria criteria = session.createCriteria(Produto.class);
            Criteria produtoKitCriteria = criteria.createCriteria("produtoKits");
            produtoKitCriteria.add(Restrictions.like("id", Integer.valueOf(kit_id)));
            List vList = criteria.list();
            for (int i = 0; i < vList.size(); i++) {
                Produto vProduto = (Produto) vList.get(i);
                if (vProduto.getId().intValue() == produto_id) {
                    vEncontrouProduto = true;
                }
            }
        } catch (NoSuchElementException e) {
        }
        transaction.commit();
        session.flush();
        session.close();
        return vEncontrouProduto;
    }
}
