package seris.database.DAO;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import seris.HibernateUtil;
import seris.database.Altadirecao;

public class AltadirecaoDAO {

    public AltadirecaoDAO() {
    }

    public static Altadirecao consultarUsuario(int usuario_id) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        Altadirecao vAltadirecao = new Altadirecao();
        try {
            vAltadirecao = (Altadirecao) session.createQuery("from Altadirecao WHERE usuario_id=" + usuario_id + "").list().iterator().next();
        } catch (NoSuchElementException e) {
        }
        transaction.commit();
        session.flush();
        session.close();
        return vAltadirecao;
    }
}
