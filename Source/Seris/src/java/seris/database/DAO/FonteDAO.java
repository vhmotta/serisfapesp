package seris.database.DAO;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import seris.HibernateUtil;
import seris.database.Fonte;

public class FonteDAO {

    public FonteDAO() {
    }

    public static List consultarListaFontes() {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        List vList = session.createQuery("from Fonte").list();
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }

    public static List consultarListaFontesPorKit(String kitSelecionado) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        Criteria criteria = session.createCriteria(Fonte.class);
        criteria.addOrder(Order.asc("nome"));
        Criteria fonteKitCriteria = criteria.createCriteria("fonteKits");
        fonteKitCriteria.add(Restrictions.like("id", Integer.valueOf(Integer.parseInt(kitSelecionado))));
        List vList = criteria.list();
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }

    public static Fonte consultarFonte(int id) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        Fonte vFonte = new Fonte();
        try {
            vFonte = (Fonte) session.createQuery("from Fonte WHERE id=" + id + "").list().iterator().next();
        } catch (NoSuchElementException e) {
        }
        transaction.commit();
        session.flush();
        session.close();
        return vFonte;
    }

    public static Fonte consultarFonte(String fonte, Session session) {
        Criteria criteria = session.createCriteria(Fonte.class);
        criteria.add(Restrictions.eq("nome", fonte));
        List vList = criteria.list();

        Fonte vFonte = null;
        if (vList.size() > 0) {
            vFonte = (Fonte) vList.get(0);
        }

        return vFonte;
    }

    public static boolean consultarFonteKit(int fonte_id, int kit_id) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        boolean vEncontrouFonte = false;
        try {
            Criteria criteria = session.createCriteria(Fonte.class);
            Criteria fonteKitCriteria = criteria.createCriteria("fonteKits");
            fonteKitCriteria.add(Restrictions.like("id", Integer.valueOf(kit_id)));
            List vList = criteria.list();
            for (int i = 0; i < vList.size(); i++) {
                Fonte vFonte = (Fonte) vList.get(i);
                if (vFonte.getId().intValue() == fonte_id) {
                    vEncontrouFonte = true;
                }
            }
        } catch (NoSuchElementException e) {
        }
        transaction.commit();
        session.flush();
        session.close();
        return vEncontrouFonte;
    }
}
