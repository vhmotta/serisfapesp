package seris.database.DAO;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import seris.HibernateUtil;
import seris.database.Ator;

public class AtorDAO {

    public AtorDAO() {
    }

    public static List consultarListaAtores() {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        List vList = session.createQuery("from Ator").list();
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }

    public static List consultarListaAtoresPorKit(String kitSelecionado) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        Criteria criteria = session.createCriteria(Ator.class);
        criteria.addOrder(Order.asc("nome"));
        Criteria atorKitCriteria = criteria.createCriteria("atorKits");
        atorKitCriteria.add(Restrictions.like("id", Integer.valueOf(Integer.parseInt(kitSelecionado))));
        List vList = criteria.list();
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }

    public static Ator consultarAtor(int id) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        Ator vAtor = new Ator();
        try {
            vAtor = (Ator) session.createQuery("from Ator WHERE id=" + id + "").list().iterator().next();
        } catch (NoSuchElementException e) {
        }
        transaction.commit();
        session.flush();
        session.close();
        return vAtor;
    }

    public static boolean consultarAtorKit(int ator_id, int kit_id) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        boolean vEncontrouAtor = false;
        try {
            Criteria criteria = session.createCriteria(Ator.class);
            Criteria atorKitCriteria = criteria.createCriteria("atorKits");
            atorKitCriteria.add(Restrictions.like("id", Integer.valueOf(kit_id)));
            List vList = criteria.list();
            for (int i = 0; i < vList.size(); i++) {
                Ator vAtor = (Ator) vList.get(i);
                if (vAtor.getId().intValue() == ator_id) {
                    vEncontrouAtor = true;
                }
            }
        } catch (NoSuchElementException e) {
        }
        transaction.commit();
        session.flush();
        session.close();
        return vEncontrouAtor;
    }
}
