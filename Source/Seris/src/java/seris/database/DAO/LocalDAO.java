package seris.database.DAO;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import seris.HibernateUtil;
import seris.database.Local;

public class LocalDAO {

    public LocalDAO() {
    }

    public static List consultarListaLocais() {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        List vList = session.createQuery("from Local l order by l.cidade").list();
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }

    public static Local consultarLocal(int id) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        Local vLocal = new Local();
        try {
            vLocal = (Local) session.createQuery("from Local WHERE id=" + id + "").list().iterator().next();
        } catch (NoSuchElementException e) {
        }
        transaction.commit();
        session.flush();
        session.close();
        return vLocal;
    }
}
