package seris.database.DAO;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import seris.HibernateUtil;
import seris.database.Administrador;

public class AdministradorDAO {

    public AdministradorDAO() {
    }

    public static Administrador consultarUsuario(int usuario_id) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        Administrador vAdministrador = new Administrador();
        try {
            vAdministrador = (Administrador) session.createQuery("from Administrador WHERE usuario_id=" + usuario_id + "").list().iterator().next();
        } catch (NoSuchElementException e) {
        }
        transaction.commit();
        session.flush();
        session.close();
        return vAdministrador;
    }
}
