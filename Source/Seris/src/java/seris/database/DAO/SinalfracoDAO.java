package seris.database.DAO;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import seris.HibernateUtil;
import seris.database.Ator;
import seris.database.Departamento;
import seris.database.Dominio;
import seris.database.Fonte;
import seris.database.Grupo;
import seris.database.Kiq;
import seris.database.KiqSinalfraco;
import seris.database.Kit;
import seris.database.Mercado;
import seris.database.Perfil;
import seris.database.Produto;
import seris.database.Sinalfraco;
import seris.database.Usuario;
import seris.utils.Utils;
import seris2.database.ConceitoSemantico;

public class SinalfracoDAO {

    public SinalfracoDAO() {
    }

    public static Sinalfraco consultarSinalfraco(int id) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        Sinalfraco vSinalfraco = new Sinalfraco();
        try {
            vSinalfraco = (Sinalfraco) session.createQuery("from Sinalfraco WHERE id=" + id + "").list().iterator().next();
        } catch (NoSuchElementException e) {
        }
        transaction.commit();
        session.flush();
        session.close();
        return vSinalfraco;
    }

    public static List consultarListaSinaisFracos() {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        List vList = session.createQuery("from Sinalfraco").list();
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }

    public static String[][] consultarRelatorioSinaisFracos(int dominio_id, int kit_id, int ator_id, int produto_id, int fonte_id, int mercado_id, int avaliacao_id, int usuario_id, String dataInicio, String dataFinal) {
        String[][] vRelatorio = (String[][]) null;

        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();

        List vList = new ArrayList();
        Criteria criteria = session.createCriteria(Sinalfraco.class);
        Criteria kiqSinalfracoCriteria = criteria.createCriteria("kiqSinalfracos");
        Criteria kiqCriteria = kiqSinalfracoCriteria.createCriteria("kiq");
        Criteria kitCriteria = kiqCriteria.createCriteria("kit");
        kitCriteria.add(Restrictions.like("id", Integer.valueOf(kit_id)));
        if (usuario_id != 0) {
            Criteria usuarioCriteria = criteria.createCriteria("usuario");
            usuarioCriteria.add(Restrictions.like("id", Integer.valueOf(usuario_id)));
        }
        if (mercado_id != 0) {
            Criteria mercadoCriteria = criteria.createCriteria("mercado");
            mercadoCriteria.add(Restrictions.like("id", Integer.valueOf(mercado_id)));
        }
        if (fonte_id != 0) {
            Criteria fonteCriteria = criteria.createCriteria("fonte");
            fonteCriteria.add(Restrictions.like("id", Integer.valueOf(fonte_id)));
        }
        if (ator_id != 0) {
            Criteria atorCriteria = criteria.createCriteria("ator");
            atorCriteria.add(Restrictions.like("id", Integer.valueOf(ator_id)));
        }
        if (produto_id != 0) {
            Criteria produtoCriteria = criteria.createCriteria("produto");
            produtoCriteria.add(Restrictions.like("id", Integer.valueOf(produto_id)));
        }
        if (avaliacao_id != 0) {
            Criteria avaliacaoCriteria = criteria.createCriteria("avaliacao");
            avaliacaoCriteria.add(Restrictions.like("id", Integer.valueOf(avaliacao_id)));
        }

        if ((!dataInicio.equals("00/00/0000")) && (!dataFinal.equals("00/00/0000"))) {
            criteria.add(Restrictions.between("data", Utils.ConverteStringParaDate(dataInicio), Utils.ConverteStringParaDate(dataFinal)));
        }
        criteria.addOrder(Order.asc("usuario"));
        criteria.addOrder(Order.asc("data"));
        vList = criteria.list();

        System.out.println("vList.size()=" + vList.size());

        Dominio d = DominioDAO.consultarDominio(dominio_id);
        Kit k = KitDAO.consultarKit(kit_id);

        int vTotalPorKit = consultarTotalSinaisFracosPorKit(kit_id);
        int vTotalConsulta = vList.size();
        double vPorcentagem = 0.0D;
        if (vTotalPorKit > 0) {
            vPorcentagem = Double.parseDouble(Integer.toString(vTotalConsulta)) / Double.parseDouble(Integer.toString(vTotalPorKit)) * 100.0D;
        }

        vRelatorio = new String[vList.size()][15];
        for (int i = 0; i < vList.size(); i++) {
            Sinalfraco sf = (Sinalfraco) vList.get(i);

            Usuario u = sf.getUsuario();

            vRelatorio[i][0] = d.getNome();
            vRelatorio[i][1] = k.getDescricao();
            if (sf.getMercado() != null) {
                vRelatorio[i][2] = sf.getMercado().getNome();
            } else {
                vRelatorio[i][2] = "";
            }
            if (sf.getProduto() != null) {
                vRelatorio[i][3] = sf.getProduto().getDescricao();
            } else {
                vRelatorio[i][3] = "";
            }
            vRelatorio[i][4] = sf.getAtor().getNome();
            vRelatorio[i][5] = sf.getFonte().getNome();
            vRelatorio[i][6] = sf.getAvaliacao().getRelevancia();
            vRelatorio[i][7] = sf.getDescricao();
            vRelatorio[i][8] = u.getNome();
            vRelatorio[i][9] = sf.getInformacoes();
            vRelatorio[i][10] = sf.getComentarios();
            vRelatorio[i][11] = Utils.DoubleToString(vPorcentagem);
            vRelatorio[i][12] = String.valueOf(vTotalPorKit);
            vRelatorio[i][13] = String.valueOf(vTotalConsulta);
            vRelatorio[i][14] = Utils.FormatarDataFrm(String.valueOf(sf.getData()));
        }

        transaction.commit();
        session.flush();
        session.close();
        return vRelatorio;
    }

    public static String[][] consultarRelatorioSinaisFracos(int dominio_id, int kit_id, int usuario_id, int perfil_id, int grupo_id, int departamento_id, String dataInicio, String dataFinal) {
        String[][] vRelatorio = (String[][]) null;

        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();

        List vList = new ArrayList();
        Criteria criteria = session.createCriteria(Sinalfraco.class);

        System.out.println(kit_id);
        if (kit_id != 0) {
            Criteria kiqSinalfracoCriteria = criteria.createCriteria("kiqSinalfracos");
            Criteria kiqCriteria = kiqSinalfracoCriteria.createCriteria("kiq");
            Criteria kitCriteria = kiqCriteria.createCriteria("kit");
            kitCriteria.add(Restrictions.like("id", Integer.valueOf(kit_id)));
        } else if (dominio_id != 0) {
            Criteria kiqSinalfracoCriteria = criteria.createCriteria("kiqSinalfracos");
            Criteria kiqCriteria = kiqSinalfracoCriteria.createCriteria("kiq");
            Criteria kitCriteria = kiqCriteria.createCriteria("kit");
            Dominio d = DominioDAO.consultarDominio(dominio_id);
            kitCriteria.add(Restrictions.like("dominio", d));
        }

        Criteria usuarioCriteria = criteria.createCriteria("usuario");

        if (usuario_id != 0) {
            usuarioCriteria.add(Restrictions.like("id", Integer.valueOf(usuario_id)));
        }
        if (perfil_id != 0) {
            Criteria perfilCriteria = usuarioCriteria.createCriteria("perfil");
            perfilCriteria.add(Restrictions.like("id", Integer.valueOf(perfil_id)));
        }
        if (grupo_id != 0) {
            Criteria grupoCriteria = usuarioCriteria.createCriteria("grupo");
            grupoCriteria.add(Restrictions.like("id", Integer.valueOf(grupo_id)));
        }
        if (departamento_id != 0) {
            Criteria departamentoCriteria = usuarioCriteria.createCriteria("departamento");
            departamentoCriteria.add(Restrictions.like("id", Integer.valueOf(departamento_id)));
        }

        if ((!dataInicio.equals("00/00/0000")) && (!dataFinal.equals("00/00/0000"))) {
            criteria.add(Restrictions.between("data", Utils.ConverteStringParaDate(dataInicio), Utils.ConverteStringParaDate(dataFinal)));
        }
        criteria.addOrder(Order.asc("usuario"));
        criteria.addOrder(Order.asc("data"));
        vList = criteria.list();

        System.out.println("vList.size()=" + vList.size());

        Dominio d = DominioDAO.consultarDominio(dominio_id);

        int vTotalPorDominio = consultarTotalSinaisFracosPorDominio(dominio_id);
        int vTotalConsulta = vList.size();
        double vPorcentagem = 0.0D;
        if (vTotalPorDominio > 0) {
            vPorcentagem = Double.parseDouble(Integer.toString(vTotalConsulta)) / Double.parseDouble(Integer.toString(vTotalPorDominio)) * 100.0D;
        }

        vRelatorio = new String[vList.size()][14];
        for (int i = 0; i < vList.size(); i++) {
            Sinalfraco sf = (Sinalfraco) vList.get(i);

            Usuario u = sf.getUsuario();

            Kit k = null;
            Set kiqsSinalfkiqraco = sf.getKiqSinalfracos();
            Iterator iterator = kiqsSinalfkiqraco.iterator();
            while (iterator.hasNext()) {
                KiqSinalfraco ksf = (KiqSinalfraco) iterator.next();
                k = ksf.getKiq().getKit();
            }

            vRelatorio[i][0] = d.getNome();
            vRelatorio[i][1] = k.getDescricao();
            vRelatorio[i][2] = u.getNome();
            vRelatorio[i][3] = u.getEmail();
            vRelatorio[i][4] = u.getPerfil().getNome();
            vRelatorio[i][5] = u.getGrupo().getNome();
            vRelatorio[i][6] = u.getDepartamento().getNome();
            vRelatorio[i][7] = sf.getDescricao();
            vRelatorio[i][8] = (sf.getInformacoes() == null ? "" : sf.getInformacoes());
            vRelatorio[i][9] = (sf.getComentarios() == null ? "" : sf.getComentarios());
            vRelatorio[i][10] = Utils.DoubleToString(vPorcentagem);
            vRelatorio[i][11] = String.valueOf(vTotalPorDominio);
            vRelatorio[i][12] = String.valueOf(vTotalConsulta);
            vRelatorio[i][13] = Utils.FormatarDataFrm(String.valueOf(sf.getData()));
        }

        transaction.commit();
        session.flush();
        session.close();
        return vRelatorio;
    }

    public static int consultarTotalSinaisFracosPorKit(int kit_id) {
        int vTotal = 0;

        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();

        List vList = new ArrayList();
        Criteria criteria = session.createCriteria(Sinalfraco.class);
        if (kit_id != 0) {
            Criteria kiqSinalfracoCriteria = criteria.createCriteria("kiqSinalfracos");
            Criteria kiqCriteria = kiqSinalfracoCriteria.createCriteria("kiq");
            Criteria kitCriteria = kiqCriteria.createCriteria("kit");
            kitCriteria.add(Restrictions.like("id", Integer.valueOf(kit_id)));
        }
        vList = criteria.list();
        System.out.println("vList.size()=" + vList.size());

        vTotal = vList.size();
        transaction.commit();
        session.flush();
        session.close();
        return vTotal;
    }

    public static int consultarTotalSinaisFracosPorDominio(int dominio_id) {
        int vTotal = 0;

        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();

        List vList = new ArrayList();
        Criteria criteria = session.createCriteria(Sinalfraco.class);
        if (dominio_id != 0) {
            Criteria kiqSinalfracoCriteria = criteria.createCriteria("kiqSinalfracos");
            Criteria kiqCriteria = kiqSinalfracoCriteria.createCriteria("kiq");
            Criteria kitCriteria = kiqCriteria.createCriteria("kit");
            Dominio d = DominioDAO.consultarDominio(dominio_id);
            kitCriteria.add(Restrictions.like("dominio", d));
        }

        vList = criteria.list();
        System.out.println("vList.size()=" + vList.size());

        vTotal = vList.size();
        transaction.commit();
        session.flush();
        session.close();
        return vTotal;
    }

    public static String[][] consultarListaSinaisFracosNews(String idConceitoNews) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();

        String aux = "";

        if ((idConceitoNews != null) && (!idConceitoNews.equals(""))) {
            List instancias = seris2.database.DAO.ConceitoSemanticoDAO.consultarInstanciasClassificadasDosConceitos(Integer.valueOf(Integer.parseInt(idConceitoNews)));

            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < instancias.size(); i++) {
                ConceitoSemantico cs = (ConceitoSemantico) instancias.get(i);
                aux = cs.getConceito();

                if (sb.toString().length() > 0) {
                    sb.append(" or UPPER(descricao) like UPPER('%" + aux.replace("'", "%") + "%')");
                } else {
                    sb.append("UPPER(descricao) like UPPER('%" + aux.replace("'", "%") + "%')");
                }
            }
            aux = "";
            if (sb.toString().length() > 0) {
                aux = " where (" + sb.toString() + ") ";
            }
        }

        String sql = "from Sinalfraco sf " + aux + "order by sf.data desc, sf.id desc";

        List vList = session.createQuery(sql).list();

        String[][] vRelatorio = (String[][]) null;

        vRelatorio = new String[vList.size()][10];

        for (int i = 0; i < vList.size(); i++) {
            try {
                Sinalfraco sinalFraco = (Sinalfraco) vList.get(i);
                KiqSinalfraco kiqSF = (KiqSinalfraco) sinalFraco.getKiqSinalfracos().iterator().next();
                Kiq kiq = kiqSF.getKiq();
                Kit kit = kiq.getKit();
                Dominio dominio = kit.getDominio();

                Usuario usuario = sinalFraco.getUsuario();

                vRelatorio[i][0] = usuario.getNome();
                vRelatorio[i][1] = usuario.getEmail();
                vRelatorio[i][2] = usuario.getPerfil().getNome();
                vRelatorio[i][3] = usuario.getGrupo().getNome();
                vRelatorio[i][4] = usuario.getDepartamento().getNome();
                vRelatorio[i][5] = Utils.FormatarDataFrm(String.valueOf(sinalFraco.getData()));
                vRelatorio[i][6] = sinalFraco.getDescricao();
                vRelatorio[i][7] = sinalFraco.getInformacoes();
                vRelatorio[i][8] = sinalFraco.getComentarios();
                vRelatorio[i][9] = dominio.getNome();
            } catch (Exception e) {
            }
        }

        transaction.commit();
        session.flush();
        session.close();

        return vRelatorio;
    }

    public static List consultarListaSinaisFracosSemLista(Integer idLista, Date dataInicial, Date dataFinal, Integer kitId, Integer atorId, Integer produtoId, Integer mercadoId, Integer fonteId) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();

        List vList = new ArrayList();
        Criteria criteria = session.createCriteria(Sinalfraco.class);
        Criteria kiqSinalfracoCriteria = criteria.createCriteria("kiqSinalfracos");
        Criteria kiqCriteria = kiqSinalfracoCriteria.createCriteria("kiq");
        Criteria kitCriteria = kiqCriteria.createCriteria("kit");
        kitCriteria.add(Restrictions.like("id", kitId));

        List listasSinaisFracosList = seris2.database.DAO.ListasSinaisFracosDAO.consultarIdsListasSinaisFracosByListaId(idLista.intValue());

        if (listasSinaisFracosList.size() > 0) {
            criteria.add(Restrictions.not(Restrictions.in("id", listasSinaisFracosList)));
        }

        if (dataInicial != null) {
            criteria.add(Restrictions.ge("data", dataInicial));
        }
        if (dataFinal != null) {
            criteria.add(Restrictions.le("data", dataFinal));
        }

        if (mercadoId != null) {
            Criteria mercadoCriteria = criteria.createCriteria("mercado");
            mercadoCriteria.add(Restrictions.like("id", mercadoId));
        }
        if (fonteId != null) {
            Criteria fonteCriteria = criteria.createCriteria("fonte");
            fonteCriteria.add(Restrictions.like("id", fonteId));
        }
        if (atorId != null) {
            Criteria atorCriteria = criteria.createCriteria("ator");
            atorCriteria.add(Restrictions.like("id", atorId));
        }
        if (produtoId != null) {
            Criteria produtoCriteria = criteria.createCriteria("produto");
            produtoCriteria.add(Restrictions.like("id", produtoId));
        }

        vList = criteria.list();

        transaction.commit();
        session.flush();
        session.close();

        return vList;
    }

    public static Object[] consultarInfoSinalFracoById(int id) {
        Object[] result = {new Sinalfraco(), "", "", "", "", "", "", ""};

        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        Sinalfraco vSinalfraco = new Sinalfraco();
        try {
            vSinalfraco = (Sinalfraco) session.createQuery("from Sinalfraco WHERE id=" + id + "").list().iterator().next();

            KiqSinalfraco kiqSF = (KiqSinalfraco) vSinalfraco.getKiqSinalfracos().iterator().next();
            Kiq kiq = kiqSF.getKiq();
            Kit kit = kiq.getKit();
            Dominio dominio = kit.getDominio();

            Usuario usuario = vSinalfraco.getUsuario();

            result[0] = vSinalfraco;
            result[1] = usuario.getNome();
            result[2] = dominio.getNome();
            result[3] = vSinalfraco.getAtor().getNome();
            if (vSinalfraco.getProduto() != null) {
                result[4] = vSinalfraco.getProduto().getDescricao();
            }
            if (vSinalfraco.getMercado() != null) {
                result[5] = vSinalfraco.getMercado().getNome();
            }
            result[6] = vSinalfraco.getFonte().getNome();
            result[7] = kit.getDescricao();
        } catch (NoSuchElementException e) {
        }

        transaction.commit();
        session.flush();
        session.close();

        return result;
    }

    public static boolean consultarInfoSinalFracoByDescricao(String descricao) {
        boolean result = false;

        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();

        Criteria criteria = session.createCriteria(Sinalfraco.class);
        criteria.add(Restrictions.like("descricao", descricao));
        Sinalfraco vSinalfraco = null;
        try {
            List list = criteria.list();

            if (list.size() > 0) {
                vSinalfraco = (Sinalfraco) list.get(0);
            }

            if (vSinalfraco != null) {
                result = true;
            }
        } catch (NoSuchElementException e) {
        }

        transaction.commit();
        session.flush();
        session.close();

        return result;
    }

    public static List consultarListasSinaisFracosBySinonimos(String texto, int idKit, int nivel) {
        String txtOriginal = texto;

        List vList = new ArrayList();

        List palavrasFortes = new ArrayList();

        if (palavrasFortes.size() > 0) {
            List sinonimos = seris2.database.DAO.SinonimosRelacaoDAO.consultarSinonimosPalavras(palavrasFortes, nivel);

            if (sinonimos.size() > 0) {
                Session session = HibernateUtil.getSession();
                Transaction transaction = session.beginTransaction();
                transaction.begin();

                StringBuilder query = new StringBuilder("select sf from Sinalfraco sf, KiqSinalfraco ksf, Kiq k where ");
                query.append("sf.id = ksf.sinalfraco.id and ksf.kiq.id = k.id and k.kit.id = " + idKit);
                query.append(" and sf.descricao <> '" + txtOriginal + "' and (");

                for (int i = 0; i < sinonimos.size(); i++) {
                    if (i == 0) {
                        query.append("UPPER(sf.descricao) like '%" + ((String) sinonimos.get(i)).toUpperCase() + "%' ");
                    } else {
                        query.append("or UPPER(sf.descricao) like '%" + ((String) sinonimos.get(i)).toUpperCase() + "%' ");
                    }
                }
                query.append(")");

                vList = session.createQuery(query.toString()).list();
                transaction.commit();
                session.flush();
                session.close();
            }
        }
        return vList;
    }

    public static List consultarSinaisFracosByKit(int idKit) {
        List vList = new ArrayList();

        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();

        StringBuilder query = new StringBuilder("select sf from Sinalfraco sf, KiqSinalfraco ksf, Kiq k where ");
        query.append("sf.id = ksf.sinalfraco.id and ksf.kiq.id = k.id and k.kit.id = " + idKit);

        vList = session.createQuery(query.toString()).list();
        transaction.commit();
        session.flush();
        session.close();

        return vList;
    }

    public static List consultarSinaisFracosByDominio(int idDominio) {
        return consultarSinaisFracosByDominio(idDominio, null, null);
    }

    public static List consultarSinaisFracosByDominio(int idDominio, Calendar dataInicial, Calendar dataFinal) {
        List vList = new ArrayList();

        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();

        StringBuilder query = new StringBuilder("select sf from Sinalfraco sf, KiqSinalfraco ksf, Kiq k, Kit kt where ");
        query.append("sf.id = ksf.sinalfraco.id and ksf.kiq.id = k.id and k.kit.id = kt.id and kt.dominio.id = " + idDominio);

        if (dataInicial != null) {
            query.append(" and sf.data >= '" + seris.functions.BasicFunctions.formatCalendarYMD(dataInicial, "-") + "'");
        }
        if (dataFinal != null) {
            query.append(" and sf.data <= '" + seris.functions.BasicFunctions.formatCalendarYMD(dataFinal, "-") + "'");
        }

        vList = session.createQuery(query.toString()).list();
        transaction.commit();
        session.flush();
        session.close();

        return vList;
    }
}
