package seris.database.DAO;

import java.util.Iterator;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import seris.HibernateUtil;
import seris.database.Avaliacao;

public class AvaliacaoDAO {

    public AvaliacaoDAO() {
    }

    public static List consultarListaAvaliacoes() {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        List vList = session.createQuery("from Avaliacao a order by relevancia").list();
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }

    public static Avaliacao consultarAvaliacao(int id) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        Avaliacao avaliacao = (Avaliacao) session.createQuery("from Avaliacao where id=" + id).list().iterator().next();
        transaction.commit();
        session.flush();
        session.close();
        return avaliacao;
    }
}
