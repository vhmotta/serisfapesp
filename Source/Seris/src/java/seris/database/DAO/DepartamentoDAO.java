package seris.database.DAO;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import seris.HibernateUtil;
import seris.database.Departamento;

public class DepartamentoDAO {

    public DepartamentoDAO() {
    }

    public static List consultarListaDepartamentos() {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        List vList = session.createQuery("from Departamento d order by d.nome").list();
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }

    public static Departamento consultarDepartamento(int id) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        Departamento vDepartamento = new Departamento();
        try {
            vDepartamento = (Departamento) session.createQuery("from Departamento WHERE id=" + id + "").list().iterator().next();
        } catch (NoSuchElementException e) {
        }
        transaction.commit();
        session.flush();
        session.close();
        return vDepartamento;
    }
}
