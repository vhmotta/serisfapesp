package seris.database.DAO;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import seris.HibernateUtil;
import seris.database.Dominio;
import seris.database.Kit;

public class KitDAO {

    public KitDAO() {
    }

    public static List consultarListaKits() {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        List vList = session.createQuery("from Kit").list();
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }

    public static Kit consultarKit(int kit_id) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        Kit kit = (Kit) session.createQuery("from Kit where id=" + kit_id).list().iterator().next();
        transaction.commit();
        session.flush();
        session.close();
        return kit;
    }

    public static List consultarListaKitsPorDominiosUsuario(int dominioSelecionado) {
        System.out.println("dominioSelecionado=" + dominioSelecionado);

        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();

        List vList = new ArrayList();
        Criteria criteria = session.createCriteria(Kit.class);
        criteria.addOrder(Order.asc("descricao"));
        Dominio d = DominioDAO.consultarDominio(dominioSelecionado);
        criteria.add(Restrictions.like("dominio", d));
        vList = criteria.list();

        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }

    public static List consultarListaKitsPorUsuario(int usuarioId) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();

        String query = "select k from Kit k where k.dominio.id in (select du.dominio.id from DominioUsuario du where du.usuario.id=" + usuarioId + ") " + "order by k.dominio.id, k.descricao";

        List vList = session.createQuery(query).list();
        transaction.commit();
        session.flush();
        session.close();
        return vList;
    }
}
