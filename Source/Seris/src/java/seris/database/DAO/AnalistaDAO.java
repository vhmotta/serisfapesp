package seris.database.DAO;

import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import seris.HibernateUtil;
import seris.database.Analista;

public class AnalistaDAO {

    public AnalistaDAO() {
    }

    public static Analista consultarUsuario(int usuario_id) {
        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        Analista vAnalista = new Analista();
        try {
            vAnalista = (Analista) session.createQuery("from Analista WHERE usuario_id=" + usuario_id + "").list().iterator().next();
        } catch (NoSuchElementException e) {
        }
        transaction.commit();
        session.flush();
        session.close();
        return vAnalista;
    }
}
