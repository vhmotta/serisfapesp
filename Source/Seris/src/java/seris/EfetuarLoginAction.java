package seris;

import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.hibernate.Session;
import org.hibernate.Transaction;
import seris.database.Administrador;
import seris.database.Altadirecao;
import seris.database.Analista;
import seris.database.DAO.AltadirecaoDAO;
import seris.database.Suportedominio;
import seris.database.Usuario;

public class EfetuarLoginAction extends DispatchAction {

    private static final String HOME = "home";
    private static final String LOGIN = "login";
    private static final String PERFIL = "perfil";

    public EfetuarLoginAction() {
    }

    public ActionForward login(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        EfetuarLoginActionForm formBean = (EfetuarLoginActionForm) form;

        Session session = HibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        transaction.begin();
        Usuario usuario = null;
        try {
            usuario = (Usuario) session.createQuery("from Usuario WHERE login='" + formBean.getLogin() + "'").list().iterator().next();
        } catch (NoSuchElementException e) {
        }
        transaction.commit();
        session.flush();
        session.close();
        if (usuario == null) {
            formBean.reset(mapping, request);
            formBean.setMensagem("Usuário não encontrado!");
        } else {
            String senhaCadastrada = usuario.getSenha();
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            md5.reset();
            md5.update(formBean.getSenha().getBytes());
            byte[] cifra = md5.digest();
            StringBuffer cifraHex = new StringBuffer();
            for (int i = 0; i < cifra.length; i++) {
                cifraHex.append(Integer.toHexString(0xFF & cifra[i]));
            }
            String senhaInformada = cifraHex.toString();

            if (!senhaInformada.equals(senhaCadastrada)) {
                formBean.reset(mapping, request);
                formBean.setMensagem("Senha incorreta!");
            } else {
                formBean.setMensagem(null);
                formBean.setSenha(null);
                formBean.setNivel(usuario.getNivel());
                formBean.setNome(usuario.getNome());
                formBean.setId(usuario.getId());
                HttpSession sessao = request.getSession(true);
                sessao.setAttribute("nivel", Integer.valueOf(usuario.getNivel()));
                sessao.setAttribute("nome", usuario.getNome());
                sessao.setAttribute("usuario_id", Integer.valueOf(usuario.getId()));

                List perfis = new ArrayList();

                Administrador vAdministrador = seris.database.DAO.AdministradorDAO.consultarUsuario(usuario.getId());
                List vListaPatrocinador = seris.database.DAO.PatrocinadorDAO.consultarListaUsuario(usuario.getId());
                Suportedominio vSuportedominio = seris.database.DAO.SuportedominioDAO.consultarUsuario(usuario.getId());
                Altadirecao vAltadirecao = AltadirecaoDAO.consultarUsuario(usuario.getId());
                Analista vAnalista = seris.database.DAO.AnalistaDAO.consultarUsuario(usuario.getId());

                boolean vExistePerfil = false;
                int vSomaPerfis = 0;
                if ((vAdministrador != null) && (vAdministrador.getUsuarioId() != 0)) {
                    vExistePerfil = true;
                    vSomaPerfis++;
                    sessao.setAttribute("nivel", Integer.valueOf(0));
                    perfis.add("Administrador");
                }
                if ((vSuportedominio != null) && (vSuportedominio.getUsuarioId() != 0)) {
                    vExistePerfil = true;
                    vSomaPerfis++;
                    sessao.setAttribute("nivel", Integer.valueOf(2));
                    perfis.add("Suporte Domínio");
                }
                if (vListaPatrocinador.size() > 0) {
                    vExistePerfil = true;
                    vSomaPerfis++;
                    sessao.setAttribute("nivel", Integer.valueOf(1));
                    perfis.add("Patrocinador");
                }
                if ((vAltadirecao != null) && (vAltadirecao.getUsuarioId() != 0)) {
                    vExistePerfil = true;
                    vSomaPerfis++;
                    sessao.setAttribute("nivel", Integer.valueOf(3));
                    perfis.add("Alta Direção");
                }
                if ((vAnalista != null) && (vAnalista.getUsuarioId() != 0)) {
                    vExistePerfil = true;
                    vSomaPerfis++;
                    sessao.setAttribute("nivel", Integer.valueOf(5));
                    perfis.add("Facilitador");
                }
                if ((usuario != null) && (usuario.getId() != 0)
                        && (usuario.getCaptador() == 1)) {
                    vExistePerfil = true;
                    vSomaPerfis++;
                    sessao.setAttribute("nivel", Integer.valueOf(4));
                    perfis.add("Captador");
                }

                sessao.setAttribute("perfis", perfis);

                if ((vSomaPerfis == 1) && (vExistePerfil == true)) {
                    return mapping.findForward("home");
                }
                return mapping.findForward("perfil");
            }
        }

        return mapping.findForward("login");
    }

    public ActionForward selecionarPerfil(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        EfetuarLoginActionForm formBean = (EfetuarLoginActionForm) form;

        HttpSession sessao = request.getSession(true);
        sessao.setAttribute("nivel", Integer.valueOf(formBean.getNivel()));

        return mapping.findForward("home");
    }

    public ActionForward logout(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        EfetuarLoginActionForm formBean = (EfetuarLoginActionForm) form;
        formBean.reset(mapping, request);
        formBean = null;

        request.getSession().removeAttribute("nivel");
        request.getSession().removeAttribute("nome");
        request.getSession().removeAttribute("usuario_id");
        request.getSession().removeAttribute("dominioSelecionado");
        request.getSession().removeAttribute("kitSelecionado");
        request.getSession().removeAttribute("tipoConsulta");
        request.getSession().removeAttribute("ator");
        request.getSession().removeAttribute("fonte");
        request.getSession().removeAttribute("mercado");
        request.getSession().removeAttribute("produto");
        request.getSession().removeAttribute("avaliacao");
        request.getSession().removeAttribute("usuario");
        request.getSession().removeAttribute("perfil");
        request.getSession().removeAttribute("grupo");
        request.getSession().removeAttribute("departamento");
        request.getSession().removeAttribute("dataInicio");
        request.getSession().removeAttribute("dataFinal");

        return mapping.findForward("login");
    }
}
