package seris.ontologia;

public class PalavrasGraus {

    private String palavra;

    private String graus;

    public PalavrasGraus(String palavra, String graus) {
        this.palavra = palavra;
        this.graus = graus;
    }

    public String getPalavra() {
        return palavra;
    }

    public void setPalavra(String palavra) {
        this.palavra = palavra;
    }

    public String getGraus() {
        return graus;
    }

    public void setGraus(String graus) {
        this.graus = graus;
    }
}
