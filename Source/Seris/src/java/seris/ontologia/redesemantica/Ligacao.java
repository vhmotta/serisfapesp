package seris.ontologia.redesemantica;

public class Ligacao {

    private int id;

    private String nome;

    private Conceito conceito;

    private int sentido;

    public Ligacao(int id, String nome, Conceito conceito, int sentido) {
        this.id = id;
        this.nome = nome;
        this.conceito = conceito;
        this.sentido = sentido;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Conceito getConceito() {
        return conceito;
    }

    public void setConceito(Conceito conceito) {
        this.conceito = conceito;
    }

    public int getSentido() {
        return sentido;
    }

    public void setSentido(int sentido) {
        this.sentido = sentido;
    }
}
