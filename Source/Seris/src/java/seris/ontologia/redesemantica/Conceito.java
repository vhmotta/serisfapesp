package seris.ontologia.redesemantica;

import java.util.ArrayList;
import java.util.List;

public class Conceito {

    private int id;
    private int idPai;
    private String nome;
    private int grau;
    private float peso;
    private List<Ligacao> ligacoes = new ArrayList();
    private List<Instancia> listInstancias = new ArrayList();

    public Conceito() {
    }

    public Conceito(int id, int idPai, String nome, int grau, float peso) {
        this.id = id;
        this.idPai = idPai;
        this.nome = nome;
        this.grau = grau;
        this.peso = peso;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Ligacao> getLigacoes() {
        return ligacoes;
    }

    public void setLigacoes(List<Ligacao> ligacoes) {
        this.ligacoes = ligacoes;
    }

    public List<Instancia> getListInstancias() {
        return listInstancias;
    }

    public void setListInstancias(List<Instancia> listInstancias) {
        this.listInstancias = listInstancias;
    }

    public int getGrau() {
        return grau;
    }

    public void setGrau(int grau) {
        this.grau = grau;
    }

    public float getPeso() {
        return peso;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }

    public int getIdPai() {
        return idPai;
    }

    public void setIdPai(int idPai) {
        this.idPai = idPai;
    }
}
