package seris.ontologia;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.ResourceBundle;
import seris.database.DAO.SinalfracoDAO;
import seris.database.Sinalfraco;
import seris.functions.BasicFunctions;
import seris.functions.ConnectJdbcDataBase;
import seris.functions.QueryPS;
import seris.ontologia.redesemantica.Conceito;
import seris.ontologia.redesemantica.Instancia;
import seris.ontologia.redesemantica.Ligacao;
import seris2.database.DAO.ListasSinaisFracosDAO;
import seris2.database.ListasSinaisFracos;

public class OntologiaManager {

    private int dominioId;
    private String kitDescricao;
    private String sinalFracoDescricao;
    private List ignorar = new ArrayList();
    private List compostas = new ArrayList();
    private List<String> listString;
    private int sinalFracoId;
    private int listaId;
    private String graficoIds;
    private int tipoSemantica = -1;
    private float conceitoPesoExtraValue = 0.2F;
    private List idsConceitoPesoExtra = new ArrayList();
    private List<ClasseSinalFraco> classeSFList = new ArrayList();
    private List<Conceito> conceitosRedeSemantica;
    private int[] faixaValores = new int[6];
    private int rede;
    private Character marcarPalavras;
    private Calendar dataInicial = null;
    private Calendar dataFinal = null;
    private List palavrasEncontradaOntologia = new ArrayList();
    private List<ClasseSinalFraco> g0list = new ArrayList();
    private List<ClasseSinalFraco> g1list = new ArrayList();
    private List<ClasseSinalFraco> g2list = new ArrayList();
    private List<ClasseSinalFraco> g3list = new ArrayList();
    private List<ClasseSinalFraco> g4list = new ArrayList();
    private List<ClasseSinalFraco> g5list = new ArrayList();

    public OntologiaManager(int id, String sinalFracoDescricao, String kitDescricao, int listaId, String graficoIds, int nivelConexao, int dominioId, int tipoSemantica, int rede, Character marcarPalavras, String dataInicial, String dataFinal) {
        if (sinalFracoDescricao.indexOf("-") <= 3) {
            sinalFracoDescricao = sinalFracoDescricao.substring(sinalFracoDescricao.indexOf("-") + 1).trim();
        }

        this.kitDescricao = kitDescricao;

        this.sinalFracoDescricao = sinalFracoDescricao;
        sinalFracoId = id;

        this.dominioId = dominioId;
        this.listaId = listaId;
        this.graficoIds = graficoIds;

        ignorar = OntologiaFunctions.getPlavrasIgnorar();
        compostas = OntologiaFunctions.getPlavrasCompostas(dominioId);

        this.tipoSemantica = tipoSemantica;
        this.rede = rede;
        this.marcarPalavras = marcarPalavras;

        this.dataInicial = BasicFunctions.getCalendarDMY(dataInicial);
        this.dataFinal = BasicFunctions.getCalendarDMY(dataFinal);
    }

    public void carregarClasseSinalFraco() {
        carregarOntologia();

        List sinalFracoList = SinalfracoDAO.consultarSinaisFracosByDominio(dominioId, dataInicial, dataFinal);

        List idSFlista = new ArrayList();
        idSFlista.add(Integer.valueOf(sinalFracoId));

        List listasSinaisFracosList = ListasSinaisFracosDAO.consultarListasSinaisFracosByListaId(listaId);

        for (int i = 0; i < listasSinaisFracosList.size(); i++) {
            ListasSinaisFracos vListasSinaisFracos = (ListasSinaisFracos) listasSinaisFracosList.get(i);
            if (vListasSinaisFracos.getSinalFraco() != null) {
                idSFlista.add(Integer.valueOf(vListasSinaisFracos.getSinalFraco().getId()));
            }
        }

        if (graficoIds.trim().length() > 0) {
            String[] strGraficoIds = graficoIds.split(",");
            for (int i = 0; i < strGraficoIds.length; i++) {
                idSFlista.add(Integer.valueOf(Integer.parseInt(strGraficoIds[i].trim())));
            }
        }

        if ((tipoSemantica == 0) || (tipoSemantica == 2)) {
            for (int i = 0; i < sinalFracoList.size(); i++) {
                Sinalfraco sf = (Sinalfraco) sinalFracoList.get(i);

                ClasseSinalFraco classeSinalFraco = new ClasseSinalFraco(getConceitosRedeSemantica(), marcarPalavras);
                classeSinalFraco.setSinailfraco(sf, ignorar, compostas, rede);

                if ((!idSFlista.contains(Integer.valueOf(sf.getId()))) && (classeSinalFraco.getClasse() > 0.0F)) {
                    classeSFList.add(classeSinalFraco);
                }
            }
        }

        if ((tipoSemantica == 1) || (tipoSemantica == 2)) {
            for (int i = 0; i < sinalFracoList.size(); i++) {
                Sinalfraco sf = (Sinalfraco) sinalFracoList.get(i);

                if (sf.getComentarios().trim().length() > 0) {
                    ClasseSinalFraco classeSinalFraco = new ClasseSinalFraco(getConceitosRedeSemantica(), marcarPalavras);
                    classeSinalFraco.setTextoSF(2);
                    classeSinalFraco.setSinailfraco(sf, ignorar, compostas, rede);

                    if ((!idSFlista.contains(Integer.valueOf(sf.getId()))) && (classeSinalFraco.getClasse() > 0.0F)) {
                        classeSFList.add(classeSinalFraco);
                    }
                }
            }
        }

        // AVALIAR
        Collections.sort(classeSFList, new Comparator() {
            @Override
            public int compare(Object o1, Object o2) {
                ClasseSinalFraco sf1 = (ClasseSinalFraco) o1;
                ClasseSinalFraco sf2 = (ClasseSinalFraco) o2;
                int result = 0;

                result = (sf1.getClasse() < sf2.getClasse()) ? 1 : ((sf1.getClasse() > sf2.getClasse()) ? -1 : 0);

                return result;
            }
        });

        List<ClasseSinalFraco> finalList = new ArrayList();
        int total = 0;

        for (int i = 0; i < classeSFList.size(); i++) {
            ClasseSinalFraco classeSinalFraco = (ClasseSinalFraco) classeSFList.get(i);
            finalList.add(classeSinalFraco);
            total++;
            if (total >= 20) {
                break;
            }
        }

        if (finalList.size() > 0) {
            ClasseSinalFraco classeSinalFraco = (ClasseSinalFraco) finalList.get(0);
            int maximo = Math.round(classeSinalFraco.getClasse());
            classeSinalFraco = (ClasseSinalFraco) finalList.get(finalList.size() - 1);
            int minimo = Math.round(classeSinalFraco.getClasse()) - 1;

            int dif = (maximo - minimo) / 6;
            faixaValores[0] = minimo;
            faixaValores[1] = (faixaValores[0] + dif);
            faixaValores[2] = (faixaValores[1] + dif);
            faixaValores[3] = (faixaValores[2] + dif);
            faixaValores[4] = (faixaValores[3] + dif);
            faixaValores[5] = (faixaValores[4] + dif);

            for (int i = 0; i < finalList.size(); i++) {
                classeSinalFraco = (ClasseSinalFraco) finalList.get(i);

                if (classeSinalFraco.getClasse() >= faixaValores[5]) {
                    getG0list().add(classeSinalFraco);
                } else if (classeSinalFraco.getClasse() >= faixaValores[4]) {
                    getG1list().add(classeSinalFraco);
                } else if (classeSinalFraco.getClasse() >= faixaValores[3]) {
                    getG2list().add(classeSinalFraco);
                } else if (classeSinalFraco.getClasse() >= faixaValores[2]) {
                    getG3list().add(classeSinalFraco);
                } else if (classeSinalFraco.getClasse() >= faixaValores[1]) {
                    getG4list().add(classeSinalFraco);
                } else if (classeSinalFraco.getClasse() >= faixaValores[0]) {
                    getG5list().add(classeSinalFraco);
                }
            }
        }
    }

    public void carregarOntologia() {
        palavrasEncontradaOntologia = new ArrayList();

        List<String> palavrasKit = OntologiaFunctions.getPalavrasFortes(kitDescricao, ignorar, compostas);

        List instancias = obterInstancias(palavrasKit, palavrasEncontradaOntologia);

        System.out.println();
        System.out.println("Palavras fortes Kit: " + palavrasKit);
        System.out.println("Palavras encontradas na ontologia Kit: " + palavrasEncontradaOntologia);
        System.out.println();

        List<Conceito> conceitosKit = obterConceitosDasInstancias(instancias);

        for (int i = 0; i < conceitosKit.size(); i++) {
            Conceito conceito = (Conceito) conceitosKit.get(i);
            idsConceitoPesoExtra.add(Integer.valueOf(conceito.getId()));
        }

        palavrasEncontradaOntologia = new ArrayList();

        List<String> palavras = OntologiaFunctions.getPalavrasFortes(sinalFracoDescricao, ignorar, compostas);

        instancias = obterInstancias(palavras, palavrasEncontradaOntologia);

        System.out.println();
        System.out.println("Palavras fortes: " + palavras);
        System.out.println("Palavras encontradas na ontologia: " + palavrasEncontradaOntologia);
        System.out.println();

        setConceitosRedeSemantica(obterConceitosDasInstancias(instancias));

        carregarRedeSemantica(getConceitosRedeSemantica(), 3);

        for (int i = 0; i < getConceitosRedeSemantica().size(); i++) {
            listString = new ArrayList();
            Conceito conceito = (Conceito) getConceitosRedeSemantica().get(i);
            System.out.println("Rede: " + (i + 1) + " => Conceito: " + conceito.getId() + " - " + conceito.getNome());
            exibirRede(conceito, null, null);
        }
    }

    private List<Instancia> obterInstancias(List<String> palavrasTexto, List palavrasEncontradaOntologia) {
        List<Instancia> result = new ArrayList();

        QueryPS qry = new QueryPS(ConnectJdbcDataBase.getJdbcConnection());

        List idsUsados = new ArrayList();

        for (int j = 0; j < palavrasTexto.size(); j++) {
            String palavra = (String) palavrasTexto.get(j);

            qry.setQuery("select distinct cs.id, cs.conceito from conceito_semantico cs where tipo='I' and upper(cs.conceito) like upper(?) and id_dominio = ?;");
            qry.setParameterValue(1, OntologiaFunctions.tratarPalavraConsultaOntologiaSQL(palavra));
            qry.setParameterValue(2, Integer.valueOf(dominioId));
            qry.executeSQL();

            if (qry.numRegistro > 0) {
                if (!palavrasEncontradaOntologia.contains(palavra)) {
                    palavrasEncontradaOntologia.add(palavra);
                }

                do {
                    try {
                        String descricao = qry.getString(2);

                        Instancia instancia = new Instancia(qry.getInt(1), descricao);

                        if (idsUsados.indexOf(Integer.valueOf(instancia.getId())) < 0) {
                            idsUsados.add(Integer.valueOf(instancia.getId()));
                            result.add(instancia);
                        }

                    } catch (Exception ex) {
                    }
                } while (qry.next());
            }
            qry.close();
        }

        return result;
    }

    private List<Conceito> obterConceitosDasInstancias(List<Instancia> instancias) {
        List<Conceito> result = new ArrayList();

        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < instancias.size(); i++) {
            Instancia instancia = (Instancia) instancias.get(i);

            if (sb.toString().length() == 0) {
                sb.append(instancia.getId());
            } else {
                sb.append(", ").append(instancia.getId());
            }
        }

        if (sb.toString().length() > 0) {
            List idsUsados = new ArrayList();

            QueryPS qry = new QueryPS(ConnectJdbcDataBase.getJdbcConnection());

            String sql = "select distinct cs.id, cs.conceito from conceito_regra cr inner join conceito_semantico cs on cs.id = cr.id_conceito2 and cs.tipo='C' and cs.id_dominio = ? where cr.id_conceito1 in (" + sb.toString() + ")";

            qry.setQuery(sql);
            qry.setParameterValue(1, Integer.valueOf(dominioId));
            qry.executeSQL();

            if (qry.numRegistro > 0) {
                do {

                    try {

                        int id = qry.getInt(1);
                        float peso = 1.0F;

                        if (idsConceitoPesoExtra.contains(Integer.valueOf(id))) {
                            peso += conceitoPesoExtraValue;
                        }

                        Conceito conceito = new Conceito(id, 0, qry.getString(2), 0, peso);

                        if (idsUsados.indexOf(Integer.valueOf(conceito.getId())) < 0) {
                            idsUsados.add(Integer.valueOf(conceito.getId()));
                            result.add(conceito);
                            atualizarInstancias(conceito);
                        }

                    } catch (Exception ex) {
                    }
                } while (qry.next());
            }
        }
        return result;
    }

    private void carregarRedeSemantica(List<Conceito> conceitos, int grau) {
        QueryPS qry = new QueryPS(ConnectJdbcDataBase.getJdbcConnection());

        List<Conceito> proximosConceitos = new ArrayList();

        if (grau > 0) {
            for (int i = 0; i < conceitos.size(); i++) {
                Conceito conceito = (Conceito) conceitos.get(i);

                String sql = "select distinct cs.id, cs.conceito, cr.id_regra, rs.regra, rs.peso from conceito_regra cr inner join conceito_semantico cs on cs.id = cr.id_conceito2 and cs.tipo='C' and cs.id_dominio = ? inner join regra_semantica rs on rs.id = cr.id_regra where cr.id_conceito1 = ?";

                if (conceito.getIdPai() != 0) {
                    sql = sql + " and (cr.id_conceito2 != ?)";
                }

                qry.setQuery(sql);
                qry.setParameterValue(1, Integer.valueOf(dominioId));
                qry.setParameterValue(2, Integer.valueOf(conceito.getId()));
                if (conceito.getIdPai() != 0) {
                    qry.setParameterValue(3, Integer.valueOf(conceito.getIdPai()));
                }
                qry.executeSQL();
                int id;
                float peso;
                Conceito conceitoAux;
                Ligacao ligacao;
                if (qry.numRegistro > 0) {
                    do {
                        try {
                            id = qry.getInt(1);
                            peso = qry.getFloat(5);

                            if (idsConceitoPesoExtra.contains(Integer.valueOf(id))) {
                                peso += conceitoPesoExtraValue;
                            }

                            conceitoAux = new Conceito(id, conceito.getId(), qry.getString(2), conceito.getGrau() + 1, peso);
                            ligacao = new Ligacao(qry.getInt(3), qry.getString(4), conceitoAux, 0);

                            conceito.getLigacoes().add(ligacao);

                            atualizarInstancias(conceitoAux);
                            proximosConceitos.add(conceitoAux);

                        } catch (Exception ex) {
                        }
                    } while (qry.next());
                }
                qry.close();

                sql = "select distinct cs.id, cs.conceito, cr.id_regra, rs.regra, rs.peso from conceito_regra cr inner join conceito_semantico cs on cs.id = cr.id_conceito1 and cs.tipo='C' and cs.id_dominio = ? inner join regra_semantica rs on rs.id = cr.id_regra where cr.id_conceito2 = ?";

                if (conceito.getIdPai() != 0) {
                    sql = sql + " and (cr.id_conceito1 != ?)";
                }

                qry.setQuery(sql);
                qry.setParameterValue(1, Integer.valueOf(dominioId));
                qry.setParameterValue(2, Integer.valueOf(conceito.getId()));
                if (conceito.getIdPai() != 0) {
                    qry.setParameterValue(3, Integer.valueOf(conceito.getIdPai()));
                }
                qry.executeSQL();

                if (qry.numRegistro > 0) {
                    do {
                        try {
                            id = qry.getInt(1);
                            peso = qry.getFloat(5);

                            if (idsConceitoPesoExtra.contains(Integer.valueOf(id))) {
                                peso += conceitoPesoExtraValue;
                            }

                            conceitoAux = new Conceito(id, conceito.getId(), qry.getString(2), conceito.getGrau() + 1, peso);
                            ligacao = new Ligacao(qry.getInt(3), qry.getString(4), conceitoAux, 1);

                            conceito.getLigacoes().add(ligacao);

                            atualizarInstancias(conceitoAux);
                            proximosConceitos.add(conceitoAux);

                        } catch (Exception ex) {
                        }
                    } while (qry.next());
                }
                qry.close();
            }

            carregarRedeSemantica(proximosConceitos, grau - 1);
        }
    }

    private void exibirRede(Conceito conceito, String espaco, String fraseAnterior) {
        if (espaco == null) {
            espaco = "   ";
        }

        String nome = conceito.getNome();
        int qtdInstancia = conceito.getListInstancias().size();

        List<Ligacao> ligacoes = conceito.getLigacoes();

        for (int j = 0; j < ligacoes.size(); j++) {
            Ligacao ligacao = (Ligacao) ligacoes.get(j);
            conceito = ligacao.getConceito();
            String frase;
            if (ligacao.getSentido() == 0) {
                frase = nome + "(" + qtdInstancia + ") " + ligacao.getNome() + " " + conceito.getNome() + "(" + conceito.getListInstancias().size() + ")";
            } else {
                frase = conceito.getNome() + "(" + conceito.getListInstancias().size() + ") " + ligacao.getNome() + " " + nome + "(" + qtdInstancia + ")";
            }

            if ((fraseAnterior == null) || (!frase.equals(fraseAnterior))) {
                listString.add(frase);
                System.out.println(espaco + "(" + nome + ") " + frase);
                exibirRede(conceito, espaco + "   ", frase);
            }
        }
    }

    private void atualizarInstancias(Conceito conceito) {
        QueryPS qry = new QueryPS(ConnectJdbcDataBase.getJdbcConnection());

        String sql = "select distinct cs.id, cs.conceito from conceito_regra cr inner join conceito_semantico cs on cs.id = cr.id_conceito1 and cs.tipo='I' where cr.id_conceito2 = ?";

        qry.setQuery(sql);
        qry.setParameterValue(1, Integer.valueOf(conceito.getId()));
        qry.executeSQL();

        conceito.setListInstancias(new ArrayList());

        if (qry.numRegistro > 0) {
            do {
                try {
                    Instancia instancia = new Instancia(qry.getInt(1), qry.getString(2));
                    conceito.getListInstancias().add(instancia);
                } catch (Exception ex) {
                }
            } while (qry.next());
        }
    }

    public String getHTML(List<ClasseSinalFraco> lista, String descricao, String cor) {
        StringBuilder sb = new StringBuilder();

        StringBuilder sbDesc = new StringBuilder();

        for (int i = 0; i < descricao.length(); i++) {
            sbDesc.append(descricao.substring(i, i + 1) + "<br>");
        }

        ResourceBundle resource = ResourceBundle.getBundle("seris.SerisIdioma");

        if (lista.size() == 0) {

            sb.append("<tr height=30><td width=25 align=center bgcolor=\"" + cor + "\" style=\"color: #ffffff;\"><b><div style=\"color: #ffffff;\">" + sbDesc.toString() + "</div></b></td>");
            sb.append("<td><b>" + resource.getString("label.conexaoNaoEncontrada") + "</b></td></tr>");
            sb.append("<tr style=\"height: 2px;\"><td colspan=2 bgcolor=\"" + cor + "\"></td></tr>");
        } else {
            sb.append("<tr style=\"height: 3px;\"><td bgcolor=\"" + cor + "\"></td><td></td></tr>");

            sb.append("<tr><td width=25 rowspan=" + (lista.size() + 0) + " align=center bgcolor=\"" + cor + "\"><b><div style=\"color: #ffffff;\">" + sbDesc.toString() + "</div></b></td>");

            for (int i = 0; i < lista.size(); i++) {
                ClasseSinalFraco csf = (ClasseSinalFraco) lista.get(i);
                Sinalfraco sf = csf.getSinailfraco();

                String desc = "";
                String descSF = "";
                String descHTML = "";
                String comentario = "";

                if (sf.getDescricao().indexOf("MG assina protocolo para") >= 0) {
                    System.out.println("achou");
                }

                if (csf.getTextoSF() == 1) {
                    desc = sf.getDescricao().replace("\n", "<BR>").replace("\r", "<BR>").replace("'", "<ASPAS>");

                    descHTML = csf.getTexto().replace("<", "&lt;").replace(">", "&gt;");
                    descHTML = descHTML.replace("{SINALMENOR}", "<").replace("{SINALMAIOR}", ">");
                    comentario = " - " + resource.getString("label.sinalFraco");
                } else {
                    desc = sf.getComentarios().replace("\n", "<BR>").replace("\r", "<BR>").replace("'", "<ASPAS>");
                    descSF = sf.getDescricao().replace("\n", "<BR>").replace("\r", "<BR>").replace("'", "<ASPAS>");

                    descHTML = csf.getTexto().replace("<", "&lt;").replace(">", "&gt;");
                    descHTML = descHTML.replace("{SINALMENOR}", "<").replace("{SINALMAIOR}", ">");
                    comentario = " - " + resource.getString("label.conhecimento");
                }

                sb.append("<td>");

                sb.append("  <div style=\"border: solid 2px #fff;\">");
                sb.append("    <span style=\"font-size: 10px; color: #990000; cursor: hand; cursor: pointer;\" onmouseover=\"exibirinfo('" + sf.getId() + "');\"> <b>" + resource.getString("label.detalhes") + comentario + " </b> </span>");

                sb.append("    <div id=\"tableSFOntologia" + sf.getId() + "\" style=\"cursor: hand; cursor: pointer; border: solid 2px #fff; color: #000;\" ");
                sb.append("    onMouseOver=\"this.style.backgroundColor='#EEEEEE'; this.style.borderColor='#AAA';\" ");
                sb.append("    onMouseOut=\"this.style.backgroundColor='#fff'; this.style.borderColor='#fff';\" ");
                sb.append("    onClick=\"if (this.style.color != 'rgb(153, 153, 153)' && this.style.color != '#AAA' && this.style.color != '#AAAAAA') ");
                sb.append("    { ");
                if (csf.getTextoSF() == 1) {
                    sb.append("      adicionarSinalFracoOntologia(null, '" + desc + "', " + sf.getId() + ",0); ");
                } else {
                    sb.append("      adicionarSinalFracoComentario(null, '" + descSF + "', '" + desc + "', " + sf.getId() + "); ");
                }
                sb.append("    }; this.style.color='#999999'; \">");
                sb.append(descHTML);
                sb.append("    </div>");
                sb.append("  </div>");

                if (i < lista.size() - 1) {
                    sb.append("<hr color=#EEEEEE>");
                }
                sb.append("</td></tr>");
            }

            sb.append("<tr style=\"height: 2px;\"><td colspan=2 bgcolor=\"" + cor + "\"></td></tr>");
        }
        return sb.toString();
    }

    public String getHTMLGrifarSF() {
        StringBuilder sb = new StringBuilder();

        if (palavrasEncontradaOntologia != null) {
            for (int i = 0; i < palavrasEncontradaOntologia.size(); i++) {
                String palavra = (String) palavrasEncontradaOntologia.get(i);
                sb.append("addPalavraGrifarOntologia('").append(palavra).append("');\n");
            }
        }

        return sb.toString();
    }

    public List<ClasseSinalFraco> getG0list() {
        return g0list;
    }

    public List<ClasseSinalFraco> getG1list() {
        return g1list;
    }

    public List<ClasseSinalFraco> getG2list() {
        return g2list;
    }

    public List<ClasseSinalFraco> getG3list() {
        return g3list;
    }

    public List<ClasseSinalFraco> getG4list() {
        return g4list;
    }

    public List<ClasseSinalFraco> getG5list() {
        return g5list;
    }

    public List<Conceito> getConceitosRedeSemantica() {
        return conceitosRedeSemantica;
    }

    public void setConceitosRedeSemantica(List<Conceito> conceitosRedeSemantica) {
        this.conceitosRedeSemantica = conceitosRedeSemantica;
    }
}
