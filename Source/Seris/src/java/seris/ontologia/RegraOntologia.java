package seris.ontologia;

import java.util.ArrayList;
import java.util.List;

public class RegraOntologia {

    public RegraOntologia() {
    }

    private List<ConceitoInstancia> g0 = new ArrayList();
    private List<ConceitoInstancia> g1 = new ArrayList();
    private List<ConceitoInstancia> g2 = new ArrayList();
    private List<ConceitoInstancia> g3 = new ArrayList();
    private List<ConceitoInstancia> g4 = new ArrayList();
    private List<ConceitoInstancia> g5 = new ArrayList();

    public List<ConceitoInstancia> getG0() {
        return g0;
    }

    public void setG0(List<ConceitoInstancia> g0) {
        this.g0 = g0;
    }

    public List<ConceitoInstancia> getG1() {
        return g1;
    }

    public void setG1(List<ConceitoInstancia> g1) {
        this.g1 = g1;
    }

    public List<ConceitoInstancia> getG2() {
        return g2;
    }

    public void setG2(List<ConceitoInstancia> g2) {
        this.g2 = g2;
    }

    public List<ConceitoInstancia> getG3() {
        return g3;
    }

    public void setG3(List<ConceitoInstancia> g3) {
        this.g3 = g3;
    }

    public List<ConceitoInstancia> getG4() {
        return g4;
    }

    public void setG4(List<ConceitoInstancia> g3) {
        g4 = g4;
    }

    public List<ConceitoInstancia> getG5() {
        return g5;
    }

    public void setG5(List<ConceitoInstancia> g3) {
        g5 = g5;
    }
}
