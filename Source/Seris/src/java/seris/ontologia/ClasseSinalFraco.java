package seris.ontologia;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import seris.database.Sinalfraco;
import seris.functions.BasicFunctions;
import seris.ontologia.redesemantica.Conceito;
import seris.ontologia.redesemantica.Instancia;
import seris.ontologia.redesemantica.Ligacao;

public class ClasseSinalFraco {

    private final String g0color = "#ff841f";
    private final String g1color = "#ffc91f";
    private final String g2color = "#fffb37";
    private final String g3color = "#fffdad";
    private final int g0value = 45;
    private final int g1value = 30;
    private final int g2value = 10;
    private final int g3value = 5;
    private float classe = 0.0F;
    private Sinalfraco sinailfraco;
    private String texto;
    private int textoSF = 1;
    List<Conceito> conceitosRedeSemantica;
    List idsConceitosUsados;
    List<PalavrasGraus> palavrasGrausUsadas = new ArrayList();
    private Character marcarPalavras = Character.valueOf('N');

    public ClasseSinalFraco(List<Conceito> conceitosRedeSemantica, Character marcarPalavras) {
        this.conceitosRedeSemantica = conceitosRedeSemantica;
        this.marcarPalavras = marcarPalavras;
    }

    public float getClasse() {
        return classe;
    }

    public void setClasse(float classe) {
        this.classe = classe;
    }

    public Sinalfraco getSinailfraco() {
        return sinailfraco;
    }

    public void setSinailfraco(Sinalfraco sinailfraco, List ignorar, List compostas, int rede) {
        this.sinailfraco = sinailfraco;
        calcularClasse(ignorar, compostas, rede);
    }

    private void calcularClasse(List ignorar, List compostas, int rede) {
        String texto = "";
        if (textoSF == 1) {
            texto = sinailfraco.getDescricao();
            System.out.println(sinailfraco.getDescricao());
        } else if (textoSF == 2) {
            texto = sinailfraco.getComentarios();
            System.out.println(sinailfraco.getComentarios());
        }

        setTexto(texto);

        texto = BasicFunctions.trocarAcento(texto);
        texto = texto.toUpperCase();

        List<String> palavrasFortesSF = OntologiaFunctions.getPalavrasFortes(texto, ignorar, compostas);

        idsConceitosUsados = new ArrayList();

        int inicio = 0;
        int fim = conceitosRedeSemantica.size();

        if (rede > 0) {
            inicio = rede - 1;
            fim = rede;
        }

        boolean first = true;
        float total = 0.0F;

        for (int i = inicio; i < fim; i++) {

            List<String> palavrasFortesSFAux = new ArrayList();
            palavrasFortesSFAux.addAll(palavrasFortesSF);

            Conceito conceito = (Conceito) conceitosRedeSemantica.get(i);

            if (!first) {
                System.out.println("Total Rede: " + classe);
            }

            System.out.println("Rede[" + (i + 1) + "]: " + conceito.getNome());

            idsConceitosUsados = new ArrayList();

            classe = 0.0F;

            calcularPeso(palavrasFortesSFAux, conceito);

            first = false;
            total += classe;
        }

        System.out.println("Total Rede: " + classe);

        classe = total;

        System.out.println("Total de pontos: " + classe);
        System.out.println("");

        if (marcarPalavras.charValue() == 'S') {
            for (int i = 0; i < palavrasGrausUsadas.size(); i++) {
                PalavrasGraus palavrasGraus = (PalavrasGraus) palavrasGrausUsadas.get(i);
                colorirTexto(palavrasGraus.getPalavra(), palavrasGraus.getGraus());
            }
        }
    }

    public int getTextoSF() {
        return textoSF;
    }

    public void setTextoSF(int textoSF) {
        this.textoSF = textoSF;
    }

    private void calcularPeso(List<String> palavrasFortesSF, Conceito conceito) {
        if (!idsConceitosUsados.contains(Integer.valueOf(conceito.getId()))) {

            for (int i = palavrasFortesSF.size() - 1; i >= 0; i--) {
                String palavra = (String) palavrasFortesSF.get(i);

                boolean parar = false;

                for (int j = 0; j < conceito.getListInstancias().size(); j++) {
                    Instancia instancia = (Instancia) conceito.getListInstancias().get(j);
                    String strInstancia = instancia.getNome();
                    strInstancia = BasicFunctions.trocarAcento(strInstancia);
                    strInstancia = strInstancia.toUpperCase();

                    if (palavra.equals(strInstancia)) {
                        if (marcarPalavras.charValue() == 'S') {
                            adicionarPalavraGraus(palavra, conceito.getGrau());
                        }

                        float pontos = getPesoGrau(conceito.getGrau()) * conceito.getPeso();
                        classe += pontos;
                        System.out.println(palavra + " pontuou " + conceito.getNome() + "(Grau: " + conceito.getGrau() + ")(Ligações: " + conceito.getLigacoes().size() + ")->" + strInstancia + " = " + pontos);

                        break;
                    }
                }
            }
        }

        for (int i = 0; i < conceito.getLigacoes().size(); i++) {
            Ligacao ligacao = (Ligacao) conceito.getLigacoes().get(i);
            calcularPeso(palavrasFortesSF, ligacao.getConceito());
        }
    }

    private float getPesoGrau(int grau) {
        float result = 0.0F;

        if (grau == 0) {
            getClass();
            result = 45.0F;
        } else if (grau == 1) {
            getClass();
            result = 30.0F;
        } else if (grau == 2) {
            getClass();
            result = 10.0F;
        } else if (grau == 3) {
            getClass();
            result = 5.0F;
        }

        return result;
    }

    private void colorirTexto(String palavra, String grau) {
        String cor = "#ffffff";

        int count = 0;
        if (grau.indexOf("3") >= 0) {
            getClass();
            cor = "#fffdad";
            count++;
        }
        if (grau.indexOf("2") >= 0) {
            getClass();
            cor = "#fffb37";
            count++;
        }
        if (grau.indexOf("1") >= 0) {
            getClass();
            cor = "#ffc91f";
            count++;
        }
        if (grau.indexOf("0") >= 0) {
            getClass();
            cor = "#ff841f";
            count++;
        }

        String auxTexto = getTexto();
        auxTexto = BasicFunctions.trocarAcento(auxTexto);
        auxTexto = auxTexto.toUpperCase();

        int indice = auxTexto.indexOf(palavra);

        if (indice >= 0) {
            String negritoini = "";
            String negritofim = "";
            if (count > 1) {
                negritoini = "{SINALMENOR}b{SINALMAIOR}";
                negritofim = "{SINALMENOR}/b{SINALMAIOR}";
            }
            int indfinal = indice + palavra.length();

            StringBuilder result = new StringBuilder();

            if (indice > 0) {
                result.append(getTexto().substring(0, indice));
            }

            result.append("{SINALMENOR}font style=\"background-color: " + cor + ";\"{SINALMAIOR}" + negritoini + getTexto().substring(indice, indfinal) + negritofim + "{SINALMENOR}/font{SINALMAIOR}");

            if (getTexto().length() >= indfinal) {
                result.append(getTexto().substring(indfinal));
            }

            setTexto(result.toString());
        }
    }

    public String getTexto() {
        return texto;
    }

    public void setTexto(String texto) {
        this.texto = texto;
    }

    private void adicionarPalavraGraus(String palavra, int grau) {
        PalavrasGraus pg = null;
        for (int i = 0; i < palavrasGrausUsadas.size(); i++) {
            pg = (PalavrasGraus) palavrasGrausUsadas.get(i);

            if (pg.getPalavra().equals(palavra)) {
                pg.setGraus(pg.getGraus() + ";" + String.valueOf(grau));
                break;
            }
            pg = null;
        }

        if (pg == null) {
            pg = new PalavrasGraus(palavra, String.valueOf(grau));
            palavrasGrausUsadas.add(pg);
        }
    }
}
