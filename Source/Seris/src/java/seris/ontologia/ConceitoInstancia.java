package seris.ontologia;

import java.util.ArrayList;
import java.util.List;

public class ConceitoInstancia {

    private int id;
    private String nome;
    private Character tipo;
    private List<ConceitoInstancia> conceitoInstancia = new ArrayList();
    private float peso;

    public ConceitoInstancia() {
    }

    public ConceitoInstancia(int id, String nome, Character tipo, float peso) {
        this.id = id;
        this.nome = nome;
        this.tipo = tipo;
        this.peso = peso;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Character getTipo() {
        return tipo;
    }

    public void setTipo(Character tipo) {
        this.tipo = tipo;
    }

    public float getPeso() {
        return peso;
    }

    public void setPeso(float peso) {
        this.peso = peso;
    }

    public List<ConceitoInstancia> getConceitoInstancia() {
        return conceitoInstancia;
    }

    public void setConceitoInstancia(List<ConceitoInstancia> conceitoInstancia) {
        this.conceitoInstancia = conceitoInstancia;
    }
}
