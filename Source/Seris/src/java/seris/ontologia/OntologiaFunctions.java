package seris.ontologia;

import java.util.ArrayList;
import java.util.List;
import seris.functions.BasicFunctions;
import seris.functions.ConnectJdbcDataBase;
import seris.functions.QueryPS;

public class OntologiaFunctions {

    public OntologiaFunctions() {
    }

    public static List<String> getPalavrasFortes(String sinalFraco, List ignorar, List compostas) {
        List<String> result = new ArrayList();

        sinalFraco = BasicFunctions.trocarAcento(sinalFraco).replace("\n", " ").replace(".", " ").replace(",", " ").toUpperCase();
        sinalFraco = sinalFraco.replace("(", " ").replace(")", " ").replace(":", " ");

        for (int i = 0; i < compostas.size(); i++) {
            String aux = (String) compostas.get(i);
            aux = BasicFunctions.trocarAcento(aux).toUpperCase();

            if ((sinalFraco.indexOf(aux) >= 0) && (result.indexOf(aux) < 0)) {
                result.add(aux);
                sinalFraco = sinalFraco.replace(aux, "").trim();
            }
        }

        String[] palavras = sinalFraco.split(" ");

        for (int i = 0; i < palavras.length; i++) {
            String aux = palavras[i].trim();
            if ((ignorar.indexOf(aux) < 0) && (result.indexOf(aux) < 0) && (aux.length() > 0)) {
                result.add(aux);
            }
        }

        return result;
    }

    public static List<String> getPlavrasIgnorar() {
        List<String> ignorar = new ArrayList();

        QueryPS qry = new QueryPS(ConnectJdbcDataBase.getJdbcConnection());

        qry.setQuery("select UPPER(palavra) from semantica_ignorar;");
        qry.executeSQL();

        if (qry.numRegistro > 0) {
            do {
                try {
                    String aux = qry.getString(1);
                    aux = BasicFunctions.trocarAcento(aux).toUpperCase();

                    ignorar.add(aux);
                } catch (Exception ex) {
                }
            } while (qry.next());
        }

        return ignorar;
    }

    public static List<String> getPlavrasCompostas(int dominioId) {
        List<String> compostas = new ArrayList();

        QueryPS qry = new QueryPS(ConnectJdbcDataBase.getJdbcConnection());

        qry.setQuery("select palavra_composta from semantica_palavra_composta where id_dominio = ?;");
        qry.setParameterValue(1, Integer.valueOf(dominioId));
        qry.executeSQL();

        if (qry.numRegistro > 0) {
            do {
                try {
                    compostas.add(qry.getString(1));
                } catch (Exception ex) {
                }
            } while (qry.next());
        }

        return compostas;
    }

    public static List<String> getPlavrasFuturas() {
        List<String> compostas = new ArrayList();

        QueryPS qry = new QueryPS(ConnectJdbcDataBase.getJdbcConnection());

        qry.setQuery("select palavra from semantica_futuro;");
        qry.executeSQL();

        if (qry.numRegistro > 0) {
            do {
                try {
                    String futura = qry.getString(1);
                    futura = BasicFunctions.trocarAcento(futura).toUpperCase();
                    compostas.add(futura);
                } catch (Exception ex) {
                }
            } while (qry.next());
        }

        return compostas;
    }

    public static String tratarPalavraConsultaOntologia(String palavra) {
        return palavra;
    }

    public static String tratarPalavraConsultaOntologiaSQL(String palavra) {
        return tratarPalavraConsultaOntologia(palavra);
    }
}
