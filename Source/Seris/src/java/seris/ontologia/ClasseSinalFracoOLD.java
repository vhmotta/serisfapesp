package seris.ontologia;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;
import seris.database.Sinalfraco;
import seris.functions.BasicFunctions;

public class ClasseSinalFracoOLD {

    private final int g0value = 45;
    private final int g1value = 30;
    private final int g2value = 10;
    private final int g3value = 5;
    private final int g4value = 0;
    private final int g5value = 0;
    private final float pesoCompostas = 1.0F;
    private int classe = -1;
    private Sinalfraco sinailfraco;
    private RegraOntologia regraOntologia;
    private boolean valido = true;

    public ClasseSinalFracoOLD(RegraOntologia regraOntologia) {
        this.regraOntologia = regraOntologia;
    }

    public int getClasse() {
        return classe;
    }

    public void setClasse(int classe) {
        this.classe = classe;
    }

    public Sinalfraco getSinailfraco() {
        return sinailfraco;
    }

    public void setSinailfraco(Sinalfraco sinailfraco, List ignorar, List compostas, int nivelConexao) {
        this.sinailfraco = sinailfraco;
        calcularClasse(ignorar, compostas, nivelConexao);
    }

    public RegraOntologia getRegraOntologia() {
        return regraOntologia;
    }

    public void setRegraOntologia(RegraOntologia regraOntologia) {
        this.regraOntologia = regraOntologia;
    }

    private void calcularClasse(List ignorar, List compostas, int nivelConexao) {
        String texto = sinailfraco.getDescricao();
        texto = BasicFunctions.trocarAcento(texto);
        texto = texto.toUpperCase();

        if (texto.indexOf("SAO PAULO") >= 0) {
            System.out.println();
        }

        System.out.println(sinailfraco.getDescricao());

        int total0 = 0;
        int total1 = 0;
        int total2 = 0;
        int total3 = 0;
        int total4 = 0;
        int total5 = 0;

        List<String> palavrasFortesSF = OntologiaFunctions.getPalavrasFortes(texto, ignorar, compostas);

        List palavrasUsadas = new ArrayList();

        for (int i = 0; i < regraOntologia.getG0().size(); i++) {
            ConceitoInstancia ci = (ConceitoInstancia) regraOntologia.getG0().get(i);
            List<String> palavrasFortes = OntologiaFunctions.getPalavrasFortes(ci.getNome().toUpperCase(), ignorar, compostas);

            for (int j = 0; j < palavrasFortes.size(); j++) {
                String palavra = (String) palavrasFortes.get(j);

                String radical = OntologiaFunctions.tratarPalavraConsultaOntologia(palavra);
                radical = BasicFunctions.trocarAcento(radical);
                radical = radical.toUpperCase();

                String excluirPalavra = radical;

                for (int k = palavrasFortesSF.size() - 1; k >= 0; k--) {
                    if ((palavrasUsadas.indexOf(excluirPalavra) < 0) && (((((String) palavrasFortesSF.get(k)).indexOf(" ") >= 0) && (((String) palavrasFortesSF.get(k)).equals(palavra))) || ((((String) palavrasFortesSF.get(k)).indexOf(" ") < 0) && (((String) palavrasFortesSF.get(k)).indexOf(radical) >= 0)))) {
                        palavrasUsadas.add(excluirPalavra);
                        System.out.println("G0: " + palavra + "(" + radical + ")");

                        if ((((String) palavrasFortesSF.get(k)).indexOf(" ") >= 0) && (((String) palavrasFortesSF.get(k)).equals(palavra))) {
                            getClass();
                            getClass();
                            total0 = (int) (total0 + 45.0F * 1.0F);
                        } else {
                            getClass();
                            total0 += 45;
                        }

                        palavrasFortesSF.remove(k);
                    }
                }
            }
        }

        for (int i = 0; i < regraOntologia.getG1().size(); i++) {
            ConceitoInstancia ci = (ConceitoInstancia) regraOntologia.getG1().get(i);
            List<String> palavrasFortes = OntologiaFunctions.getPalavrasFortes(ci.getNome().toUpperCase(), ignorar, compostas);

            for (int j = 0; j < palavrasFortes.size(); j++) {
                String palavra = (String) palavrasFortes.get(j);

                String radical = OntologiaFunctions.tratarPalavraConsultaOntologia(palavra);
                radical = BasicFunctions.trocarAcento(radical);
                radical = radical.toUpperCase();

                String excluirPalavra = radical;

                for (int k = palavrasFortesSF.size() - 1; k >= 0; k--) {
                    if ((palavrasUsadas.indexOf(excluirPalavra) < 0) && (((((String) palavrasFortesSF.get(k)).indexOf(" ") >= 0) && (((String) palavrasFortesSF.get(k)).equals(palavra))) || ((((String) palavrasFortesSF.get(k)).indexOf(" ") < 0) && (((String) palavrasFortesSF.get(k)).indexOf(radical) >= 0)))) {
                        palavrasUsadas.add(excluirPalavra);
                        System.out.println("G1: " + palavra + "(" + radical + ")");

                        if ((((String) palavrasFortesSF.get(k)).indexOf(" ") >= 0) && (((String) palavrasFortesSF.get(k)).equals(palavra))) {
                            getClass();
                            getClass();
                            total1 = (int) (total1 + 30.0F * 1.0F);
                        } else {
                            getClass();
                            total1 += 30;
                        }

                        palavrasFortesSF.remove(k);
                    }
                }
            }
        }

        for (int i = 0; i < regraOntologia.getG2().size(); i++) {
            ConceitoInstancia ci = (ConceitoInstancia) regraOntologia.getG2().get(i);
            List<String> palavrasFortes = OntologiaFunctions.getPalavrasFortes(ci.getNome().toUpperCase(), ignorar, compostas);

            for (int j = 0; j < palavrasFortes.size(); j++) {
                String palavra = (String) palavrasFortes.get(j);

                String radical = OntologiaFunctions.tratarPalavraConsultaOntologia(palavra);
                radical = BasicFunctions.trocarAcento(radical);
                radical = radical.toUpperCase();

                String excluirPalavra = radical;

                for (int k = palavrasFortesSF.size() - 1; k >= 0; k--) {
                    if ((palavrasUsadas.indexOf(excluirPalavra) < 0) && (((((String) palavrasFortesSF.get(k)).indexOf(" ") >= 0) && (((String) palavrasFortesSF.get(k)).equals(palavra))) || ((((String) palavrasFortesSF.get(k)).indexOf(" ") < 0) && (((String) palavrasFortesSF.get(k)).indexOf(radical) >= 0)))) {
                        palavrasUsadas.add(excluirPalavra);
                        System.out.println("G2: " + palavra + "(" + radical + ")");

                        if ((((String) palavrasFortesSF.get(k)).indexOf(" ") >= 0) && (((String) palavrasFortesSF.get(k)).equals(palavra))) {
                            getClass();
                            getClass();
                            total2 = (int) (total2 + 10.0F * 1.0F);
                        } else {
                            getClass();
                            total2 += 10;
                        }

                        palavrasFortesSF.remove(k);
                    }
                }
            }
        }

        for (int i = 0; i < regraOntologia.getG3().size(); i++) {
            ConceitoInstancia ci = (ConceitoInstancia) regraOntologia.getG3().get(i);
            List<String> palavrasFortes = OntologiaFunctions.getPalavrasFortes(ci.getNome().toUpperCase(), ignorar, compostas);

            for (int j = 0; j < palavrasFortes.size(); j++) {
                String palavra = (String) palavrasFortes.get(j);

                String radical = OntologiaFunctions.tratarPalavraConsultaOntologia(palavra);
                radical = BasicFunctions.trocarAcento(radical);
                radical = radical.toUpperCase();

                String excluirPalavra = radical;

                for (int k = palavrasFortesSF.size() - 1; k >= 0; k--) {
                    if ((palavrasUsadas.indexOf(excluirPalavra) < 0) && (((((String) palavrasFortesSF.get(k)).indexOf(" ") >= 0) && (((String) palavrasFortesSF.get(k)).equals(palavra))) || ((((String) palavrasFortesSF.get(k)).indexOf(" ") < 0) && (((String) palavrasFortesSF.get(k)).indexOf(radical) >= 0)))) {
                        palavrasUsadas.add(excluirPalavra);
                        System.out.println("G3: " + palavra + "(" + radical + ")");

                        if ((((String) palavrasFortesSF.get(k)).indexOf(" ") >= 0) && (((String) palavrasFortesSF.get(k)).equals(palavra))) {
                            getClass();
                            getClass();
                            total3 = (int) (total3 + 5.0F * 1.0F);
                        } else {
                            getClass();
                            total3 += 5;
                        }

                        palavrasFortesSF.remove(k);
                    }
                }
            }
        }

        for (int i = 0; i < regraOntologia.getG4().size(); i++) {
            ConceitoInstancia ci = (ConceitoInstancia) regraOntologia.getG4().get(i);
            List<String> palavrasFortes = OntologiaFunctions.getPalavrasFortes(ci.getNome().toUpperCase(), ignorar, compostas);

            for (int j = 0; j < palavrasFortes.size(); j++) {
                String palavra = (String) palavrasFortes.get(j);

                String radical = OntologiaFunctions.tratarPalavraConsultaOntologia(palavra);
                radical = BasicFunctions.trocarAcento(radical);
                radical = radical.toUpperCase();

                String excluirPalavra = radical;

                for (int k = palavrasFortesSF.size() - 1; k >= 0; k--) {
                    if ((palavrasUsadas.indexOf(excluirPalavra) < 0) && (((((String) palavrasFortesSF.get(k)).indexOf(" ") >= 0) && (((String) palavrasFortesSF.get(k)).equals(palavra))) || ((((String) palavrasFortesSF.get(k)).indexOf(" ") < 0) && (((String) palavrasFortesSF.get(k)).indexOf(radical) >= 0)))) {
                        palavrasUsadas.add(excluirPalavra);
                        System.out.println("G4: " + palavra + "(" + radical + ")");

                        if ((((String) palavrasFortesSF.get(k)).indexOf(" ") >= 0) && (((String) palavrasFortesSF.get(k)).equals(palavra))) {
                            getClass();
                            getClass();
                            total4 = (int) (total4 + 0.0F * 1.0F);
                        } else {
                            getClass();
                            total4 += 0;
                        }

                        palavrasFortesSF.remove(k);
                    }
                }
            }
        }

        for (int i = 0; i < regraOntologia.getG5().size(); i++) {
            ConceitoInstancia ci = (ConceitoInstancia) regraOntologia.getG5().get(i);
            List<String> palavrasFortes = OntologiaFunctions.getPalavrasFortes(ci.getNome().toUpperCase(), ignorar, compostas);

            for (int j = 0; j < palavrasFortes.size(); j++) {
                String palavra = (String) palavrasFortes.get(j);

                String radical = OntologiaFunctions.tratarPalavraConsultaOntologia(palavra);
                radical = BasicFunctions.trocarAcento(radical);
                radical = radical.toUpperCase();

                String excluirPalavra = radical;

                for (int k = palavrasFortesSF.size() - 1; k >= 0; k--) {
                    if ((palavrasUsadas.indexOf(excluirPalavra) < 0) && (((((String) palavrasFortesSF.get(k)).indexOf(" ") >= 0) && (((String) palavrasFortesSF.get(k)).equals(palavra))) || ((((String) palavrasFortesSF.get(k)).indexOf(" ") < 0) && (((String) palavrasFortesSF.get(k)).indexOf(radical) >= 0)))) {
                        palavrasUsadas.add(excluirPalavra);
                        System.out.println("G5: " + palavra + "(" + radical + ")");

                        if ((((String) palavrasFortesSF.get(k)).indexOf(" ") >= 0) && (((String) palavrasFortesSF.get(k)).equals(palavra))) {
                            getClass();
                            getClass();
                            total5 = (int) (total5 + 0.0F * 1.0F);
                        } else {
                            getClass();
                            total5 += 0;
                        }

                        palavrasFortesSF.remove(k);
                    }
                }
            }
        }

        int total = 0;
        if (nivelConexao == 0) {
            total = total0 + total1 + total2 + total3 + total4 + total5;

            if ((total0 == 0) && (total1 == 0)) {
                valido = false;
            }
        } else if (nivelConexao == 1) {
            total = total2 + total3 + total4 + total5;

            if ((total0 > 0) || (total1 > 0) || ((total2 == 0) && (total3 == 0))) {
                valido = false;
            }
        } else if (nivelConexao == 2) {
            total = total4 + total5;

            if ((total0 > 0) || (total1 > 0) || (total2 > 0) || (total3 > 0)) {
                valido = false;
            }
        }

        classe = total;

        System.out.println(classe);
    }

    public boolean getValido() {
        return valido;
    }

    public void setValido(boolean valido) {
        this.valido = valido;
    }
}
