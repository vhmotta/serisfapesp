package seris;

import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;

public class CadastrarGrupoActionForm
        extends ActionForm {

    private int id = 0;
    private String nome;
    private List grupos;
    private String task = " ";

    public List getGrupos() {
        return grupos;
    }

    public void setGrupos(List grupos) {
        this.grupos = grupos;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getTask() {
        return task;
    }

    public void setTask(String task) {
        this.task = task;
    }

    public CadastrarGrupoActionForm() {
    }

    public ActionErrors validate(ActionMapping mapping, HttpServletRequest request) {
        ActionErrors errors = new ActionErrors();
        if ((getNome() == null) || (getNome().length() < 1)) {
            errors.add("name", new ActionMessage("error.name.required"));
        }

        return errors;
    }
}
