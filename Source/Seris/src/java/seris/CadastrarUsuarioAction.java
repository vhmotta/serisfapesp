package seris;

import java.io.PrintStream;
import java.security.MessageDigest;
import java.util.Iterator;
import java.util.List;
import java.util.ResourceBundle;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import seris.database.Administrador;
import seris.database.Altadirecao;
import seris.database.Analista;
import seris.database.DAO.AltadirecaoDAO;
import seris.database.DAO.AnalistaDAO;
import seris.database.DAO.SuportedominioDAO;
import seris.database.Departamento;
import seris.database.Dominio;
import seris.database.DominioUsuario;
import seris.database.Grupo;
import seris.database.Patrocinador;
import seris.database.Perfil;
import seris.database.Suportedominio;
import seris.database.Usuario;

public class CadastrarUsuarioAction extends DispatchAction {

    private static final String SUCCESS = "success";
    private static final String FAILURE = "failure";
    private ResourceBundle resource = ResourceBundle.getBundle("seris.SerisIdioma");

    public CadastrarUsuarioAction() {
    }

    public ActionForward save(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) {
        try {
            int vnivel = Integer.parseInt(String.valueOf(request.getSession().getAttribute("nivel")));

            if ((vnivel == 0) || (vnivel == 2)) {
                String mensagem = resource.getString("message.salvoSucesso");

                CadastrarUsuarioActionForm formBean = (CadastrarUsuarioActionForm) form;
                List niveis = Nivel.getNiveis();
                formBean.setNiveis(niveis);

                Session session = HibernateUtil.getSession();
                Transaction transaction = session.beginTransaction();
                transaction.begin();
                formBean.setPerfis(session.createQuery("from Perfil").list());
                formBean.setDepartamentos(session.createQuery("from Departamento").list());
                formBean.setGrupos(session.createQuery("from Grupo").list());
                formBean.setDominios(session.createQuery("from Dominio").list());
                Departamento departamento = (Departamento) session.createQuery("from Departamento where id=" + formBean.getDepartamento()).iterate().next();
                Grupo grupo = (Grupo) session.createQuery("from Grupo where id=" + formBean.getGrupo()).iterate().next();
                Perfil perfil = (Perfil) session.createQuery("from Perfil where id=" + formBean.getPerfil()).iterate().next();

                int nivel = 0;
                String[] vNivel = request.getParameterValues("nivel");
                if (vNivel != null) {
                    for (int i = 0; i < vNivel.length; i++) {
                        nivel = Integer.parseInt(vNivel[i]);

                        if (vNivel[i].equals("4")) {
                            formBean.setCaptador(1);
                        }
                    }
                }

                String[] vDominio = request.getParameterValues("dominio");
                formBean.setDominiosSelecionados(vDominio);
                String[] dominiosSelecionados = formBean.getDominiosSelecionados();

                System.out.println("formBean.getId()=" + formBean.getId());
                if (formBean.getId().intValue() == 0) {
                    MessageDigest md5 = MessageDigest.getInstance("MD5");
                    md5.reset();
                    md5.update(formBean.getSenha().getBytes());
                    byte[] cifra = md5.digest();
                    StringBuffer cifraHex = new StringBuffer();
                    for (int i = 0; i < cifra.length; i++) {
                        cifraHex.append(Integer.toHexString(0xFF & cifra[i]));
                    }
                    String senhaInformada = cifraHex.toString();

                    Usuario usuario = new Usuario(departamento, grupo, perfil, nivel, formBean.getNome(), formBean.getLogin(), senhaInformada, formBean.getEmail(), formBean.getCelular(), formBean.getLocalId(), formBean.getCaptador());

                    List vList = session.createQuery("from Usuario u where u.nome = '" + formBean.getNome() + "' or u.email = '" + formBean.getEmail() + "' or u.login = '" + formBean.getLogin() + "'").list();

                    if (vList.size() == 0) {
                        session.save(usuario);
                    } else {
                        transaction.rollback();
                        session.flush();
                        session.close();
                        request.setAttribute("message", resource.getString("message.registroExistente"));
                        return mapping.findForward("failure");
                    }

                    if (usuario.getId() != 0) {
                        if (vNivel != null) {
                            for (int i = 0; i < vNivel.length; i++) {
                                System.out.println("Cadastrar Usuário>>>" + vNivel[i] + " >>>>" + usuario.getId());
                                if (vNivel[i].equals("0")) {
                                    Administrador administrador = new Administrador(usuario.getId());
                                    session.save(administrador);
                                } else if (vNivel[i].equals("1")) {
                                    for (int j = 0; j < dominiosSelecionados.length; j++) {
                                        System.out.println("dominioSelecionado[j]=" + dominiosSelecionados[j]);
                                        Dominio d = (Dominio) session.createQuery("from Dominio where id=" + dominiosSelecionados[j]).list().iterator().next();
                                        Patrocinador patrocinador = new Patrocinador(usuario, d);
                                        session.save(patrocinador);
                                    }
                                } else if (vNivel[i].equals("2")) {
                                    Suportedominio suportedominio = new Suportedominio(usuario.getId(), usuario);
                                    session.save(suportedominio);
                                } else if (vNivel[i].equals("3")) {
                                    Altadirecao altadirecao = new Altadirecao(usuario.getId(), usuario);
                                    session.save(altadirecao);
                                } else if (vNivel[i].equals("5")) {
                                    Analista analista = new Analista(usuario.getId());
                                    session.save(analista);
                                }
                            }
                        }

                        Dominio dominio = null;
                        DominioUsuario dominioUsuario = null;
                        for (int i = 0; i < dominiosSelecionados.length; i++) {
                            dominio = (Dominio) session.createQuery("from Dominio where id=" + dominiosSelecionados[i]).list().iterator().next();
                            dominioUsuario = new DominioUsuario(usuario, dominio);
                            session.save(dominioUsuario);
                        }
                    }
                } else {
                    String senhaInformada = request.getParameter("senhaUsuario");
                    if ((formBean.getSenha() != null) && (!formBean.getSenha().equals(""))) {
                        MessageDigest md5 = MessageDigest.getInstance("MD5");
                        md5.reset();
                        md5.update(formBean.getSenha().getBytes());
                        byte[] cifra = md5.digest();
                        StringBuffer cifraHex = new StringBuffer();
                        for (int i = 0; i < cifra.length; i++) {
                            cifraHex.append(Integer.toHexString(0xFF & cifra[i]));
                        }
                        senhaInformada = cifraHex.toString();
                    }

                    Usuario usuario = new Usuario(formBean.getId().intValue(), departamento, grupo, perfil, nivel, formBean.getNome(), formBean.getLogin(), senhaInformada, formBean.getEmail(), formBean.getCelular(), formBean.getLocalId(), formBean.getCaptador());

                    List vList = session.createQuery("from Usuario u where (u.id <> " + formBean.getId() + ") and (u.nome = '" + formBean.getNome() + "' or u.email = '" + formBean.getEmail() + "' or u.login = '" + formBean.getLogin() + "')").list();

                    if (vList.size() == 0) {
                        session.update(usuario);
                    } else {
                        transaction.rollback();
                        session.flush();
                        session.close();
                        request.setAttribute("message", resource.getString("message.registroExistente"));
                        return mapping.findForward("failure");
                    }

                    transaction.commit();
                    transaction.begin();

                    System.out.println(usuario.getId());
                    if (usuario.getId() != 0) {
                        Administrador vAdministrador = seris.database.DAO.AdministradorDAO.consultarUsuario(usuario.getId());
                        List vListaPatrocinador = seris.database.DAO.PatrocinadorDAO.consultarListaUsuario(usuario.getId());
                        Suportedominio vSuportedominio = SuportedominioDAO.consultarUsuario(usuario.getId());
                        Altadirecao vAltadirecao = AltadirecaoDAO.consultarUsuario(usuario.getId());
                        Analista vAnalista = AnalistaDAO.consultarUsuario(usuario.getId());

                        if ((vAdministrador != null) && (vAdministrador.getUsuarioId() != 0)) {
                            if (!VerificaNivel(vNivel, "0")) {
                                System.out.println("deletar administrador");
                                session.delete(vAdministrador);
                            }
                        } else if (VerificaNivel(vNivel, "0") == true) {
                            Administrador administrador = new Administrador(usuario.getId());
                            session.save(administrador);
                        }

                        if (vListaPatrocinador.size() > 0) {
                            for (int i = 0; i < vListaPatrocinador.size(); i++) {
                                Patrocinador p = (Patrocinador) vListaPatrocinador.get(i);
                                if (!VerificaNivel(vNivel, "1")) {
                                    session.delete(p);
                                }
                            }
                        }
                        if (VerificaNivel(vNivel, "1") == true) {
                            for (int j = 0; j < dominiosSelecionados.length; j++) {
                                System.out.println("dominioSelecionado[j]=" + dominiosSelecionados[j]);
                                Dominio d = (Dominio) session.createQuery("from Dominio where id=" + dominiosSelecionados[j]).list().iterator().next();
                                Patrocinador patrocinador = new Patrocinador(usuario, d);
                                session.save(patrocinador);
                            }
                        }

                        if ((vSuportedominio != null) && (vSuportedominio.getUsuarioId() != 0)) {
                            if (!VerificaNivel(vNivel, "2")) {
                                session.delete(vSuportedominio);
                            }
                        } else if (VerificaNivel(vNivel, "2") == true) {
                            Suportedominio suportedominio = new Suportedominio(usuario.getId(), usuario);
                            session.save(suportedominio);
                        }

                        if ((vAltadirecao != null) && (vAltadirecao.getUsuarioId() != 0)) {
                            if (!VerificaNivel(vNivel, "3")) {
                                session.delete(vAltadirecao);
                            }
                        } else if (VerificaNivel(vNivel, "3") == true) {
                            Altadirecao altadirecao = new Altadirecao(usuario.getId(), usuario);
                            session.save(altadirecao);
                        }

                        if ((vAnalista != null) && (vAnalista.getUsuarioId() != 0)) {
                            if (!VerificaNivel(vNivel, "5")) {
                                session.delete(vAnalista);
                            }
                        } else if (VerificaNivel(vNivel, "5") == true) {
                            Analista analista = new Analista(usuario.getId());
                            session.save(analista);
                        }

                        Dominio dominio = null;
                        DominioUsuario dominioUsuario = null;
                        for (int i = 0; i < dominiosSelecionados.length; i++) {
                            System.out.println("dominioSelecionado[i]=" + dominiosSelecionados[i]);
                            dominio = (Dominio) session.createQuery("from Dominio where id=" + dominiosSelecionados[i]).list().iterator().next();
                            dominioUsuario = new DominioUsuario(usuario, dominio);
                            session.saveOrUpdate(dominioUsuario);
                        }
                    }
                }

                formBean.reset(mapping, request);
                formBean.setNiveis(niveis);
                formBean.setPerfis(session.createQuery("from Perfil").list());
                formBean.setDepartamentos(session.createQuery("from Departamento").list());
                formBean.setGrupos(session.createQuery("from Grupo").list());

                transaction.commit();
                session.flush();
                session.close();
                request.setAttribute("message", mensagem);
                return mapping.findForward("success");
            }
            request.setAttribute("message", resource.getString("message.usuarioSemPermissao"));
            return mapping.findForward("failure");
        } catch (Exception ex) {
            ex.printStackTrace();
            request.setAttribute("message", resource.getString("message.falhaCadastrar"));
        }
        return mapping.findForward("failure");
    }

    private boolean VerificaNivel(String[] vNivel, String nivel) {
        boolean check = false;
        for (int i = 0; i < vNivel.length; i++) {
            if (vNivel[i].equals(nivel)) {
                check = true;
            }
        }
        return check;
    }

    public ActionForward delete(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            int vNivel = -1;
            if (request.getSession().getAttribute("nivel") != null) {
                vNivel = Integer.parseInt(String.valueOf(request.getSession().getAttribute("nivel")));
            }
            if ((vNivel == 1) || (vNivel == 2)) {
                CadastrarUsuarioActionForm formBean = (CadastrarUsuarioActionForm) form;
                if (formBean.getId().intValue() > 0) {
                    Session session = HibernateUtil.getSession();
                    Transaction transaction = session.beginTransaction();
                    transaction.begin();

                    Criteria criteria = session.createCriteria(seris.database.Sinalfraco.class);
                    Criteria usuarioCriteria = criteria.createCriteria("usuario");
                    usuarioCriteria.add(org.hibernate.criterion.Restrictions.like("id", formBean.getId()));
                    List vList = criteria.list();

                    if (vList.size() == 0) {
                        Usuario usuario = (Usuario) session.createQuery("from Usuario where id=" + formBean.getId()).list().iterator().next();

                        session.createQuery("delete from Administrador where usuarioId=" + formBean.getId()).executeUpdate();
                        session.createQuery("delete from Altadirecao where usuarioId=" + formBean.getId()).executeUpdate();
                        session.createQuery("delete from Analista where usuarioId=" + formBean.getId()).executeUpdate();
                        session.createQuery("delete from Suportedominio where usuarioId=" + formBean.getId()).executeUpdate();

                        session.delete(usuario);
                        transaction.commit();
                        session.flush();
                        session.close();
                    } else {
                        transaction.rollback();
                        session.flush();
                        session.close();
                        request.setAttribute("message", resource.getString("message.existeLancamentoUsuarioSF"));
                        return mapping.findForward("failure");
                    }

                    formBean.setId(Integer.valueOf(0));
                }
                request.setAttribute("message", resource.getString("message.deletadoSucesso"));
            } else {
                request.setAttribute("message", resource.getString("message.usuarioSemPermissao"));
            }
            return mapping.findForward("success");
        } catch (Exception ex) {
            request.setAttribute("message", resource.getString("message.falhaDeletar"));
        }
        return mapping.findForward("failure");
    }

    public ActionForward novo(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        return mapping.findForward("success");
    }

    public ActionForward pesquisar(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        return mapping.findForward("success");
    }
}
