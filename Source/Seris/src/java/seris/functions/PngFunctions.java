package seris.functions;

import java.io.PrintStream;
import java.util.ResourceBundle;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import jgraphics.PainelView;
import seris2.database.CriacaoSentido;
import seris2.database.CriacaoSentidoGraficos;
import seris2.database.DAO.CriacaoSentidoDAO;
import seris2.database.DAO.CriacaoSentidoGraficosDAO;

public class PngFunctions {

    public PngFunctions() {
    }

    private String fileName = null;

    public String getFileName() {
        UUID uuid = UUID.randomUUID();
        fileName = (uuid.toString() + ".png");
        return fileName;
    }

    public String getLinkDownload(HttpServletRequest request, String arquivo) {
        String path = request.getContextPath();
        return "<script> window.location.href = '" + path + "/download.jsp?RemoteFile=/pdf/" + arquivo + "'; </script>";
    }

    public void createPngSCS(HttpServletRequest request, HttpServletResponse response) {
        try {
            ResourceBundle resourceConfig = ResourceBundle.getBundle("seris.seris");
            String file = getFileName();

            String idSCS = request.getParameter("id");
            String idLista = request.getParameter("listaId");

            PainelView painelView = new PainelView();
            painelView.setShowScroll(true);

            painelView.setSize(2500, 2500);

            String xml = null;
            if ((idLista != null) && (!idLista.equals(""))) {
                CriacaoSentidoGraficos criacaoSentidoGraficos = CriacaoSentidoGraficosDAO.consultarCriacaoSentidoGraficos(idLista);
                xml = criacaoSentidoGraficos.getGrafico();
            } else if ((idSCS != null) && (!idSCS.equals(""))) {
                CriacaoSentido criacaoSentido = CriacaoSentidoDAO.consultarCriacaoSentido(Integer.parseInt(idSCS));
                xml = criacaoSentido.getGrafico();
            }

            if (xml != null) {
                painelView.openXML(xml);
                painelView.savePNG(resourceConfig.getString("SystemPath") + "/png/" + file);

                response.sendRedirect("../download.jsp?RemoteFile=/png/" + file);
            }
        } catch (Exception ex) {
            System.out.println("Erro ao criar Imagem. Err[" + ex.getMessage() + "]");
        }
    }
}
