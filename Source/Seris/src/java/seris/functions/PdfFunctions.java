package seris.functions;

import java.io.File;
import java.io.PrintStream;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperManager;
import net.sf.jasperreports.engine.JasperPrint;

public class PdfFunctions {

    public PdfFunctions() {
    }

    private String fileName = null;

    public String getFileName() {
        UUID uuid = UUID.randomUUID();
        fileName = (uuid.toString() + ".pdf");
        return fileName;
    }

    public String getLinkDownload(HttpServletRequest request, String arquivo) {
        String path = request.getContextPath();
        return "<script> window.location.href = '" + path + "/download.jsp?RemoteFile=/pdf/" + arquivo + "'; </script>";
    }

    public void createListaSF(HttpServletRequest request, HttpServletResponse response) {
        try {
            Connection con = ConnectJdbcDataBase.getJdbcConnection();

            ResourceBundle resourceConfig = ResourceBundle.getBundle("seris.seris");
            ResourceBundle resourceIdioma = ResourceBundle.getBundle("seris.SerisIdioma");
            File reportFile = new File(resourceConfig.getString("SystemPath") + "/jasper/001ListaSF.jasper");

            Integer idSCS = Integer.valueOf(Integer.parseInt(request.getParameter("id")));
            Integer idLista = Integer.valueOf(Integer.parseInt(request.getParameter("listaId")));

            String file = "ListaSF" + idLista + ".pdf";

            Map parameters = new HashMap();
            if (idLista != null) {
                parameters.put("IdLista", idLista);
            }
            if (idSCS != null) {
                parameters.put("IdSCS", idSCS);
            }

            parameters.put("Tema", resourceIdioma.getString("label.tema"));
            parameters.put("Cabecalho", resourceIdioma.getString("label.scanSlogan2Relatorio"));
            parameters.put("ListaSF", resourceIdioma.getString("label.listaSinaisFracos"));
            parameters.put("Dominio", resourceIdioma.getString("label.dominio"));
            parameters.put("Kit", resourceIdioma.getString("label.kit"));
            parameters.put("Periodo", resourceIdioma.getString("label.periodo"));
            parameters.put("DataRealizacao", resourceIdioma.getString("label.dataRealizacaoSCS"));
            parameters.put("Patrocinador", resourceIdioma.getString("label.patrocinador"));
            parameters.put("Facilitador", resourceIdioma.getString("label.facilitador"));
            parameters.put("Participantes", resourceIdioma.getString("label.participantes"));
            parameters.put("Numero", resourceIdioma.getString("label.numero"));
            parameters.put("Data", resourceIdioma.getString("label.data"));
            parameters.put("SinalFraco", resourceIdioma.getString("label.sinalFraco"));
            parameters.put("InformacoesAdicionais", resourceIdioma.getString("label.informacoesAdicionais"));

            parameters.put("SUBREPORT_DIR", resourceConfig.getString("SystemPath") + "/jasper/");
            parameters.put("ImagemLogo", resourceConfig.getString("SystemPath") + "/imagens/logo.jpg");

            JasperPrint jasperPrint = JasperManager.fillReport(reportFile.getPath(), parameters, con);
            JasperExportManager.exportReportToPdfFile(jasperPrint, resourceConfig.getString("SystemPath") + "/pdf/" + file);

            response.sendRedirect("download.jsp?RemoteFile=/pdf/" + file);
        } catch (Exception ex) {
            System.out.println("Erro ao criar relatório. Err[" + ex.getMessage() + "]");
        }
    }
}
