package seris.functions;

import java.io.PrintStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ResourceBundle;

public class ConnectJdbcDataBase {

    private static Connection con = null;
    private static PersistConstants persistConstants = null;

    public ConnectJdbcDataBase() {
    }

    private static void setJdbcConnection() {
        if (persistConstants == null) {
            carregarParametros();
        }

        String database = persistConstants.getDatabase();
        String driver = persistConstants.getDriver();
        String ipserver = persistConstants.getIpserver();
        String user = persistConstants.getUser();
        String password = persistConstants.getPassword();

        String host = "jdbc:mysql://" + ipserver + ":3306/" + database;

        int err = 1;
        while ((err > 0) && (err <= 3)) {
            try {
                Class.forName(driver).newInstance();
                con = DriverManager.getConnection(host, user, password);
                err = 0;
            } catch (Exception ex) {
                con = null;

                System.out.println("Erro_Conexao=[" + ex.getMessage() + " Tentativas: " + String.valueOf(err) + "]");
                if (ex.getMessage().indexOf("I/O") >= 0) {
                    err = 0;
                } else {
                    err++;
                    try {
                        Thread.sleep(10000L);
                    } catch (Exception e) {
                    }
                }
            }
        }
    }

    private static void carregarParametros() {
        try {
            ResourceBundle resourceConfig = ResourceBundle.getBundle("JdbcConnection");
            persistConstants = new PersistConstants();
            persistConstants.setDatabase(resourceConfig.getString("database"));
            persistConstants.setDriver(resourceConfig.getString("driver"));
            persistConstants.setIpserver(resourceConfig.getString("ipserver"));
            persistConstants.setUser(resourceConfig.getString("user"));
            persistConstants.setPassword(resourceConfig.getString("password"));
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
    }

    public static Connection getJdbcConnection() {
        try {
            if ((con == null) || (con.isClosed())) {
                setJdbcConnection();
            }
        } catch (Exception ex) {
        }

        return con;
    }
}
