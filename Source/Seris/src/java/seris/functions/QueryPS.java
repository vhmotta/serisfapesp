package seris.functions;

import java.io.PrintStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.Calendar;

public class QueryPS {

    private Connection con = null;

    private ResultSet res = null;

    private ResultSetMetaData meta = null;

    public int numRegistro;

    public PreparedStatement pstmt = null;
    public String strclasse = null;
    private String strSQL = null;

    public QueryPS(Connection con) {
        this.con = con;
        numRegistro = 0;
    }

    public String getQuery() {
        return pstmt.toString();
    }

    public boolean setQuery(String query) {
        boolean result = true;

        try {
            query = query.replace(";", " ");
            strSQL = query;

            pstmt = con.prepareStatement(query);
        } catch (Exception ex) {
            System.out.println("QueryPS.setQuery(): " + ex.getMessage());
            System.out.println("Query: " + strSQL);
            result = false;
        }

        return result;
    }

    public void setParameterValue(int indice, String value) {
        try {
            pstmt.setString(indice, value);
        } catch (Exception ex) {
        }
    }

    public void setParameterValue(int indice, BigDecimal value) {
        try {
            pstmt.setBigDecimal(indice, value);
        } catch (Exception ex) {
        }
    }

    public void setParameterValue(int indice, Boolean value) {
        try {
            pstmt.setBoolean(indice, value.booleanValue());
        } catch (Exception ex) {
        }
    }

    public void setParameterValue(int indice, Date value) {
        try {
            pstmt.setDate(indice, value);
        } catch (Exception ex) {
        }
    }

    public void setParameterValue(int indice, Calendar value) {
        try {
            pstmt.setDate(indice, new Date(value.getTimeInMillis()));
        } catch (Exception ex) {
        }
    }

    public void setParameterValue(int indice, Double value) {
        try {
            pstmt.setDouble(indice, value.doubleValue());
        } catch (Exception ex) {
        }
    }

    public void setParameterValue(int indice, Float value) {
        try {
            pstmt.setFloat(indice, value.floatValue());
        } catch (Exception ex) {
        }
    }

    public void setParameterValue(int indice, Short value) {
        try {
            pstmt.setShort(indice, value.shortValue());
        } catch (Exception ex) {
        }
    }

    public void setParameterValue(int indice, Integer value) {
        try {
            pstmt.setInt(indice, value.intValue());
        } catch (Exception ex) {
        }
    }

    public void setParameterValue(int indice, Long value) {
        try {
            pstmt.setLong(indice, value.longValue());
        } catch (Exception ex) {
        }
    }

    public void setParameterValue(int indice, Time value) {
        try {
            pstmt.setTime(indice, value);
        } catch (Exception ex) {
        }
    }

    public void setParameterValue(int indice, Timestamp value) {
        try {
            pstmt.setTimestamp(indice, value);
        } catch (Exception ex) {
        }
    }

    public boolean executeSQL() {
        boolean res = false;
        try {
            this.res = pstmt.executeQuery();
            meta = this.res.getMetaData();
            this.res.last();
            numRegistro = this.res.getRow();
            try {
                this.res.beforeFirst();
            } catch (Exception e) {
            }
            res = true;
        } catch (Exception ex) {
            System.out.println("QueryPS.executeSQL(): " + ex.getMessage());
            System.out.println("Query: " + strSQL);
            try {
                con.rollback();
            } catch (Exception e) {
            }
        }
        return res;
    }

    public boolean updateSQL() {
        boolean res = false;
        try {
            numRegistro = pstmt.executeUpdate();
            res = true;
        } catch (Exception ex) {
            System.out.println("QueryPS.updateSQL(): " + ex.getMessage());
            System.out.println("Query: " + strSQL);
            try {
                con.rollback();
            } catch (Exception e) {
            }
        }
        return res;
    }

    public boolean first() {
        try {
            res.first();
            return true;
        } catch (Exception ex) {
            System.out.println("QueryPS.first(): " + ex.getMessage());
            System.out.println("Query: " + strSQL);
        }
        return false;
    }

    public boolean beforeFirst() {
        try {
            res.beforeFirst();
            return true;
        } catch (Exception ex) {
            System.out.println("QueryPS.beforeFirst(): " + ex.getMessage());
            System.out.println("Query: " + strSQL);
        }
        return false;
    }

    public boolean last() {
        try {
            res.last();
            return true;
        } catch (Exception ex) {
            System.out.println("QueryPS.last(): " + ex.getMessage());
            System.out.println("Query: " + strSQL);
        }
        return false;
    }

    public boolean afterLast() {
        try {
            res.afterLast();
            return true;
        } catch (Exception ex) {
            System.out.println("QueryPS.afterLast(): " + ex.getMessage());
            System.out.println("Query: " + strSQL);
        }
        return false;
    }

    public boolean next() {
        try {
            if (res.next()) {
                return true;
            }
            return false;
        } catch (Exception ex) {
            System.out.println("QueryPS.next(): " + ex.getMessage());
            System.out.println("Query: " + strSQL);
        }
        return false;
    }

    public boolean previous() {
        try {
            if (res.previous()) {
                return true;
            }
            return false;
        } catch (Exception ex) {
            System.out.println("QueryPS.previous(): " + ex.getMessage());
            System.out.println("Query: " + strSQL);
        }
        return false;
    }

    public boolean moverPara(int pos) {
        try {
            if (res.relative(pos)) {
                return true;
            }
            return false;
        } catch (Exception ex) {
            System.out.println("QueryPS.moverPara(): " + ex.getMessage());
            System.out.println("Query: " + strSQL);
        }
        return false;
    }

    public String[] getValues() {
        try {
            int ncol = meta.getColumnCount();

            String[] valores = new String[ncol];

            if (res.getRow() > 0) {
                for (int i = 0; i < ncol; i++) {
                    valores[i] = res.getString(i + 1);
                }
            }

            return valores;
        } catch (Exception ex) {
            System.out.println("QueryPS.getValues(): " + ex.getMessage());
            System.out.println("Query: " + strSQL);
        }
        return null;
    }

    public String getString(int col)
            throws SQLException {
        return res.getString(col);
    }

    public float getFloat(int col)
            throws SQLException {
        return res.getFloat(col);
    }

    public double getDouble(int col)
            throws SQLException {
        return res.getDouble(col);
    }

    public int getInt(int col)
            throws SQLException {
        return res.getInt(col);
    }

    public BigDecimal getBigDecimal(int col)
            throws SQLException {
        return res.getBigDecimal(col);
    }

    public boolean getBoolean(int col)
            throws SQLException {
        return res.getBoolean(col);
    }

    public byte getByte(int col)
            throws SQLException {
        return res.getByte(col);
    }

    public Date getDate(int col)
            throws SQLException {
        return res.getDate(col);
    }

    public Calendar getCalendar(int col)
            throws SQLException {
        Calendar cal = null;
        Timestamp data = res.getTimestamp(col);
        if (data != null) {
            cal = Calendar.getInstance();
            cal.setTime(data);
        }
        return cal;
    }

    public long getLong(int col)
            throws SQLException {
        return res.getLong(col);
    }

    public short getShort(int col)
            throws SQLException {
        return res.getShort(col);
    }

    public Time getTime(int col)
            throws SQLException {
        return res.getTime(col);
    }

    public Timestamp getTimestamp(int col)
            throws SQLException {
        return res.getTimestamp(col);
    }

    public ResultSet getResultSet() {
        return res;
    }

    public void close() {
        try {
            if (res != null) {
                res.close();
            }
        } catch (Exception e) {
            System.out.println("Não fechou conexão manual (ResultSet): " + e.getMessage().trim());
            System.out.println("Query: " + strSQL);
        }
        try {
            if (pstmt != null) {
                pstmt.close();
            }
        } catch (Exception e) {
            System.out.println("Não fechou conexão manual (PreparedStatement): " + e.getMessage().trim());
            System.out.println("Query: " + strSQL);
        }
    }

    public void finalize() {
        try {
            if (res != null) {
                res.close();
            }
        } catch (Exception e) {
            System.out.println("Não fechou conexão coletor (ResultSet): " + e.getMessage().trim());
            System.out.println("Query: " + strSQL);
        }
        try {
            if (pstmt != null) {
                pstmt.close();
            }
        } catch (Exception e) {
            System.out.println("Não fechou conexão coletor (PreparedStatement): " + e.getMessage().trim());
            System.out.println("Query: " + strSQL);
        }
    }

    public String getStrSQL() {
        return strSQL;
    }

    public void setStrSQL(String strSQL) {
        this.strSQL = strSQL;
    }
}
