package seris.functions;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.charset.CharacterCodingException;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.SortedMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import seris.database.Altadirecao;
import seris.database.Analista;
import seris.database.DAO.AltadirecaoDAO;
import seris.database.DAO.AnalistaDAO;
import seris.database.DAO.PatrocinadorDAO;
import seris.database.DAO.UsuarioDAO;
import seris.database.Usuario;

public class BasicFunctions {

    public BasicFunctions() {
    }

    public static String getFuncaoCriacaoSentido(HttpServletRequest request) {
        String funcao = null;

        List perfis = (List) request.getSession().getAttribute("perfis");

        if (perfis != null) {
            if (perfis.indexOf("Facilitador") >= 0) {
                funcao = "Facilitador";
            } else if (perfis.indexOf("Patrocinador") >= 0) {
                funcao = "Patrocinador";
            } else if (perfis.indexOf("Captador") >= 0) {
                funcao = "Captador";
            } else if (perfis.indexOf("Alta Direção") >= 0) {
                funcao = "Alta Direção";
            }
        }

        return funcao;
    }

    public static List getFuncaoCriacaoSentido(int idUsuario, HttpServletRequest request) {
        List funcao = new ArrayList();

        Analista vAnalista = AnalistaDAO.consultarUsuario(idUsuario);

        if ((vAnalista != null) && (vAnalista.getUsuarioId() != 0)) {
            funcao.add("Facilitador");
        }

        List vListaPatrocinador = PatrocinadorDAO.consultarListaUsuario(idUsuario);
        if (vListaPatrocinador.size() > 0) {
            funcao.add("Patrocinador");
        }

        Usuario usuario = UsuarioDAO.consultarUsuario(idUsuario);
        if ((usuario != null) && (usuario.getId() != 0) && (usuario.getCaptador() == 1)) {
            funcao.add("Captador");
        }

        Altadirecao vAltadirecao = AltadirecaoDAO.consultarUsuario(idUsuario);
        if ((vAltadirecao != null) && (vAltadirecao.getUsuarioId() != 0)) {
            funcao.add("Alta Direção");
        }

        return funcao;
    }

    public static boolean pertenceFuncao(String funcao, HttpServletRequest request) {
        boolean pertence = false;

        List perfis = (List) request.getSession().getAttribute("perfis");

        if ((perfis != null)
                && (perfis.indexOf(funcao) >= 0)) {
            pertence = true;
        }

        return pertence;
    }

    public static String trocarAcento(String text) {
        String str1 = "ÇÀÁÂÄÃÈÉÊËÌÍÎÏÒÓÔÖÕÙÚÛÜçàáâäãèéêëìíîïòóôöõùúûü";
        String str2 = "CAAAAAEEEEIIIIOOOOOUUUUcaaaaaeeeeiiiiooooouuuu";

        for (int i = 0; i < str1.length(); i++) {
            text = text.replace(str1.charAt(i), str2.charAt(i));
        }

        return text;
    }

    public static String obterValorTexto(String texto, String inicio, String fim, int numero, boolean removerQuebra) {
        String auxiliar = null;
        if (texto != null) {
            auxiliar = texto;
            if (removerQuebra) {
                auxiliar = auxiliar.replace("\n", " ");
                auxiliar = auxiliar.replace("\r", " ");
            }
            while (auxiliar.indexOf("  ") >= 0) {
                auxiliar = auxiliar.replace("  ", " ");
            }

            boolean error = false;
            if (inicio != null) {
                for (int i = 1; i < numero; i++) {
                    if (auxiliar.indexOf(inicio) >= 0) {
                        auxiliar = auxiliar.substring(auxiliar.indexOf(inicio) + inicio.length());
                    } else {
                        error = true;
                        auxiliar = null;
                    }
                }
            }

            if (!error) {
                if ((inicio != null) && (auxiliar.indexOf(inicio) >= 0)) {
                    auxiliar = auxiliar.substring(auxiliar.indexOf(inicio) + inicio.length()).trim();
                }

                if ((fim != null) && (auxiliar.indexOf(fim) >= 0)) {
                    auxiliar = auxiliar.substring(0, auxiliar.indexOf(fim));
                }
                auxiliar = auxiliar.trim();
            }
        }
        return auxiliar;
    }

    public static String readText(File file) {
        String resultado = null;
        try {
            if ((file != null) && (file.exists())) {
                InputStream is = new FileInputStream(file);
                byte[] dados = new byte[is.available()];
                is.read(dados);

                resultado = new String(dados, "ISO-8859-1");
                is.close();
            }
        } catch (Exception ex) {
        }

        return resultado;
    }

    public static String readText(String file) {
        File arq = new File(file);
        return readText(arq);
    }

    public static boolean testUTF(byte[] b, String csnam) {
        CharsetDecoder cd = ((Charset) Charset.availableCharsets().get(csnam)).newDecoder();
        try {
            cd.decode(ByteBuffer.wrap(b));
        } catch (CharacterCodingException e) {
            return false;
        }
        return true;
    }

    public static Calendar getCalendarDMY(String data) {
        Calendar cal = null;

        try {
            if ((data != null) && (!data.equals(""))) {
                if (data.indexOf(" ") >= 0) {
                    data = data.substring(0, data.indexOf(" ")).trim();
                } else {
                    data = data.trim();
                }
                data = data.replace('-', '/');
                String[] dados = data.split("/");

                cal = Calendar.getInstance();
                cal.set(1, Integer.parseInt(dados[2]));
                cal.set(2, Integer.parseInt(dados[1]) - 1);
                cal.set(5, Integer.parseInt(dados[0]));
            }
        } catch (Exception ex) {
            cal = null;
        }
        return cal;
    }

    public static String formatCalendarYMD(Calendar cal, String separacao) {
        String dia = String.valueOf(cal.get(5));
        String mes = String.valueOf(cal.get(2) + 1);
        String ano = String.valueOf(cal.get(1));

        while (dia.length() < 2) {
            dia = "0" + dia;
        }
        while (mes.length() < 2) {
            mes = "0" + mes;
        }
        while (ano.length() < 4) {
            ano = "0" + ano;
        }

        return ano + separacao + mes + separacao + dia;
    }

    public static String formatCalendarDMY(Calendar cal, String separacao) {
        String dia = String.valueOf(cal.get(5));
        String mes = String.valueOf(cal.get(2) + 1);
        String ano = String.valueOf(cal.get(1));

        while (dia.length() < 2) {
            dia = "0" + dia;
        }
        while (mes.length() < 2) {
            mes = "0" + mes;
        }
        while (ano.length() < 4) {
            ano = "0" + ano;
        }

        return dia + separacao + mes + separacao + ano;
    }

    public static void sumDay(Calendar calendar, int dias) {
        for (int i = 0; i < Math.abs(dias); i++) {
            if (dias > 0) {
                calendar.setTimeInMillis(calendar.getTimeInMillis() + 86400000L);
            } else {
                calendar.setTimeInMillis(calendar.getTimeInMillis() - 86400000L);
            }
        }
    }

    public static void exportarGraficoPNG(String fileName) {
    }
}
