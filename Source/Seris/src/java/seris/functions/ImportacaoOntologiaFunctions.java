package seris.functions;

import java.io.PrintStream;
import java.sql.Connection;

public class ImportacaoOntologiaFunctions {

    private static int dominioId = 6;

    public ImportacaoOntologiaFunctions() {
    }

    public static void main(String[] args) {
        ImportacaoOntologiaFunctions iof = new ImportacaoOntologiaFunctions();

        String dir = "/mnt/arquivos/vmotta/Desktop/ontologia/fibras/";

        System.out.println("Importando Ligações: ");
        iof.importarLigacoes(dir + "ligacao.txt");

        System.out.println("Importando Compostas: ");
        iof.importarPalavrasCompostas(dir + "palavrascompostas.txt");

        System.out.println("Importando Conceitos: ");
        iof.importarConceitoSemantico(dir + "conceitos.txt");

        System.out.println("Importando Instâncias: ");
        iof.importarInstanciaSemantica(dir + "instancias.txt");
    }

    public void importarLigacoes(String file) {
        String texto = BasicFunctions.readText(file);
        String[] linhas = texto.split("\n");

        Connection con = ConnectJdbcDataBase.getJdbcConnection();
        QueryPS qry = new QueryPS(con);

        qry.setQuery("delete from regra_semantica where id_dominio = ?");
        qry.setParameterValue(1, Integer.valueOf(dominioId));
        qry.updateSQL();

        qry.setQuery("delete from conceito_semantico where id_dominio = ?");
        qry.setParameterValue(1, Integer.valueOf(dominioId));
        qry.updateSQL();

        for (int i = 0; i < linhas.length; i++) {
            System.out.println("Ligações: " + i + "/" + linhas.length);

            String linha = linhas[i];
            String[] values = linha.split("\t", 2);

            inserirLigacao(values[0].trim(), values[1].trim(), qry);
        }
    }

    public void importarExcecoes(String file) {
        String texto = BasicFunctions.readText(file);
        String[] linhas = texto.split("\n");

        Connection con = ConnectJdbcDataBase.getJdbcConnection();
        QueryPS qry = new QueryPS(con);

        qry.setQuery("delete from semantica_ignorar");
        qry.updateSQL();

        for (int i = 0; i < linhas.length; i++) {
            System.out.println("Exceções: " + i + "/" + linhas.length);

            String linha = linhas[i];
            inserirTabela("semantica_ignorar", "palavra", linha.trim(), qry);
        }
    }

    public void importarPalavrasCompostas(String file) {
        String texto = BasicFunctions.readText(file);
        String[] linhas = texto.split("\n");

        Connection con = ConnectJdbcDataBase.getJdbcConnection();
        QueryPS qry = new QueryPS(con);

        qry.setQuery("delete from semantica_palavra_composta where id_dominio = ?");
        qry.setParameterValue(1, Integer.valueOf(dominioId));
        qry.updateSQL();

        for (int i = 0; i < linhas.length; i++) {
            System.out.println("Compostas: " + i + "/" + linhas.length);

            String linha = linhas[i].trim();
            inserirTabelaDominio("semantica_palavra_composta", "palavra_composta", linha.trim(), qry);
        }
    }

    public void importarPalavrasFuturo(String file) {
        String texto = BasicFunctions.readText(file);
        String[] linhas = texto.split("\n");

        Connection con = ConnectJdbcDataBase.getJdbcConnection();
        QueryPS qry = new QueryPS(con);

        qry.setQuery("delete from semantica_futuro");
        qry.updateSQL();

        for (int i = 0; i < linhas.length; i++) {
            System.out.println("Futuro: " + i + "/" + linhas.length);

            String linha = linhas[i].trim();
            inserirTabela("semantica_futuro", "palavra", linha.trim(), qry);
        }
    }

    public void importarConceitoSemantico(String file) {
        String texto = BasicFunctions.readText(file);
        String[] linhas = texto.split("\n");

        Connection con = ConnectJdbcDataBase.getJdbcConnection();
        QueryPS qry = new QueryPS(con);

        for (int i = 0; i < linhas.length; i++) {
            System.out.println("Conceitos " + i + "/" + linhas.length);

            String linha = linhas[i].trim();
            String[] values = linha.split("\t", 3);

            if (values.length >= 3) {
                String palavra1 = values[0].trim();
                String regra = values[1].trim();
                String palavra2 = values[2].trim();

                inserirConceito(palavra1, "C", null, qry);
                inserirTabelaDominio("regra_semantica", "regra", regra, qry);
                inserirConceito(palavra2, "C", null, qry);

                inserirConceitoRegra(palavra1, regra, palavra2, "C", qry);
            }
        }
    }

    public void importarInstanciaSemantica(String file) {
        String texto = BasicFunctions.readText(file);
        String[] linhas = texto.split("\n");

        Connection con = ConnectJdbcDataBase.getJdbcConnection();
        QueryPS qry = new QueryPS(con);

        String regra = "É UM";

        inserirTabelaDominio("regra_semantica", "regra", regra, qry);

        for (int i = 0; i < linhas.length; i++) {
            System.out.println("Instâncias: " + i + "/" + linhas.length);

            String linha = linhas[i].trim();
            String[] values = linha.split("\t", 3);

            if (values.length >= 2) {
                String conceito = values[0].trim();
                String instancia = values[1].trim();
                String classe = null;

                if (values.length >= 3) {
                    classe = values[2].trim();
                }

                inserirConceito(conceito, "C", null, qry);
                inserirConceito(instancia, "I", classe, qry);

                inserirConceitoRegra(instancia, regra, conceito, "I", qry);
            }
        }
    }

    private void inserirTabela(String tabela, String campo, String palavra, QueryPS qry) {
        if (palavra.length() > 0) {
            qry.setQuery("insert into " + tabela + " (" + campo + ") value (?)");
            qry.setParameterValue(1, palavra);
            qry.updateSQL();
            qry.close();
        }
    }

    private void inserirTabelaDominio(String tabela, String campo, String palavra, QueryPS qry) {
        if (palavra.length() > 0) {
            qry.setQuery("insert into " + tabela + " (id_dominio, " + campo + ") value (?,?)");
            qry.setParameterValue(1, Integer.valueOf(dominioId));
            qry.setParameterValue(2, palavra);
            qry.updateSQL();
            qry.close();
        }
    }

    private void inserirLigacao(String palavra, String peso, QueryPS qry) {
        if (palavra.length() > 0) {
            qry.setQuery("insert into regra_semantica (id_dominio, regra, peso) value (?,?,?)");
            qry.setParameterValue(1, Integer.valueOf(dominioId));
            qry.setParameterValue(2, palavra);
            qry.setParameterValue(3, Float.valueOf(Float.parseFloat(peso)));
            qry.updateSQL();
            qry.close();
        }
    }

    private void inserirConceito(String palavra, String tipo, String classe, QueryPS qry) {
        if (palavra.length() > 0) {
            qry.setQuery("insert into conceito_semantico (conceito, tipo, classificacao, id_dominio) value (?,?,?,?)");
            qry.setParameterValue(1, palavra);
            qry.setParameterValue(2, tipo);
            qry.setParameterValue(3, classe);
            qry.setParameterValue(4, Integer.valueOf(dominioId));
            qry.updateSQL();
            qry.close();
        }
    }

    private void inserirConceitoRegra(String palavra1, String ligacao, String palavra2, String tipo1, QueryPS qry) {
        if ((palavra1.length() > 0) && (ligacao.length() > 0) && (palavra2.length() > 0)) {
            qry.setQuery("insert into conceito_regra (id_conceito1, id_regra, id_conceito2) value ((select id from conceito_semantico where tipo = ? and conceito = ? and id_dominio = ?), (select id from regra_semantica where regra = ? and id_dominio = ?), (select id from conceito_semantico where tipo = 'C' and conceito = ? and id_dominio = ?))");

            qry.setParameterValue(1, tipo1);
            qry.setParameterValue(2, palavra1);
            qry.setParameterValue(3, Integer.valueOf(dominioId));
            qry.setParameterValue(4, ligacao);
            qry.setParameterValue(5, Integer.valueOf(dominioId));
            qry.setParameterValue(6, palavra2);
            qry.setParameterValue(7, Integer.valueOf(dominioId));
            qry.updateSQL();
            qry.close();
        }
    }
}
