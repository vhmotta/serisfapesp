package seris.functions;

import java.util.Hashtable;
import javax.servlet.http.HttpServletRequest;
import javazoom.upload.MultipartFormDataRequest;
import javazoom.upload.UploadBean;
import javazoom.upload.UploadFile;
import uploadutilities.FileMover;

public class BasicUploadFile {

    private MultipartFormDataRequest myrequest = null;
    private Hashtable files = null;
    private FileMover fileMover = null;
    private UploadBean upBean = null;
    private String strError = null;
    private String fileName = "";

    public BasicUploadFile(HttpServletRequest request, String folderStore) {
        try {
            if (MultipartFormDataRequest.isMultipartFormData(request)) {
                myrequest = new MultipartFormDataRequest(request);
                files = myrequest.getFiles();
                fileMover = new FileMover();
                fileMover.setAltFolder(folderStore);
                upBean = new UploadBean();
                upBean.setOverwrite(true);
                upBean.setFolderstore(folderStore);
                upBean.addUploadListener(fileMover);
            } else {
                strError = "O formulário não é 'multipart/form-data'.";
            }
        } catch (Exception ex) {
            strError = ex.getMessage();
        }
    }

    public void setDiretory(String directory) {
        if (strError == null) {
            fileMover.setAltFolder(directory);
        }
    }

    public void setNewFileName(String newName) {
        if (strError == null) {
            fileMover.setNewfilename(newName);
        }
    }

    public String uploadFile(String field) {
        String res = null;
        if (strError != null) {
            res = strError;
        } else if ((files != null) || (!files.isEmpty())) {
            try {
                UploadFile file = (UploadFile) files.get(field);
                fileName = file.getFileName();
                if (file.getFileSize() == 0L) {
                    res = "Arquivo não encontrado.";
                } else {
                    upBean.store(myrequest, field);
                }
            } catch (Exception ex) {
                res = "Erro ao efetuar upload de " + field + ".";
            }
        } else {
            res = "Nenhum arquivo para upload foi encontrado.";
        }
        return res;
    }

    public String getFileName() {
        return fileName;
    }

    public MultipartFormDataRequest getMyrequest() {
        return myrequest;
    }
}
