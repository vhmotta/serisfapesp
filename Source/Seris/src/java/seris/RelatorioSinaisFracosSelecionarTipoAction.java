package seris;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.Action;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

public class RelatorioSinaisFracosSelecionarTipoAction
        extends Action {

    private static final String SUCCESS = "success";
    private static final String FAILURE = "failure";

    public RelatorioSinaisFracosSelecionarTipoAction() {
    }

    public ActionForward execute(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        RelatorioSinaisFracosActionForm formBean = (RelatorioSinaisFracosActionForm) form;
        String tipoConsulta = formBean.getTipoConsulta();
        HttpSession sessao = request.getSession(true);

        request.getSession().removeAttribute("dominioSelecionado");
        request.getSession().removeAttribute("kitSelecionado");
        request.getSession().removeAttribute("tipoConsulta");
        request.getSession().removeAttribute("ator");
        request.getSession().removeAttribute("fonte");
        request.getSession().removeAttribute("mercado");
        request.getSession().removeAttribute("produto");
        request.getSession().removeAttribute("avaliacao");
        request.getSession().removeAttribute("usuario");
        request.getSession().removeAttribute("perfil");
        request.getSession().removeAttribute("grupo");
        request.getSession().removeAttribute("departamento");
        request.getSession().removeAttribute("dataInicio");
        request.getSession().removeAttribute("dataFinal");

        sessao.setAttribute("tipoConsulta", tipoConsulta);

        if ((tipoConsulta != null) && (!tipoConsulta.equals(""))) {
            return mapping.findForward("success");
        }
        return mapping.findForward("failure");
    }
}
